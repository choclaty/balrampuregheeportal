USE [pd_order]
GO
/****** Object:  StoredProcedure [dbo].[sp_bill]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_bill]    
(@p_formmode int ,@p_billid float = NULL,	 @p_uid1 bigint = NULL,	 @p_billdate datetime = NULL,	 @p_billTime datetime = NULL,	 
@p_billcode nvarchar(100) = NULL,	 @p_billcode_1 bigint = NULL,	 @p_custname nvarchar(max) = NULL,	 @p_mobileno nvarchar(100) = NULL,	 @p_taxtype nvarchar(100) = NULL,	 @p_prclamt float = NULL,	 @p_discount float = NULL,	 @p_totamt float = NULL,	 @p_roffamt float = NULL,	 @p_grossamt float = NULL,	 @p_adddesc nvarchar(255) = NULL,	 @p_cardname nvarchar(50) = NULL,	 @p_referenceno nvarchar(50) = NULL,	 @p_paymentdate datetime = NULL,	 @p_payremarks nvarchar(max) = NULL,	 @p_cashamt float = NULL,	 @p_creditpay float = NULL,	 @p_giftcardPay float = NULL,	 @p_walletamt float = NULL,	 @p_wallettype bigint = NULL,	 @p_paidamt float = NULL,	 @p_advamt float = NULL,	 @p_amtrtn float = NULL,	 @p_totalamt float = NULL,	 @p_counterid bigint = NULL,	 @p_uid bigint = NULL,	 @p_level nvarchar(10) = NULL, @p_addedby bigint = NULL,  @p_Errstr nvarchar(max) out,  @p_RetVal bigint out ,@P_retbillid bigint out) AS  
if @p_Formmode = 0  begin  
set @p_uid1 = (select isnull(Max(uid1),0)+1 from bill Where level = @p_level)  
declare @tempbilid nvarchar(100)
set @tempbilid = CONVERT(nvarchar,@p_uid) +''+ CONVERT(nvarchar,@p_uid1)
set @p_billid = @tempbilid
Set @P_retbillid = @p_billid  
INSERT INTO [dbo].[bill] (billid,	uid1,	billdate,	billTime,	billcode,	billcode_1,	custname,	mobileno,	taxtype,	prclamt,	discount,	totamt,	roffamt,	grossamt,	adddesc,	cardname,	referenceno,	paymentdate,	payremarks,	cashamt,	creditpay,	giftcardPay,	walletamt,	wallettype,	paidamt,	advamt,	amtrtn,	totalamt,	counterid,	uid,	level,	tflg,	addedby)  
VALUES ( @p_billid,	 @p_uid1,@p_billdate,	 @p_billTime,	 @p_billcode,	 @p_billcode_1,	 @p_custname,	 @p_mobileno,	 @p_taxtype,	 @p_prclamt,	 @p_discount,	 @p_totamt,	 @p_roffamt,	 @p_grossamt,	 @p_adddesc,	 @p_cardname,	 @p_referenceno,	 @p_paymentdate,	 @p_payremarks,	 @p_cashamt,	 @p_creditpay,	 @p_giftcardPay,	 @p_walletamt,	 @p_wallettype,	 @p_paidamt,	 @p_advamt,	 @p_amtrtn,	 @p_totalamt,	 @p_counterid,	 @p_uid,	 @p_level,0,	 @p_addedby)  end   
else if @p_Formmode = 1 begin  
update [bill]  set billdate =  @p_billdate,	billTime =  @p_billTime,	billcode =  @p_billcode,	billcode_1 =  @p_billcode_1,	custname =  @p_custname,	mobileno =  @p_mobileno,	taxtype =  @p_taxtype,	prclamt =  @p_prclamt,	discount =  @p_discount,	totamt =  @p_totamt,	roffamt =  @p_roffamt,	grossamt =  @p_grossamt,	adddesc =  @p_adddesc,	cardname =  @p_cardname,	referenceno =  @p_referenceno,	paymentdate =  @p_paymentdate,	payremarks =  @p_payremarks,	cashamt =  @p_cashamt,	creditpay =  @p_creditpay,	giftcardPay =  @p_giftcardPay,	walletamt =  @p_walletamt,	wallettype =  @p_wallettype,	paidamt =  @p_paidamt,	advamt =  @p_advamt,	amtrtn =  @p_amtrtn,	totalamt =  @p_totalamt,	counterid =  @p_counterid,tflg =  1,	updatedby =  @p_addedby  WHere [billid] =@p_billid and [level]=@P_level   end   
else if @p_Formmode =2 begin  update [bill] set delflg = 1,delby = @p_addedby,deldatetime = GETDATE() 
WHere [billid] =@p_billid and [level]=@P_level end
BEGIN  SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage  SET @p_Retval = ERROR_NUMBER()  set @p_Errstr = ERROR_MESSAGE()  Set @P_retbillid = @p_billid END







GO
/****** Object:  StoredProcedure [dbo].[sp_billdtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_billdtl] (@p_formmode int,@p_billdtlid  bigint ,@p_uid1  bigint =null ,@p_billid  bigint =null ,@p_itemid  bigint =null ,  @p_unitid bigint=null,    
@p_qty  float =null ,  @p_grate  float =null ,@p_discperc  float =null ,@p_discamt  float =null ,@p_netrate  float =null ,@p_totamt  float =null ,  
@p_sgstperc  float =null ,  @p_sgstamt  float =null ,  @p_cgstperc  float =null ,@p_cgstamt  float =null ,  @p_igstperc  float =null ,  
@p_igstamt  float =null ,@p_soid bigInt=null,@p_sodtlid bigInt=null,  @p_schmid bigInt=null,@p_schmdtlid bigInt=null  
,@p_isvoid bit=null,@p_prclitmyn bit=null,@p_Addnote nvarchar(Max) = null,@p_uid bigint = null,@P_level NVARCHAR(10),  
@p_addedby  bigint =null ,    
@p_Errstr nvarchar(max) out,@p_RetVal bigint out , @p_retbilldtlid BigInt Out) AS           
if @p_Formmode = 1 begin      
  
set @p_uid1 = (select isnull(Max(uid1),0)+1 from billdtl Where level = @P_level )    
declare @tempbilldtl nvarchar(100)
set @tempbilldtl = CONVERT(nvarchar,@p_uid) +''+CONVERT(nvarchar,@p_uid1)
set @p_billdtlid = @tempbilldtl  
Set @p_retbilldtlid = @p_billdtlid  
    
INSERT INTO [billdtl] ([billdtlid],[uid1] ,[billid] ,[itemid] ,[qty] ,[grate] ,[discperc] ,[discamt],[netrate] , [totamt] ,[sgstperc] ,    
[sgstamt] ,[cgstperc] ,  [cgstamt] ,[igstperc] ,[igstamt] ,[tflg] ,[addedby] ,[addedatetime] ,  
[unitid],[soid],  [sodtlid],[schmid],[schmdtlid],[isvoid],[prclitmyn],[Addnote],[uid],[level])       
VALUES(@p_billdtlid,@p_uid1 ,@p_billid ,@p_itemid ,@p_qty ,@p_grate ,@p_discperc ,@p_discamt,@p_netrate ,@p_totamt ,@p_sgstperc   
,@p_sgstamt ,@p_cgstperc ,    @p_cgstamt ,@p_igstperc ,@p_igstamt ,0 ,@p_addedby ,GetDate(),  
@p_unitid,@p_soid,@p_sodtlid,@p_schmid,@p_schmdtlid,@p_isvoid,@p_prclitmyn,@p_Addnote,@p_uid,@P_level) end            
else   if   
@p_Formmode = 2 begin    
update [billdtl]  set [itemid]=@p_itemid ,[qty]=@p_qty ,[grate]=@p_grate ,[discperc]=@p_discperc ,[discamt]=@p_discamt,[netrate]=@p_netrate ,  
[totamt]=@p_totamt ,[sgstperc]=@p_sgstperc,[sgstamt]=@p_sgstamt,[cgstperc]=@p_cgstperc ,[cgstamt]=@p_cgstamt ,[igstperc]=@p_igstperc ,      
[igstamt]=@p_igstamt,[tflg]=1 ,[updatedby] = @p_addedby ,[updateddatetime]=GetDate(),[unitid]=@p_unitid,[soid]=@p_soid,      
[sodtlid]=@p_sodtlid,[schmid]=@p_schmid,[schmdtlid]=@p_schmdtlid ,isvoid=@p_isvoid ,prclitmyn = @p_prclitmyn ,Addnote = @P_Addnote   
WHere [billdtlid] = @p_billdtlid and [billid] =@p_billid end           
else if @p_Formmode = 3 begin    
update [billdtl] set delflg = 1,delby = @p_addedby,deldatetime = GETDATE()     
WHere [billdtlid] = @p_billdtlid and [billid] =@p_billid end       
BEGIN SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage SET @p_Retval = ERROR_NUMBER()    
set @p_Errstr = ERROR_MESSAGE() Set @p_retbilldtlid = @p_billid END  




GO
/****** Object:  StoredProcedure [dbo].[sp_cartItemList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[sp_cartItemList]    Script Date: 10-01-2020 01:29:09 AM ******/      
/*    
select * from checkauthkey order by id desc    
exec sp_cartItemList '',50,'0U6Y608YH1WHKEXDW9LWQX6GANVD79',0,''    
*/    
CREATE PROCEDURE [dbo].[sp_cartItemList]                
@p_deviceregid nvarchar(100)=null,                
@p_userid int=null,                
@p_authkey nvarchar(50) =null,                
@p_errorcode bigint out,                 
@p_errormessage nvarchar(max) out                
AS                
Begin                
               
   declare @tblItem as table(packid nvarchar(10),              
   Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
   ingredients nvarchar(max),nutritioninfo nvarchar(max),itembarcode nvarchar(50),              
   itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
   salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
   itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500)    ,packname nvarchar(500)  ,taxtype nvarchar(10),itemqty nvarchar(10)  
   ,review nvarchar(100)                   
  )              
                    
 insert into @tblItem                
  select 0 as packid,mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'') = '' then mitem.salerate else pricedtl.rate end salerate,
     case when isnull(pricedtl.Rate,'')='' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,                  
     case when isnull(pricedtl.sgstper,'')='' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'')='' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'')='' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,'' ,isnull(mitem.taxtype,0) as   taxtype,mCartItem.ItemQty  
     ,isnull(reviewitem.review,0) as  review      
     from               
     mCartItem inner join mitem on mCartItem.Itemid = mitem.itemid               
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where userid = @p_userid  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid         
     inner join munit on mitem.unitid = munit.unitid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
  WHERE mCartItem.userid = @p_userid and packid <=0          
             
    union all           
              
    Select mcartitem.packid, mstpackitmlink.itemid,mitem.itemcatid, mitem.itemdesc,mitem.ingredients,mitem.nutritioninfo,          
   mitem.itembarcode,mitem.itemname,mitem.itmimgename,0 as discperc,packval as salerate,packval as Discrate,mitem.sgstper,mitem.cgstper,          
   mitem.igstper,mitem.itemwt,munit.unitname,mitem.validfor,mstpack.packname,isnull(mitem.taxtype,0) as   taxtype,mCartItem.ItemQty  
   ,isnull(reviewitem.review,0) as  review  
   from mcartitem inner join           
   mstpack on mcartitem.packid = mstpack.id          
   inner join mstpackitmlink on mstpack.id = mstpackitmlink.packid and mcartitem.itemid = mstpackitmlink.itemid          
   inner join mitem on mitem.itemid =   mstpackitmlink.itemid                
   left join ( Select avg(Review) as review,itemid  from mReviewItem Where userid = @p_userid  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid         
   inner join munit on mitem.unitid = munit.unitid                 
     WHERE mCartItem.userid = @p_userid              
           
             
  select * from @tblItem              
  if exists(select top 1 * from @tblItem)            
  BEGIN              
   SET @p_errorcode = 1              
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')              
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage              
       
  END              
  else              
  BEGIN              
   SET @p_errorcode = 0              
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'No Record')              
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage              
               
  END              
    
 exec spl_calculatetax @p_userid    
End 


GO
/****** Object:  StoredProcedure [dbo].[sp_GetItemDetails]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
select * from checkauthkey order by id desc    
exec sp_GetItemDetails '123',11,60,'KPSX71DWO2',0,''    
*/       
CREATE PROCEDURE [dbo].[sp_GetItemDetails]            
@p_deviceregid nvarchar(100)=null,            
@itemid int=null,           
@p_userid int=null,            
@p_authkey nvarchar(50) =null,            
@p_errorcode bigint out,             
@p_errormessage nvarchar(max) out ,        
@p_cartitemret bigint=null out           
AS            
Begin            
 
  declare @tblItem as table(          
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),          
  ingredients nvarchar(max),nutritioninfo nvarchar(max),itembarcode nvarchar(50),          
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),          
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),          
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500)  ,favitem nvarchar(10),taxtype nvarchar(10),itemqty nvarchar(10),review nvarchar(100)        
  )          
          
  insert into @tblItem          
  select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,          
     case when isnull(pricedtl.Rate,'') = '' then mitem.salerate else pricedtl.rate end salerate,    
  case when isnull(pricedtl.Rate,'')='' then   mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,        
     case when isnull(pricedtl.sgstper,'')='' then mitem.sgstper else pricedtl.sgstper end sgstper,          
     case when isnull(pricedtl.cgstper,'')='' then mitem.cgstper else pricedtl.cgstper end cgstper,          
     case when isnull(pricedtl.igstper,'')='' then mitem.igstper else pricedtl.igstper end igstper,          
     itemwt,munit.unitname,validfor,      
     case when mfavitem.itemid is null then '0' else '1' end favitem ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty,    
     isnull(reviewitem.review,0) as  review         
     from mitem             
     inner join munit on mitem.unitid = munit.unitid     
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3 group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid         
     LEFT join (select distinct itemid,userid from mFavItem where userid = @p_userid) as mfavitem on mitem.itemid = mfavitem.Itemid                   
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = @p_userid       
     group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid       
           
     left join           
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate            
   from ( select *, row_number() over (            
      partition by [itemid]             
      order by [pldatetime] desc            
   ) as RN            
   From           
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,           
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate            
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level           
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=           
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and           
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )            
    ) as A ) X where RN = 1) pricedtl           
   on mitem.itemid = pricedtl.itemid WHERE mitem.Itemid = @Itemid            
  
  if (Select count(*) from mcartitem WHere userid = @p_userid) > 0        
 begin        
  set @p_cartitemret = 1        
 end        
  else        
 begin        
  set @p_cartitemret = 0        
 end  
          
  select * from @tblItem  
  
  SET @p_errorcode = 1                  
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                  
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage , @p_cartitemret as cartitemret    
                
End 


GO
/****** Object:  StoredProcedure [dbo].[sp_getuserprofile]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
select * from checkauthkey order by id desc
exec sp_getuserprofile 20,'8LM4J9JC5Y60HKTR3YO478N5IAF1LM',0,0,''
*/


CREATE PROCEDURE [dbo].[sp_getuserprofile]  
 @p_userid bigint,  
 @p_deviceregid nvarchar(100)=null, 
 @p_authkey nvarchar(100),  
 @p_retid bigint OUT,  
 @p_errorcode bigint OUT,  
 @p_errormessage NVARCHAR(100) OUT  
AS  
Begin  
  
   
      select id as userid,firstname as fullname,City,patimg,contactno as mobileno,useremail as emailid from UserRegistration where id = @p_userid  
  
   SET @p_errorcode = 1  
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Update Record')  

   
 SELECT @p_retid as retid, @p_errorcode AS errorcode, @p_errormessage AS errormessage  
End   



GO
/****** Object:  StoredProcedure [dbo].[sp_mitem]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_mitem](@p_formmode int, @p_itemid bigint = NULL,  @p_uid1 bigint = NULL,  @p_itemcatid bigint = NULL,      
@p_itembarcode nvarchar(25) = NULL,  @p_itemname nvarchar(200) = NULL,  @p_unitid bigint = NULL,  @p_itemdesc nvarchar(max) = NULL,  @p_ingredients nvarchar(max) = NULL,  @p_nutritioninfo nvarchar(300) = NULL,  @p_itemstatus nvarchar(20) = NULL,  @p_hsncode nvarchar(40) = NULL,    
@p_sgstper float = NULL,  @p_cgstper float = NULL,  @p_igstper float = NULL,  @p_itemwt decimal = NULL,  @p_selflifetype nvarchar(80) = NULL,  @p_selflifeval float = NULL,  @p_salerate float = NULL,  @p_purcrate float = NULL,    
@p_DefQty float = NULL,  @p_favitem bit = NULL,  @p_itmimgename nvarchar(50) = NULL,  @p_uid bigint = NULL,  @p_level nvarchar(10) = NULL,  @p_addedby bigint = NULL,  
@p_validfor nvarchar(100) = null,@p_jan bit,@p_feb bit,@p_mar bit,@p_apr bit,@p_may bit,@p_jun bit,@p_jul bit,@p_aug bit,@p_sep bit,@p_oct bit,@p_nov bit,@p_dec bit,  @p_taxtype bigint = 0,
@p_Errstr nvarchar(max) out,@p_RetVal bigint out) AS        
if @p_Formmode = 1 begin     
INSERT INTO [mitem] (itemid, uid1, itemcatid, itembarcode, itemname, unitid, itemdesc, ingredients, nutritioninfo, itemstatus, hsncode, sgstper, cgstper, igstper, itemwt, selflifetype, selflifeval, salerate, purcrate, DefQty, favitem, itmimgename, uid, tflg, level, addedby,addedatetime,validfor,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec,taxtype)  
VALUES ( @p_itemid,  @p_uid1,  @p_itemcatid,  @p_itembarcode,  @p_itemname,  @p_unitid,  @p_itemdesc,  @p_ingredients,  @p_nutritioninfo,  @p_itemstatus,    
@p_hsncode,  @p_sgstper,  @p_cgstper,  @p_igstper,  @p_itemwt,  @p_selflifetype,  @p_selflifeval,  @p_salerate,  @p_purcrate,  @p_DefQty,  @p_favitem,    
@p_itmimgename,  @p_uid,0,  @p_level,  @p_addedby,GETDATE(),@p_validfor,@p_jan,@p_feb,@p_mar,@p_apr,@p_may,@p_jun,@p_jul,@p_aug,@p_sep,@p_oct,@p_nov,@p_dec,@p_taxtype) end        
else if @p_Formmode = 2 begin     
update [mitem]  set itemid =  @p_itemid, uid1 =  @p_uid1, itemcatid =  @p_itemcatid, itembarcode =  @p_itembarcode, itemname =  @p_itemname, unitid =  @p_unitid, itemdesc =  @p_itemdesc, ingredients =  @p_ingredients, nutritioninfo =  @p_nutritioninfo,   
itemstatus =  @p_itemstatus, hsncode =  @p_hsncode, sgstper =  @p_sgstper, cgstper =  @p_cgstper, igstper =  @p_igstper, itemwt =  @p_itemwt, selflifetype =  @p_selflifetype, selflifeval =  @p_selflifeval, salerate =  @p_salerate, purcrate =  @p_purcrate,   
DefQty =  @p_DefQty, favitem =  @p_favitem, itmimgename =  @p_itmimgename, tflg =  1, updatedby =  @p_addedby,updateddatetime = GETDATE() ,  
validfor = @p_validfor,jan = @p_jan,feb = @p_feb,mar = @p_mar,apr = @p_apr,may = @p_may,jun = @p_jun,jul = @p_jul,aug = @p_aug,sep = @p_sep,oct = @p_oct,nov = @p_nov,dec = @p_dec  ,
taxtype = @p_taxtype
Where [itemid] =@p_itemid end        
else if @p_Formmode =3 begin     
update [mitem] set delflg = 1,delby = @p_addedby,deldatetime = GETDATE() WHere [itemid] =@p_itemid end      
BEGIN SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage SET @p_Retval = ERROR_NUMBER() set @p_Errstr = ERROR_MESSAGE()  END 


GO
/****** Object:  StoredProcedure [dbo].[sp_munit]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  Procedure [dbo].[sp_munit](@p_formmode int,@p_unitid BigInt,@p_uid1 BigInt,@p_unitcode nvarchar(10) = null,@p_unitname nvarchar(200) = null
,@p_NoofDecimal bigint = null, @p_uid bigint = NULL,	 @p_level nvarchar(10) = NULL, @p_addedby bigint = NULL,@p_Errstr nvarchar(max) out,
@p_RetVal bigint out ) AS  
if @p_Formmode = 0 begin   
insert into munit(unitid,uid1,unitcode,unitname,NoofDecimal,uid,level,tflg,addedby,addedatetime)  
values(@p_unitid,@p_uid1,@p_unitcode,@p_unitname, @p_NoofDecimal,@p_uid,	 @p_level,	 0,	 @p_addedby,GETDATE())  end 
else if @p_Formmode = 1 begin  update munit set unitcode = @p_unitcode,unitname = @p_unitname,
updatedby = @p_addedby,updateddatetime = GETDATE(),NoofDecimal= @p_NoofDecimal 
WHere unitid = @p_unitid   end 
else if @p_Formmode = 2 begin  update munit set delflg = 1,delby = @p_addedby,deldatetime = GETDATE() 
WHere unitid = @p_unitid   end 
BEGIN  SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage;   SET @p_Retval = ERROR_NUMBER()  set @p_Errstr = ERROR_MESSAGE()  END







GO
/****** Object:  StoredProcedure [dbo].[sp_Portallogin]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Portallogin]
	@p_userpwd NVARCHAR(200), 
	@p_username NVARCHAR(200)=null
	--@p_errorcode	INT OUT,
	--@p_errormessage	NVARCHAR(100) OUT			
AS
BEGIN	


SELECT username,userpassword,userid FROM 
mstuser 
WHERE username = @p_username and userpassword = @p_userpwd
	--BEGIN TRY
	--	Declare @userid nvarchar(100)
		
	--	set @userid = (select userid from mstuser where username = @p_username and userpassword = @p_userpwd)

	--	SET @p_errorcode = 1
	--	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess')
	--END TRY
	--BEGIN CATCH 	
	--	SET @p_errorcode = 0
	--	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid Login')
	--END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[sp_sodtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_sodtl] (
 @p_Formmode int,@p_sodtlid bigint = NULL,	 @p_uid1 bigint = NULL,	 @p_soid bigint = NULL,	 @p_itemid bigint = NULL,	 @p_unitid bigint = NULL,	 @p_itmdeldate datetime = NULL,	 @p_itemdeltime datetime = NULL,	 @p_qty float = NULL,	 @p_grate float = NULL,	 
@p_discperc float = NULL,	 @p_discamt float = NULL,	 @p_netrate float = NULL,	 @p_totamt float = NULL,	 @p_sgstperc float = NULL,	 @p_sgstamt float = NULL,	 @p_cgstperc float = NULL,	 @p_cgstamt float = NULL,	 @p_igstperc float = NULL,	 @p_igstamt float = NULL,	 
@p_Addnote nvarchar(max) = NULL,	 @p_prclitmyn bit = NULL,	 @p_uid bigint = NULL,	 @p_level nvarchar(10) = NULL,	 @p_addedby bigint = NULL,@p_Errstr nvarchar(max) out,   @p_RetVal bigint out ) AS   

if @p_Formmode = 1 begin 

set @p_uid1 = (select isnull(Max(uid1),0)+1 from sodtl  )    

declare @tempbilldtl nvarchar(100)
set @tempbilldtl = CONVERT(nvarchar,@p_uid) +''+CONVERT(nvarchar,@p_uid1)
set @p_sodtlid = @tempbilldtl  


INSERT INTO [sodtl] (sodtlid,	uid1,	soid,	itemid,	unitid,	itmdeldate,	itemdeltime,	qty,	grate,	discperc,	discamt,	netrate,	totamt,	sgstperc,	sgstamt,	cgstperc,	cgstamt,	igstperc,	igstamt,	Addnote,	prclitmyn,	uid,	tflg,	level,	addedby,addedatetime)  

VALUES( @p_sodtlid,	 @p_uid1,	 @p_soid,	 @p_itemid,	 @p_unitid,	 @p_itmdeldate,	 @p_itemdeltime,	 @p_qty,	 @p_grate,	 @p_discperc,	 @p_discamt,	 @p_netrate,	 @p_totamt,	 @p_sgstperc,	 @p_sgstamt,	 @p_cgstperc,	 @p_cgstamt,	 @p_igstperc,	 @p_igstamt,	 

@p_Addnote,	 @p_prclitmyn,	 @p_uid,	 0,	 @p_level,	 @p_addedby,GETDATE()) end    



else if @p_Formmode = 2 begin 

update [sodtl]  set itemid =  @p_itemid,	unitid =  @p_unitid,	itmdeldate =  @p_itmdeldate,	itemdeltime =  @p_itemdeltime,	qty =  @p_qty,	grate =  @p_grate,	discperc =  @p_discperc,	discamt =  @p_discamt,	netrate =  @p_netrate,	totamt =  @p_totamt,	sgstperc =  @p_sgstperc,	
sgstamt =  @p_sgstamt,	cgstperc =  @p_cgstperc,	cgstamt =  @p_cgstamt,	igstperc =  @p_igstperc,	igstamt =  @p_igstamt,	Addnote =  @p_Addnote,	prclitmyn =  @p_prclitmyn,	tflg =  1,	updatedby =  @p_addedby,updateddatetime = GETDATE()
WHere [sodtlid] = @p_sodtlid and [soid]=@p_soid end  
begin SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage   SET @p_Retval = ERROR_NUMBER() set @p_Errstr = ERROR_MESSAGE()  END

























GO
/****** Object:  StoredProcedure [dbo].[sp_sorder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_sorder] 
(@p_formmode int ,@p_soid float = NULL,	 @p_uid1 bigint = NULL,	 @p_custid bigint = NULL,	 @p_customername nvarchar(50) = NULL,	 @p_mobileno nvarchar(100) = NULL,@p_emailid  nvarchar(100) = NULL,	 @p_socode nvarchar(100) = NULL,	 @p_socode_1 bigint = NULL
,@p_sodate datetime = NULL,	 @p_sotime datetime = NULL,	@p_deliverydays bigint = NULL,	 @p_reminderdays bigint = NULL,	 @p_taxtype bigint = NULL,	 @p_roffamt float = NULL,	 @p_discamt float = NULL,	 @p_totamt float = NULL,	 @p_advamt float = NULL,	 @p_dailytrgentype bigint = NULL,	 
@p_deliverycharge float = NULL,	 @p_deliverytype bigint = NULL,	 @p_deliverystatus bit = NULL,	 @p_referenceno nvarchar(20) = NULL,	 @p_referencedate datetime = NULL,	 @p_wallettype bigint = NULL,	 @p_remarks nvarchar(250) = NULL,	 @p_paymenttype bigint = NULL,	 
@p_counterid bigint = NULL, @p_level nvarchar(10) = null,@p_addedby bigint = NULL,@p_uid bigint = null,@p_Errstr nvarchar(max) out,@p_RetVal bigint out,@p_retid bigint out ) AS  

if @p_Formmode = 0  begin  

set @p_uid1 = (select isnull(Max(uid1),0)+1 from sorder)  

declare @tempbilid nvarchar(100)

set @tempbilid = CONVERT(nvarchar,@p_uid) +''+ CONVERT(nvarchar,@p_uid1)

set @p_soid = @tempbilid

set @p_retid = @p_soid
set @p_socode = @p_soid
set @p_socode_1= @p_soid


INSERT INTO sorder (soid,	uid1,	custid,	customername,	mobileno,	socode,	socode_1,	sodate,	sotime,	deliverydays,	reminderdays,	taxtype,	roffamt,	discamt,	totamt,	advamt,	dailytrgentype,	deliverycharge,	deliverytype,	deliverystatus,	referenceno,	referencedate,	
wallettype,	remarks,	paymenttype,	counterid,	uid,tflg,[level],addedby,addedatetime,emailid,ordstatus,deliverydate,delpersonid	,DelAddressid,deviceid,TXNID,promocode)   
VALUES ( @p_soid,	 @p_uid1,	 @p_custid,	 @p_customername,	 @p_mobileno,	 @p_socode,	 @p_socode_1,	 @p_sodate,	 @p_sotime,	 @p_deliverydays,	 @p_reminderdays,	 @p_taxtype,	 @p_roffamt,	 @p_discamt,	 @p_totamt,	 @p_advamt,	 @p_dailytrgentype,	 
@p_deliverycharge,	 @p_deliverytype,	 @p_deliverystatus,	 @p_referenceno,	 @p_referencedate,	 @p_wallettype,	 @p_remarks,	 @p_paymenttype,	 @p_counterid,	 @p_uid,0,@p_level,@p_addedby,GETDATE(),@p_emailid,'Pending',@p_sodate,0,0,'','','')  end 

else if @p_Formmode = 1 begin 
update [sorder]  set custid =  @p_custid,	customername =  @p_customername,	mobileno =  @p_mobileno,	socode =  @p_socode,	socode_1 =  @p_socode_1,	sodate =  @p_sodate,	sotime =  @p_sotime,	deliverydays =  @p_deliverydays,	reminderdays =  @p_reminderdays,	
taxtype =  @p_taxtype,	roffamt =  @p_roffamt,	discamt =  @p_discamt,	totamt =  @p_totamt,	advamt =  @p_advamt,	dailytrgentype =  @p_dailytrgentype,	deliverycharge =  @p_deliverycharge,	deliverytype =  @p_deliverytype,	deliverystatus =  @p_deliverystatus,	
referenceno =  @p_referenceno,	referencedate =  @p_referencedate,	wallettype =  @p_wallettype,	remarks =  @p_remarks,	paymenttype =  @p_paymenttype,	counterid =  @p_counterid,updatedby =  @p_addedby,updateddatetime = GETDATE(),tflg = 1
WHere [soid] =@p_soid end  

begin SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage  SET @p_Retval = ERROR_NUMBER() set @p_Errstr = ERROR_MESSAGE()  END


GO
/****** Object:  StoredProcedure [dbo].[sp_userlogin]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_userlogin]
	@p_password NVARCHAR(200), 
	@p_email NVARCHAR(200)=null,
	@p_mobileno nvarchar(20)=null, 
	@p_deviceregid nvarchar(10),
	@p_authkey nvarchar(50),
	@p_otpno  nvarchar(50),
	@p_errorcode	INT OUT,
	@p_errormessage	NVARCHAR(100) OUT			
AS
BEGIN
	DECLARE @tblLogin AS TABLE
	(
		userid nvarchar(100), 
		username nvarchar(100),
		gender	nvarchar(100),
		address	nvarchar(100),
		city	nvarchar(100),
		age	nvarchar(100),
		email	nvarchar(100),
		mobileno nvarchar(50),
		userstatus nvarchar(100),
		img nvarchar(max)
	)
	
	Declare @userid nvarchar(100)


	if (@p_mobileno != '' )
	begin
		INSERT INTO @tblLogin	
		select id
		,firstname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
		case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno,case when userstatus = 'Inactive' then 0 else 1 end,
		case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
		from userregistration
		Where userpwd = @p_password and contactno= @p_mobileno 
	end
	else
	begin
	insert into @tblLogin
		select id
		,firstname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
		case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno,case when userstatus = 'Inactive' then 0 else 1 end,
		case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
		from userregistration
		Where userpwd = @p_password and useremail = @p_email 
	
	end

	set @userid = (select userid from @tblLogin)

	IF (EXISTS (select * from userappdevice Where id = @p_deviceregid ))
	BEGIN
		update userappdevice set userid = @userid Where id = @p_deviceregid		
	END

	IF (NOT EXISTS (select * from checkauthkey Where (userid = @userid or deviceregid = @p_deviceregid) and authkeystatus = 'Active'))
		BEGIN
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime,deviceregid) values (@userid,@p_authkey,'Active',Getdate(),@p_deviceregid)
		END
	else
		BEGIN
			/* deactive status if not active */
			update checkauthkey set authkeystatus = 'InActive' Where (userid = @userid or deviceregid=@p_deviceregid) and CONVERT(DATETIME, CONVERT(CHAR(8), adddatetime, 112)) < CONVERT(DATETIME, CONVERT(CHAR(8), getdate(), 112)) 
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime,deviceregid) values (@userid,@p_authkey,'Active',Getdate(),@p_deviceregid)
		END

	IF (NOT EXISTS (SELECT 1 FROM @tblLogin))
		BEGIN
			SELECT * FROM @tblLogin WHERE userstatus = 1
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid Login')
		END

	ELSE IF(NOT EXISTS (SELECT 1 FROM @tblLogin WHERE userstatus = 1))
		BEGIN		
			--SELECT * FROM @tblLogin WHERE patstatus = 'Active'
			update userregistration set otpno = @p_otpno Where id = @userid

			DECLARE @tblLogin1 AS TABLE 
			(userid nvarchar(100),username nvarchar(100),
			gender	nvarchar(100),address	nvarchar(100),city	nvarchar(100),age	nvarchar(100),email	nvarchar(100),mobileno nvarchar(50) ,userstatus nvarchar(100),otp nvarchar(50),	img nvarchar(max))

			if(@p_mobileno!='')
			begin
			insert into @tblLogin1 
				select id
				,firstname +''+ lastname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
				case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno, case when userstatus = 'Inactive' then 0 else 1 end ,otpno,
				case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
				from userregistration
				Where userpwd = @p_password and contactno = @p_mobileno 			
			end
			else
			begin
				insert into @tblLogin1 
				select id
				,firstname +''+ lastname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
				case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno,case when userstatus = 'Inactive' then 0 else 1 end ,otpno,
				case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
				from userregistration
				Where useremail = @p_email and userpwd = @p_password 
			end

			set @userid = (select userid from @tblLogin1)

			SELECT * FROM @tblLogin1
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Inactive Account' )			
		END
	ELSE
		BEGIN
			SELECT * FROM @tblLogin
			SET @p_errorcode = 1
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess' )
		END
	SELECT @p_errorcode AS errorcode, @p_errormessage AS errormessage

END









GO
/****** Object:  StoredProcedure [dbo].[sp_userprofileupdate]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_userprofileupdate]    
 @p_userid bigint,    
 @p_firstname nvarchar(100),     
 @p_lastname nvarchar(100) =null,    
 @p_city nvarchar(100) ,    
 @p_stateid nvarchar(100)=null,     
 @p_countryid nvarchar(100)=null,     
 @p_email nvarchar(100) =null,    
 @p_mobile nvarchar(100) ,    
 @p_imgpath nvarchar(100)=null,    
 @p_deviceregid nvarchar(100)=null,    
 @p_imagename nvarchar(100)=null,    
 @p_authkey nvarchar(100),    
 @p_retid bigint OUT,    
 @p_errorcode bigint OUT,    
 @p_errormessage NVARCHAR(100) OUT    
AS    
Begin    
    
	if isnull(@p_imagename,'') <> '' 
		begin    
			update UserRegistration set firstname=@p_firstname, lastname=@p_lastname, countryid=@p_countryid, stateid=@p_stateid,     
			city=@p_city,patimg = @p_imagename,  contactno=@p_mobile
			where id = @p_userid      
       end
    else
		begin
			update UserRegistration set firstname=@p_firstname, lastname=@p_lastname, countryid=@p_countryid, stateid=@p_stateid,     
			city=@p_city,  contactno=@p_mobile
			where id = @p_userid      		
		end
   select id as userid,firstname as fullname,City,patimg,contactno as mobileno,useremail as emailid from UserRegistration where id = @p_userid    
    
   SET @p_errorcode = 1    
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Update Record')    
    
 SELECT @p_retid as retid, @p_errorcode AS errorcode, @p_errormessage AS errormessage    
End     
    
    
    
    
    
    


GO
/****** Object:  StoredProcedure [dbo].[sp_usersignup]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_usersignup]
	@p_firstname nvarchar(100), 
	@p_lastname nvarchar(100) =null,
	@p_city nvarchar(100) ,
	@p_stateid nvarchar(100)=null, 
	@p_countryid nvarchar(100)=null, 
	@p_email nvarchar(100) =null,
	@p_userpwd nvarchar(100) ,
	@p_mobile nvarchar(100) ,
	@p_deviceregid nvarchar(10),
	@p_authkey nvarchar(10),
	@p_otpno nvarchar(10),
	@p_retid bigint OUT,
	@p_errorcode	bigint OUT,
	@p_errormessage	NVARCHAR(100) OUT
AS
Begin

	declare @Userid bigint

	--//Save Record into Patient Registration Table
	IF (NOT EXISTS (select * from UserRegistration Where contactno = @p_mobile))
		BEGIN
			insert into UserRegistration (firstname, lastname, countryid, stateid, city, contactno, useremail, userpwd, socialtype,  userstatus,otpno,senddatetime,otpstatus) values(
				@p_firstname,@p_lastname,@p_countryid,@p_stateid,@p_city,@p_mobile,@p_email,@p_userpwd,'',  'Inactive',@p_otpno,GETDATE(),'Inactive')

			set @p_retid = (SELECT SCOPE_IDENTITY())

			--// Insert / update record into check authkey table
			IF (NOT EXISTS (select * from checkauthkey Where userid = @Userid and authkeystatus = 'Active'))
				BEGIN
					/*Insert record into checkauthkey*/
					insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@Userid,@p_authkey,'Active',Getdate())
				END
			else
				BEGIN
					/* deactive status if not active */
					update checkauthkey set authkeystatus = 'InActive' Where userid = @Userid

					/*Insert record into checkauthkey*/
					insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@Userid,@p_authkey,'Active',Getdate())
				END

			SET @p_errorcode = 1
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Signup Successfully')
		END
	ELSE
		BEGIN
			SET @p_retid = 0
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'User Exist')
		END
	SELECT @p_retid as retid, @p_errorcode AS errorcode, @p_errormessage AS errormessage
End 










GO
/****** Object:  StoredProcedure [dbo].[sp_userverifyotp]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_userverifyotp]
	@p_userid NVARCHAR(200), 
	@p_otpno NVARCHAR(200), 
	@p_deviceregid nvarchar(10),
	@p_authkey nvarchar(50),
	@p_errorcode	INT OUT,
	@p_errormessage	NVARCHAR(100) OUT			
AS
BEGIN
	DECLARE @tblLogin AS TABLE
	(
		userid nvarchar(100), 
		username nvarchar(100),
		gender	nvarchar(100),
		address	nvarchar(100),
		city	nvarchar(100),
		age	nvarchar(100),
		email	nvarchar(100),
		userstatus nvarchar(100),
		img nvarchar(max)
	)

	Declare @userid nvarchar(100)

	--//update otp status & patient status
	begin
	declare @strupdate nvarchar(max)
		update userregistration set userstatus = 'Active', otpstatus = 'Active' Where id = @p_userid and otpno = @p_otpno
	end
			INSERT INTO @tblLogin
			select id			
			,firstname,isnull(gender,''),isnull(address,''),isnull(city,''),
			case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail, case when userstatus = 'Active' Then 1 else 0 end userstatus ,
			case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
			from userregistration	
			Where id = @p_userid and otpno = @p_otpno

	set @userid = (select userid from @tblLogin)

	IF (EXISTS (select * from userappdevice Where id = @p_deviceregid ))
	BEGIN
		/*update patient id record into patientappdevice*/
		update userappdevice set userid = @userid Where id = @p_deviceregid		
	END
	IF (NOT EXISTS (select * from checkauthkey Where userid = @userid and authkeystatus = 'Active'))
		BEGIN
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@userid,@p_authkey,'Active',Getdate())
		END
	else
		BEGIN
			/* deactive status if not active */
			update checkauthkey set authkeystatus = 'InActive' Where userid = @userid
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@userid,@p_authkey,'Active',Getdate())
		END
	IF (NOT EXISTS (SELECT 1 FROM @tblLogin))
		BEGIN
			SELECT * FROM @tblLogin WHERE userstatus = '1'
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid OTP')
		END
	ELSE
		BEGIN
			SELECT * FROM @tblLogin
			SET @p_errorcode = 1
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess' )
	END
	SELECT @p_errorcode AS errorcode, @p_errormessage AS errormessage
END





GO
/****** Object:  StoredProcedure [dbo].[spGenerateID]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
spGenerateID 'uid1','munit','1'
*/
CREATE PROCEDURE [dbo].[spGenerateID]        
 (               
    @sFieldName        sysname,  
    @sTableName        sysname,
    @level nvarchar(20)  
      
  )            
AS       
 BEGIN    
    
EXEC ('select  isnull(max(isnull([' + @sFieldName + '],0)),0)+1   
       from [' + @sTableName + ']  where level = ''' + @level + '''')  
       
       
END  




GO
/****** Object:  StoredProcedure [dbo].[spl_AddAddress]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
CREATE PROCEDURE [dbo].[spl_AddAddress]        
@p_deviceregid nvarchar(100)=null,        
@p_userid int=null,        
@p_authkey nvarchar(50) =null,        
@p_Type nvarchar(25)  = null,        
@p_FlatNo nvarchar(100)  = null,        
@p_Address1 nvarchar(255)  = null,        
@p_Address2 nvarchar(255)  = null,        
@p_City nvarchar(100)  = null,        
@p_State nvarchar(100)  = null,        
@p_Country nvarchar(100)  = null,        
@p_ZipCode nvarchar(10) = NULL,  
@p_MobileNo nvarchar(50)  = null,        
@p_FullName nvarchar(100)  = null,        
@p_DefAddress nvarchar(10) = NULL,  
  
@p_id nvarchar(100) = NULL,        
@p_formmode bigint = 0,        
      
@p_errorcode bigint out,         
@p_errormessage nvarchar(max) out        
AS        
Begin        
 
 if isnull(@p_formmode,0) = 0      
 begin      
       
  insert into userAddress ([userid],[Type],[FlatNo],[Address1],[Address2],[City],[State],[Country],[ZipCode],FullName,MobileNo,DefAddress )         
  values(@p_userid,@p_Type,@p_FlatNo,@p_Address1,@p_Address2,@p_City,@p_State,@p_Country,@p_ZipCode,@p_FullName,@p_MobileNo,@p_DefAddress )        
    
     if(@p_DefAddress=1)  
   begin  
  update userAddress set DefAddress = 0 where userid = @p_userid and id <> @@IDENTITY  
   end  
  
  select * from userAddress where id = @@IDENTITY         
  
 end       
else      
 begin      
  update userAddress set [Type]=@p_Type,[FlatNo]=@p_FlatNo,[Address1]=@p_Address1,[Address2]=@p_Address2,[City]=@p_City,[State]=@p_State      
  ,[Country] = @p_Country,[ZipCode]=@p_ZipCode,FullName = @p_FullName,DefAddress =@p_DefAddress,MobileNo = @p_MobileNo      
  Where id = @p_id and userid = @p_userid      
  
   if(@p_DefAddress=1)  
   begin  
  update userAddress set DefAddress = 0 where userid = @p_userid and id <> @p_id  
   end  
         
  select * from userAddress where id = @p_id         
 end        
 END          


GO
/****** Object:  StoredProcedure [dbo].[spl_AddToCart]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
CREATE PROCEDURE [dbo].[spl_AddToCart]          
@p_deviceregid nvarchar(100)=null,          
@itemid int=null,          
@packid int=null,          
@userid int=null,          
@ItemQty int =null,          
@p_authkey nvarchar(50) =null,          
@p_errorcode bigint out,           
@p_errormessage nvarchar(max) out          
AS          
Begin          
 
 if(isnull(@packid,0)>0)      
 BEGIN      
  if exists(Select * from mcartitem where userid = @userid and packid = @packid and itemid = @itemid)        
  begin           
   update mcartitem set itemqty = ItemQty + 1 where userid = @userid and packid = @packid and itemid = @itemid      
  end        
  else        
  begin  
   if isnull(@ItemQty,0) = 0  
    begin  
     set @ItemQty = 1  
    end          
   INSERT INTO mCartItem(userid,Itemid,packid,ItemQty) values(@userid,@itemid,@packid,@ItemQty)          
  end        
   END      
   ELSE      
   BEGIN      
  if exists(Select * from mcartitem where userid = @userid and itemid = @itemid)        
   begin           
    update mcartitem set itemqty = ItemQty + 1 where userid = @userid and itemid = @itemid        
   end        
  else        
  begin  
     if isnull(@ItemQty,0) = 0  
   begin  
    set @ItemQty = 1  
   end     
   INSERT INTO mCartItem(userid,Itemid,packid,ItemQty) values(@userid,@itemid,@packid,@ItemQty)          
  end        
 END      
 SET @p_errorcode = 1          
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')          
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage          
              
End


GO
/****** Object:  StoredProcedure [dbo].[spl_AddToFav]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spl_AddToFav]
@p_deviceregid nvarchar(100)=null,
@itemid int=null,
@userid int=null,
@favitembool BIT=null,
@p_authkey nvarchar(50) =null,
@p_errorcode bigint out, 
@p_errormessage nvarchar(max) out
AS
Begin

		if(@favitembool = 1)
			INSERT INTO mFavItem(userid,Itemid) values(@userid,@itemid)			
		else
		begin
			DELETE FROM mFavItem WHERE userid =@userid AND Itemid =@Itemid
		end
		select Itemid from mfavitem where userid = @userid
	
End 




GO
/****** Object:  StoredProcedure [dbo].[spl_calculatetax]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- spl_calculatetax '50'    
    
CREATE procedure [dbo].[spl_calculatetax]    
@p_userid nvarchar(10)    
as    
begin    
    
 select userid,
 (select top 1 scharges from mshipingchrg    
 Where DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) <=       
 CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and       
 scdate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )) as DeliveryCharges,
 
 sum(Baseamt) as ItemTotal,sum(discamt) as discamt,    
 sum(case when taxtype =0 then 
 BaseAmt - (cgstamt + sgstamt) - (discamt) else BaseAmt end) as NetAmt,
     
  sum(cgstamt) as cgstamt,sum(sgstamt)as sgstamt,sum(cgstamt+sgstamt) as totalgstamt,
      
  sum (
  
  (case when taxtype =0 then BaseAmt - (cgstamt + sgstamt) else BaseAmt  end)     
  -discamt + cgstamt + sgstamt) 
  
  + (select top 1 scharges from mshipingchrg    
   Where DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) <=       
   CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and       
   scdate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )        
   ) as TotalAmt    
 from    
 (    
 select itemid,packid,userid, Baseamt,taxtype,    
 Case when  discper >0 then (BaseAmt*Discper)/100 else 0 end as discamt,    
 case when taxtype = 0 then     
  round( (case when (cgstper+sgstper) > 0 then     
   (baseamt - (baseAmt / ((cgstper + sgstPer+100)/100)))/2    
   else 0 end),2)    
 else    
 (    
 round( (case when (sgstper) > 0 then     
   (baseamt * sgstper/100)    
   else 0 end),2)    
 )    
 end as sgstamt,    
 case when taxtype = 0 then     
  round((case when (cgstper+sgstper) > 0 then     
   (baseamt - (baseAmt / ((cgstper + sgstPer+100)/100)))/2    
   else 0 end),2)    
 else    
 ( round( (case when (cgstper) > 0 then     
   (baseamt * cgstper/100)    
   else 0 end),2)    
 )    
 end as cgstamt,    
 case when taxtype = 0 then     
  round( (case when (igstper) > 0 then     
   (baseamt - (baseAmt / ((igstper+100)/100)))    
   else 0 end),2)    
 else    
 (    
 round( (case when (igstper) > 0 then     
   (baseamt * igstper/100)    
   else 0 end),2)    
 )    
 end as igstamt    
 from    
   (select mCartItem.userid,mcartitem.Itemid,mCartItem.packid, discper,isnull(mitem.taxtype,0) as taxtype,    
     (case when Isnull(mCartItem.Packid,0) > 0 then     
   mstpackitmlink.Packval else case when isnull(pricedtl.Rate,'') = '' then mitem.salerate else pricedtl.rate end end )* mCartItem.ItemQty as  BaseAmt,          
   case when isnull(pricedtl.sgstper,0)=0 then mitem.sgstper else pricedtl.sgstper end sgstper,      
   case when isnull(pricedtl.cgstper,0)=0 then mitem.cgstper else pricedtl.cgstper end cgstper,      
   case when isnull(pricedtl.igstper,0)=0 then mitem.igstper else pricedtl.igstper end igstper    
   from  mCartItem inner join mitem on mCartItem.Itemid = mitem.itemid     
   inner join munit on mitem.unitid = munit.unitid    
   left join mstpackitmlink on mCartItem.Itemid = mstpackitmlink.itemid and     
   mCartItem.Packid = mstpackitmlink.packid      
   left join       
   (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate        
    from ( select *, row_number() over (        
    partition by [itemid]         
    order by [pldatetime] desc        
    ) as RN        
    From       
   (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,       
  mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate        
  from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level       
  Where DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=       
  CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and       
  pricedate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )        
  ) as A ) X where RN = 1) pricedtl       
  on mitem.itemid = pricedtl.itemid      
  ) B ) C     
  where userid = @p_userid group by userid     
    
end


GO
/****** Object:  StoredProcedure [dbo].[spl_clearcart]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
/*    
select  * from checkauthkey order by id desc    
exec spl_clearcart 2,2,0,'QCJM1BWTMBJZLGY5CPNKV6AYGVTAC1',0,''    
*/    
CREATE procedure [dbo].[spl_clearcart]    
(    
@p_userid int=null,    
@p_itemid int=null,
@p_packid int=null,          
@p_authkey nvarchar(50) =null,          
@p_errorcode bigint out,           
@p_errormessage nvarchar(max) out    
)    
as    
begin    
    
   declare @qtycnt bigint
      
 
 if(isnull(@p_packid,0)>0)  
   BEGIN  
   
   set @qtycnt = (Select itemqty from mcartitem where userid = @p_userid and packid = @p_packid and itemid = @p_itemid)    
  --if exists(Select * from mcartitem where userid = @userid and packid = @packid and itemid = @itemid)    
	  if @qtycnt > 1
		   begin       
			update mcartitem set itemqty = ItemQty - 1 where userid = @p_userid and packid = @p_packid and itemid = @p_itemid  
		   end    
	  else    
		   begin      
			delete from mcartitem where userid = @p_userid and packid = @p_packid and itemid = @p_itemid  
		   end    
	   END  
   ELSE  
   BEGIN  
	  if isnull(@p_itemid,0) > 0    
	  begin    
		  
		  set @qtycnt = (Select itemqty from mcartitem Where itemid = @p_itemid and userid = @p_userid)    
		  if @qtycnt >  1    
		   begin    
				update mcartitem set itemqty = itemqty -1 Where itemid = @p_itemid and userid = @p_userid    
		   end    
		  else    
		  begin    
				delete from mcartitem Where itemid = @p_itemid and userid = @p_userid    
		  end
	 END	      
	 else    
		  begin    
		   delete from mcartitem Where  userid = @p_userid    
		  end      
  end
    
  if @p_itemid > 0  
 begin  
  SET @p_errorcode = 1        
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Delete Data')        
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage        
   end  
    
    
end 


GO
/****** Object:  StoredProcedure [dbo].[spl_CompletedOrder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
  
exec spl_NewOrder '2019-12-30','2019-12-30'  
exec spl_NewOrder '',''  
*/  
  
CREATE procedure [dbo].[spl_CompletedOrder]  
@p_startdate nvarchar(100)  =null,  
@p_enddate nvarchar(100) =null  
As  
  
BEGIN  
  
   
  
  --select sorder.Soid,CustomerName,MobileNo,sorder.totamt as TotalAmt from sorder where ordstatus <> 'Delivered' order by sodate desc  
  
    
 select sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus   
 from sorder   
 inner join sodtl on sorder.soid = sodtl.soid  
 where ordstatus = 'Delivered'  
 and  sodate between @p_startdate and @p_enddate   
 order by sodate desc  
  
  
END  
  


GO
/****** Object:  StoredProcedure [dbo].[spl_Dashboard]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*      
exec dbo.spl_Dashboard      
*/      
CREATE procedure [dbo].[spl_Dashboard]      
As      
begin      
      
Select sum(neworder) as neworder,sum(Completedorder) as Completedorder, sum(Newuser) as Newuser,sum(Collection) as Collection ,sum(CurrentOrders) as CurrentOrders     
from(
      
Select Count(distinct sorder.soid) as NewOrder, 0 as Completedorder,0 as Newuser,0 as Collection  ,0 as CurrentOrders    
from sorder inner join sodtl on sorder.soid = sodtl.soid      
where ordstatus = 'Pending'  
  
union all      
      
Select 0 as NewOrder, Count(*) as Completedorder,0 as Newuser,0 as Collection ,0 as CurrentOrders     
from sorder       
where ordstatus = 'Delivered'      

union all      
      
Select 0 as NewOrder, 0 as Completedorder,0 as Newuser,0 as Collection ,Count(*) as CurrentOrders     
from sorder       
where (ordstatus <> 'Delivered' and ordstatus <> 'Failed' and ordstatus <> 'Pending')

 
union all      
      
Select 0 as NewOrder, 0 as Completedorder,Count(*) as Newuser,0 as Collection ,0 as CurrentOrders     
from userregistration       
where userstatus = 'Active'      
      
union all      
Select 0 as NewOrder, 0 as Completedorder,0 as Newuser,sum(totamt) as Collection ,0 as CurrentOrders     
from Bill       
) as a      
      
SELECT       
 MonthYear, SUM(TotalAmount) AS TotAmt       
FROM       
 (      
  SELECT       
   DATEPART(MM, BillDate) AS MonthNo,      
   DATEPART(YY, BillDate) AS YearNo,      
   CONVERT(NVARCHAR(3),DATENAME(MM, BillDate)) +'-'+ Convert(nvarchar(4),DATEPART(yy, BillDate)) AS MonthYear,      
   SUM(totamt) AS Totalamount      
  FROM Bill       
  WHERE BillDate >= DATEADD(MONTH, -3, GETDATE())         
  GROUP BY BillDate      
) a Group by MonthNo, MonthYear, YearNo      
ORDER BY YearNo ASC, MonthNo ASC        
      
      
SELECT       
 MonthYear, SUM(TotalAmount) AS TotAmt       
FROM       
 (      
  SELECT       
   DATEPART(MM, SoDate) AS MonthNo,      
   DATEPART(YY, SoDate) AS YearNo,      
   CONVERT(NVARCHAR(3),DATENAME(MM, SoDate)) +'-'+ Convert(nvarchar(4),DATEPART(yy, SoDate)) AS MonthYear,      
   SUM(totamt) AS Totalamount      
  FROM sorder       
  WHERE SoDate >= DATEADD(MONTH, -3, GETDATE())         
  GROUP BY SoDate      
) a Group by MonthNo, MonthYear, YearNo      
ORDER BY YearNo ASC, MonthNo ASC      
      
end 


GO
/****** Object:  StoredProcedure [dbo].[spl_DeliveryReport]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

exec spl_NewOrder '2019-12-30','2019-12-30'
exec spl_NewOrder '',''
*/

CREATE procedure [dbo].[spl_DeliveryReport]
@p_startdate nvarchar(100)  =null,
@p_enddate nvarchar(100) =null
As

BEGIN

	

	--select sorder.Soid,CustomerName,MobileNo,sorder.totamt as TotalAmt from sorder where ordstatus = 'Delivered'

	select sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus 
	from sorder 
	inner join sodtl on sorder.soid = sodtl.soid
	where ordstatus = 'Delivered'
	--and  sodate between @p_startdate and @p_enddate 
	order by sodate desc



END





GO
/****** Object:  StoredProcedure [dbo].[spl_Deliverystatushistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_Deliverystatushistory]      
@p_delperid bigint,      
@p_soid bigint,      
@p_itemid bigint=null,      
@p_packid bigint=null,      
@p_orderstatus nvarchar(50),      
@p_deldate nvarchar(20),      
@p_deltime nvarchar(20),      
@p_errorcode bigint out,      
@p_errormessage nvarchar(max) out         
as      
begin      
      
 if(@p_packid >0)      
 BEGIN      
  insert into OrderStatusHistory (soid,itemid,packid,orderstatus,delpersonid,deldate,deltime)       
   values(@p_soid,@p_itemid,@p_packid,@p_orderstatus,@p_delperid,@p_deldate,@p_deltime)     
       
   update orderpackageentrydtl set ordstatus = @p_orderstatus,delpersonid = @p_delperid,actualdeldate = @p_deldate,actualdeltime = @p_deltime    
   Where soid = @p_soid and itemid = @p_itemid and packid = @p_packid  and deliverydate = @p_deldate  
              
 END      
 ELSE      
 BEGIN      
  insert into OrderStatusHistory (soid,orderstatus,delpersonid,deldate,deltime)       
   values(@p_soid,@p_orderstatus,@p_delperid,@p_deldate,@p_deltime)      
       
   update sorder set ordstatus = @p_orderstatus where soid = @p_soid    
       
 END      
 SET @p_errorcode = 1              
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')              
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage       
end


GO
/****** Object:  StoredProcedure [dbo].[spl_dellogin]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
spl_dellogin '1234','sumit','',0,'',0
*/
CREATE PROCEDURE [dbo].[spl_dellogin]    
 @p_password NVARCHAR(200),     
 @p_email NVARCHAR(200),     
 @p_deviceid NVARCHAR(max) = null,     
 @p_errorcode INT OUT,    
 @p_errormessage NVARCHAR(100) OUT,    
 @p_delid int out    
AS    
BEGIN    

DECLARE @tblLogin AS TABLE  
 (  
  delid nvarchar(100),   
  delname nvarchar(100),    
  address nvarchar(100),  
  TelNo nvarchar(100),  
MobileNo nvarchar(100),  
Email nvarchar(100),  
PinCode nvarchar(100)  
 )
    
 set @p_delid =( select delid from mstuser inner join mdelboy on mstuser.userid = mdelboy.userid    
    where username = @p_email and userpassword = @p_password and Isactive = 1)    

if isnull(@p_delid,0) > 0
    begin
		update mdelboy set deviceid  = @p_deviceid  where delid = @p_delid       
         insert into @tblLogin 
         select delid,delbyName,isnull(address1,'') + '' + isnull(address2,'') as Address,isnull(TelNo,''),isnull(MobileNo,''),isnull(Email,''),isnull(PinCode,'')
         from mdelboy
         where delid = @p_delid
         select * from @tblLogin
		 SET @p_errorcode = 1    
		 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess' )    		
    end
else
	begin
	select * from @tblLogin
		SET @p_errorcode = 0  
	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid Login')  	
	end
      

    
 SELECT @p_errorcode AS errorcode, @p_errormessage AS errormessage,@p_delid as delid    
    
END


GO
/****** Object:  StoredProcedure [dbo].[spl_getAddress]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[spl_getAddress]  
@p_deviceregid nvarchar(100)=null,  
@p_userid int=null,  
@p_addid nvarchar(20)=null,  
@p_authkey nvarchar(50) =null,  
@p_errorcode bigint out,   
@p_errormessage nvarchar(max) out ,
@p_defaddress nvarchar(max) out   
AS  
Begin  
set @p_defaddress = ''
 
  select   id,userid,Type,FlatNo,Address1,Address2,City,State,Country,ZipCode,DefAddress,isnull(MobileNo,'') as MobileNo,isnull(FullName,'') as FullName
  from userAddress where userid = @p_userid  and id = @p_addid  
  
  select @p_DefAddress = DefAddress
  from userAddress where userid = @p_userid  and id = @p_addid  

  SET @p_errorcode = 1  
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')  
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage ,@p_defaddress as  DefAddressret
    
   
End   


GO
/****** Object:  StoredProcedure [dbo].[spl_GetCatItem]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
CREATE PROCEDURE [dbo].[spl_GetCatItem]    
--@p_deviceregid int=null,    
@p_authkey nvarchar(50) =null,    
@p_errorcode bigint out,     
@p_errormessage nvarchar(max) out    
AS    
Begin    
  --select itemcatid,itemcatcode,itemcatname,grpimage,itmcattype   
  --from mitemcat    
    
    
  Select mitemcat.itemcatid as itemcatid,itemcatcode,mitemcat.itemcatname as itemcatname,  mitemcat.grpimage, itmcattype  
 from mitemcat   
 Inner join (  
 Select distinct mitem.itemcatid from mitem   
 inner join mitemcat on (mitem.itemcatid = mitemcat.itemcatid)   
 where ISNULL(mitem.delflg,0) = 0 and mitem.itemstatus = 'active'  )   
 item on (mitemcat.itemcatid = item.itemcatid)   
 where isnull(itemcategory,'General') = 'General'
    
End     
  
    


GO
/****** Object:  StoredProcedure [dbo].[spl_GetCatWiseItem]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * From checkauthkey order by id desc        
-- spl_GetCatWiseItem 'ertwet','',20,'AQQ377ZJTZ5SKKRRF2PR1DOUCECEVT',0,''    
              
CREATE PROCEDURE [dbo].[spl_GetCatWiseItem]        
@p_deviceregid nvarchar(100)=null,                
@itemcatid nvarchar(100)=null,                
@userid nvarchar(10) =null,              
@p_authkey nvarchar(50) =null,                
@p_errorcode bigint out,                 
@p_errormessage nvarchar(max) out,      
@p_cartitemret bigint=null out      
AS                
Begin                
        
  declare @tblItem as table(              
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
  ingredients nvarchar(max),nutritioninfo nvarchar(200),itembarcode nvarchar(50),              
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500),favitem nvarchar(5) ,taxtype nvarchar(10),itemqty nvarchar(10) ,review   nvarchar(10)          
  )              
              
  declare @tblItemOffer as table(              
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
  ingredients nvarchar(max),nutritioninfo nvarchar(200),itembarcode nvarchar(50),              
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500),favitem nvarchar(5) ,taxtype nvarchar(10),itemqty nvarchar(10)  ,review   nvarchar(10)            
  )              
              
  declare @tblItempopuler as table(              
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
  ingredients nvarchar(max),nutritioninfo nvarchar(200),itembarcode nvarchar(50),              
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500),favitem nvarchar(5),taxtype nvarchar(10),itemqty nvarchar(10) ,review   nvarchar(10)        
  )              
              
  declare @MonthName nvarchar(5)               
  set @MonthName = convert(char(3), CONVERT(datetime2, SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') , 1), 0)              
              
  declare @SqlString nvarchar(max)              
              

 if(isnull(@itemcatid,'')='' or @itemcatid = 0)              
 BEGIN              
               
  -- For General Item selection                
               
 set @SqlString = ' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem ,isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty,      
     isnull(reviewitem.review,0) as  review        
     from mitem            
     inner join munit on mitem.unitid = munit.unitid       
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid          
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid                    
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid         
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
  where isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+' = 1 '              
              
         
 Insert into @tblItem exec(@sqlstring)              
               
 select * from @tblItem              
              
 --- Top Offer Item selection              
               
               
set @SqlString ='select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,              
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty ,      
     isnull(reviewitem.review,0) as  review       
     from mitem                 
     inner join munit on mitem.unitid = munit.unitid      
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid                  
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid         
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   WHERE discper >0 and isnull(mitem.itemstatus,''Active'') = ''Active''              
   and '+@MonthName+' = 1  '              
              
  insert into @tblItemOffer exec (@SqlString)              
              
  select * from @tblItemOffer              
              
 -- For populer Item selection              
               
set @SqlString = ' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,               
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem   ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty  ,      
     isnull(reviewitem.review,0) as  review      
     from mitem                   
     inner join munit on mitem.unitid = munit.unitid              
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid                  
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid               
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   where mitem.itemid in (select top 7 itemid from billdtl group by itemid order by sum(qty) desc)              
   and isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+'=1'              
              
  Insert into @tblItempopuler exec(@SqlString)      
          
  select * from @tblItempopuler              
               
 if (Select count(*) from mcartitem WHere userid = @userid) > 0      
 begin      
 set @p_cartitemret = 1      
 end      
 else      
 begin      
 set @p_cartitemret = 0      
 end      
    
  SET @p_errorcode = 1                
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage , @p_cartitemret as cartitemret      
 END                
 ELSE               
   BEGIN              
              
set @SqlString=' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem   ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty,      
     isnull(reviewitem.review,0) as  review        
     from mitem                 
     inner join munit on mitem.unitid = munit.unitid              
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid      
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid               
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid                
   WHERE ('+@itemcatid+' is null or itemcatid = '+@itemcatid+') and isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+'=1'              
              
        insert into @tblItem exec(@sqlstring)              
             
 --- Top Offer Item selection              
               
               
set @SqlString ='select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem  ,isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty,      
     isnull(reviewitem.review,0) as  review          
     from mitem                 
     inner join munit on mitem.unitid = munit.unitid              
  left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid           
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid              
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   WHERE ('+@itemcatid+' is null or itemcatid = '+@itemcatid+') and discper >0 and isnull(mitem.itemstatus,''Active'') = ''Active''              
   and '+@MonthName+' = 1  '              
              
  insert into @tblItemOffer exec (@SqlString)              
              
              
 -- For populer Item selection              
               
set @SqlString = ' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,              
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,               
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem   ,isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty ,      
     isnull(reviewitem.review,0) as  review         
     from mitem                   
     inner join munit on mitem.unitid = munit.unitid              
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid           
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid           
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid                  
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   where ('+@itemcatid+' is null or itemcatid = '+@itemcatid+') and  mitem.itemid in (select top 7 itemid from billdtl group by itemid order by sum(qty) desc)              
   and isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+'=1'              
              
  Insert into @tblItempopuler exec(@SqlString)              
             
              
  select * from @tblItem              
  select * from @tblItemOffer              
  select * from @tblItempopuler              
  if (Select count(*) from mcartitem WHere userid = @userid) > 0      
 begin      
  set @p_cartitemret = 1      
 end      
  else      
 begin      
  set @p_cartitemret = 0      
 end      
              
  SET @p_errorcode = 1                
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage , @p_cartitemret as cartitemret      
 END                 
End 


GO
/****** Object:  StoredProcedure [dbo].[Spl_Getcurrentorder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
    
    
*/    
    
CREATE procedure [dbo].[Spl_Getcurrentorder]    
@p_OrderDate nvarchar(25),    
@p_OrderTime nvarchar(20),    
@p_CustId nvarchar(25) ,  
@p_errorcode bigint out,       
@p_errormessage nvarchar(max) out     
As    
Begin    
    
create table #tblItem (soid nvarchar(40),socode nvarchar(50),ordstatus nvarchar(max),sodate nvarchar(25),sotime nvarchar(25),delboyname nvarchar(100))    
    
insert into #tblItem    
select soid,socode,isnull(ordstatus,'Pending') as ordstatus,convert(nvarchar, sodate,103) as sodate,convert(nvarchar, sotime,108)as sotime ,
mdelboy.delbyName + '('+ isnull(mdelboy.MobileNo,'') +')' as delboyname
from sorder
inner join mdelboy on mdelboy.delid = sorder.delpersonid
where custid = @p_CustId and format(sodate,'yyyy-MM-dd') = @p_OrderDate    
and ordstatus <> 'Delivered' and ordstatus <>'Pending' and ordstatus <> 'Failed'    
union all    
select orderpackageentrydtl.soid,socode,isnull(orderpackageentrydtl.ordstatus,'Pending') as ordstatus,convert(nvarchar,orderpackageentrydtl.deliverydate,103),convert(nvarchar,orderpackageentrydtl.deliverytime,108),
mdelboy.delbyName + '('+ isnull(mdelboy.MobileNo,'') +')' as delboyname     
from sorder 
inner join orderpackageentrydtl  on sorder.soid = orderpackageentrydtl.soid     
inner join mdelboy on mdelboy.delid = sorder.delpersonid
where custid = @p_CustId and FORMAT(orderpackageentrydtl.deliverydate,'yyyy-MM-dd') = @p_OrderDate    
and (FORMAT(DATEADD(MINUTE, -30, deliverytime),'HH:mm') >@p_OrderTime or FORMAT(DATEADD(MINUTE, +50, deliverytime),'HH:mm') >@p_OrderTime)       
and isnull(orderpackageentrydtl.ordstatus,'') <> 'Delivered' and isnull(orderpackageentrydtl.ordstatus,'') <>'Pending' and isnull(orderpackageentrydtl.ordstatus,'') <> 'Failed'    
    
    
Select * from #tblItem    
    
  SET @p_errorcode = 1      
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')      
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage      
    
    
END


GO
/****** Object:  StoredProcedure [dbo].[spl_GetDelBoyDetail]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec dbo.spl_Dashboard
*/
CREATE procedure [dbo].[spl_GetDelBoyDetail]
As
begin
		select delid AS ID,delbyName AS Name from  mdelboy
end  


GO
/****** Object:  StoredProcedure [dbo].[spl_Getdelboyorderdtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
exec spl_Getdelboyorderdtl 7242,'2020-02-10','12',0,''    
*/        
CREATE PROCEDURE [dbo].[spl_Getdelboyorderdtl]              
@p_soid nvarchar(50),           
@p_orddate nvarchar(50)  ,  
@p_packid nvarchar(50) = null,  
@p_errorcode bigint out,               
@p_errormessage nvarchar(max) out              
AS              
Begin              
          
declare @tblItem as table(            
  soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),          
sotime nvarchar(100),totamt nvarchar(100),itemname nvarchar(100),qty nvarchar(100),addnote nvarchar(max),packdays nvarchar(200),delpersonid nvarchar(100),          
packname nvarchar(100),addresstype nvarchar(100),address1 nvarchar(max),address2 nvarchar(max),packid nvarchar(100),itemid nvarchar(100),orderstatus nvarchar(100),
paymentdetails nvarchar(100)          
 )            
  
if isnull(@p_packid,0)<=0  
 begin  
 insert into @tblItem        
 Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,        
 convert(nvarchar,sotime,108) as sotime,sorder.totamt,mitem.itemname,sodtl.qty,sodtl.addnote          
 ,  0 as packdays,          
 sorder.delpersonid,'' as packname,userAddress.Type as addresstype,          
 case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +          
 case when isnull(address1,'') = '' then  '' else address1 + ',' end +          
 case when isnull(address2,'') = '' then  '' else address2 + ',' end as address1,          
           
 case When isnull(city,'') = '' then  '' else city + '' end +          
 case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +          
 case when isnull(state,'') = '' then  '' else state + '' end as address2          
 ,sodtl.packid,sodtl.itemid,      
 sorder.ordstatus as orderstaus , 
 case  when paymenttype = 0 then 'Cash'
 when paymenttype = 1 then 'Card'
 when paymenttype = 2 Then 'Wallet' else 'Cash' End as paymentdetails
  
 from sorder          
 inner join sodtl on sodtl.soid = sorder.soid          
 inner join mitem on mitem.itemid = sodtl.itemid   
 inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid           
 where sorder.soid = @p_soid    and isnull(sodtl.packid,0)<=0  
 end  
else  
 begin  
  insert into @tblItem        
   Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,        
   convert(nvarchar,sotime,108) as sotime,sorder.totamt,mitem.itemname,sodtl.qty,sodtl.addnote          
   ,  case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays,          
   sorder.delpersonid,isnull(mstpack.packname,'') as packname,userAddress.Type as addresstype,          
   case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +          
   case when isnull(address1,'') = '' then  '' else address1 + ',' end +          
   case when isnull(address2,'') = '' then  '' else address2 + ',' end as address1,          
             
   case When isnull(city,'') = '' then  '' else city + '' end +          
   case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +          
   case when isnull(state,'') = '' then  '' else state + '' end as address2          
   ,sodtl.packid,sodtl.itemid,      
   isnull(packstatus.packordstatus,'Pending') as orderstaus  ,   
   case when paymenttype = 0 then 'Cash'
   when paymenttype = 1 then 'Card'
   when paymenttype = 2 Then 'Wallet' else 'Cash' End as paymentdetails
      
   from sorder          
   inner join sodtl on sodtl.soid = sorder.soid          
   inner join mitem on mitem.itemid = sodtl.itemid   
   inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid          
     
   Inner join mstpack on sodtl.packid = mstpack.id               
   Inner join (          
   select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus        
   from orderpackageentrydtl          
   inner join mstpack on mstpack.id =  orderpackageentrydtl.packid          
   Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered' and orderpackageentrydtl.packid = @p_packid         
   group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus      
   ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid        
         
   Inner join (        
   select soid ,orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus      
   from orderpackageentrydtl        
   inner join mstpack on mstpack.id =  orderpackageentrydtl.packid    
   and orderpackageentrydtl.deliverydate = @p_orddate and  orderpackageentrydtl.packid = @p_packid  
   ) as packstatus on packstatus.soid = sorder.soid and sodtl.packid = packstatus.packid        
    where sorder.soid = @p_soid  and mstpack.id = @p_packid  
         
 end  
   
 select * from @tblItem    where orderstatus <> 'Delivered'        
            
  SET @p_errorcode = 1              
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')              
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage              
 END


GO
/****** Object:  StoredProcedure [dbo].[spl_GetdelboyorderdtlNew]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*              
exec spl_GetdelboyorderdtlNew 7261,'2020-02-18','0',0,''              
*/              
CREATE PROCEDURE [dbo].[spl_GetdelboyorderdtlNew]            
@p_soid nvarchar(50),                     
@p_orddate nvarchar(50)  ,            
@p_packid nvarchar(50) = null,            
@p_errorcode bigint out,                     
@p_errormessage nvarchar(max) out                    
AS                    
Begin                    
                
 declare @tblItem as table(                  
 soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),                
 sotime nvarchar(100),delpersonname nvarchar(500),                
 addresstype nvarchar(100),address1 nvarchar(max),address2 nvarchar(max),            
 netamt  nvarchar(100),deliverycharge nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100),orderstatus nvarchar(100),paymentType nvarchar(50),        
 packid nvarchar(100)                
  )                  
                 
 declare @tblItemdtl as table(                  
 soid nvarchar(100),orderno nvarchar(100),itemname nvarchar(100),qty nvarchar(100),addnote nvarchar(max),packdays nvarchar(200),          
 packname nvarchar(100),packid nvarchar(100),itemid nvarchar(100),            
 netamt  nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100) ,orderstatus nvarchar(100),deliverydate nvarchar(100),deliverytime nvarchar(100)              
  )                  
        
if isnull(@p_packid,0)<=0            
begin            
  insert into @tblItem              
    Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,              
  convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,                
  case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +                
  case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +                
  case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,            
  case When isnull(city,'') = '' then  '' else city + '' end +                
  case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +                
  case when isnull(state,'') = '' then  '' else state + '' end as address2                
  ,sorder.totamt - sorder.deliverycharge + sorder.discamt - (sodtl.sgstamt+sodtl.cgstamt)  as netamt,sorder.deliverycharge,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
   sorder.totamt as totamt,sorder.ordstatus,          
  case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment,'0' as packid          
  from sorder                
  inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl where isnull(sodtl.packid,0)<=0 group by soid) sodtl on sodtl.soid = sorder.soid                
  inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid                
  left join mdelboy on  sorder.delpersonid = mdelboy.delid                    
  where sorder.soid = @p_soid  --and isnull(sodtl.packid,0)<=0            
           
           
  insert into @tblItemdtl           
  Select sorder.soid,sorder.socode as orderno,mitem.itemname,sodtl.qty,sodtl.addnote                
  ,'' as packdays, '' packname,                
  sodtl.packid,sodtl.itemid,sodtl.netrate as netamt,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
  sodtl.totamt ,sorder.ordstatus ,Convert(nvarchar,sodate,103) as deliverydate,        
  convert(nvarchar,sotime,108) as deliverytime        
  from sorder                
  inner join sodtl on sodtl.soid = sorder.soid                
  inner join mitem on mitem.itemid = sodtl.itemid                
          
     where sorder.soid = @p_soid    and isnull(sodtl.packid,0)<=0         
             
   select * from @tblItem where  packid<=0        
   select * from @tblItemdtl     
 end        
 else        
 begin        
        
  insert into @tblItem              
  --Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,              
  --convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,                
  --case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +                
  --case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +                
  --case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,            
  --case When isnull(city,'') = '' then  '' else city + '' end +                
  --case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +                
  --case when isnull(state,'') = '' then  '' else state + '' end as address2                
  --,(sodtl.qty * sodtl.grate) as netamt,sorder.deliverycharge,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
  --(sodtl.grate + sorder.deliverycharge - sorder.discamt) as totamt,sorder.ordstatus,          
  --case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment ,'0' as  packid        
  --from sorder                
  --inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl where isnull(sodtl.packid,0)>0 group by soid) sodtl on sodtl.soid = sorder.soid                
  --inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid                
  --left join mdelboy on  sorder.delpersonid = mdelboy.delid        
            
  --where sorder.soid = @p_soid  --and mstpack.id = @p_packid            


    Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,              
  convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,                
  case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +                
  case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +                
  case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,            
  case When isnull(city,'') = '' then  '' else city + '' end +                
  case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +                
  case when isnull(state,'') = '' then  '' else state + '' end as address2                
  ,sorder.totamt - sorder.deliverycharge + sorder.discamt - (sodtl.sgstamt+sodtl.cgstamt)  as netamt,sorder.deliverycharge,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
   sorder.totamt as totamt,sorder.ordstatus,          
  case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment,'0' as packid          
  from sorder                
  inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl where isnull(sodtl.packid,0)>0 group by soid) sodtl on sodtl.soid = sorder.soid 
  inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid                
  left join mdelboy on  sorder.delpersonid = mdelboy.delid          
    where sorder.soid = @p_soid  --and mstpack.id = @p_packid            
           
  insert into @tblItemdtl           
  Select sorder.soid,sorder.socode as orderno,mitem.itemname,sodtl.qty,sodtl.addnote                
  ,case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays,                
  isnull(mstpack.packname,'') as packname,                
  sodtl.packid,sodtl.itemid,(sodtl.netrate) as netamt,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
  sodtl.totamt  ,isnull(packstatus.packordstatus,'Pending') as orderstaus  ,        
  Convert(nvarchar,packstatus.deliverydate,103) as deliverydate,        
  Convert(nvarchar,packstatus.deliverytime,108) as deliverytime        
  from sorder                
  inner join sodtl on sodtl.soid = sorder.soid                
  inner join mitem on mitem.itemid = sodtl.itemid                
  left join mstpack on sodtl.packid = mstpack.id                 
  left join (                
  select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus              
  from orderpackageentrydtl                
  inner join mstpack on mstpack.id =  orderpackageentrydtl.packid                
  Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered'                
  group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus            
  ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid             
  left join (              
  select soid ,orderpackageentrydtl.packid,orderpackageentrydtl.itemid  ,orderpackageentrydtl.ordstatus as packordstatus ,        
  deliverydate,deliverytime           
  from orderpackageentrydtl              
  inner join mstpack on mstpack.id =  orderpackageentrydtl.packid              
  and orderpackageentrydtl.deliverydate = @p_orddate            
  ) as packstatus on packstatus.soid = sorder.soid and sodtl.itemid = packstatus.itemid and sodtl.packid = packstatus.packid              
     where sorder.soid = @p_soid    and mstpack.id = @p_packid               
        
   select * from @tblItem          
   select * from @tblItemdtl        
           
 end        
         
        
                  
  SET @p_errorcode = 1                    
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')               
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage                    
 END


GO
/****** Object:  StoredProcedure [dbo].[spl_Getdelboyorderlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*              
spl_Getdelboyorderlist '12','2020-03-02',0,''                
*/              
                
CREATE PROCEDURE [dbo].[spl_Getdelboyorderlist]                  
@p_delboyid nvarchar(50),            
@p_orddate nvarchar(50)  ,                
@p_errorcode bigint out,                   
@p_errormessage nvarchar(max) out                  
AS                  
Begin                  
                  
  declare @tblItem as table(                
  soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),        
  customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),        
  sotime nvarchar(100),totamt nvarchar(100),delpersonid nvarchar(100),         
  ordstatus  nvarchar(100),deliverydate nvarchar(100),deliverytime nvarchar(100),    
  Module nvarchar(100),packdays nvarchar(100),packid nvarchar(100),deliverytimesort datetime,ImageName nvarchar(100), orddate datetime)    
                
insert into @tblItem               
Select soid,orderno,custid,customername,mobileno,sodate,sotime,              
 totamt,delpersonid,ordstatus,delidate,delitime,Module,packdays,packid,sotimesort,ImageName  , orddate  
 from     
(    
  Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,mobileno,      
 Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,              
 sorder.totamt,sorder.delpersonid,sorder.ordstatus,Convert(nvarchar,sodate,103) as delidate,Convert(nvarchar,sotime,108) as delitime,  
 'Normal' as Module ,'0' as packdays ,0 as packid,sotime as sotimesort,mitem.itmimgename as ImageName , sodate as orddate   
 from sorder inner join (select * from (SELECT soid,itemid,packid,ROW_NUMBER() OVER (PARTITION BY soid ORDER BY itemid asc) AS rn FROM sodtl ) a where rn = 1) as sodtl on sorder.soid = sodtl.soid    
 inner join mitem on sodtl.itemid = mitem.itemid
   where isnull(packid,0) <= 0  and sorder.delpersonid = @p_delboyid and ordstatus <> 'Pending'    
     
  union    
    
  Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,mobileno,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,              
 sorder.totamt,sorder.delpersonid  ,             
 case When sorder.ordstatus ='Accept'  then sorder.ordstatus  else isnull(packstatus.packordstatus,'Pending')  end as orderstaus ,      
 Convert(nvarchar,packstatus.deliverydate,103) as delidate, Convert(nvarchar,packstatus.deliverytime,108) as delitime ,'Package' as Module    
 ,  case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays     
 ,sodtl.packid, packstatus.deliverytime as deliverytimesort,mitem.itmimgename as ImageName  , sodate as orddate  
 from sorder    
 inner join (select distinct soid,packid,itemid from sodtl where isnull(packid,0) >= 0 ) as sodtl on sodtl.soid = sorder.soid    
 inner join mitem on sodtl.itemid = mitem.itemid    
 inner join mstpack on sodtl.packid = mstpack.id               
 inner join (              
 select Distinct soid ,orderpackageentrydtl.packid ,orderpackageentrydtl.ordstatus as packordstatus,deliverydate,deliverytime     
 from orderpackageentrydtl    
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid    
 and orderpackageentrydtl.deliverydate = @p_orddate            
 ) as packstatus on packstatus.soid = sorder.soid and sodtl.packid = packstatus.packid     
 left join (            
 select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus          
 from orderpackageentrydtl            
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid            
 Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered'            
 group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus        
 ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid          
           
   where sorder.delpersonid = @p_delboyid and ordstatus <> 'Pending'        
    ) a      
  --where sodate = @p_orddate  
    
  select * from @tblItem where orddate = @p_orddate           
                
   SET @p_errorcode = 1                  
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                  
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage                  
 END 


GO
/****** Object:  StoredProcedure [dbo].[spl_GetDelDevicedata]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spl_GetDelDevicedata]  
@p_delid bigint,  
@p_errorcode int out,  
@p_errormessage nvarchar(100) out  
as   
BEGIN  
 select delid,deviceid from mdelboy where delid = @p_delid  
 SET @p_errorcode = 1              
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')              
   
END


GO
/****** Object:  StoredProcedure [dbo].[spl_getoiderlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*            
select * from checkauthkey            
exec spl_getoiderlist 50,'2020-02-07','UYTKPWJ8BGQKWM3WNXQZLT5DH0OIYR',0,''            
*/        
              


CREATE PROCEDURE [dbo].[spl_getoiderlist]              
@p_custid nvarchar(50),            
@p_orddate nvarchar(50),        
@p_authkey nvarchar(50) =null,              
@p_errorcode bigint out,               
@p_errormessage nvarchar(max) out              
AS              
Begin       
  Create table #temptbl(soid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),socode nvarchar(100),            
  sodate nvarchar(100),sotime nvarchar(100),discamt nvarchar(50),totamt nvarchar(max),advamt nvarchar(100),deliverydate nvarchar(100),            
  deliverycharge nvarchar(100),deliverytype  nvarchar(100),ordstatus   nvarchar(100),    
  custid nvarchar(100),paymentdetails nvarchar(100),Module nvarchar(100),packdays nvarchar(100),    
  packid nvarchar(100),packname nvarchar(100),deliverytime nvarchar(100),itemname nvarchar(max),ImageName nvarchar(max))            
    
  Create table #temptblpast(soid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),socode nvarchar(100),            
  sodate nvarchar(100),sotime nvarchar(100),discamt nvarchar(50),totamt nvarchar(max),advamt nvarchar(100),deliverydate nvarchar(100),            
  deliverycharge nvarchar(100),deliverytype  nvarchar(100),ordstatus   nvarchar(100),    
  custid nvarchar(100),paymentdetails nvarchar(100),Module nvarchar(100),packdays nvarchar(100),    
  packid nvarchar(100),packname nvarchar(100),deliverytime nvarchar(100),itemname nvarchar(max),ImageName nvarchar(max) , orderdate datetime)        
                 
        
  insert into  #temptbl         
    select soid,customername,mobileno,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,discamt, totamt, advamt,    
 Convert(nvarchar,deliverydate,103) as deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,    
 packname,Convert(nvarchar,deliverytime,108) as deliverytime,itemname  ,ImageName    
    from ordermainlist    
    Where custid = @p_custid and deliverydate = @p_orddate and ordstatus <> 'Delivered'    
    
  insert into  #temptblpast         
    select top 10 soid,customername,mobileno,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,discamt, totamt, advamt,    
 Convert(nvarchar,deliverydate,103) as deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,    
 packname,Convert(nvarchar,deliverytime,108) as deliverytime,itemname ,ImageName ,  sodate + '' + sotime   
    from ordermainlist    
    Where custid = @p_custid and ordstatus = 'Delivered'    
    order by soid desc         
           
   select * from #temptbl    
   select * from #temptblpast 
   SET @p_errorcode = 1                    
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                    
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage                    
                
End 

GO
/****** Object:  StoredProcedure [dbo].[spl_GetOrderDevicedata]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spl_GetOrderDevicedata]
@p_SOId bigint,
@p_errorcode int out,
@p_errormessage nvarchar(100) out
as 
BEGIN
	select Soid,Deviceid from sorder where soid = @p_soid
	SET @p_errorcode = 1            
	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')            
 
END


GO
/****** Object:  StoredProcedure [dbo].[spl_getorderdtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  

spl_getorderdtl '11'  

*/  

CREATE procedure [dbo].[spl_getorderdtl]  

(  

@p_soid nvarchar(100)  

)  

as   

begin  

  

 select sorder.soid,custid,socode,customername,useraddress.MobileNo,Convert(nvarchar,sorder.sodate,103) + ' ' + Convert(nvarchar,sotime,108) as sodate,Convert(nvarchar,sotime,108) as sotime,

 deliverydate,totamt,taxtype ,   

 type + '-' + Flatno + ',' + Address1 + ',' + Address2 + '(' + ZipCode +')' as address,city,state ,

 case 

 when paymenttype = 0 then 'Cash'

 when paymenttype = 1 then 'Card'

 when paymenttype = 1 then 'Wallet' end as paymentstatus,case when isnull(packAvail,'')='' then 'No' else PackAvail end as packavailable

 from sorder    

 inner join userAddress on userAddress.id = sorder.DelAddressid  

 left join (select distinct soid,case when packid>0 then 'Yes' else 'No' end as packAvail from sodtl where packid >0 ) sodtl on sorder.soid = sodtl.soid 

 Where isnull(sorder.ordstatus,'Pending') = 'Pending' and sorder.soid = @p_soid  

  

  Select sorder.soid,sodtl.itemid,sorder.socode,Convert(nvarchar,sorder.sodate,103) as sodate,sorder.custid,customername, mitemcat.itemcatid,mitem.itemname,unitname,sodtl.qty,   

  sodtl.sodtlid, sodtl.grate, sodtl.discperc, sodtl.discamt, sodtl.netrate,  sodtl.cgstperc, sodtl.cgstamt, sodtl.sgstperc, sodtl.sgstamt,     

  sodtl.igstperc, sodtl.igstamt, sodtl.totamt, sodtl.netrate,mitem.DefQty,mitemcat.itemcatname ,'' as packagename 

  ,  Convert(nvarchar,deliverydate,103) as deliverydate,Convert(nvarchar,deliverydate,108) as deliverytime

  from sorder

  inner join sodtl on (sorder.soid = sodtl.soid)    

  inner join mitem on (sodtl.itemid = mitem.itemid) 

  inner join mitemcat on (mitem.itemcatid = mitemcat.itemcatid) 

  inner join munit on (mitem.unitid = munit.unitid)      


  where sorder.soid =   @p_soid  

  order by sorder.soid,sodtl.sodtlid   

end  



GO
/****** Object:  StoredProcedure [dbo].[spl_Getorderlistdtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*      
exec spl_Getorderlistdtl 7344,'2020-03-11',0,''      
*/      
CREATE PROCEDURE [dbo].[spl_Getorderlistdtl]    
@p_soid nvarchar(50)=null,    
@p_orddate nvarchar(50)=null,    
@p_errorcode bigint out,             
@p_errormessage nvarchar(max) out            
AS            
Begin            
        
 declare @tblItem as table(          
 soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),        
 sotime nvarchar(100),delpersonname nvarchar(500),        
 addresstype nvarchar(100),address1 nvarchar(max),address2 nvarchar(max),    
 netamt  nvarchar(100),deliverycharge nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100),orderstatus nvarchar(100),paymentType nvarchar(50)        
  )          
         
 declare @tblItemdtl as table(          
 soid nvarchar(100),orderno nvarchar(100),itemname nvarchar(100),qty nvarchar(100),addnote nvarchar(max),packdays nvarchar(200),  
 packname nvarchar(100),packid nvarchar(100),itemid nvarchar(100),    
 netamt  nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100)       
  )          
    
 insert into @tblItem      
 Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,      
 convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,        
 case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +        
 case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +        
 case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,    
 case When isnull(city,'') = '' then  '' else city + '' end +        
 case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +        
 case when isnull(state,'') = '' then  '' else state + '' end as address2        
 ,sorder.totamt - sorder.deliverycharge + sorder.discamt - (sodtl.sgstamt+sodtl.cgstamt) as netamt,sorder.deliverycharge,sorder.discamt,
 (sodtl.sgstamt+sodtl.cgstamt)as Tax,    
 sorder.totamt as totamt,sorder.ordstatus,  
 case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment  
 from sorder        
 inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl group by soid) sodtl on sodtl.soid = sorder.soid        
 inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid        
 left join mdelboy on  sorder.delpersonid = mdelboy.delid
 where sorder.soid = @p_soid      
  
  
 insert into @tblItemdtl   
 Select sorder.soid,sorder.socode as orderno,mitem.itemname,sodtl.qty,sodtl.addnote        
 ,case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays,        
 isnull(mstpack.packname,'') as packname,        
 sodtl.packid,sodtl.itemid,sodtl.netrate as netamt,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,    
 sodtl.totamt as totamt  
 from sorder        
 inner join sodtl on sodtl.soid = sorder.soid        
 inner join mitem on mitem.itemid = sodtl.itemid        
 left join mstpack on sodtl.packid = mstpack.id         
 left join (        
 select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus      
 from orderpackageentrydtl        
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid        
 Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered'        
 group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus    
 ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid     
 left join (      
 select soid ,orderpackageentrydtl.packid,orderpackageentrydtl.itemid  ,orderpackageentrydtl.ordstatus as packordstatus    
 from orderpackageentrydtl      
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid      
 and orderpackageentrydtl.deliverydate = @p_orddate    
 ) as packstatus on packstatus.soid = sorder.soid and sodtl.itemid = packstatus.itemid and sodtl.packid = packstatus.packid      
 where sorder.soid = @p_soid      
  
   select * from @tblItem  
             
   select * from @tblItemdtl          
          
  SET @p_errorcode = 1            
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')            
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage            
 END


GO
/****** Object:  StoredProcedure [dbo].[spl_getpackageitemlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                      
select * from checkauthkey                      
exec spl_getpackageitemlist  '11', '','50','0U6Y608YH1WHKEXDW9LWQX6GANVD79',0,''                      
*/                      
                        
CREATE PROCEDURE [dbo].[spl_getpackageitemlist]                        
@p_packid nvarchar(50) =null,                     
@p_itemid nvarchar(50) =null,          
@p_userid nvarchar(50) =null,                     
@p_authkey nvarchar(50) =null,                        
@p_errorcode bigint out,                         
@p_errormessage nvarchar(max) out ,        
@p_cartitemret bigint=null out                       
AS                        
Begin                        
  Create table #temptbl(                    
 packid nvarchar(100),itemid nvarchar(100),itemcatid nvarchar(100),                    
 itemname nvarchar(100),itemdesc nvarchar(max),itmimgename nvarchar(100),                    
 itembarcode nvarchar(100),ingredients nvarchar(max),nutritioninfo nvarchar(max),                    
 discper nvarchar(10),itemwt nvarchar(25),validfor nvarchar(100),unitname nvarchar(100),                    
 salerate nvarchar(100),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10) ,packname nvarchar(200)           
 ,favitem nvarchar(10) ,taxtype nvarchar(10) , itemqty nvarchar(100),review  nvarchar(100))                      
                          
                    
if isnull(@p_itemid,'') = ''                
 begin                
                
    insert into  #temptbl                        
    Select mstpackitmlink.packid, mstpackitmlink.itemid,mitem.itemcatid, mitem.itemname,mitem.itemdesc,mitem.itmimgename,                    
    mitem.itembarcode,mitem.ingredients,mitem.nutritioninfo,0 as discperc,mitem.itemwt,mitem.validfor,                    
    munit.unitname,packval as salerate,mitem.sgstper,mitem.cgstper,mitem.igstper,mstpack.packname ,          
    case when mfavitem.itemid is null then '0' else '1' end favitem ,          
    isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty ,        
    isnull(reviewitem.review,0) as  review                    
    from mstpackitmlink                    
    inner join mitem on mitem.itemid =   mstpackitmlink.itemid                    
    inner join munit on mitem.unitid = munit.unitid             
    left join ( Select avg(Review) as review,itemid  from mReviewItem Where userid = @p_userid  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid           
    inner join mstpack on mstpack.id = mstpackitmlink.packid          
    left join (select Itemid,userid, sum(itemqty) as itemqty,packid from mcartitem           
    WHere userid = @p_userid and packid = @p_packid group by Itemid,userid,packid) as cartitem on mitem.itemid = cartitem.itemid     
    LEFT join (select distinct itemid,userid from mFavItem where userid = @p_userid) as mfavitem on mstpackitmlink.itemid = mfavitem.Itemid                  
    where mstpackitmlink.packid = @p_packid  and mitem.itemstatus = 'Active'                  
                 
 end                
else                
 begin                
                
    insert into  #temptbl                        
    Select mstpackitmlink.packid, mstpackitmlink.itemid,mitem.itemcatid, mitem.itemname,mitem.itemdesc,mitem.itmimgename,                    
    mitem.itembarcode,mitem.ingredients,mitem.nutritioninfo,0 as discperc,mitem.itemwt,mitem.validfor,                    
    munit.unitname,packval as salerate,mitem.sgstper,mitem.cgstper,mitem.igstper,mstpack.packname  ,          
    case when mfavitem.itemid is null then '0' else '1' end favitem ,          
    isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty,        
    isnull(reviewitem.review,0) as  review            
    from mstpackitmlink                    
    inner join mitem on mitem.itemid =   mstpackitmlink.itemid                    
    inner join munit on mitem.unitid = munit.unitid           
 left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid             
    inner join mstpack on mstpack.id = mstpackitmlink.packid           
    left join (select Itemid,userid, sum(itemqty) as itemqty,packid from mcartitem WHere userid = @p_userid and packid = @p_packid group by Itemid,userid,packid) as cartitem on           
    mitem.itemid = cartitem.itemid    
    LEFT join (select distinct itemid,userid from mFavItem where userid = @p_userid) as mfavitem on mstpackitmlink.itemid = mfavitem.Itemid                  
    where mstpackitmlink.packid = @p_packid  and mitem.itemstatus = 'Active'   and mstpackitmlink.itemid = @p_itemid                
                 
 end                
         
   if (Select count(*) from mcartitem WHere userid = @p_userid) > 0        
 begin        
  set @p_cartitemret = 1        
 end        
  else        
 begin        
  set @p_cartitemret = 0        
 end        
         
  select * from #temptbl                      
                      
  SET @p_errorcode = 1                        
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                        
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage, @p_cartitemret as cartitemret                        
                            
End 



GO
/****** Object:  StoredProcedure [dbo].[spl_getshippingcharge]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 /*
 exec spl_getshippingcharge '2020-02-01 10:00','2020-02-01',0,''
 */
create procedure [dbo].[spl_getshippingcharge]
 (
 @p_strcurdtTime_1 nvarchar(100),
 @p_strcurdt_1 nvarchar(100),
 @p_errorcode bigint out,       
 @p_errormessage nvarchar(max) out      
 )
 as
 begin
 
   declare @tblItem as table(    
  pricelistdate nvarchar(100),pricelistid nvarchar(100),scharges nvarchar(100) )    

	insert into @tblItem
	 Select DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) as PlDateTime, mshipingchrg.scid as pricelistid,scharges  
	 from mshipingchrg 
	 Where DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) <= CONVERT(datetime, @p_strcurdtTime_1 , 120)  
	 and scdate<=@p_strcurdt_1
 
   
  select * from @tblItem    
    
    
    
  SET @p_errorcode = 1      
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')      
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage      
 end
 
 
 



GO
/****** Object:  StoredProcedure [dbo].[spl_Itemwiseorder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE procedure [dbo].[spl_Itemwiseorder]
  @p_startdate nvarchar(100),
  @p_enddate nvarchar(100)
  as
  BEGIN
	select socode,convert(nvarchar,sodate,103) as sodate,convert(nvarchar, sotime,108) as sotime,itemname,itemid,customername,soid,Qty,Rate,NetAmt,Discamt,
		TotalAmt,TaxPerc,TaxAmt,totalamount,BillAmt,CustId,OrdStatus 
		from ItemwiseSalesOrderRegister where Sodate between @p_startdate and @p_enddate order by sodate desc
  END


GO
/****** Object:  StoredProcedure [dbo].[spl_listAddress]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spl_listAddress]
@p_deviceregid nvarchar(100)=null,
@p_userid int=null,
@p_authkey nvarchar(50) =null,
@p_errorcode bigint out, 
@p_errormessage nvarchar(max) out
AS
Begin

		select * from userAddress where userid = @p_userid 
	
End 


GO
/****** Object:  StoredProcedure [dbo].[spl_NewOrder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
        
exec spl_NewOrder '2019-12-30','2019-12-30'        
exec spl_NewOrder '',''        
*/        
        
CREATE procedure [dbo].[spl_NewOrder]        
--@p_startdate nvarchar(100)  =null,        
--@p_enddate nvarchar(100) =null        
As        
        
BEGIN        
        
         
        
  --select sorder.Soid,CustomerName,MobileNo,sorder.totamt as TotalAmt from sorder where ordstatus <> 'Delivered' order by sodate desc        
        
          
 select distinct sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus         
 from sorder         
 inner join sodtl on sorder.soid = sodtl.soid        
 where ordstatus = 'Pending'        
 --and  sodate between @p_startdate and @p_enddate         
 order by sodate desc        
        
        
END 


GO
/****** Object:  StoredProcedure [dbo].[spl_OrderSummary]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

exec spl_OrderSummary '2019-12-30','2019-12-30'
exec spl_OrderSummary '',''
*/


CREATE procedure [dbo].[spl_OrderSummary]
@p_startdate nvarchar(100)  =null,
@p_enddate nvarchar(100) =null
As

BEGIN

	

	select sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus 
	from sorder 
	inner join sodtl on sorder.soid = sodtl.soid
	--where  sodate between @p_startdate and @p_enddate 
	order by sodate desc

END



GO
/****** Object:  StoredProcedure [dbo].[spl_orderupdate]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_orderupdate]        
@p_soid bigint,        
@p_errorcode bigint out,        
@p_errormessage nvarchar(max) out           
as        
begin        
        
   update sorder set ordstatus = 'Pending' where soid = @p_soid      

 SET @p_errorcode = 1                
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')                
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage         
end


GO
/****** Object:  StoredProcedure [dbo].[spl_removeaddress]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
/*    
select  * from checkauthkey order by id desc    
exec spl_removeaddress 20,17,'XE07QL0NE8N5R0KAWS0GHGS6KADQLA',0,''    
*/    
CREATE procedure [dbo].[spl_removeaddress]    
(    
@p_userid int=null,    
@p_id int=null,          
@p_authkey nvarchar(50) =null,          
@p_errorcode bigint out,           
@p_errormessage nvarchar(max) out    
)    
as    
begin    
    

	delete from userAddress Where id = @p_id and userid = @p_userid    
    
    select * from userAddress where  userid = @p_userid    
       
  SET @p_errorcode = 1        
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Delete Data')        
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage        
    
    
end


GO
/****** Object:  StoredProcedure [dbo].[spl_SalesSummary]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

exec spl_SalesSummary '2019-12-30','2019-12-30'
exec spl_SalesSummary '',''
*/

CREATE procedure [dbo].[spl_SalesSummary]
@p_startdate nvarchar(100)  =null,
@p_enddate nvarchar(100) =null
As

BEGIN

	

	select Billid,billcode,Convert(nvarchar,Billdate,103) as Billdate,COnvert(nvarchar,billtime,108) as BillTime,CustName as CustomerName,MobileNo,totamt as TotalAmt 
	from bill 
	where  Billdate between @p_startdate and @p_enddate 
	order by billdate desc

END




GO
/****** Object:  StoredProcedure [dbo].[spl_saveotploginhistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_saveotploginhistory]    
(    
@mode nvarchar(100),    
@mobileno nvarchar(100),    
@otp nvarchar(100),    
@errorcode bigint out,           
@errormessage nvarchar(max) out,  
@retid bigint out     
)    
as    
begin    
 if @mode = '0'    
   begin    
    if Exists (select top 1 * from userregistration where contactno = @mobileno)    
     BEGIN    
   insert into otploginhistory(mobileno,otp,adddatetime,otpstatus)    
   values(@mobileno,@otp,GETDATE(),'Inactive')    
     END    
    Else    
     BEGIN    
   insert into UserRegistration (contactno,otpstatus,userstatus)     
    values (@mobileno,'Active','Active')    
    
   insert into otploginhistory(mobileno,otp,adddatetime,otpstatus)    
    values(@mobileno,@otp,GETDATE(),'Inactive')    
     END    
   end    
 else    
 begin    
 update otploginhistory set otp = @otp Where mobileno = @mobileno and otpstatus = 'Inactive'     
 end    
  
  set @retid = (select top 1 id from userregistration where contactno = @mobileno)  
    
  SET @errorcode = 1                
  SET @errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                
  SELECT  @errorcode AS errorcode, @errormessage AS ErrorMessage  , @retid as retid  
end 


GO
/****** Object:  StoredProcedure [dbo].[spl_savepurchasehistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

exec savepurchasehistory 'dairy product','2020-04-15','15:00',100,100,0,'Online','123456',0,'',0,''

*/

CREATE procedure [dbo].[spl_savepurchasehistory]

(

 @p_purchasetype nvarchar(100) = NULL,@p_purchasedate datetime = NULL,@p_purchasetime datetime = NULL,@p_purchaseval float = 0,

 @p_amount float = null, @p_orderid bigint = 0,@p_paymentby nvarchar(100) = NULL,@p_refno  nvarchar(100) = NULL,@p_Adduserid bigint = NULL, @p_custid nvarchar(50) = NULL,

 @p_errorcode bigint out, @p_errormessage nvarchar(max) out

)

as

Begin
	

	insert into purchasehistory (purchasetype,purchasedate,purchasetime,purchaseval,amount,orderid,custid,paymentby,refno,adduserid,add_date_time)

	values (@p_purchasetype,@p_purchasedate,@p_purchasetime,@p_purchaseval,@p_amount,@p_orderid,@p_custid,@p_paymentby,@p_refno,@p_adduserid,Getdate())


	set @p_errorcode = 1

	set @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data') 

	Select @p_errorcode as errorcode,@p_errormessage as errormessage

end
GO
/****** Object:  StoredProcedure [dbo].[spl_savesodtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_savesodtl]          
(          
 @p_sodtlid bigint = NULL,@p_uid1 bigint = NULL,@p_soid bigint = NULL,@p_itemid bigint = NULL,@p_unitid bigint = NULL,@p_itmdeldate datetime = NULL,          
 @p_itemdeltime datetime = NULL,@p_qty float = NULL,@p_grate float = NULL,@p_discperc float = NULL,@p_discamt float = NULL,@p_netrate float = NULL,          
 @p_totamt float = NULL,@p_sgstperc float = NULL,@p_sgstamt float = NULL,@p_cgstperc float = NULL,@p_cgstamt float = NULL,@p_igstperc float = NULL,          
 @p_igstamt float = NULL,@p_Addnote nvarchar(max) = NULL,@p_prclitmyn bit = NULL,@p_uid bigint = NULL,@p_level nvarchar(10) = NULL,          
 @p_addedby bigint = NULL,@p_packid bigint = 0,@p_remarks nvarchar(max) = null,@p_custid bigint = null,@p_deldate datetime,@p_deltime datetime,@p_DeviceId nvarchar(200) = null,      
 @p_errorcode bigint out,@p_errormessage nvarchar(max) out          
)          
as          
Begin          
          
          
    set @p_errormessage=''  set @p_errorcode=0            
 select @p_level = level, @p_uid = uid from registerinfo where infoid = 1           
    set @p_uid1 = (select isnull(max(uid1), 0) + 1  from sodtl where level =@p_level)            
          
 set @p_sodtlid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_uid1)          
          set @p_unitid = (select unitid from mitem where itemid = @p_itemid)
 insert into sodtl (sodtlid,uid1,soid,itemid,unitid,itmdeldate,itemdeltime,qty,grate,discperc,discamt,netrate,totamt,sgstperc,sgstamt,cgstperc,cgstamt,igstperc,igstamt,Addnote,prclitmyn,uid,tflg,level,addedby,addedatetime,packid,deviceid)          
 values ( @p_sodtlid,@p_uid1,@p_soid,@p_itemid,@p_unitid,@p_itmdeldate,@p_itemdeltime,@p_qty,@p_grate,@p_discperc,@p_discamt,@p_netrate,@p_totamt,@p_sgstperc,@p_sgstamt,@p_cgstperc,@p_cgstamt,@p_igstperc,@p_igstamt,@p_Addnote,@p_prclitmyn,@p_uid,0,@p_level,   @p_addedby,getdate(),@p_packid,@p_DeviceId)          
          
         
    if @p_packid > 0      
    begin      
  declare @p_packdays bigint      
  declare @p_packuid1 bigint      
  declare @p_pacordid bigint      
           
    set @p_packdays = (select packdays-1 from mstpack Where id = @p_packid)      
 set @p_packuid1 = (select isnull(max(uid1), 0) + 1  from orderpackageentry where level =@p_level)                
 set @p_pacordid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_packuid1)          
           
  --insert into orderpackageentry(id,uid1,custid,soid,remarks,uid,tflg,level,addedby,addedatetime,deviceid)      
  --values (@p_pacordid,@p_packuid1,@p_custid,@p_soid,@p_remarks,@p_uid,0,@p_level,@p_addedby,getdate(),@p_DeviceId)      
      
  --declare @p_packduid1 bigint      
  --declare @p_packdordid bigint      
           
  --set @p_packduid1 = (select isnull(max(uid1), 0) + 1  from orderpackageentrydtl where level =@p_level)                
  --set @p_packdordid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_packduid1)          
           
  declare @maxdate datetime = Dateadd(day,@p_packdays,@p_deldate)      
           
      ;WITH mycte AS        
   (        
     SELECT @p_deldate as  DateValue      
     UNION ALL        
     SELECT  DateValue + 1       
     FROM    mycte           
     WHERE   DateValue  < @maxdate        
   )          
           
           
   insert into orderpackageentrydtl  (itemid,soid,packid,qty,deliverydate,deliverytime,uid,tflg,level,addedby,addedatetime,deviceid)      
   SELECT  @p_itemid,@p_soid, @p_packid,@p_qty,DateValue,@p_deltime,@p_uid,0,@p_level,@p_addedby,getdate(),@p_DeviceId      
   FROM    mycte        
   OPTION (MAXRECURSION 0)        
        
        
 end      
  
   delete from mCartItem Where  userid = @p_addedby    
           
 SET @p_errorcode = 1            
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')            
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage            
            
end


GO
/****** Object:  StoredProcedure [dbo].[spl_savesorder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from sorder            
            
CREATE procedure [dbo].[spl_savesorder]            
(            
            
 @p_soid float = NULL,@p_uid1 bigint = NULL,@p_custid bigint = NULL,@p_customername nvarchar(50) = NULL,@p_mobileno nvarchar(100) = NULL,            
 @p_socode nvarchar(100) = NULL,@p_socode_1 bigint = NULL,@p_sodate datetime = NULL,@p_sotime datetime = NULL,@p_deliverydays bigint = NULL,            
 @p_reminderdays bigint = NULL,@p_taxtype bigint = NULL,@p_roffamt float = NULL,@p_discamt float = NULL,@p_totamt float = NULL,@p_advamt float = NULL,            
 @p_deliverydate datetime = NULL,@p_dailytrgentype bigint = NULL,@p_deliverycharge float = NULL,@p_deliverytype bigint = NULL,@p_deliverystatus bit = NULL,            
 @p_referenceno nvarchar(20) = NULL,@p_referencedate datetime = NULL,@p_wallettype bigint = NULL,@p_remarks nvarchar(250) = NULL,@p_DelAddressid nvarchar(10) = null,      
 @p_paymenttype bigint = NULL,@p_counterid bigint = NULL,@p_uid bigint = NULL,@p_level nvarchar(10) = NULL,@p_addedby bigint = NULL,@p_DeviceId nvarchar(200) = null,@p_promocode nvarchar(200) = null,      
 @p_errorcode bigint out,@p_errormessage nvarchar(max) out   ,@p_respid bigint out  , @p_retuseremail nvarchar(100)=null out           
)            
as            
begin            
            
    set @p_errormessage=''  set @p_errorcode=0              
    select @p_level = level, @p_uid = uid from registerinfo where infoid = 1             
    set @p_uid1 = (select isnull(max(uid1), 0) + 1  from sorder where level =@p_level)              
    
select @p_retuseremail = useremail from userregistration Where id = @p_custid    
            
  set @p_soid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_uid1)          
  set @p_socode = @p_soid      
  set @p_socode_1 = @p_uid1      
  insert into sorder (soid, uid1, custid, customername, mobileno, socode, socode_1, sodate, sotime, deliverydays, reminderdays, taxtype, roffamt, discamt, totamt, advamt, deliverydate, dailytrgentype, deliverycharge, deliverytype, deliverystatus, referenceno, referencedate, wallettype, remarks, paymenttype, counterid, uid, level, addedby, addedatetime,tflg,ordstatus,DelAddressid,deviceid,promocode)
  values( @p_soid,@p_uid1,@p_custid,@p_customername,@p_mobileno,@p_socode,@p_socode_1,@p_sodate,@p_sotime,@p_deliverydays,@p_reminderdays,@p_taxtype,@p_roffamt,@p_discamt,@p_totamt,@p_advamt,@p_deliverydate,@p_dailytrgentype,@p_deliverycharge,@p_deliverytype,
  @p_deliverystatus,@p_referenceno,@p_referencedate,@p_wallettype,@p_remarks,@p_paymenttype,@p_counterid,@p_uid,@p_level,@p_addedby,Getdate(),0,case when @p_paymenttype = '0' then 'Pending' else 'Failed' end,@p_DelAddressid,@p_DeviceId,@p_promocode)
            
  set @p_respid = @p_soid              
  SET @p_errorcode = 1              
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')              
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage ,@p_respid as respid , @p_retuseremail as retuseremail              
            
end


GO
/****** Object:  StoredProcedure [dbo].[spl_updateorderstatus]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

spl_updateorderstatus '','','',''

*/
CREATE procedure [dbo].[spl_updateorderstatus]
(
 @p_ordstatus nvarchar(100),
 @p_delpersonid nvarchar(100),
 @p_userid  nvarchar(100),
 @p_soid nvarchar(100)
 ) as 
 
 begin
 
 Update sorder set  ordstatus =@p_ordstatus, delpersonid =  @p_delpersonid , 
 updatedby =  @p_userid , updateddatetime = CONVERT(datetime2, SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') , 1), tflg = 1 
 Where soid =  @p_soid
 
 end



GO
/****** Object:  StoredProcedure [dbo].[spl_updateorderstatusold]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spl_updateorderstatusold]        
@p_soid bigint,        
@p_errorcode bigint out,        
@p_errormessage nvarchar(max) out           
as        
begin        

   update sorder set ordstatus = 'Pending' where soid = @p_soid      
         

 SET @p_errorcode = 1                
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')                
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage         
end



GO
/****** Object:  UserDefinedFunction [dbo].[FnGetCompanyIdWithCategories]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[FnGetCompanyIdWithCategories]()
RETURNS  @rtnTable TABLE 
(
    -- columns returned by the function
    ID Bigint NOT NULL,
    Name nvarchar(255) NOT NULL
)
AS
BEGIN

--This select returns data
insert into @rtnTable
select compid,compname from mstcompany
return
END




GO
/****** Object:  UserDefinedFunction [dbo].[GetDirectoryPath]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE FUNCTION [dbo].[GetDirectoryPath] ( @Path NVARCHAR(MAX) ) RETURNS NVARCHAR(MAX) AS BEGIN RETURN LEFT(@Path, LEN(@Path) - CHARINDEX('', REVERSE(@Path))) END  




GO
/****** Object:  UserDefinedFunction [dbo].[ReturnBrcode]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ReturnBrcode]
(@comp_id bigint)
RETURNS VARCHAR(50)

AS

BEGIN

   DECLARE @brcode_name VARCHAR(50);

set @brcode_name = (select compcode from mstcompany where compid = @comp_id);

   RETURN @brcode_name;

END;




GO
/****** Object:  UserDefinedFunction [dbo].[ReturnBrcode1]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ReturnBrcode1]
()
RETURNS VARCHAR(50)

AS

BEGIN

   DECLARE @brcode_name VARCHAR(50);

set @brcode_name = (select compcode from mstcompany );

   RETURN @brcode_name;

END;




GO
/****** Object:  Table [dbo].[bill]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bill](
	[billid] [float] NOT NULL,
	[uid1] [bigint] NULL,
	[billdate] [datetime] NULL,
	[billTime] [datetime] NULL,
	[billcode] [nvarchar](100) NULL,
	[billcode_1] [bigint] NULL,
	[custname] [nvarchar](max) NULL,
	[custid] [bigint] NULL,
	[mobileno] [nvarchar](100) NULL,
	[taxtype] [nvarchar](100) NULL,
	[prclamt] [float] NULL,
	[discount] [float] NULL,
	[totamt] [float] NULL,
	[roffamt] [float] NULL,
	[grossamt] [float] NULL,
	[adddesc] [nvarchar](255) NULL,
	[cardname] [nvarchar](50) NULL,
	[referenceno] [nvarchar](50) NULL,
	[paymentdate] [datetime] NULL,
	[payremarks] [nvarchar](max) NULL,
	[cashamt] [float] NULL,
	[creditpay] [float] NULL,
	[giftcardPay] [float] NULL,
	[walletamt] [float] NULL,
	[wallettype] [bigint] NULL,
	[paidamt] [float] NULL,
	[advamt] [float] NULL,
	[amtrtn] [float] NULL,
	[totalamt] [float] NULL,
	[counterid] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[DelPersonid] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[billid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[billdtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[billdtl](
	[billdtlid] [float] NOT NULL,
	[uid1] [bigint] NULL,
	[billid] [bigint] NULL,
	[itemid] [bigint] NULL,
	[unitid] [bigint] NULL,
	[qty] [float] NULL,
	[grate] [float] NULL,
	[discperc] [float] NULL,
	[discamt] [float] NULL,
	[netrate] [float] NULL,
	[totamt] [float] NULL,
	[sgstperc] [float] NULL,
	[sgstamt] [float] NULL,
	[cgstperc] [float] NULL,
	[cgstamt] [float] NULL,
	[igstperc] [float] NULL,
	[igstamt] [float] NULL,
	[soid] [float] NULL,
	[sodtlid] [float] NULL,
	[schmid] [bigint] NULL,
	[schmdtlid] [bigint] NULL,
	[isvoid] [bit] NULL,
	[prclitmyn] [bit] NULL,
	[Addnote] [nvarchar](max) NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[billdtlid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[createsmstemplate]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[createsmstemplate](
	[SMSTemplateId] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[SMSType] [nvarchar](100) NULL,
	[SMSFor] [nvarchar](100) NULL,
	[SMSEng] [nvarchar](max) NULL,
	[SMSHindi] [nvarchar](max) NULL,
	[SMSRegional] [nvarchar](max) NULL,
	[SMSSubType] [nvarchar](100) NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[SMSTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[emailhistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emailhistory](
	[id] [bigint] NOT NULL,
	[senddate] [datetime] NULL,
	[sendtime] [datetime] NULL,
	[emailmessage] [nvarchar](250) NULL,
	[sendflg] [bigint] NULL,
	[emailtempname] [nvarchar](100) NULL,
	[emailtempid] [bigint] NULL,
	[emailsubject] [nvarchar](200) NULL,
	[sendemailid] [nvarchar](200) NULL,
	[ExportPdfPath] [nvarchar](max) NULL,
	[ErrDescription] [nvarchar](max) NULL,
	[uid1] [bigint] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[emailsetting]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emailsetting](
	[Id] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[portalname] [varchar](50) NULL,
	[smtpname] [varchar](150) NULL,
	[portno] [varchar](50) NULL,
	[emailid] [varchar](50) NULL,
	[password] [varchar](150) NULL,
	[receiveemail] [varchar](max) NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](20) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[EnableSSL] [nvarchar](10) NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mCartItem]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mCartItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[Itemid] [int] NOT NULL,
	[ItemQty] [int] NULL,
	[packid] [int] NULL,
 CONSTRAINT [PK_mCartItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mdelboy]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mdelboy](
	[delid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[delbyName] [nvarchar](50) NULL,
	[userid] [bigint] NULL,
	[Isactive] [bit] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[address1] [nvarchar](100) NULL,
	[address2] [nvarchar](100) NULL,
	[TelNo] [nvarchar](100) NULL,
	[MobileNo] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[PinCode] [nvarchar](100) NULL,
	[CityId] [bigint] NULL,
	[StateId] [bigint] NULL,
	[CountryId] [bigint] NULL,
	[deviceid] [nvarchar](max) NULL,
 CONSTRAINT [PK__mdelboy__00F8BFE75C043931] PRIMARY KEY CLUSTERED 
(
	[delid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mdesc]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mdesc](
	[descid] [bigint] NOT NULL,
	[uid1] [bigint] NOT NULL,
	[desccode] [nvarchar](50) NULL,
	[description] [nvarchar](100) NULL,
	[rate] [float] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[desctype] [nvarchar](100) NULL,
 CONSTRAINT [PK__mdesc__93B54C54695E344F] PRIMARY KEY CLUSTERED 
(
	[descid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mitem]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mitem](
	[itemid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[itemcatid] [bigint] NOT NULL,
	[itembarcode] [nvarchar](25) NULL,
	[itemname] [nvarchar](200) NULL,
	[unitid] [bigint] NULL,
	[itemdesc] [nvarchar](max) NULL,
	[ingredients] [nvarchar](max) NULL,
	[nutritioninfo] [nvarchar](300) NULL,
	[itemstatus] [nvarchar](20) NULL,
	[hsncode] [nvarchar](40) NULL,
	[sgstper] [float] NULL,
	[cgstper] [float] NULL,
	[igstper] [float] NULL,
	[itemwt] [decimal](10, 3) NULL,
	[selflifetype] [nvarchar](80) NULL,
	[selflifeval] [float] NULL,
	[salerate] [float] NULL,
	[purcrate] [float] NULL,
	[DefQty] [float] NULL,
	[favitem] [bit] NULL,
	[itmimgename] [nvarchar](50) NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[validfor] [nvarchar](100) NULL,
	[jan] [bit] NULL,
	[feb] [bit] NULL,
	[mar] [bit] NULL,
	[apr] [bit] NULL,
	[may] [bit] NULL,
	[jun] [bit] NULL,
	[jul] [bit] NULL,
	[aug] [bit] NULL,
	[sep] [bit] NULL,
	[oct] [bit] NULL,
	[nov] [bit] NULL,
	[dec] [bit] NULL,
	[taxtype] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[itemid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mitemcat]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mitemcat](
	[itemcatid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[itemcatcode] [nvarchar](10) NULL,
	[itemcatname] [nvarchar](100) NULL,
	[UnitId] [bigint] NULL,
	[subitemcatid] [bigint] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[itmcattype] [nvarchar](50) NULL,
	[grpimage] [nvarchar](max) NULL,
	[maincat] [int] NULL,
	[itemcategory] [nvarchar](100) NULL,
 CONSTRAINT [PK__mitemcat__37C099E464997F32] PRIMARY KEY CLUSTERED 
(
	[itemcatid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mshipingchrg]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mshipingchrg](
	[scid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[scdate] [datetime] NULL,
	[sctime] [datetime] NULL,
	[scharges] [float] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[scid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstcombo]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstcombo](
	[fieldname] [nvarchar](100) NULL,
	[fieldvalue] [nvarchar](150) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstcompany]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstcompany](
	[compid] [bigint] NOT NULL,
	[compcode] [nvarchar](10) NULL,
	[compname] [nvarchar](100) NULL,
	[compaddress1] [nvarchar](100) NULL,
	[compaddress2] [nvarchar](100) NULL,
	[compaddress3] [nvarchar](100) NULL,
	[compcity] [nvarchar](50) NULL,
	[compzipcode] [bigint] NULL,
	[compstate] [nvarchar](50) NULL,
	[compcountry] [nvarchar](50) NULL,
	[compphone] [nvarchar](20) NULL,
	[compmobile] [nvarchar](60) NULL,
	[compemailid] [nvarchar](100) NULL,
	[compwebsite] [nvarchar](100) NULL,
	[compcstno] [nvarchar](50) NULL,
	[comppanno] [nvarchar](50) NULL,
	[compvatno] [nvarchar](50) NULL,
	[compgstno] [nvarchar](50) NULL,
	[compgststno] [bigint] NULL,
	[compfssino] [nvarchar](50) NULL,
	[compnservice] [nvarchar](50) NULL,
	[compdlno1] [nvarchar](50) NULL,
	[compdlno2] [nvarchar](50) NULL,
	[branchcode] [nvarchar](20) NOT NULL,
	[branchcodeid] [bigint] NULL,
	[serialkey] [nvarchar](50) NULL,
	[custid] [nvarchar](10) NULL,
	[adduserid] [bigint] NULL,
	[adddatetime] [datetime] NULL,
	[edituserid] [bigint] NULL,
	[editdatetime] [datetime] NULL,
	[syncflg] [bigint] NULL,
	[glid] [bigint] NULL,
	[ownermobile] [nvarchar](120) NULL,
PRIMARY KEY CLUSTERED 
(
	[compid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstMessageTemplate]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstMessageTemplate](
	[msgid] [int] NOT NULL,
	[messagetype] [nvarchar](200) NULL,
	[messagetext] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstrights]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstrights](
	[rightsid] [bigint] NOT NULL,
	[rightsname] [nvarchar](100) NULL,
	[modulename] [nvarchar](50) NULL,
	[moduletype] [nvarchar](50) NULL,
	[submoduletype] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[rightsid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstsmssubtype]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstsmssubtype](
	[Id] [bigint] NULL,
	[SMSTypeId] [bigint] NULL,
	[SMSSubType] [nvarchar](200) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstsmstype]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstsmstype](
	[Id] [bigint] NULL,
	[SMSType] [nvarchar](200) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstuser]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstuser](
	[userid] [bigint] NOT NULL,
	[uid1] [bigint] NOT NULL,
	[username] [nvarchar](100) NULL,
	[userpassword] [nvarchar](100) NULL,
	[emailid] [nvarchar](50) NULL,
	[ntlock] [nvarchar](50) NULL,
	[userstatus] [bigint] NULL,
	[MobileNo] [bigint] NULL,
	[DefUser] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[macid] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstuserrights]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstuserrights](
	[urightsid] [bigint] NOT NULL,
	[userid] [bigint] NULL,
	[rightsid] [bigint] NULL,
	[addrights] [bit] NULL,
	[editrights] [bit] NULL,
	[deleterights] [bit] NULL,
	[printrights] [bit] NULL,
	[viewrights] [bit] NULL,
	[exportrights] [bit] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[munit]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[munit](
	[unitid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[unitcode] [nvarchar](10) NULL,
	[unitname] [nvarchar](100) NULL,
	[NoofDecimal] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[unitid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderStatusHistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatusHistory](
	[soid] [bigint] NULL,
	[itemid] [bigint] NULL,
	[packid] [bigint] NULL,
	[orderstatus] [nvarchar](50) NULL,
	[delpersonid] [bigint] NULL,
	[deldate] [datetime] NULL,
	[deltime] [datetime] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[paymentHistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paymentHistory](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Soid] [bigint] NULL,
	[PayType] [nvarchar](50) NULL,
	[PayAmt] [nvarchar](50) NULL,
	[PayStatus] [nvarchar](100) NULL,
	[Response] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[purchasehistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[purchasehistory](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[purchasetype] [nvarchar](100) NULL,
	[purchasedate] [datetime] NULL,
	[purchasetime] [datetime] NULL,
	[purchaseval] [float] NULL,
	[amount] [float] NULL,
	[orderid] [bigint] NULL,
	[custid] [nvarchar](100) NULL,
	[paymentby] [nvarchar](100) NULL,
	[refno] [nvarchar](250) NULL,
	[adduserid] [bigint] NULL,
	[add_date_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[registerinfo]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[registerinfo](
	[infoid] [int] NOT NULL,
	[orgname] [nvarchar](80) NULL,
	[owname] [nvarchar](50) NULL,
	[add1] [nvarchar](250) NULL,
	[add2] [nvarchar](250) NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[country] [nvarchar](50) NULL,
	[pin] [nvarchar](50) NULL,
	[telno] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[mobileno] [nvarchar](50) NULL,
	[level] [nvarchar](5) NULL,
	[curname] [nvarchar](150) NULL,
	[cursymbol] [nvarchar](150) NULL,
	[mlvlid] [bigint] NULL,
	[lvltype] [nvarchar](50) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[uid] [bigint] NULL,
	[panno] [nvarchar](20) NULL,
	[gstin] [nvarchar](20) NULL,
	[fssino] [nvarchar](20) NULL,
	[website] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[infoid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[smshistory]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[smshistory](
	[ID] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[MOBILENO] [nvarchar](50) NULL,
	[MESSAGE] [nvarchar](max) NULL,
	[SENDFLG] [float] NULL,
	[MSGTYPE] [nvarchar](100) NULL,
	[RESPONSEID] [nvarchar](max) NULL,
	[SmsPersonName] [nvarchar](250) NULL,
	[sendDate] [datetime] NULL,
	[sendTime] [datetime] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[smssetting]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[smssetting](
	[ID] [int] NOT NULL,
	[SMSSENDTYPE] [int] NULL,
	[SMSUSERNAME] [nvarchar](100) NULL,
	[SMSUSERPASSWORD] [nvarchar](100) NULL,
	[SMSSENDERID] [nvarchar](50) NULL,
	[SmsApi] [int] NULL,
	[Feed] [nvarchar](100) NULL,
	[SmsApi1] [int] NULL,
	[AppSmsType] [int] NULL,
	[ProUserName] [nvarchar](100) NULL,
	[ProPassword] [nvarchar](100) NULL,
	[ProFeed] [nvarchar](100) NULL,
	[SMSAPIKEY] [nvarchar](250) NULL,
	[ProSMSAPIKEY] [nvarchar](250) NULL,
	[ProSMSApi] [bigint] NULL,
	[TCountryCode] [float] NULL,
	[PCountryCode] [float] NULL,
	[SMSBill] [int] NULL,
	[SMSOrder] [int] NULL,
	[ColSMS] [int] NULL,
	[BillEdit] [int] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sodtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sodtl](
	[sodtlid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[soid] [bigint] NULL,
	[itemid] [bigint] NULL,
	[unitid] [bigint] NULL,
	[itmdeldate] [datetime] NULL,
	[itemdeltime] [datetime] NULL,
	[qty] [float] NULL,
	[grate] [float] NULL,
	[discperc] [float] NULL,
	[discamt] [float] NULL,
	[netrate] [float] NULL,
	[totamt] [float] NULL,
	[sgstperc] [float] NULL,
	[sgstamt] [float] NULL,
	[cgstperc] [float] NULL,
	[cgstamt] [float] NULL,
	[igstperc] [float] NULL,
	[igstamt] [float] NULL,
	[Addnote] [nvarchar](max) NULL,
	[prclitmyn] [bit] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[packid] [bigint] NULL,
	[deviceid] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[sodtlid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sorder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sorder](
	[soid] [float] NOT NULL,
	[uid1] [bigint] NULL,
	[custid] [bigint] NULL,
	[customername] [nvarchar](50) NULL,
	[mobileno] [nvarchar](100) NULL,
	[socode] [nvarchar](100) NULL,
	[socode_1] [bigint] NULL,
	[sodate] [datetime] NULL,
	[sotime] [datetime] NULL,
	[deliverydays] [bigint] NULL,
	[reminderdays] [bigint] NULL,
	[taxtype] [bigint] NULL,
	[roffamt] [float] NULL,
	[discamt] [float] NULL,
	[totamt] [float] NULL,
	[advamt] [float] NULL,
	[deliverydate] [datetime] NULL,
	[dailytrgentype] [bigint] NULL,
	[deliverycharge] [float] NULL,
	[deliverytype] [bigint] NULL,
	[deliverystatus] [bit] NULL,
	[referenceno] [nvarchar](20) NULL,
	[referencedate] [datetime] NULL,
	[wallettype] [bigint] NULL,
	[remarks] [nvarchar](250) NULL,
	[paymenttype] [bigint] NULL,
	[counterid] [bigint] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[ordstatus] [nvarchar](100) NULL,
	[delpersonid] [bigint] NULL,
	[DelAddressid] [bigint] NULL,
	[deviceid] [nvarchar](200) NULL,
	[TXNID] [nvarchar](max) NULL,
	[promocode] [nvarchar](20) NULL,
	[emailid] [nvarchar](100) NULL,
 CONSTRAINT [PK__sorder__3317C4174EAA3E13] PRIMARY KEY CLUSTERED 
(
	[soid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[userAddress]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userAddress](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[Type] [nvarchar](25) NULL,
	[FlatNo] [nvarchar](100) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[DefAddress] [bigint] NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FullName] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[userappdevice]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userappdevice](
	[userid] [bigint] NULL,
	[devicetype] [nvarchar](20) NULL,
	[deviceid] [nvarchar](max) NULL,
	[devicetoken] [nvarchar](max) NULL,
	[ipaddress] [nvarchar](50) NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[devicename] [nvarchar](100) NULL,
	[deviceosversion] [nvarchar](100) NULL,
	[devicestatus] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[userlog]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userlog](
	[logid] [bigint] NOT NULL,
	[userid] [bigint] NULL,
	[logindate] [datetime] NULL,
	[logintime] [datetime] NULL,
	[logouttime] [datetime] NULL,
	[machinename] [nvarchar](50) NULL,
	[macid] [nvarchar](50) NULL,
	[totaltime] [datetime] NULL,
	[uid1] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[logid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRegistration]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRegistration](
	[firstname] [nvarchar](250) NULL,
	[lastname] [nvarchar](250) NULL,
	[dateofbirth] [datetime] NULL,
	[gender] [nvarchar](250) NULL,
	[contactno] [nvarchar](250) NULL,
	[address] [nvarchar](max) NULL,
	[useremail] [nvarchar](max) NULL,
	[userpwd] [nvarchar](max) NULL,
	[area] [nvarchar](250) NULL,
	[zipcode] [nvarchar](250) NULL,
	[city] [nvarchar](250) NULL,
	[stateid] [int] NULL,
	[countryid] [int] NULL,
	[patimg] [nvarchar](250) NULL,
	[device_id] [nvarchar](max) NULL,
	[device_type] [nvarchar](max) NULL,
	[device_token] [nvarchar](max) NULL,
	[verifylink] [nvarchar](250) NULL,
	[verificationcodedate] [datetime] NULL,
	[uid1] [varchar](50) NULL,
	[socialtype] [varchar](50) NULL,
	[height] [nvarchar](25) NULL,
	[weight] [nvarchar](25) NULL,
	[bloodgroup] [nvarchar](25) NULL,
	[otpno] [nvarchar](10) NULL,
	[senddatetime] [datetime] NULL,
	[otpstatus] [nvarchar](10) NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[userstatus] [nvarchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[mitemlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View[dbo].[mitemlist] as (select mitem.itemid, mitem.itemcatid, mitemcat.itemcatname,mitem.itemname,   mitem.unitid, munit.unitname, mitem.itembarcode, mitem.itemstatus from mitem Left join mitemcat on (mitem.itemcatid = mitemcat.itemcatid)  Left
 join munit on (mitem.unitid = munit.unitid)   where IsNull(mitem.delflg, 0) = 0)  




GO
/****** Object:  View [dbo].[salesregisterdetail]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[salesregisterdetail] as select bill.billid,bill.billdate ,mitemlist.ItemName,billdtl.Qty,billdtl.grate,billdtl.discperc,
billdtl.discamt,billdtl.netrate,billdtl.totamt,billdtl.sgstperc,billdtl.sgstamt,billdtl.cgstperc,billdtl.cgstamt,billdtl.igstperc,billdtl.igstamt 
from bill 
inner join billdtl on bill.billid = billdtl.billid 
inner join mitemlist on mitemlist.ItemId = billdtl.ItemId 
where isnull(bill.delflg,0) = 0



GO
/****** Object:  View [dbo].[orderstatuslist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[orderstatuslist] as     
Select sorder.soid,customername,mobileno,socode,sodate,sotime,sorder.discamt,          
sorder.totamt,advamt,sodate as deliverydate,deliverycharge,deliverytype,ordstatus,sodtl.itemid,itemname,qty,sorder.custid,  
case  when paymenttype = 0 then 'Cash'  
when paymenttype = 1 then 'Card'  
when paymenttype = 2 Then 'Wallet' else 'Cash' End as paymentdetails,'Normal' as Module ,'0' as packdays ,0 as packid,'' as packname,sotime as deliverytime,mitem.itmimgename as ImageName
from sodtl          
inner join mitem on mitem.itemid = sodtl.itemid            
inner join sorder on sorder.soid = sodtl.soid            
where isnull(sodtl.packid,0)<=0    


GO
/****** Object:  View [dbo].[ordermainlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[ordermainlist] as 
Select soid,customername,mobileno,socode,sodate,sotime,discamt, totamt, advamt,
deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,packname,deliverytime,
STUFF((SELECT ', ', Convert(nvarchar,i.qty) +' X '+i.itemname as [text()] 
FROM orderstatuslist i Where i.deliverydate = orderstatuslist.deliverydate and 
i.soid = orderstatuslist.soid and i.custid = orderstatuslist.custid and i.deliverytime = orderstatuslist.deliverytime      
group by i.itemname,i.qty  FOR XML PATH ('')), 1, 2, '') as itemname,
(select top 1 ImageName from orderstatuslist i where i.deliverydate = orderstatuslist.deliverydate and 
i.soid = orderstatuslist.soid and i.custid = orderstatuslist.custid and i.deliverytime = orderstatuslist.deliverytime) as ImageName
From orderstatuslist 
--Where custid = 50 and deliverydate = '2020-02-10'
Group By soid,customername,mobileno,socode,sodate,sotime,discamt, totamt, advamt,
deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,packname,deliverytime




GO
/****** Object:  View [dbo].[SalesBillList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[SalesBillList] as 
Select billid,billcode,billdate,billtime,custname, Case When Isnull(convert(bigint, mobileno),0) = 0 then 0  else convert(bigint, mobileno)  end as mobileno,  
totamt,cashamt,creditpay,giftcardpay, WalletAmt,advamt
from(select billid,billcode as billcode,billdate,  Convert(nvarchar(5),billtime,108) as billtime,  
Case When isnull(bill.custname,'')= '' Then userregistration.firstname   else bill.custname end as custname,    bill.mobileno,discount,totamt,  
isnull(cashAmt,0) as cashamt,  isnull(creditpay,0) as creditpay,      
isnull(giftcardPay,0) as giftcardPay,  isnull(WalletAmt,0) as WalletAmt, 
isnull(advamt,0) as advamt
from bill inner join userregistration on bill.custid=userregistration.id where Isnull(bill.Delflg,0 ) = 0 )  as a



GO
/****** Object:  View [dbo].[GetAllContactDetails]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[GetAllContactDetails] as Select Distinct MOBILENO,custname as GlName,'' as RemarkLang from SalesBillList Where isnull(Convert(BigInt,MOBILENO,0),0) > 0





GO
/****** Object:  UserDefinedFunction [dbo].[Fun_CompDtl]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[Fun_CompDtl]()    
returns table     
as    
return(select * from mstcompany  )




GO
/****** Object:  View [dbo].[CounterwiseSalesSummary]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[CounterwiseSalesSummary] as  
Select billcode, billcode_1, billdate, 
Case When IsNull(bill.custname, '') = '' Then userregistration.firstname  else bill.custname end as customername,  
'' as countername,0 as counterid, mstuser.username,  bill.totamt, cashamt, creditpay,  
giftcardPay, walletamt   
From bill 
Inner Join userregistration on (bill.custid = userregistration.id)  
inner join mstuser on (mstuser.userid = bill.addedby) 






GO
/****** Object:  View [dbo].[createsmstemplatelist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[createsmstemplatelist] as Select SMSTemplateId,uid1,SMSType,SMSFor,SMSEng,SMSHindi,SMSRegional,SMSSubType 
From CreateSMSTemplate





GO
/****** Object:  View [dbo].[datewisesalessummery]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[datewisesalessummery] as 
select billdate as billdate, convert(nvarchar, MAX(billcode_1)) as Maxbillcode, convert(nvarchar, MIN(billcode_1)) as Minbillcode, 
count(bill.billid) as totbill,  sum(totamt) AS Amount, 0 as CounterId,'' as CounterName  
From bill  
where Isnull(bill.Delflg,0)=0 
Group by billdate 



GO
/****** Object:  View [dbo].[ItemDescriptionList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[ItemDescriptionList] as select descid,desccode,description from mdesc




GO
/****** Object:  View [dbo].[ItemWiseSalesOrderRegister]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ItemWiseSalesOrderRegister] as  
Select   socode,sodate,sotime,mitem.itemname,mitem.itemid,sorder.customername,sorder.soid,      
isnull(sum(sodtl.qty),0) as Qty ,(sum(sodtl.grate)) as Rate,    sum(sodtl.netrate)as NetAMT,sodtl.discamt as Discamt, sum(sodtl.totamt) as TotalAmt,      
(sum(sodtl.sgstperc)+sum(sodtl.cgstperc)+sum(sodtl.igstperc)) as TaxPerc,    (sum(sodtl.sgstamt)+sum(sodtl.cgstamt)+sum(sodtl.igstamt)) as TaxAmt,      
(sum(sodtl.totamt)+(sum(sodtl.sgstamt)+sum(sodtl.cgstamt)+   sum(sodtl.igstamt)))  as totalamount,  sorder.totamt as BillAmt,      
sorder.custid,ordstatus   
from sorder INNER JOIN sodtl on sorder.soid= sodtl.soid           
INNER JOIN mitem on sodtl.itemid = mitem.itemid        
LEFT JOIN billdtl ON   sodtl.itemid = billdtl.itemid AND    sodtl.soid = billdtl.soid AND   sodtl.sodtlid = billdtl.sodtlid      
group by   socode,sodate,sotime,mitem.itemname,mitem.itemid,sodtl.discamt,  sorder.customername,   sorder.totamt,sorder.taxtype,sodtl.totamt,      
sorder.roffamt,sorder.soid,sodtl.itemid,   socode_1, billdtl.itemid , sorder.custid,ordstatus   
  


GO
/****** Object:  View [dbo].[ItemWiseSalesRegister]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[ItemWiseSalesRegister] as     
Select bill.billid,billcode,billdate,userregistration.firstname,
mitem.itemname,mitem.itemid,      sum(billdtl.qty) as qty,(billdtl.grate)as Rate,sum(billdtl.netrate)Netrate,      
sum(billdtl.discamt)as DiscAmt,sum(billdtl.totamt) as TOtalAMt,      ((cgstperc)+(sgstperc)+(igstperc))as taxperc,(sum(cgstamt)+sum(sgstamt) +  
sum(igstamt)) as TaxAmt,    sum(billdtl.totamt)+(sum(cgstamt)+sum(sgstamt) +sum(igstamt)) as total,  bill.roffamt,  bill.totamt,
bill.grossamt,   (bill.totalamt)-sum(cgstperc)+sum(sgstperc)+sum(igstperc) as TaxBeforamt,  (sum(qty)* sum(grate)) as grossrate,    
1 as nodecqty,billcode_1,userregistration.id ,bill.prclamt 
from bill      
INNER JOIN billdtl on bill.billid = billdtl.billid and isnull(billdtl.isvoid,0) = 0    
INNER JOIN mitem on billdtl.itemid = mitem.itemid     
INNER JOIN userregistration on bill.custid = userregistration.id      
group by billcode,billdate,bill.custname,mitem.itemname,mitem.itemid,bill.totamt,bill.taxtype,    
bill.grossamt,bill.billid,bill.totalamt,billcode_1,userregistration.id,cgstperc,sgstperc,igstperc,
billdtl.grate,bill.prclamt,bill.roffamt,userregistration.firstname





GO
/****** Object:  View [dbo].[mdelboylist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[mdelboylist] as select delid,delbyName from mdelboy





GO
/****** Object:  View [dbo].[mdesclist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[mdesclist] as select descid,desccode as DescCode,description as Description,rate as Rate from mdesc




GO
/****** Object:  View [dbo].[mitemcatlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View[dbo].[mitemcatlist] as 
 (
 select itemcatid, itemcatcode,itemcatname,munit.unitname
 from mitemcat 
 Left join munit on munit.unitid = mitemcat.unitid 
 where ISNULL(mitemcat.delflg, 0) = 0 
 ) 




GO
/****** Object:  View [dbo].[munitlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create View[dbo].[munitlist] as (select unitid, unitcode, unitname, NoofDecimal from munit where ISNULL(munit.delflg, 0) = 0)



GO
/****** Object:  View [dbo].[pendingsalesorder]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create view [dbo].[pendingsalesorder] as  
 select sorder.soid,sorder.socode,sorder.sodate,sorder.deliverydate,sorder.sotime,   sorder.custid,userregistration.firstname,sorder.totamt as netamt,
 sorder.advamt,   sorder.roffamt,   sodtl.itemid,sodtl.unitid,sodtl.qty,sodtl.grate,sodtl.netrate,sodtl.discamt,   sodtl.totamt, 
 sodtl.cgstamt,   sodtl.sgstamt,sodtl.igstamt,userregistration.socialtype,  userregistration.contactno, '' as custcode, mitem.itembarcode,mitem.itemname,   mitem.DefQty,mstuser.username, IsNull(sale.recqty, 0 ) as recqty,
 sorder.level   
 from sorder    
 inner join sodtl on (sorder.soid=sodtl.soid and sorder.level=sodtl.level)   
 inner Join userregistration on (sorder.custid = userregistration.id)   
 inner join mitem on (sodtl.itemid = mitem.itemid)   
 inner join mstuser on (sorder.addedby=mstuser.userid)   
 left join (
 select billdtl.itemid, billdtl.soid, billdtl.sodtlid, sum(billdtl.qty) as recqty,bill.level
 from bill 
 inner join billdtl on (bill.billid = billdtl.billid  and bill.level = billdtl.level)   
 where isnull(billdtl.soid,0) > 0 And isnull(billdtl.sodtlid, 0) > 0 
 group by billdtl.itemid, billdtl.soid, billdtl.sodtlid,bill.level
 ) sale on   (sale.soid = sodtl.soid and sale.sodtlid = sodtl.sodtlid and  sale.itemid = sodtl.itemid and sale.level = sodtl.level ) 




GO
/****** Object:  View [dbo].[PendingSalesOrderItemWise]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[PendingSalesOrderItemWise] as  
select distinct sorder.soid, sorder.socode,sorder.sodate,  Case When sorder.customername = '' Then userregistration.firstname else sorder.customername END as custname,mitem.itemid,mitem.itemname,qty ,userregistration.id   
from sorder   
INNER JOIN sodtl on sorder.soid = sodtl.soid  
INNER JOIN userregistration on sorder.custid = userregistration.id   
INNER JOIN mitem on sodtl.itemid = mitem.itemid    
where  sodtl.itemid not in (
select isnull (itemid,0) as itemid from billdtl where itemid >0) or   sodtl.soid not in (select isnull (soid,0) as soid from billdtl where soid > 0) or   
sodtl.sodtlid not in (Select isnull (sodtlid,0) as sodtlid from billdtl where sodtlid>0 )





GO
/****** Object:  View [dbo].[pensolist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[pensolist] as  
select sorder.soid,sorder.socode,sorder.sodate,sodtl.itmdeldate,sorder.sotime,   sorder.custid,userregistration.firstname,sorder.totamt as netamt,
sorder.advamt,   sorder.roffamt,   sodtl.itemid,sodtl.unitid,sodtl.qty,sodtl.grate,sodtl.netrate,sodtl.discamt,   
sodtl.totamt, sodtl.cgstamt,   sodtl.sgstamt,sodtl.igstamt,  userregistration.contactno, '' as custcode, 
mitem.itemname,mstuser.username, 
IsNull(sale.recqty, 0 ) as recqty   
from sorder    
inner join sodtl on (sorder.soid=sodtl.soid )   
 inner Join userregistration on (sorder.custid = userregistration.id)   
inner join mitem on (sodtl.itemid = mitem.itemid)   
inner join mstuser on (sorder.addedby=mstuser.userid)   
left join (select billdtl.itemid, billdtl.soid, billdtl.sodtlid, sum(billdtl.qty) as recqty   
from bill 
inner join billdtl on (bill.billid = billdtl.billid  )   
where isnull(billdtl.soid,0) > 0 And isnull(billdtl.sodtlid, 0) > 0  
group by billdtl.itemid, billdtl.soid, billdtl.sodtlid) sale on   (sale.soid = sodtl.soid and sale.sodtlid = sodtl.sodtlid and   sale.itemid = sodtl.itemid  )





GO
/****** Object:  View [dbo].[SalesOrderBookList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[SalesOrderBookList] as  
select sorder.soid,socode,sodate,userregistration.firstname,customername,  
sorder.mobileno,sorder.totamt,sorder.advamt,   CASE WHEN paymenttype=1 THEN 'Cash' WHEN paymenttype=2 THEN 'Cheque' WHEN paymenttype=3 THEN 'Credit'  
WHEN paymenttype=4 THEN 'Cr Dr Card'   WHEN paymenttype=5 THEN 'Free' WHEN paymenttype=6 THEN 'Gift Card' WHEN paymenttype=7 THEN 'Prepaid Card'  
WHEN paymenttype=8 THEN 'Pending Settlement'   WHEN paymenttype=9 THEN 'Wallet' END as paymenttype,sum(sodtl.discamt) as discamt,
counterid    
from sorder 
inner join userregistration on sorder.custid=userregistration.id   
inner join sodtl on sorder.soid = sodtl.soid  
Group by sorder.soid,socode,sodate,userregistration.firstname,customername,sorder.mobileno,   sorder.totamt,advamt,sorder.paymenttype,counterid





GO
/****** Object:  View [dbo].[SalesOrderRegister]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[SalesOrderRegister] as 
select socode,socode_1,sorder.sodate,  Case When sorder.customername = '' Then userregistration.firstname else sorder.customername END as custname,   
sum(sodtl.totamt)+sum (sodtl.discamt) as Besicamt,sum (sodtl.discamt) as Discamt,    sorder.advamt,(sum(sodtl.sgstamt)+sum(sodtl.cgstamt)+  sum(sodtl.igstamt))   as TaxAmount,  
sorder.totamt,userregistration.id
from sorder     
INNER JOIN sodtl on sorder.soid = sodtl.soid 
inner join userregistration on sorder.custid=userregistration.id  
Group by socode,socode_1,sorder.sodate,sorder.advamt,sorder.roffamt,sorder.taxtype,userregistration.firstname,sorder.totamt,  
sorder.discamt   ,sorder.customername ,userregistration.id 





GO
/****** Object:  View [dbo].[SalesRegister]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[SalesRegister] as 
select bill.billid,bill.billdate ,bill.billcode,userregistration.firstname,bill.totamt from bill inner join userregistration on userregistration.id = bill.custid where isnull(bill.delflg,0) = 0



GO
/****** Object:  View [dbo].[SalesTaxationList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[SalesTaxationList] as Select Distinct sgstperc,cgstperc,billid,Sum(sgstamt) as sgstamt , sum(cgstamt) as cgstamt from billDtl Group By sgstperc,cgstperc,billid




GO
/****** Object:  View [dbo].[selectDeliveryManList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[selectDeliveryManList] as select delid,delbyName as PersonName from mdelboy where  isnull(Isactive,0) = 1





GO
/****** Object:  View [dbo].[SelectPOList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[SelectPOList] as select Distinct sorder.soid,socode,sodate,deliverydate,mitem.itemid,Itemname,itembarcode,Qty,grate ,sodtlid  
from sorder 
inner join sodtl on sorder.soid = sodtl.soid 
inner join mitem on mitem.itemid  = sodtl.itemid  
where sodtl.itemid not in (Select isnull(itemid,0) as itemid from billdtl where itemid>0) or sodtl.soid not in (Select isnull(soid,0) as soid from billdtl where soid>0) or sodtl.sodtlid not in (Select isnull(sodtlid,0) as sodtlid from billdtl where sodtlid>0)





GO
/****** Object:  View [dbo].[SmsLogList]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[SmsLogList] As  Select sendDate,[MOBILENO],[SmsPersonName],[MESSAGE],[SendFlg] from smsHistory


GO
/****** Object:  View [dbo].[userlist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[userlist] as select userid, username from mstuser 





GO
/****** Object:  View [dbo].[userloglist]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[userloglist] as 
select mstUser.UserID,mstuser.UserName,Convert(nvarchar,Userlog.Logindate,103) as Logindate,Convert(nvarchar,Userlog.logintime,108) as logintime,
Convert(nvarchar,Userlog.logouttime,108) as logouttime,Convert(nvarchar,Userlog.totaltime,108) as totaltime,MachineName,userlog.MacId
from Userlog 
inner join mstuser on Userlog.userid =  mstuser.UserID





GO
/****** Object:  View [dbo].[UserwiseSalesSummaryBillDetail]    Script Date: 23/10/2021 19:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create View [dbo].[UserwiseSalesSummaryBillDetail] as SELECT bill.billcode,(isnull(sum(billdtl.totamt),0) + isnull(bill.prclamt,0) ) as Basic,isnull(totalamt,0) as Total,   
CASE WHEN isnull(sum(cashamt),0) > 0 THEN 'Csh' WHEN isnull(sum(creditpay),0) > 0 THEN 'Cre'  WHEN isnull(sum(giftcardPay),0) > 0 THEN 'Gft' WHEN isnull(sum(walletamt),0) > 0 THEN 'Wlt' END as PayType,  
isnull(sum(sgstamt)+sum(cgstamt)+sum(igstamt),0) as Tax,mstUser.userId as userid,billdate     
FROM bill  inner join billdtl on bill.billid = billdtl.billid 
inner join mstUser on bill.addedby=mstUser.userid  
Group By userid,bill.billid,billdate,totalamt,billcode ,prclamt





GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (1, 1, N'Sales Order', N'Customer', N'Thanks for choosing $OrganizationName$ . We will deliver your order shortly. Order No: $OrderNo$. Amount: $Amount$', N'', N'', N'Sales Order', 7, 1, N'1', 0, CAST(0x0000AACE000B42D0 AS DateTime), 0, CAST(0x0000AB350180CBD0 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (2, 2, N'Sales Invoice', N'Customer', N'Thank you for shoping for us. your bill no : $DocNo$. Total Bill Amount :  $Amount$.Future assistance 
$OrganizationName$,$OrganizationPhone$', N'', N'', N'Sales Invoice', 1, 1, N'1', 0, CAST(0x0000AB3300023280 AS DateTime), 0, CAST(0x0000AB39000AFC80 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (3, 3, N'Sales Order', N'Order Status', N'Your Order is $OrderStatus$ With Order No : $OrderNo$.', N'', N'', N'Sales Order', 1, 0, N'1', 0, CAST(0x0000AB38017C66D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (4, 4, N'OTP SMS', N'OTP SMS', N'$otpno$ is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', NULL, NULL, N'OTP SMS', 1, 0, N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (5, 5, N'Forgot Password SMS', N'Forgot Password SMS', N'Please use $forgotpwd$ this password for login.', NULL, NULL, N'Forgot Password SMS', 1, 0, N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[emailsetting] ([Id], [uid1], [portalname], [smtpname], [portno], [emailid], [password], [receiveemail], [uid], [tflg], [level], [addedby], [addedatetime], [EnableSSL], [updatedby], [updateddatetime]) VALUES (1, 1, N'GMAIL', N'smtp.gmail.com', N'587', N'sumit@gmail.com', N'VDqL2QHwiM+2uDb+h4p7jQ==', N'', 7, 1, N'1', 1, CAST(0x0000AAFD017352FC AS DateTime), N'false', 1, CAST(0x0000AB390006AB6C AS DateTime))
GO
INSERT [dbo].[mdelboy] ([delid], [uid1], [delbyName], [userid], [Isactive], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [address1], [address2], [TelNo], [MobileNo], [Email], [PinCode], [CityId], [StateId], [CountryId], [deviceid]) VALUES (11, 1, N'rajan', 3, 1, 1, 0, N'1', 1, CAST(0x0000AB2F00076A70 AS DateTime), 1, CAST(0x0000AB2F00076A70 AS DateTime), NULL, NULL, NULL, N'A-123', N'', N'', N'9427949390', N'rajan@gmail.com', N'', 72, 71, 71, N'cUfjKbnWCIE:APA91bEYCYx-dA_ZhqaLfz7Sus2rr8zgPJ40tWfoQuCxuluI4UZOzIHsic4krxZ3VCDajIokwLefbps2QVfUtyqa2H3jAwRTmorFaUJ1qrHmlTSw9diubL29KBok1sg-K2LZy3q29Fj4')
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (11, 1, 11, N'1', N'Beauty n Greens', 11, N'Beauty n Greens', N'CS1 maint: BOT: original-url status unknown · CS1: Julian–Gregorian uncertainty · Articles with short description', N'Detoxification is a type of alternative-medicine treatment which aims to rid the body of ... Detoxification and body cleansing products and diets have been criticized for their unsound', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_11.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00016557 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (11, 1, N'1', N'General Juice', 11, 0, 1, 1, N'1', 1, CAST(0x0000AB7D0186EA38 AS DateTime), 1, CAST(0x0000AB9B01217400 AS DateTime), NULL, NULL, NULL, N'All', N'itemcat_11.JPEG', 1, N'General')
GO
INSERT [dbo].[mshipingchrg] ([scid], [uid1], [scdate], [sctime], [scharges], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (71, 1, CAST(0x0000AAD300000000 AS DateTime), CAST(0x000000000014DFC0 AS DateTime), 20, 7, 1, N'1', 1, CAST(0x0000AAD30014DFC0 AS DateTime), 1, CAST(0x0000AB2F0007B0C0 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'ItmStatus', N'Active!InActive')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'BalanceType', N'Opening!Closing')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'SalesType', N'Cash!Cr/Dr Card!GiftCard!Wallet')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'RateType', N'Inclusive!Exclide')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'selfvalue', N'Days!Month!Year')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'Seprator', N'-!\\!/')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'SMSFor', N'Customer')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'schmtype', N'Normal')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'schmop', N'%!-')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'yn', N'yes!No')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'cattype', N'All!Men!Women!Kids!Other')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'desctype', N'Item!Cancel Order!Other')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'packdays', N'10!15!20!30!35!40!45!50!55!60!65!70!75!80!85!90!95!100!105!110!115!120!125!130!135!140!145!150!155!160!165!170!175!180')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'packdays', N'10!15!20!30!35!40!45!50!55!60!65!70!75!80!85!90!95!100!105!110!115!120!125!130!135!140!145!150!155!160!165!170!175!180')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'transtype', N'Payment!Receive')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'validfor', N'!Not Valid For Pregnant Women!Not Valid For Child')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'offervalid', N'Items!Total Amount')
GO
INSERT [dbo].[mstcompany] ([compid], [compcode], [compname], [compaddress1], [compaddress2], [compaddress3], [compcity], [compzipcode], [compstate], [compcountry], [compphone], [compmobile], [compemailid], [compwebsite], [compcstno], [comppanno], [compvatno], [compgstno], [compgststno], [compfssino], [compnservice], [compdlno1], [compdlno2], [branchcode], [branchcodeid], [serialkey], [custid], [adduserid], [adddatetime], [edituserid], [editdatetime], [syncflg], [glid], [ownermobile]) VALUES (1, N'SRAP', N'S R AMUL PARLOUR', N'B-204, SHREENAND RESIDENCY', N'NR. SADGURU HOMES', N'NEW MANINAGAR', N'AHMEDABAD', 380009, N'GUJARAT', NULL, N'8000838147', NULL, N'INFO@SRAP.COM', N'SRAP.COM', N'1234567890', N'1234567890', N'1234567890', N'1234567890', 24, N'', N'', N'1234567890', N'1234567890', N'HO1', 100000, N'P98W-X7FE-CAGI-Z31R', N'CUST0005', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (1, N'Invalid Login', N'Invalid Login Details Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (2, N'Inactive Account', N'Your account is inactive.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (3, N'Fetch Data', N'Fetch data successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (4, N'No Record', N'Record Not Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (5, N'Save Data', N'Save data successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (6, N'Delete Data', N'Delete data successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (7, N'Update Record', N'Record update successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (8, N'LoginSuccess', N'Login successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (9, N'child record', N'Child record found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (10, N'Invalid Authentication', N'Invalid Authentication Key Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (11, N'Valid Authentication', N'Authentication Key Validate Successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (12, N'No Authentication', N'Authentication Key Not Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (13, N'Signup Successfully', N'Signup Successfully')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (14, N'User Exist', N'Mobile number already registered.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (15, N'Invalid OTP', N'Invalid OTP Details Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'Record Exists', N'Record Already Exists')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (19, N'Log Out', N'Log Out Successfully')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'No Shift', N'No shifts available, please change the date or try again later.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'Cancel Record', N'Record cancel successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (16, N'OTP Sent', N'OTP Send successfully')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (17, N'Email Not Exist', N'Email id does not exists')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'Email Exist', N'This email address is already registered!')
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (1, N'Item Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (2, N'Item Group Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (3, N'Item Unit Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (4, N'Country Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (5, N'State Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (6, N'City Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (7, N'Customer Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (8, N'Counter Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (9, N'Price List Entry', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (10, N'Scheme Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (11, N'Daily Scheme Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (12, N'Delivery Person Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (13, N'Description Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (14, N'Wallet Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (15, N'Sales', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (16, N'Sales Order', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (17, N'Opening / closing Cash Entry', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (18, N'Dailly Transction Entry', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (19, N'Sales Type Wise Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (20, N'Item Wise Sales Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (21, N'User Wise Sales Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (22, N'Group Wise Sales Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (23, N'User Wise Cash Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (24, N'Sales Order Register', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (25, N'Sales Register', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (26, N'PriceList Report', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (27, N'Order Approval', N'Utility', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (29, N'Package Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (30, N'Package Item Link Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstsmssubtype] ([Id], [SMSTypeId], [SMSSubType]) VALUES (1, 1, N'Sales Order')
GO
INSERT [dbo].[mstsmssubtype] ([Id], [SMSTypeId], [SMSSubType]) VALUES (2, 2, N'Sales Invoice')
GO
INSERT [dbo].[mstsmssubtype] ([Id], [SMSTypeId], [SMSSubType]) VALUES (3, 3, N'Feedback SMS')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (1, N'Sales Order')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (2, N'Sales Invoice')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (3, N'Other')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (4, N'Custom')
GO
INSERT [dbo].[mstuser] ([userid], [uid1], [username], [userpassword], [emailid], [ntlock], [userstatus], [MobileNo], [DefUser], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [macid]) VALUES (1, 1, N'admin', N'admin', NULL, N'1', NULL, NULL, NULL, NULL, N'1', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SUMIT')
GO
INSERT [dbo].[mstuser] ([userid], [uid1], [username], [userpassword], [emailid], [ntlock], [userstatus], [MobileNo], [DefUser], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [macid]) VALUES (2, 0, N'sumit', N'123', N'abc@yopmail.com', NULL, 0, 1234567891, NULL, 0, N'1', 0, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAF00163F500 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[mstuser] ([userid], [uid1], [username], [userpassword], [emailid], [ntlock], [userstatus], [MobileNo], [DefUser], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [macid]) VALUES (3, 0, N'rajan', N'123', N'123@YOPMAIL.COM', NULL, 0, 1234567890, NULL, 7, N'1', 0, 1, CAST(0x0000AB070172C9E0 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (1, 2, 1, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (2, 2, 2, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (3, 2, 3, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (4, 2, 4, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (5, 2, 5, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (6, 2, 6, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (7, 2, 7, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (8, 2, 8, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (9, 2, 9, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (10, 2, 10, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (11, 2, 11, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (12, 2, 12, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (13, 2, 13, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (14, 2, 14, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (15, 2, 15, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (16, 2, 16, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (17, 2, 17, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (18, 2, 18, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (19, 2, 19, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (20, 2, 20, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (21, 2, 21, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (22, 2, 22, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (23, 2, 23, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (24, 2, 24, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (25, 2, 25, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (26, 2, 26, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (27, 2, 27, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (28, 1, 1, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (29, 1, 2, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (30, 1, 3, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (31, 1, 4, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (32, 1, 5, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (33, 1, 6, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (34, 1, 7, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (35, 1, 8, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (36, 1, 9, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (37, 1, 10, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (38, 1, 11, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (39, 1, 12, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (40, 1, 13, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (41, 1, 14, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (42, 1, 15, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (43, 1, 16, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (44, 1, 17, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (45, 1, 18, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (46, 1, 19, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (47, 1, 20, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (48, 1, 21, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (49, 1, 22, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (50, 1, 23, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (51, 1, 24, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (52, 1, 25, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (53, 1, 26, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (54, 1, 27, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (56, 1, 29, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000000000000000 AS DateTime), 0, 0, CAST(0x0000000000000000 AS DateTime))
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (56, 1, 29, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000000000000000 AS DateTime), 0, 0, CAST(0x0000000000000000 AS DateTime))
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (57, 1, 30, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000000000000000 AS DateTime), 0, 0, CAST(0x0000000000000000 AS DateTime))
GO
INSERT [dbo].[munit] ([unitid], [uid1], [unitcode], [unitname], [NoofDecimal], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (11, 1, N'1', N'PCS', 1, 7, N'1', 0, 1, CAST(0x0000AAC201897017 AS DateTime), 1, CAST(0x0000AAC3000021D4 AS DateTime), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[purchasehistory] ON 

GO
INSERT [dbo].[purchasehistory] ([id], [purchasetype], [purchasedate], [purchasetime], [purchaseval], [amount], [orderid], [custid], [paymentby], [refno], [adduserid], [add_date_time]) VALUES (7, N'Dairy Products', CAST(0x0000ADC700000000 AS DateTime), CAST(0x0000ADC7013A7BD0 AS DateTime), 1, 1, 11, N'1', N'Online', N'pay_IBhRjdOuYcHufd', 1, CAST(0x0000ADC80009A6A1 AS DateTime))
GO
INSERT [dbo].[purchasehistory] ([id], [purchasetype], [purchasedate], [purchasetime], [purchaseval], [amount], [orderid], [custid], [paymentby], [refno], [adduserid], [add_date_time]) VALUES (8, N'Dairy Products', CAST(0x0000ADC900000000 AS DateTime), CAST(0x0000ADC9011753D0 AS DateTime), 1, 1, 12, N'1', N'Online', N'pay_ICSLEzQekCZEV0', 1, CAST(0x0000ADC9017249D4 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[purchasehistory] OFF
GO
INSERT [dbo].[registerinfo] ([infoid], [orgname], [owname], [add1], [add2], [city], [state], [country], [pin], [telno], [email], [mobileno], [level], [curname], [cursymbol], [mlvlid], [lvltype], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [uid], [panno], [gstin], [fssino], [website]) VALUES (1, N'infinity bit', N'sumit', N'a-402 , gp', N'navrangpura', N'ahmedabad', N'gujarat', N'india', N'380009', N'9427949391', N'sumti@yopmail.com', N'94279493941', N'1', NULL, NULL, NULL, NULL, 0, 0, CAST(0x0000AABE001A976C AS DateTime), 1, CAST(0x0000AAD000324330 AS DateTime), 7, N'BGEPP1849DDFGHH', N'24ASDSAD', N'fsdfsd', N'www.google.com')
GO
INSERT [dbo].[smssetting] ([ID], [SMSSENDTYPE], [SMSUSERNAME], [SMSUSERPASSWORD], [SMSSENDERID], [SmsApi], [Feed], [SmsApi1], [AppSmsType], [ProUserName], [ProPassword], [ProFeed], [SMSAPIKEY], [ProSMSAPIKEY], [ProSMSApi], [TCountryCode], [PCountryCode], [SMSBill], [SMSOrder], [ColSMS], [BillEdit], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (1, NULL, N'kamal.raval90@yahoo.com', N'', NULL, 0, N'FOJUIC', NULL, NULL, N'kamal.raval90@yahoo.com', N'', N'FOJUIC', N'293068A9LzChkz5d73e5ea', N'293068A9LzChkz5d73e5ea', 0, 91, 91, 0, 0, 0, 0, 7, 1, N'1', 0, CAST(0x0000AACE00065823 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (11, 1, 11, 1, 1, CAST(0x0000ADC800000000 AS DateTime), CAST(0x0000ADC800A78AA0 AS DateTime), 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, N'', 0, 1, 0, N'1', 1, CAST(0x0000ADC80009A6A0 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (12, 2, 11, 1, 1, CAST(0x0000ADC800000000 AS DateTime), CAST(0x0000ADC800A78AA0 AS DateTime), 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, N'', 0, 1, 0, N'1', 1, CAST(0x0000ADC80009A6A0 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (13, 3, 12, 1, 1, CAST(0x0000ADC800000000 AS DateTime), CAST(0x0000ADC800A78AA0 AS DateTime), 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, N'', 0, 1, 0, N'1', 1, CAST(0x0000ADC901724121 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (14, 4, 12, 1, 1, CAST(0x0000ADC800000000 AS DateTime), CAST(0x0000ADC800A78AA0 AS DateTime), 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, N'', 0, 1, 0, N'1', 1, CAST(0x0000ADC901724595 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode], [emailid]) VALUES (11, 1, 1, N'sumit', N'9427949390', N'11', 11, CAST(0x0000ADC700000000 AS DateTime), CAST(0x0000ADC700A78AA0 AS DateTime), 0, 0, 0, 0, 0, 1, 0, CAST(0x0000ADC700000000 AS DateTime), 0, 0, 0, 0, N'pay_IBhRjdOuYcHufd', CAST(0x0000ADC700000000 AS DateTime), 0, N'test entry check', 0, 0, 1, 0, N'1', 1, CAST(0x0000ADC80009A69F AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', 0, 0, N'', N'', N'', N'sumitpitroda@gmail.com')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode], [emailid]) VALUES (12, 2, 1, N'sumit', N'9427949390', N'12', 12, CAST(0x0000ADC700000000 AS DateTime), CAST(0x0000ADC700A78AA0 AS DateTime), 0, 0, 0, 0, 0, 1, 0, CAST(0x0000ADC700000000 AS DateTime), 0, 0, 0, 0, N'pay_ICSLEzQekCZEV0', CAST(0x0000ADC700000000 AS DateTime), 0, N'test entry check', 0, 0, 1, 0, N'1', 1, CAST(0x0000ADC901723DE7 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', 0, 0, N'', N'', N'', N'sumitpitroda@gmail.com')
GO
SET IDENTITY_INSERT [dbo].[userAddress] ON 

GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (97, 91, N'home', N'n', N'302', N'green', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'9427949390', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (1104, 91, N'home', N'qwr', N'ggh', N'vjjkk', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'3214569870', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (2104, 91, N'Office', N'qe', N'er', N'fvvv', N'Mehsana', N'Gujarat', N'India', N'384001', 1, N'1234567890', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (93, 88, N'home', N'34', N'anandnagar', N'prahaladnagr', N'Mehsana', N'Gujarat', N'India', N'384002', 0, N'8879200190', N'barkha shah')
GO
SET IDENTITY_INSERT [dbo].[userAddress] OFF
GO
SET IDENTITY_INSERT [dbo].[UserRegistration] ON 

GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'barkha', NULL, NULL, NULL, N'8879200190', NULL, N'shahbarkha91@gmail.com', N'OCVIVDJC', NULL, NULL, N'Ahmedabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 88, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'sumit', NULL, NULL, NULL, N'9427949390', NULL, N'sumitpitroda@gmail.com', N'HW1R192I', NULL, NULL, N'ahmedabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 91, N'Active')
GO
SET IDENTITY_INSERT [dbo].[UserRegistration] OFF
GO
