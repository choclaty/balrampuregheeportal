﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrderWebPortal.Models;
using OrderWebPortal.DataAccessLayer;
using System.Globalization;

namespace OrderWebPortal.Controllers
{
    [CheckSession]
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult salesSummary()
        {
            //Report report = new Report();

            //List<salesSummaryEntity> Data = report.GetsalesSummary();
            return View();
        }
        public JsonResult sales(string StartDate, String EndDate)
        {
            Report report = new Report();

            DateTime _StartDate = DateTime.Parse(StartDate, CultureInfo.InvariantCulture);
            DateTime _EndDate = DateTime.Parse(EndDate, CultureInfo.InvariantCulture);

            List<salesSummaryEntity> Data = report.GetsalesSummary(_StartDate.ToString("yyyy-MM-dd"), _EndDate.ToString("yyyy-MM-dd"));
            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OrderSummary()
        {
        //    Report report = new Report();
        //    List<OrderSummaryEntity> Data = report.GetOrderSummary();
            return View();
        }

        public JsonResult Order(string StartDate, String EndDate)
        {
            Report report = new Report();

            DateTime _StartDate = DateTime.Parse(StartDate, CultureInfo.InvariantCulture);
            DateTime _EndDate = DateTime.Parse(EndDate, CultureInfo.InvariantCulture);

            List<OrderSummaryEntity> Data = report.GetOrderSummary(_StartDate.ToString("yyyy-MM-dd"), _EndDate.ToString("yyyy-MM-dd"));
            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeliverySummary()
        {
            //Report report = new Report();

            //List<DeliverySummaryEntity> Data = report.GetDeliverySummary();
            return View();
        }

        public JsonResult Delivery(string StartDate, String EndDate)
        {
            Report report = new Report();

            DateTime _StartDate = DateTime.Parse(StartDate, CultureInfo.InvariantCulture);
            DateTime _EndDate = DateTime.Parse(EndDate, CultureInfo.InvariantCulture);

            List<DeliverySummaryEntity> Data = report.GetDeliverySummary(_StartDate.ToString("yyyy-MM-dd"), _EndDate.ToString("yyyy-MM-dd"));
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ItemWiseOrder()
        {
            //Report report = new Report();

            //List<DeliverySummaryEntity> Data = report.GetDeliverySummary();
            return View();
        }

        public JsonResult ItemOrder(string StartDate, String EndDate)
        {
            Report report = new Report();

            DateTime _StartDate = DateTime.Parse(StartDate, CultureInfo.InvariantCulture);
            DateTime _EndDate = DateTime.Parse(EndDate, CultureInfo.InvariantCulture);

            List<ItemWiseOrderEntity> Data = report.GetItemWiseOrder(_StartDate.ToString("yyyy-MM-dd"), _EndDate.ToString("yyyy-MM-dd"));
            return Json(Data, JsonRequestBehavior.AllowGet);
        }
    }
}