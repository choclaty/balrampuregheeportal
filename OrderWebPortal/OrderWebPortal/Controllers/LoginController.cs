﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrderWebPortal.Models;
using OrderWebPortal.DataAccessLayer;

namespace OrderWebPortal.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login(int? error)
        {
            ViewBag.Error = error;
            return View();
        }
        [HttpPost]
        public ActionResult LoginProcess(Login login)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Login lg = new Login();

                    LoginClass objlogin = new LoginClass();
                    lg = objlogin.GetLoginData(login);

                    if (lg.userid != null)
                    {
                        Session["UserID"] = lg.userid;
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Login", "Login", new { error = 1 });
                    }
                }
            }
            catch (Exception ex)
            {


            }
            return View(login);
        }

        public ActionResult Signout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login", "Login");
        }
    }
}