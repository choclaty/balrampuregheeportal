﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrderWebPortal.DataAccessLayer;
using OrderWebPortal.Models;

namespace OrderWebPortal.Controllers
{
    [CheckSession]
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult CompletedOrder()
        {
            //Order order = new Order();

            //List<CompletedOrderEntity> Data = order.GetCompleteOrder();
            return View();
        }
        public JsonResult CompleteOrder(string StartDate, String EndDate)
        {
            Order order = new Order();

            DateTime _StartDate = DateTime.Parse(StartDate, CultureInfo.InvariantCulture);
            DateTime _EndDate = DateTime.Parse(EndDate, CultureInfo.InvariantCulture);

            List<CompletedOrderEntity> Data = order.GetCompleteOrder(_StartDate.ToString("yyyy-MM-dd"), _EndDate.ToString("yyyy-MM-dd"));
            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult NewOrder()
        {
            Order order = new Order();
            List<OrderEntity> Data = order.GetNewOrder();
            return View(Data);
        }
        //public JsonResult Order(string StartDate, String EndDate)
        //{
        //    Order order = new Order();

        //    DateTime _StartDate = DateTime.Parse(StartDate, CultureInfo.InvariantCulture);
        //    DateTime _EndDate = DateTime.Parse(EndDate, CultureInfo.InvariantCulture);

        //    List<OrderEntity> Data = order.GetNewOrder(_StartDate.ToString("yyyy-MM-dd"), _EndDate.ToString("yyyy-MM-dd"));
        //    return Json(Data, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult Orderapproval(string SoID)
        {
            Order order = new Order();
            List<OrderDetailsEntity> OrderList = new List<OrderDetailsEntity>();
            DataSet DS = order.ApprovalData(SoID);
            if (DS.Tables.Count > 0)
            {
                if (DS.Tables[0].Rows.Count > 0)
                {
                    ViewBag.SoID = Convert.ToString(DS.Tables[0].Rows[0]["Soid"]);
                    ViewBag.socode = Convert.ToString(DS.Tables[0].Rows[0]["socode"]);
                    ViewBag.CustomerName = Convert.ToString(DS.Tables[0].Rows[0]["CustomerName"]);
                    ViewBag.deliverydate = Convert.ToString(DS.Tables[0].Rows[0]["deliverydate"]);
                    ViewBag.address = Convert.ToString(DS.Tables[0].Rows[0]["address"]);
                    ViewBag.city = Convert.ToString(DS.Tables[0].Rows[0]["city"]);
                    ViewBag.state = Convert.ToString(DS.Tables[0].Rows[0]["state"]);
                    ViewBag.referenceno = Convert.ToString(DS.Tables[0].Rows[0]["referenceno"]);
                }
                if (DS.Tables[1].Rows.Count > 0)
                {
                    
                    for (int i = 0; i < DS.Tables[1].Rows.Count; i++)
                    {
                        OrderDetailsEntity Details = new OrderDetailsEntity();

                        Details.itemname = Convert.ToString(DS.Tables[1].Rows[i]["itemname"]);

                        Details.itemcatname = Convert.ToString(DS.Tables[1].Rows[i]["itemcatname"]);
                        Details.unitname = Convert.ToString(DS.Tables[1].Rows[i]["unitname"]);
                        Details.qty = Convert.ToString(DS.Tables[1].Rows[i]["qty"]);
                        Details.netrate = Convert.ToString(DS.Tables[1].Rows[i]["netrate"]);
                        Details.totamt = Convert.ToString(DS.Tables[1].Rows[i]["totamt"]);

                        OrderList.Add(Details);
                    }
                }
            }
            DataTable DT1 = order.GetDelPerRecord();
            ViewBag.DelPerRecord = BindDropDown(DT1);
            return View(OrderList);
        }

        public ActionResult ApproveOrderapproval(string SoID)
        {
            Order order = new Order();
            ApprovedOrderEntity apord = new ApprovedOrderEntity();
            List<OrderDetailsEntity> OrderList = new List<OrderDetailsEntity>();
            DataSet DS = order.ApprovalData(SoID);
            if (DS.Tables.Count > 0)
            {
                if (DS.Tables[0].Rows.Count > 0)
                {
                    ViewBag.SoID = Convert.ToString(DS.Tables[0].Rows[0]["Soid"]);
                    ViewBag.socode = Convert.ToString(DS.Tables[0].Rows[0]["socode"]);
                    ViewBag.CustomerName = Convert.ToString(DS.Tables[0].Rows[0]["CustomerName"]);
                    ViewBag.deliverydate = Convert.ToString(DS.Tables[0].Rows[0]["deliverydate"]);
                    ViewBag.address = Convert.ToString(DS.Tables[0].Rows[0]["address"]);
                    ViewBag.city = Convert.ToString(DS.Tables[0].Rows[0]["city"]);
                    ViewBag.state = Convert.ToString(DS.Tables[0].Rows[0]["state"]);
                    ViewBag.referenceno = Convert.ToString(DS.Tables[0].Rows[0]["referenceno"]);

                    apord.Soid = Convert.ToString(DS.Tables[0].Rows[0]["Soid"]);
                    apord.socode = Convert.ToString(DS.Tables[0].Rows[0]["socode"]);
                    apord.CustomerName = Convert.ToString(DS.Tables[0].Rows[0]["CustomerName"]);
                    apord.deliverydate = Convert.ToString(DS.Tables[0].Rows[0]["deliverydate"]);
                    apord.address = Convert.ToString(DS.Tables[0].Rows[0]["address"]);
                    apord.city = Convert.ToString(DS.Tables[0].Rows[0]["city"]);
                    apord.state = Convert.ToString(DS.Tables[0].Rows[0]["state"]);
                    apord.referenceno = Convert.ToString(DS.Tables[0].Rows[0]["referenceno"]);

                }
                if (DS.Tables[1].Rows.Count > 0)
                {

                    for (int i = 0; i < DS.Tables[1].Rows.Count; i++)
                    {
                        OrderDetailsEntity Details = new OrderDetailsEntity();

                        Details.itemname = Convert.ToString(DS.Tables[1].Rows[i]["itemname"]);

                        Details.itemcatname = Convert.ToString(DS.Tables[1].Rows[i]["itemcatname"]);
                        Details.unitname = Convert.ToString(DS.Tables[1].Rows[i]["unitname"]);
                        Details.qty = Convert.ToString(DS.Tables[1].Rows[i]["qty"]);
                        Details.netrate = Convert.ToString(DS.Tables[1].Rows[i]["netrate"]);
                        Details.totamt = Convert.ToString(DS.Tables[1].Rows[i]["totamt"]);

                        OrderList.Add(Details);
                    }
                    apord.orderDetailsEntity = OrderList;
                    ViewBag.orderDetailsEntity = OrderList;
                }
            }
            DataTable DT1 = order.GetDelPerRecord();
            ViewBag.DelPerRecord = BindDropDown(DT1);
            return View(OrderList);
        }

        public ActionResult ReadyForShipOrderapproval(string SoID)
        {
            Order order = new Order();
            ApprovedOrderEntity apord = new ApprovedOrderEntity();
            List<OrderDetailsEntity> OrderList = new List<OrderDetailsEntity>();
            DataSet DS = order.ApprovalData(SoID);
            if (DS.Tables.Count > 0)
            {
                if (DS.Tables[0].Rows.Count > 0)
                {
                    ViewBag.SoID = Convert.ToString(DS.Tables[0].Rows[0]["Soid"]);
                    ViewBag.socode = Convert.ToString(DS.Tables[0].Rows[0]["socode"]);
                    ViewBag.CustomerName = Convert.ToString(DS.Tables[0].Rows[0]["CustomerName"]);
                    ViewBag.deliverydate = Convert.ToString(DS.Tables[0].Rows[0]["deliverydate"]);
                    ViewBag.address = Convert.ToString(DS.Tables[0].Rows[0]["address"]);
                    ViewBag.city = Convert.ToString(DS.Tables[0].Rows[0]["city"]);
                    ViewBag.state = Convert.ToString(DS.Tables[0].Rows[0]["state"]);
                    ViewBag.referenceno = Convert.ToString(DS.Tables[0].Rows[0]["referenceno"]);

                    apord.Soid = Convert.ToString(DS.Tables[0].Rows[0]["Soid"]);
                    apord.socode = Convert.ToString(DS.Tables[0].Rows[0]["socode"]);
                    apord.CustomerName = Convert.ToString(DS.Tables[0].Rows[0]["CustomerName"]);
                    apord.deliverydate = Convert.ToString(DS.Tables[0].Rows[0]["deliverydate"]);
                    apord.address = Convert.ToString(DS.Tables[0].Rows[0]["address"]);
                    apord.city = Convert.ToString(DS.Tables[0].Rows[0]["city"]);
                    apord.state = Convert.ToString(DS.Tables[0].Rows[0]["state"]);
                    apord.referenceno = Convert.ToString(DS.Tables[0].Rows[0]["referenceno"]);

                }
                if (DS.Tables[1].Rows.Count > 0)
                {

                    for (int i = 0; i < DS.Tables[1].Rows.Count; i++)
                    {
                        OrderDetailsEntity Details = new OrderDetailsEntity();

                        Details.itemname = Convert.ToString(DS.Tables[1].Rows[i]["itemname"]);

                        Details.itemcatname = Convert.ToString(DS.Tables[1].Rows[i]["itemcatname"]);
                        Details.unitname = Convert.ToString(DS.Tables[1].Rows[i]["unitname"]);
                        Details.qty = Convert.ToString(DS.Tables[1].Rows[i]["qty"]);
                        Details.netrate = Convert.ToString(DS.Tables[1].Rows[i]["netrate"]);
                        Details.totamt = Convert.ToString(DS.Tables[1].Rows[i]["totamt"]);

                        OrderList.Add(Details);
                    }
                    apord.orderDetailsEntity = OrderList;
                    ViewBag.orderDetailsEntity = OrderList;
                }
            }
            DataTable DT1 = order.GetDelPerRecord();
            ViewBag.DelPerRecord = BindDropDown(DT1);
            return View(OrderList);
        }

        public ActionResult ApprovedOrder()
        {
            Order order = new Order();
            List<OrderEntity> Data = order.GetApprovedOrder();
            return View(Data);
        }

        public ActionResult ReadyForShipOrder()
        {
            Order order = new Order();
            List<ReadyOrderEntity> Data = order.GetReadyForShipOrder();
            return View(Data);
        }

        public ActionResult UserRegistrationList()
        {
            Order order = new Order();
            List<UserRegistrationList> Data = order.GetUserRegistrationList();
            return View(Data);
        }
        public ActionResult SaveOrderStatusapproval(string OrderStatus, string couriercomp, string courierrefno, string SoID)
        {
            try
            {
                Order order = new Order();
                string UserID = Convert.ToString(Session["UserID"]);
                order.SaveOrderapproval(OrderStatus, "", SoID, UserID,couriercomp,courierrefno);

                return RedirectToAction("ApprovedOrder", "Order");
            }
            catch (Exception ex)
            {               
                return RedirectToAction("ApprovedOrder", "Order", new { SoID = SoID });
            }
        }

        public ActionResult SaveReadyForShipapproval(string OrderStatus, string SoID)
        {
            try
            {
                Order order = new Order();
                string UserID = Convert.ToString(Session["UserID"]);
                order.SaveOrderapproval(OrderStatus, "", SoID, UserID);

                return RedirectToAction("ReadyForShipOrder", "Order");
            }
            catch (Exception ex)
            {
                return RedirectToAction("ReadyForShipOrder", "Order", new { SoID = SoID });
            }
        }

        public ActionResult SaveOrderapproval(string OrderStatus,string DelPerID,string SoID)
        {
            try
            {
                Order order = new Order();
                string UserID = Convert.ToString(Session["UserID"]);
                order.SaveOrderapproval(OrderStatus,DelPerID,SoID,UserID);

                return RedirectToAction("NewOrder", "Order");
            }
            catch(Exception ex)
            {
                return RedirectToAction("Orderapproval", "Order",new { SoID = SoID });
            }
        }

        public static List<SelectListItem> BindDropDown(DataTable dt, string dtValue = "")
        {
            List<SelectListItem> dropdown = new List<SelectListItem>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dropdown.Add(new SelectListItem
                    {
                        Value = Convert.ToString(dt.Rows[i]["ID"]),
                        Text = Convert.ToString(dt.Rows[i]["Name"]),
                        Selected = Convert.ToString(dt.Rows[i]["ID"]) == Convert.ToString(dtValue)
                    });
                }
            }
            return dropdown;
        }

    }
}