﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrderWebPortal.DataAccessLayer;

namespace OrderWebPortal.Controllers
{
    [CheckSession]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Home home = new Home();
            DataSet DS = home.GetDashboard();

            ViewBag.NewOrder = Convert.ToString(DS.Tables[0].Rows[0]["neworder"]);
            ViewBag.Completedorder = Convert.ToString(DS.Tables[0].Rows[0]["Completedorder"]);
            ViewBag.Newuser = Convert.ToString(DS.Tables[0].Rows[0]["Newuser"]);
            ViewBag.Collection = Convert.ToString(DS.Tables[0].Rows[0]["Collection"]);

            return View();
        }

        public class RevenueReport
        {
            public  string month { get; set; }
            public decimal amount{ get; set; }

        }
        public JsonResult SalesChart()
        {
            Home home = new Home();
            DataSet DS = home.GetDashboard();

            List<RevenueReport> RevenueReport = new List<RevenueReport>();

            for(int i=0;i< DS.Tables[1].Rows.Count;i++)
            {
                RevenueReport RR = new RevenueReport();

                RR.month = Convert.ToString(DS.Tables[1].Rows[i][0]);
                RR.amount= Convert.ToDecimal(DS.Tables[1].Rows[i][1]);
                RevenueReport.Add(RR);
            }
            return Json(RevenueReport, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OrderChart()
        {
            Home home = new Home();
            DataSet DS = home.GetDashboard();

            List<RevenueReport> RevenueReport = new List<RevenueReport>();

            for (int i = 0; i < DS.Tables[2].Rows.Count; i++)
            {
                RevenueReport RR = new RevenueReport();

                RR.month = Convert.ToString(DS.Tables[2].Rows[i][0]);
                RR.amount = Convert.ToDecimal(DS.Tables[2].Rows[i][1]);

                RevenueReport.Add(RR);
            }
            return Json(RevenueReport, JsonRequestBehavior.AllowGet);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}