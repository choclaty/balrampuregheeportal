﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OrderWebPortal.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Please Enter UserName")]
        [Display(Name = "Username")]
        public string username { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string userpwd { get; set; }

        public int? userid { get; set; }


    }
}