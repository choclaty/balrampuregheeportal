﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderWebPortal.Models
{
    public class Paymentfield
    {
        public string purchasetype { get; set; }
        public string purchasedate { get; set; }
        public string purchasetime { get; set; }
        public string purchaseval { get; set; }
        public string amount { get; set; }
        public string orderid { get; set; }
        public string paymentby { get; set; }
        public string refno { get; set; }
        public string custid { get; set; }
        public string adduserid { get; set; }
        public string payee { get; set; }
        public string emailid { get; set; }
        public string mobileno { get; set; }
        public string description { get; set; }
    }

    public class sorder
    {
        public string custid { get; set; }
        public string customername { get; set; }
        public string mobileno { get; set; }
        public string emailid { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string taxtype { get; set; }
        public string roffamt { get; set; }
        public string discamt { get; set; }
        public string totamt { get; set; }
        public string advamt { get; set; }
        public string deliverydate { get; set; }
        public string deliverycharge { get; set; }
        public string deliverytype { get; set; }
        public string deliverystatus { get; set; }
        public string referenceno { get; set; }
        public string referencedate { get; set; }
        public string remarks { get; set; }
        public string paymenttype { get; set; }
        public string ordstatus { get; set; }
        public List<sodtl> sodtls { get; set; }
    }

    public class sodtl
    {
        public string soid { get; set; }
        public string itemid { get; set; }
        public string unitid { get; set; }
        public string itmdeldate { get; set; }
        public string itemdeltime { get; set; }
        public string qty { get; set; }
        public string grate { get; set; }
        public string discperc { get; set; }
        public string discamt { get; set; }
        public string netrate { get; set; }
        public string totamt { get; set; }
        public string sgstperc { get; set; }
        public string sgstamt { get; set; }
        public string cgstperc { get; set; }
        public string cgstamt { get; set; }
        public string igstperc { get; set; }
        public string igstamt { get; set; }
    }
}