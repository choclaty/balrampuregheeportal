﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderWebPortal.Models
{
    public class OrderEntity
    {
        public string Soid { get; set; }
        public string socode { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public double TotalAmt { get; set; }
        public string ordstatus { get; set; }
        public string referenceno { get; set; }
    }

    public class ReadyOrderEntity
    {
        public string Soid { get; set; }
        public string socode { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public double TotalAmt { get; set; }
        public string ordstatus { get; set; }
        public string referenceno { get; set; }
        public string shipmentcompany { get; set; }
        public string airbasenumber { get; set; }
    }

    public class CompletedOrderEntity
    {
        public string Soid { get; set; }
        public string socode { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public double TotalAmt { get; set; }
        public string ordstatus { get; set; }
    }

    public class OrderDetailsEntity
    {
        public string itemname { get; set; }
        public string itemcatname { get; set; }
        public string unitname { get; set; }
        public string qty { get; set; }
        public string netrate { get; set; }
        public string totamt { get; set; }
    }

    public class ApprovedOrderEntity
    {
        public string Soid { get; set; }
        public string socode { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public double TotalAmt { get; set; }
        public string ordstatus { get; set; }
        public string referenceno { get; set; }
        public string deliverydate { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public ICollection<OrderDetailsEntity> orderDetailsEntity { get; set; }        
    }

    public class UserRegistrationList
    {
        public long id { get; set; }
        public string customername { get; set; }
        public string contactno { get; set; }
        public string useremail { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
    }
}