﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderWebPortal.Models
{
    public class salesSummaryEntity
    {
        public string BillID { get; set; }
        public string BillCode { get; set; }
        public string BillDate { get; set; }
        public string BillTime { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public double TotalAmt { get; set; }
    }

    public class OrderSummaryEntity
    {
        public string Soid { get; set; }
        public string socode { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public double TotalAmt { get; set; }
        public string ordstatus { get; set; }
    }

    public class DeliverySummaryEntity
    {
        public string Soid { get; set; }
        public string socode { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public double TotalAmt { get; set; }
        public string ordstatus { get; set; }
    }

    public class ItemWiseOrderEntity
    {

        public string socode { get; set; }
        public string sodate { get; set; }
        public string sotime { get; set; }
        public string itemname { get; set; }
        public string itemid { get; set; }
        public string CustomerName { get; set; }
        public string Soid { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string NetAmt { get; set; }
        public string Discamt { get; set; }
        public double TotalAmt { get; set; }
        public string TaxPerc { get; set; }
        public string TaxAmt { get; set; }
        public string totalamount { get; set; }
        public string BillAmt { get; set; }
        public string CustId { get; set; }
        public string ordstatus { get; set; }
    }
}