﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace OrderWebPortal
{
    public class clsencrdecr
    {
        public string Encrypt(string text)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(text);
            return Convert.ToBase64String(plainTextBytes);
        }

        public string Decrypt(string text)
        {
            if (!string.IsNullOrEmpty( text) )
            {
                try
                {
                    var base64EncodedBytes = Convert.FromBase64String(text);
                    return Encoding.UTF8.GetString(base64EncodedBytes);
                }
                catch (Exception ex)
                {
                    return "";
                }
                
            }
            else
            {
                return "";
            }
        }
    }
}