﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net.Mime;
using System.Configuration;

namespace OrderWebPortal
{
    public class sendnotification
    {
        clsencrdecr objenc = new clsencrdecr();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
        public bool sendsmsthrougmsg91(string msgtype, string strMsg, string MobileNo, string UserId, 
            string personName , string dltid)
        {
            bool SendMsg = false;
            int SendFlg = 0;
            string ResponseId = "0", response1 = "", appKey_1 = "", senderid = "", ccode_1 = "";
            long Id = 0, routeid_1 = 0;
            try
            {
                if (PingNetwork())
                {
                    string strgetsetting = "select * from smssetting";
                    DataTable dtget = FillDataTable(strgetsetting);
                    if (dtget.Rows.Count > 0)
                    {
                        if (msgtype.ToLower() == "transctional")
                        {
                            appKey_1 = Convert.ToString(dtget.Rows[0]["smsapikey"]);
                            senderid = Convert.ToString(dtget.Rows[0]["feed"]);
                            ccode_1 = Convert.ToString(dtget.Rows[0]["tcountrycode"]);
                            routeid_1 = 4;
                        }
                        else
                        {
                            appKey_1 = Convert.ToString(dtget.Rows[0]["prosmsapikey"]);
                            senderid = Convert.ToString(dtget.Rows[0]["profeed"]);
                            ccode_1 = Convert.ToString(dtget.Rows[0]["pcountrycode"]);
                            routeid_1 = 1;
                        }
                    }
                    

                    Id = GetMaxValue("SMSHISTORY", "Id");
                    string strSQl = "insert into SMSHISTORY (ID, uid1,MOBILENO,MESSAGE,SENDFLG,MSGTYPE,SmsPersonName,sendDate,sendTime,uid,tflg,level,addedby,addedatetime)values(";
                    strSQl = strSQl + "" + Id + ", " + Id + ",'" + MobileNo + "','" + strMsg.Replace("'", "`") + "','"+ SendFlg + "','" + msgtype + "','" + personName + "','" + DateTime.Now.ToString("dd/MMM/yyyy") + "'," + 
                        "'" + DateTime.Now.ToString("dd/MMM/yyyy HH:mm") + "',1,0,1,"+ UserId +",'" + DateTime.Now.ToString("dd/MMM/yyyy HH:MM") + "')";

                    ExecuteCommand(strSQl);

                    string StrUrl1 = "https://control.msg91.com/api/sendhttp.php?authkey=" + appKey_1 + "&mobiles=" + MobileNo + "&message=" + strMsg + "&sender=" + senderid + "&route=" + routeid_1 + "&country=" + ccode_1 + "&response=json&unicode=1&DLT_TE_ID=" + dltid + "";
                    HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(StrUrl1);
                    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                    string responseString = respStreamReader.ReadToEnd();
                    respStreamReader.Close();
                    myResp.Close();

                    string strrsp1 = Convert.ToString(responseString).Trim();

                    string[] arrMain = strrsp1.Split('|');

                    if (strrsp1.Contains("success"))
                    {
                        SendMsg = true;
                        SendFlg = 1;
                        ResponseId = "SUCESS : " + response1;
                    }
                    else
                    {
                        response1 = "1";
                        SendMsg = false;
                        SendFlg = 0;
                        ResponseId = "ERROR : " + response1;
                    }

                    string strSQl1 = "Update SMSHISTORY Set SendFlg = 1,RESPONSEID = '" + ResponseId + "' Where Id = " + Id;
                    ExecuteCommand(strSQl1);
                }
            }
            catch (Exception ex)
            {
                return SendMsg;
            }
            return SendMsg;
        }

        public bool PingNetwork()
        {
            System.Uri objUrl = new System.Uri("http://www.google.com/");
            System.Net.WebRequest objWebReq;
            objWebReq = System.Net.WebRequest.Create(objUrl);
            System.Net.WebResponse objResp;
            try
            {
                objResp = objWebReq.GetResponse();
                objResp.Close();
                objWebReq = null;
                return true;
            }
            catch (Exception)
            {
                objWebReq = null;
                return false;
            }
        }

        public void SendEmailThroughSendgrid(string RecEmail_1, string EmilCont_1,
           string EmailSub_1, long Id_1, string strfromEmail_id_1, string attachmentFilename_1)
        {
            string strresult = "", Emailid_1 = "", EmilPwd_1 = "", strsmtpname_1 = "", strportno_1 = "", StrSql = "";
            int portno_1 = 0;
            long Id = 0;
            try
            {
                Id = GetMaxValue("emailhistory", "Id");
                string strSQl = "insert into emailhistory (id, senddate,sendtime,emailmessage,sendflg,emailtempname,emailtempid,emailsubject,sendemailid,ErrDescription,uid1,uid,tflg,level,addedby,addedatetime)values(";
                strSQl = strSQl + "" + Id + ",'" + DateTime.Now.ToString("yyyy/MM/dd") + "','" + DateTime.Now.ToString("yyyy/MM/dd HH:mm") + "','"+ EmilCont_1 + "',0,'','','"+ EmailSub_1 +"'," + 
                    "'" + RecEmail_1 + "','',"+ Id + ",1,0,1,1,'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm") + "')";

                ExecuteCommand(strSQl);

                StrSql = "select * from emailsetting ";
                DataTable DtSms = FillDataTable(StrSql);
             
                if (DtSms.Rows.Count > 0)
                {
                    Emailid_1 = Convert.ToString(DtSms.Rows[0]["emailid"]);
                    EmilPwd_1 = objenc.Decrypt(Convert.ToString(DtSms.Rows[0]["password"]));
                    strsmtpname_1 = Convert.ToString(DtSms.Rows[0]["smtpname"]);
                    strportno_1 = Convert.ToString(DtSms.Rows[0]["portno"]);
                }

                if (Convert.ToString(Emailid_1) != "" && Convert.ToString(EmilPwd_1) != "" && Convert.ToString(RecEmail_1) != "")
                {
                    if (strportno_1 == "")
                    {
                        portno_1 = 587;
                    }
                    else
                    {
                        portno_1 = Convert.ToInt16(strportno_1);
                    }


                    if (Convert.ToString(strfromEmail_id_1) == "")
                    {
                        strfromEmail_id_1 = "contact@puredesigheeofindia.co.in";
                    }

                    MailMessage mailMsg = new MailMessage();
                    // To
                    mailMsg.To.Add(new MailAddress(RecEmail_1, RecEmail_1));
                    // From
                    mailMsg.From = new MailAddress(strfromEmail_id_1, EmailSub_1);
                    // Subject and multipart/alternative Body
                    mailMsg.Subject = EmailSub_1;
                    //string text = EmilCont_1;
                    //string html = @"<p>html body</p>";
                    string html = EmilCont_1;
                    //mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
                    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

                    if (attachmentFilename_1 != "")
                        mailMsg.Attachments.Add(new Attachment(attachmentFilename_1));

                    // Init SmtpClient and send
                    //SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                    SmtpClient smtpClient = new SmtpClient(strsmtpname_1, Convert.ToInt32(portno_1));
                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(Emailid_1, EmilPwd_1);
                    smtpClient.Credentials = credentials;

                    smtpClient.Send(mailMsg);

                    strresult = "Update EMAILHISTORY Set SendFlg = 1 Where Id =" + Id_1;
                    bool ret = ExecuteCommand(strresult);
                }

            }
            catch (Exception ex)
            {

            }
        }

        public DataTable FillDataTable(string querystr)
        {
            DataTable dt1 = new DataTable();
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    OpenConnection();
                }
                using (SqlCommand cmd = new SqlCommand(querystr, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dt1.Load(reader);
                    }
                }
                CloseConnection();

                return dt1;
            }
            catch (Exception ex)
            {
                CloseConnection();
                return dt1;
            }
        }

        private bool OpenConnection()
        {
            try
            {
                con.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Execute Query
        public bool ExecuteCommand(string querystr)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    OpenConnection();
                }


                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = querystr;
                cmd.Connection = con;
                cmd.ExecuteNonQuery();

                CloseConnection();
                return true;
            }
            catch (Exception ex)
            {
                CloseConnection();
                return false;
            }
        }

        //Get Max Value from Table 
        public long GetMaxValue(string Tblnm1, string fieldnm1)
        {
            long ret = 0;
            string str1 = "";
            object no1;

            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    OpenConnection();
                }

                str1 = "";
                str1 = "Select IsNull(Max(" + fieldnm1 + "), 0) + 1 From " + Tblnm1;

                using (SqlCommand cmd = new SqlCommand(str1, con))
                {
                    no1 = cmd.ExecuteScalar();
                }

                ret = Convert.ToInt64(no1);

                return ret;
            }
            catch (Exception ex)
            {
                return ret;
            }
        }

    }
}