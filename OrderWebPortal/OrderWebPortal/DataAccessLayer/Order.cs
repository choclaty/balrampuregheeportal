﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using OrderWebPortal.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

namespace OrderWebPortal.DataAccessLayer
{
    public class Order
    {
        sendnotification objnoti = new sendnotification();
        public List<OrderEntity> GetNewOrder()
        {
            List<OrderEntity> OrderList = new List<OrderEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_NewOrder", con);
                // cmd.Parameters.AddWithValue("@p_startdate", StartDate);
                //cmd.Parameters.AddWithValue("@p_enddate", EndDate);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        OrderEntity NewOrderdata = new OrderEntity();

                        NewOrderdata.Soid = Convert.ToString(DT.Rows[i]["Soid"]);
                        NewOrderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        NewOrderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        NewOrderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        NewOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        NewOrderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        NewOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);
                        NewOrderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);
                        NewOrderdata.referenceno = Convert.ToString(DT.Rows[i]["referenceno"]);

                        OrderList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return OrderList;
        }

        public List<OrderEntity> GetApprovedOrder()
        {
            List<OrderEntity> OrderList = new List<OrderEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_ApprovedOrder", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        OrderEntity NewOrderdata = new OrderEntity();

                        NewOrderdata.Soid = Convert.ToString(DT.Rows[i]["Soid"]);
                        NewOrderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        NewOrderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        NewOrderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        NewOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        NewOrderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        NewOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);
                        NewOrderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);
                        NewOrderdata.referenceno = Convert.ToString(DT.Rows[i]["referenceno"]);
                        OrderList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return OrderList;
        }

        public List<ReadyOrderEntity> GetReadyForShipOrder()
        {
            List<ReadyOrderEntity> OrderList = new List<ReadyOrderEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_ReadyforShipOrder", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ReadyOrderEntity NewOrderdata = new ReadyOrderEntity();

                        NewOrderdata.Soid = Convert.ToString(DT.Rows[i]["Soid"]);
                        NewOrderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        NewOrderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        NewOrderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        NewOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        NewOrderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        NewOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);
                        NewOrderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);
                        NewOrderdata.referenceno = Convert.ToString(DT.Rows[i]["referenceno"]);

                        NewOrderdata.shipmentcompany = Convert.ToString(DT.Rows[i]["shipmentcompany"]);
                        NewOrderdata.airbasenumber = Convert.ToString(DT.Rows[i]["airbasenumber"]);

                        OrderList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return OrderList;
        }

        public List<UserRegistrationList> GetUserRegistrationList()
        {
            List<UserRegistrationList> OrderList = new List<UserRegistrationList>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_getuserlist", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        UserRegistrationList NewOrderdata = new UserRegistrationList();

                        NewOrderdata.customername = Convert.ToString(DT.Rows[i]["customername"]);
                        NewOrderdata.contactno = Convert.ToString(DT.Rows[i]["contactno"]);
                        NewOrderdata.useremail = Convert.ToString(DT.Rows[i]["useremail"]);
                        NewOrderdata.city = Convert.ToString(DT.Rows[i]["city"]);
                        NewOrderdata.state = Convert.ToString(DT.Rows[i]["state"]);
                        NewOrderdata.zipcode = Convert.ToString(DT.Rows[i]["zipcode"]);
                        
                        OrderList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return OrderList;
        }

        public List<CompletedOrderEntity> GetCompleteOrder(string StartDate, string EndDate)
        {
            List<CompletedOrderEntity> CompleteOrderList = new List<CompletedOrderEntity>();
            DataTable DT = new DataTable();

            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_CompletedOrder", con);
                cmd.Parameters.AddWithValue("@p_startdate", StartDate);
                cmd.Parameters.AddWithValue("@p_enddate", EndDate);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        CompletedOrderEntity CompleteOrderdata = new CompletedOrderEntity();

                        CompleteOrderdata.Soid = Convert.ToString(DT.Rows[i]["Soid"]);
                        CompleteOrderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        CompleteOrderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        CompleteOrderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        CompleteOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        CompleteOrderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        CompleteOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);
                        CompleteOrderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);

                        CompleteOrderList.Add(CompleteOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return CompleteOrderList;
        }

        public DataSet ApprovalData(string SoID)
        {

            DataSet DS = new DataSet();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_getorderdtl", con);
                cmd.Parameters.AddWithValue("@p_soid", SoID);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DS);
                con.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public void SaveOrderapproval(string OrderStatus, string DelPerID, string SoID, string UserID,
            string shipmentcompany = "", string airbasenumber = "")
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_updateorderstatus", con);
                cmd.Parameters.AddWithValue("@p_ordstatus", OrderStatus);
                cmd.Parameters.AddWithValue("@p_delpersonid", DelPerID);
                cmd.Parameters.AddWithValue("@p_userid", UserID);
                cmd.Parameters.AddWithValue("@p_soid", SoID);
                cmd.Parameters.AddWithValue("@p_shipmentcompany", shipmentcompany);
                cmd.Parameters.AddWithValue("@p_airbasenumber", airbasenumber);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                int ret = cmd.ExecuteNonQuery();

                if (ret != 0)
                {
                    SendOrderNotification(SoID, OrderStatus, UserID, shipmentcompany, airbasenumber);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void SendOrderNotification(string orderid, string orderstatus, string UserID, string couriercomp, string courierno)
        {
            string getSMSTemplate = "", strsql = "", smstempstr = "", mobileno = "", email = "", orderno = "", userid = "", personName = "", dlt_te_id = "", itemaname = "", orgname = "";
            DataTable dtget, dtgetorder;
            try
            {
                strsql = " select sorder.mobileno,sorder.emailid,socode,sorder .soid,customername,mitem.itemname,registerinfo.orgname  " +
                         " From sorder inner join sodtl on sodtl.soid = sorder.soid inner join mitem on mitem.itemid = sodtl.itemid " +
                         " inner join registerinfo on registerinfo.infoid = sorder.level " + 
                         " where sorder.soid = " + orderid + " ";
                dtgetorder = objnoti.FillDataTable(strsql);
                if (dtgetorder.Rows.Count > 0)
                {
                    mobileno = Convert.ToString(dtgetorder.Rows[0]["mobileno"]);
                    email = Convert.ToString(dtgetorder.Rows[0]["emailid"]);
                    orderno = Convert.ToString(dtgetorder.Rows[0]["socode"]);
                    userid = UserID;
                    personName = Convert.ToString(dtgetorder.Rows[0]["customername"]);
                    itemaname = Convert.ToString(dtgetorder.Rows[0]["itemaname"]);
                    orgname = Convert.ToString(dtgetorder.Rows[0]["orgname"]);

                    if (orderstatus == "Ready For Ship")
                    {
                        getSMSTemplate = "select * from createsmstemplate where smsfor = 'Shipped Order' ";
                    }
                    else
                    {
                        getSMSTemplate = "select * from createsmstemplate where smsfor = 'Order Status' ";
                    }

                    
                    dtget = objnoti.FillDataTable(getSMSTemplate);

                    if (dtget.Rows.Count > 0)
                    {
                        smstempstr = Convert.ToString(dtget.Rows[0]["smseng"]);
                        smstempstr = smstempstr.Replace("$OrderStatus$", orderstatus);
                        smstempstr = smstempstr.Replace("$OrderNo$", orderno);
                        
                        smstempstr = smstempstr.Replace("$orgname$", orgname);
                        smstempstr = smstempstr.Replace("$itemname$", itemaname);
                        smstempstr = smstempstr.Replace("$couriercomp$", couriercomp);
                        smstempstr = smstempstr.Replace("$refno$", courierno);

                        dlt_te_id = Convert.ToString(dtget.Rows[0]["dlt_te_id"]);
                    }

                    if (Convert.ToString(smstempstr) != "")
                    {
                        if (Convert.ToString(mobileno) != "" && Convert.ToString(smstempstr.Trim()) != "")
                        {
                            objnoti.sendsmsthrougmsg91("transctional", smstempstr, mobileno, userid, personName, dlt_te_id);
                        }

                        if (email != "" && Convert.ToString(smstempstr.Trim()) != "")
                        {
                            //send email
                            objnoti.SendEmailThroughSendgrid(email, smstempstr, "Order Status", 0, "contact@puredesigheeofindia.co.in", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public DataTable GetDelPerRecord()
        {

            DataTable DT = new DataTable();
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_GetDelBoyDetail", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return DT;
        }

        public string NotificationSend(Newtonsoft.Json.Linq.JObject logindata)
        {
            string JSONString = string.Empty;
            DataSet ds = new DataSet();

            string parastr = "", OrderStatus = "", deviceregid = "", Orderid = "";
            string sResponseFromServer = "";

            parastr = logindata.ToString();
            dynamic data1 = JObject.Parse(parastr);

            Orderid = (Convert.ToString(data1.Orderid));
            OrderStatus = (Convert.ToString(data1.OrderStatus));
            deviceregid = (Convert.ToString(data1.deviceregid));

            string serverKey = "AIzaSyDC9dqyqmK8mtWS_xwO6K56zAe3aAiI4F0";
            string senderId = "9538212888";
            string webAddr = "https://fcm.googleapis.com/fcm/send";
            string DeviceToken = deviceregid;
            string msg = OrderStatus, title = "Order Status";

            //string DeviceToken = "dP89nHu3nhI:APA91bEUaDbtRv8Z-n3QQNxPnkpV1x0WVYK-EBFZEdCPH1rvQSoo7rPmKuh-7QHstbprWwt9ukkn1I5OmLeJ8_QPYzoJwUC4gySKMCYwVYLV5SPqWstNdvXP5q_v4s2GXtSdDF_pQhXj",

            var result = "-1";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            httpWebRequest.Method = "POST";

            var data = new
            {
                to = DeviceToken,
                priority = "high",
                notification = new
                {
                    body = msg,
                    title = title
                },
            };

            var serializer = new JavaScriptSerializer();
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = serializer.Serialize(data);
                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            sResponseFromServer = result;
            return sResponseFromServer;
        }

    }
}