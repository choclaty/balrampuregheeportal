﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OrderWebPortal.Models;

namespace OrderWebPortal.DataAccessLayer
{
    public class Report
    {
        public List<salesSummaryEntity> GetsalesSummary(string StartDate, string EndDate)
        {
            List<salesSummaryEntity> salesSummaryList = new List<salesSummaryEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_SalesSummary", con);
                cmd.Parameters.AddWithValue("@p_startdate", StartDate);
                cmd.Parameters.AddWithValue("@p_enddate", EndDate);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        salesSummaryEntity NewOrderdata = new salesSummaryEntity();

                        NewOrderdata.BillID = Convert.ToString(DT.Rows[i]["BillID"]);
                        NewOrderdata.BillCode= Convert.ToString(DT.Rows[i]["BillCode"]);
                        NewOrderdata.BillDate= Convert.ToString(DT.Rows[i]["BillDate"]);
                        NewOrderdata.BillTime = Convert.ToString(DT.Rows[i]["BillTime"]);
                        NewOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        NewOrderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        NewOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);

                        salesSummaryList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return salesSummaryList;
        }


        public List<OrderSummaryEntity> GetOrderSummary(string StartDate, string EndDate)
        {
            List<OrderSummaryEntity> OrderSummaryList = new List<OrderSummaryEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_OrderSummary", con);
                cmd.Parameters.AddWithValue("@p_startdate", StartDate);
                cmd.Parameters.AddWithValue("@p_enddate", EndDate);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        OrderSummaryEntity NewOrderdata = new OrderSummaryEntity();

                        NewOrderdata.Soid = Convert.ToString(DT.Rows[i]["Soid"]);
                        NewOrderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        NewOrderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        NewOrderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        NewOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        NewOrderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        NewOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);
                        NewOrderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);

                        OrderSummaryList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return OrderSummaryList;
        }

        public List<DeliverySummaryEntity> GetDeliverySummary(string StartDate, string EndDate)
        {
            List<DeliverySummaryEntity> DeliverySummaryList = new List<DeliverySummaryEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_DeliveryReport", con);
                cmd.Parameters.AddWithValue("@p_startdate", StartDate);
                cmd.Parameters.AddWithValue("@p_enddate", EndDate);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        DeliverySummaryEntity NewOrderdata = new DeliverySummaryEntity();

                        NewOrderdata.Soid = Convert.ToString(DT.Rows[i]["Soid"]);
                        NewOrderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        NewOrderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        NewOrderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        NewOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        NewOrderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        NewOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);
                        NewOrderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);

                        DeliverySummaryList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return DeliverySummaryList;
        }


        public List<ItemWiseOrderEntity> GetItemWiseOrder(string StartDate, string EndDate)
        {
            List<ItemWiseOrderEntity> ItemWiseOrderList = new List<ItemWiseOrderEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_Itemwiseorder", con);
                cmd.Parameters.AddWithValue("@p_startdate", StartDate);
                cmd.Parameters.AddWithValue("@p_enddate", EndDate);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ItemWiseOrderEntity NewOrderdata = new ItemWiseOrderEntity();

                        NewOrderdata.Soid = Convert.ToString(DT.Rows[i]["Soid"]);
                        NewOrderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        NewOrderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        NewOrderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        NewOrderdata.itemid = Convert.ToString(DT.Rows[i]["itemid"]);
                        NewOrderdata.itemname = Convert.ToString(DT.Rows[i]["itemname"]);
                        NewOrderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        NewOrderdata.Qty = Convert.ToString(DT.Rows[i]["Qty"]);
                        NewOrderdata.Rate = Convert.ToString(DT.Rows[i]["Rate"]);
                        NewOrderdata.NetAmt = Convert.ToString(DT.Rows[i]["NetAmt"]);
                        NewOrderdata.Discamt = Convert.ToString(DT.Rows[i]["Discamt"]);
                        NewOrderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["TotalAmt"]);
                        NewOrderdata.TaxPerc = Convert.ToString(DT.Rows[i]["TaxPerc"]);
                        NewOrderdata.TaxAmt = Convert.ToString(DT.Rows[i]["TaxAmt"]);
                        NewOrderdata.totalamount = Convert.ToString(DT.Rows[i]["totalamount"]);
                        NewOrderdata.BillAmt = Convert.ToString(DT.Rows[i]["BillAmt"]);
                        NewOrderdata.CustId = Convert.ToString(DT.Rows[i]["CustId"]);
                        NewOrderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);

                        ItemWiseOrderList.Add(NewOrderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return ItemWiseOrderList;
        }
    }
}