﻿using OrderWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace OrderWebPortal.DataAccessLayer
{
    public class RozarPay
    {
        public void SavePurchaseHistory(string purchasetype, string purchasedate, string purchasetime, string purchaseval,
            string amount, string orderid, string refno, string custid, string adduserid, string paymentby)
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_savepurchasehistory", con);
                cmd.Parameters.AddWithValue("@p_purchasetype", purchasetype);
                cmd.Parameters.AddWithValue("@p_purchasedate", purchasedate);
                cmd.Parameters.AddWithValue("@p_purchasetime", purchasetime);
                cmd.Parameters.AddWithValue("@p_purchaseval", purchaseval);
                cmd.Parameters.AddWithValue("@p_amount", amount);
                cmd.Parameters.AddWithValue("@p_orderid", orderid);
                cmd.Parameters.AddWithValue("@p_paymentby", paymentby);
                cmd.Parameters.AddWithValue("@p_refno", refno);
                cmd.Parameters.AddWithValue("@p_Adduserid", adduserid);
                cmd.Parameters.AddWithValue("@p_custid", custid);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public string SaveOrderEntry(sorder soentry)
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                con.Open();

                SqlCommand cmd = new SqlCommand("sp_sorder", con);
                
                cmd.Parameters.AddWithValue("@p_formmode", 0);
                cmd.Parameters.AddWithValue("@p_soid", 0);
                cmd.Parameters.AddWithValue("@p_uid1", 0);
                cmd.Parameters.AddWithValue("@p_custid", soentry.custid);
                cmd.Parameters.AddWithValue("@p_customername", soentry.customername);
                cmd.Parameters.AddWithValue("@p_mobileno", soentry.mobileno);
                cmd.Parameters.AddWithValue("@p_emailid", soentry.emailid);                
                cmd.Parameters.AddWithValue("@p_socode", 0);
                cmd.Parameters.AddWithValue("@p_socode_1", 0);
                cmd.Parameters.AddWithValue("@p_sodate", soentry.sodate);
                cmd.Parameters.AddWithValue("@p_sotime", soentry.sotime);
                cmd.Parameters.AddWithValue("@p_deliverydays", 0);
                cmd.Parameters.AddWithValue("@p_reminderdays", 0);
                cmd.Parameters.AddWithValue("@p_taxtype", soentry.taxtype);
                cmd.Parameters.AddWithValue("@p_roffamt", soentry.roffamt);
                cmd.Parameters.AddWithValue("@p_discamt", soentry.discamt);
                cmd.Parameters.AddWithValue("@p_totamt", soentry.totamt);
                cmd.Parameters.AddWithValue("@p_advamt", soentry.advamt);
                cmd.Parameters.AddWithValue("@p_dailytrgentype", 0);
                cmd.Parameters.AddWithValue("@p_deliverycharge", soentry.deliverycharge);
                cmd.Parameters.AddWithValue("@p_deliverytype", soentry.deliverytype);
                cmd.Parameters.AddWithValue("@p_deliverystatus", 0);
                cmd.Parameters.AddWithValue("@p_referenceno", soentry.referenceno);
                cmd.Parameters.AddWithValue("@p_referencedate", soentry.referencedate);
                cmd.Parameters.AddWithValue("@p_wallettype", 0);
                cmd.Parameters.AddWithValue("@p_remarks", soentry.remarks);
                cmd.Parameters.AddWithValue("@p_paymenttype", soentry.paymenttype);
                cmd.Parameters.AddWithValue("@p_counterid", 0);
                cmd.Parameters.AddWithValue("@p_level", 1);
                cmd.Parameters.AddWithValue("@p_addedby", soentry.custid);
                cmd.Parameters.AddWithValue("@p_uid", 1);

                cmd.Parameters.Add("@p_RetVal", SqlDbType.BigInt);
                cmd.Parameters["@p_RetVal"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_Errstr", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_Errstr"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_retid", SqlDbType.BigInt);
                cmd.Parameters["@p_retid"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                
                int ret = cmd.ExecuteNonQuery();
                string retbillid_1 = Convert.ToString(cmd.Parameters["@p_retid"].Value);
                //con.Close();
                foreach (var item in soentry.sodtls)
                {
                    SqlCommand cmdchild = new SqlCommand("sp_sodtl", con);
                    //con.Open();
                    cmdchild.Parameters.AddWithValue("@p_Formmode", 1);
                    cmdchild.Parameters.AddWithValue("@p_sodtlid", 0);
                    cmdchild.Parameters.AddWithValue("@p_uid1", 0);
                    cmdchild.Parameters.AddWithValue("@p_soid", retbillid_1);
                    cmdchild.Parameters.AddWithValue("@p_itemid", item.itemid);
                    cmdchild.Parameters.AddWithValue("@p_unitid", item.unitid);
                    cmdchild.Parameters.AddWithValue("@p_itmdeldate", item.itmdeldate);
                    cmdchild.Parameters.AddWithValue("@p_itemdeltime", item.itemdeltime);
                    cmdchild.Parameters.AddWithValue("@p_qty", item.qty);
                    cmdchild.Parameters.AddWithValue("@p_grate", item.grate);
                    cmdchild.Parameters.AddWithValue("@p_discperc", item.discperc);
                    cmdchild.Parameters.AddWithValue("@p_discamt", item.discamt);
                    cmdchild.Parameters.AddWithValue("@p_netrate", item.netrate);
                    cmdchild.Parameters.AddWithValue("@p_totamt", item.totamt);
                    cmdchild.Parameters.AddWithValue("@p_sgstperc", item.sgstperc);
                    cmdchild.Parameters.AddWithValue("@p_sgstamt", item.sgstamt);
                    cmdchild.Parameters.AddWithValue("@p_cgstperc", item.cgstperc);
                    cmdchild.Parameters.AddWithValue("@p_cgstamt", item.cgstamt);
                    cmdchild.Parameters.AddWithValue("@p_igstperc", item.igstperc);
                    cmdchild.Parameters.AddWithValue("@p_igstamt", item.igstamt);
                    cmdchild.Parameters.AddWithValue("@p_Addnote", "");                    
                    cmdchild.Parameters.AddWithValue("@p_prclitmyn", 0);
                    cmdchild.Parameters.AddWithValue("@p_uid", 1);
                    cmdchild.Parameters.AddWithValue("@p_level", 1);
                    cmdchild.Parameters.AddWithValue("@p_addedby", soentry.custid);

                    cmdchild.Parameters.Add("@p_Errstr", SqlDbType.VarChar, 100);
                    cmdchild.Parameters["@p_Errstr"].Direction = ParameterDirection.Output;

                    cmdchild.Parameters.Add("@p_RetVal", SqlDbType.BigInt);
                    cmdchild.Parameters["@p_RetVal"].Direction = ParameterDirection.Output;
                    
                    cmdchild.CommandType = CommandType.StoredProcedure;
                    int retchild = cmdchild.ExecuteNonQuery();
                    //con.Close();
                }

                return retbillid_1;
            }
            catch (Exception ex)
            {
                con.Close();
                return "";
                throw;
            }
            finally
            {
                con.Close();
            }
            
        }
    }
}