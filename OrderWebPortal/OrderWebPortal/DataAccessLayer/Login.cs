﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using OrderWebPortal.Models;

namespace OrderWebPortal.DataAccessLayer
{
    public class LoginClass
    {
        public Login GetLoginData(Login login)
        {
            DataTable DT = new DataTable();
            Login logindata = new Login();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("sp_Portallogin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                  
                cmd.Parameters.AddWithValue("@p_username", login.username);
                cmd.Parameters.AddWithValue("@p_userpwd", login.userpwd);
                //cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                //cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                //cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                //cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();
                if (DT.Rows.Count > 0)
                {
                    logindata.userid = Convert.ToInt32(DT.Rows[0]["userid"]);
                    logindata.username = Convert.ToString(DT.Rows[0]["username"]);
                    logindata.userpwd = Convert.ToString(DT.Rows[0]["userpassword"]);

                    return logindata;
                }
                return logindata;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}