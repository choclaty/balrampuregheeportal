﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace OrderWebPortal
{
    public class CheckSession : FilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext context)
        {
            try
            {
                if (HttpContext.Current.Session["UserID"] == null)
                    throw new NullReferenceException();
            }
            catch (NullReferenceException)
            {
                context.Result = new HttpUnauthorizedResult();
            }
        }
        public void OnAuthenticationChallenge(AuthenticationChallengeContext context)
        {


            if (context.Result == null || context.Result is HttpUnauthorizedResult)
            {
                context.Result = new RedirectToRouteResult("Default",
                    new System.Web.Routing.RouteValueDictionary{
                        {"controller", "Login"},
                        {"action", "Login"}
                    });
            }
        }
    }
}