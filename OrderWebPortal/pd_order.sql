


/****** Object:  StoredProcedure [dbo].[sp_bill]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_bill]    
(@p_formmode int ,@p_billid float = NULL,	 @p_uid1 bigint = NULL,	 @p_billdate datetime = NULL,	 @p_billTime datetime = NULL,	 
@p_billcode nvarchar(100) = NULL,	 @p_billcode_1 bigint = NULL,	 @p_custname nvarchar(max) = NULL,	 @p_mobileno nvarchar(100) = NULL,	 @p_taxtype nvarchar(100) = NULL,	 @p_prclamt float = NULL,	 @p_discount float = NULL,	 @p_totamt float = NULL,	 @p_roffamt float = NULL,	 @p_grossamt float = NULL,	 @p_adddesc nvarchar(255) = NULL,	 @p_cardname nvarchar(50) = NULL,	 @p_referenceno nvarchar(50) = NULL,	 @p_paymentdate datetime = NULL,	 @p_payremarks nvarchar(max) = NULL,	 @p_cashamt float = NULL,	 @p_creditpay float = NULL,	 @p_giftcardPay float = NULL,	 @p_walletamt float = NULL,	 @p_wallettype bigint = NULL,	 @p_paidamt float = NULL,	 @p_advamt float = NULL,	 @p_amtrtn float = NULL,	 @p_totalamt float = NULL,	 @p_counterid bigint = NULL,	 @p_uid bigint = NULL,	 @p_level nvarchar(10) = NULL, @p_addedby bigint = NULL,  @p_Errstr nvarchar(max) out,  @p_RetVal bigint out ,@P_retbillid bigint out) AS  
if @p_Formmode = 0  begin  
set @p_uid1 = (select isnull(Max(uid1),0)+1 from bill Where level = @p_level)  
declare @tempbilid nvarchar(100)
set @tempbilid = CONVERT(nvarchar,@p_uid) +''+ CONVERT(nvarchar,@p_uid1)
set @p_billid = @tempbilid
Set @P_retbillid = @p_billid  
INSERT INTO [dbo].[bill] (billid,	uid1,	billdate,	billTime,	billcode,	billcode_1,	custname,	mobileno,	taxtype,	prclamt,	discount,	totamt,	roffamt,	grossamt,	adddesc,	cardname,	referenceno,	paymentdate,	payremarks,	cashamt,	creditpay,	giftcardPay,	walletamt,	wallettype,	paidamt,	advamt,	amtrtn,	totalamt,	counterid,	uid,	level,	tflg,	addedby)  
VALUES ( @p_billid,	 @p_uid1,@p_billdate,	 @p_billTime,	 @p_billcode,	 @p_billcode_1,	 @p_custname,	 @p_mobileno,	 @p_taxtype,	 @p_prclamt,	 @p_discount,	 @p_totamt,	 @p_roffamt,	 @p_grossamt,	 @p_adddesc,	 @p_cardname,	 @p_referenceno,	 @p_paymentdate,	 @p_payremarks,	 @p_cashamt,	 @p_creditpay,	 @p_giftcardPay,	 @p_walletamt,	 @p_wallettype,	 @p_paidamt,	 @p_advamt,	 @p_amtrtn,	 @p_totalamt,	 @p_counterid,	 @p_uid,	 @p_level,0,	 @p_addedby)  end   
else if @p_Formmode = 1 begin  
update [bill]  set billdate =  @p_billdate,	billTime =  @p_billTime,	billcode =  @p_billcode,	billcode_1 =  @p_billcode_1,	custname =  @p_custname,	mobileno =  @p_mobileno,	taxtype =  @p_taxtype,	prclamt =  @p_prclamt,	discount =  @p_discount,	totamt =  @p_totamt,	roffamt =  @p_roffamt,	grossamt =  @p_grossamt,	adddesc =  @p_adddesc,	cardname =  @p_cardname,	referenceno =  @p_referenceno,	paymentdate =  @p_paymentdate,	payremarks =  @p_payremarks,	cashamt =  @p_cashamt,	creditpay =  @p_creditpay,	giftcardPay =  @p_giftcardPay,	walletamt =  @p_walletamt,	wallettype =  @p_wallettype,	paidamt =  @p_paidamt,	advamt =  @p_advamt,	amtrtn =  @p_amtrtn,	totalamt =  @p_totalamt,	counterid =  @p_counterid,tflg =  1,	updatedby =  @p_addedby  WHere [billid] =@p_billid and [level]=@P_level   end   
else if @p_Formmode =2 begin  update [bill] set delflg = 1,delby = @p_addedby,deldatetime = GETDATE() 
WHere [billid] =@p_billid and [level]=@P_level end
BEGIN  SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage  SET @p_Retval = ERROR_NUMBER()  set @p_Errstr = ERROR_MESSAGE()  Set @P_retbillid = @p_billid END






GO
/****** Object:  StoredProcedure [dbo].[sp_billdtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_billdtl] (@p_formmode int,@p_billdtlid  bigint ,@p_uid1  bigint =null ,@p_billid  bigint =null ,@p_itemid  bigint =null ,  @p_unitid bigint=null,    
@p_qty  float =null ,  @p_grate  float =null ,@p_discperc  float =null ,@p_discamt  float =null ,@p_netrate  float =null ,@p_totamt  float =null ,  
@p_sgstperc  float =null ,  @p_sgstamt  float =null ,  @p_cgstperc  float =null ,@p_cgstamt  float =null ,  @p_igstperc  float =null ,  
@p_igstamt  float =null ,@p_soid bigInt=null,@p_sodtlid bigInt=null,  @p_schmid bigInt=null,@p_schmdtlid bigInt=null  
,@p_isvoid bit=null,@p_prclitmyn bit=null,@p_Addnote nvarchar(Max) = null,@p_uid bigint = null,@P_level NVARCHAR(10),  
@p_addedby  bigint =null ,    
@p_Errstr nvarchar(max) out,@p_RetVal bigint out , @p_retbilldtlid BigInt Out) AS           
if @p_Formmode = 1 begin      
  
set @p_uid1 = (select isnull(Max(uid1),0)+1 from billdtl Where level = @P_level )    
declare @tempbilldtl nvarchar(100)
set @tempbilldtl = CONVERT(nvarchar,@p_uid) +''+CONVERT(nvarchar,@p_uid1)
set @p_billdtlid = @tempbilldtl  
Set @p_retbilldtlid = @p_billdtlid  
    
INSERT INTO [billdtl] ([billdtlid],[uid1] ,[billid] ,[itemid] ,[qty] ,[grate] ,[discperc] ,[discamt],[netrate] , [totamt] ,[sgstperc] ,    
[sgstamt] ,[cgstperc] ,  [cgstamt] ,[igstperc] ,[igstamt] ,[tflg] ,[addedby] ,[addedatetime] ,  
[unitid],[soid],  [sodtlid],[schmid],[schmdtlid],[isvoid],[prclitmyn],[Addnote],[uid],[level])       
VALUES(@p_billdtlid,@p_uid1 ,@p_billid ,@p_itemid ,@p_qty ,@p_grate ,@p_discperc ,@p_discamt,@p_netrate ,@p_totamt ,@p_sgstperc   
,@p_sgstamt ,@p_cgstperc ,    @p_cgstamt ,@p_igstperc ,@p_igstamt ,0 ,@p_addedby ,GetDate(),  
@p_unitid,@p_soid,@p_sodtlid,@p_schmid,@p_schmdtlid,@p_isvoid,@p_prclitmyn,@p_Addnote,@p_uid,@P_level) end            
else   if   
@p_Formmode = 2 begin    
update [billdtl]  set [itemid]=@p_itemid ,[qty]=@p_qty ,[grate]=@p_grate ,[discperc]=@p_discperc ,[discamt]=@p_discamt,[netrate]=@p_netrate ,  
[totamt]=@p_totamt ,[sgstperc]=@p_sgstperc,[sgstamt]=@p_sgstamt,[cgstperc]=@p_cgstperc ,[cgstamt]=@p_cgstamt ,[igstperc]=@p_igstperc ,      
[igstamt]=@p_igstamt,[tflg]=1 ,[updatedby] = @p_addedby ,[updateddatetime]=GetDate(),[unitid]=@p_unitid,[soid]=@p_soid,      
[sodtlid]=@p_sodtlid,[schmid]=@p_schmid,[schmdtlid]=@p_schmdtlid ,isvoid=@p_isvoid ,prclitmyn = @p_prclitmyn ,Addnote = @P_Addnote   
WHere [billdtlid] = @p_billdtlid and [billid] =@p_billid end           
else if @p_Formmode = 3 begin    
update [billdtl] set delflg = 1,delby = @p_addedby,deldatetime = GETDATE()     
WHere [billdtlid] = @p_billdtlid and [billid] =@p_billid end       
BEGIN SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage SET @p_Retval = ERROR_NUMBER()    
set @p_Errstr = ERROR_MESSAGE() Set @p_retbilldtlid = @p_billid END  



GO
/****** Object:  StoredProcedure [dbo].[sp_cartItemList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[sp_cartItemList]    Script Date: 10-01-2020 01:29:09 AM ******/      
/*    
select * from checkauthkey order by id desc    
exec sp_cartItemList '',50,'0U6Y608YH1WHKEXDW9LWQX6GANVD79',0,''    
*/    
CREATE PROCEDURE [dbo].[sp_cartItemList]                
@p_deviceregid nvarchar(100)=null,                
@p_userid int=null,                
@p_authkey nvarchar(50) =null,                
@p_errorcode bigint out,                 
@p_errormessage nvarchar(max) out                
AS                
Begin                
               
   declare @tblItem as table(packid nvarchar(10),              
   Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
   ingredients nvarchar(max),nutritioninfo nvarchar(max),itembarcode nvarchar(50),              
   itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
   salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
   itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500)    ,packname nvarchar(500)  ,taxtype nvarchar(10),itemqty nvarchar(10)  
   ,review nvarchar(100)                   
  )              
                    
 insert into @tblItem                
  select 0 as packid,mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'') = '' then mitem.salerate else pricedtl.rate end salerate,
     case when isnull(pricedtl.Rate,'')='' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,                  
     case when isnull(pricedtl.sgstper,'')='' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'')='' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'')='' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,'' ,isnull(mitem.taxtype,0) as   taxtype,mCartItem.ItemQty  
     ,isnull(reviewitem.review,0) as  review      
     from               
     mCartItem inner join mitem on mCartItem.Itemid = mitem.itemid               
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where userid = @p_userid  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid         
     inner join munit on mitem.unitid = munit.unitid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
  WHERE mCartItem.userid = @p_userid and packid <=0          
             
    union all           
              
    Select mcartitem.packid, mstpackitmlink.itemid,mitem.itemcatid, mitem.itemdesc,mitem.ingredients,mitem.nutritioninfo,          
   mitem.itembarcode,mitem.itemname,mitem.itmimgename,0 as discperc,packval as salerate,packval as Discrate,mitem.sgstper,mitem.cgstper,          
   mitem.igstper,mitem.itemwt,munit.unitname,mitem.validfor,mstpack.packname,isnull(mitem.taxtype,0) as   taxtype,mCartItem.ItemQty  
   ,isnull(reviewitem.review,0) as  review  
   from mcartitem inner join           
   mstpack on mcartitem.packid = mstpack.id          
   inner join mstpackitmlink on mstpack.id = mstpackitmlink.packid and mcartitem.itemid = mstpackitmlink.itemid          
   inner join mitem on mitem.itemid =   mstpackitmlink.itemid                
   left join ( Select avg(Review) as review,itemid  from mReviewItem Where userid = @p_userid  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid         
   inner join munit on mitem.unitid = munit.unitid                 
     WHERE mCartItem.userid = @p_userid              
           
             
  select * from @tblItem              
  if exists(select top 1 * from @tblItem)            
  BEGIN              
   SET @p_errorcode = 1              
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')              
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage              
       
  END              
  else              
  BEGIN              
   SET @p_errorcode = 0              
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'No Record')              
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage              
               
  END              
    
 exec spl_calculatetax @p_userid    
End 

GO


/****** Object:  StoredProcedure [dbo].[sp_GetItemDetails]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
select * from checkauthkey order by id desc    
exec sp_GetItemDetails '123',11,60,'KPSX71DWO2',0,''    
*/       
CREATE PROCEDURE [dbo].[sp_GetItemDetails]            
@p_deviceregid nvarchar(100)=null,            
@itemid int=null,           
@p_userid int=null,            
@p_authkey nvarchar(50) =null,            
@p_errorcode bigint out,             
@p_errormessage nvarchar(max) out ,        
@p_cartitemret bigint=null out           
AS            
Begin            
 
  declare @tblItem as table(          
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),          
  ingredients nvarchar(max),nutritioninfo nvarchar(max),itembarcode nvarchar(50),          
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),          
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),          
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500)  ,favitem nvarchar(10),taxtype nvarchar(10),itemqty nvarchar(10),review nvarchar(100)        
  )          
          
  insert into @tblItem          
  select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,          
     case when isnull(pricedtl.Rate,'') = '' then mitem.salerate else pricedtl.rate end salerate,    
  case when isnull(pricedtl.Rate,'')='' then   mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,        
     case when isnull(pricedtl.sgstper,'')='' then mitem.sgstper else pricedtl.sgstper end sgstper,          
     case when isnull(pricedtl.cgstper,'')='' then mitem.cgstper else pricedtl.cgstper end cgstper,          
     case when isnull(pricedtl.igstper,'')='' then mitem.igstper else pricedtl.igstper end igstper,          
     itemwt,munit.unitname,validfor,      
     case when mfavitem.itemid is null then '0' else '1' end favitem ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty,    
     isnull(reviewitem.review,0) as  review         
     from mitem             
     inner join munit on mitem.unitid = munit.unitid     
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3 group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid         
     LEFT join (select distinct itemid,userid from mFavItem where userid = @p_userid) as mfavitem on mitem.itemid = mfavitem.Itemid                   
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = @p_userid       
     group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid       
           
     left join           
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate            
   from ( select *, row_number() over (            
      partition by [itemid]             
      order by [pldatetime] desc            
   ) as RN            
   From           
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,           
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate            
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level           
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=           
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and           
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )            
    ) as A ) X where RN = 1) pricedtl           
   on mitem.itemid = pricedtl.itemid WHERE mitem.Itemid = @Itemid            
  
  if (Select count(*) from mcartitem WHere userid = @p_userid) > 0        
 begin        
  set @p_cartitemret = 1        
 end        
  else        
 begin        
  set @p_cartitemret = 0        
 end  
          
  select * from @tblItem  
  
  SET @p_errorcode = 1                  
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                  
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage , @p_cartitemret as cartitemret    
                
End 

GO
/****** Object:  StoredProcedure [dbo].[sp_getuserprofile]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
select * from checkauthkey order by id desc
exec sp_getuserprofile 20,'8LM4J9JC5Y60HKTR3YO478N5IAF1LM',0,0,''
*/


CREATE PROCEDURE [dbo].[sp_getuserprofile]  
 @p_userid bigint,  
 @p_deviceregid nvarchar(100)=null, 
 @p_authkey nvarchar(100),  
 @p_retid bigint OUT,  
 @p_errorcode bigint OUT,  
 @p_errormessage NVARCHAR(100) OUT  
AS  
Begin  
  
   
      select id as userid,firstname as fullname,City,patimg,contactno as mobileno,useremail as emailid from UserRegistration where id = @p_userid  
  
   SET @p_errorcode = 1  
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Update Record')  

   
 SELECT @p_retid as retid, @p_errorcode AS errorcode, @p_errormessage AS errormessage  
End   


GO
/****** Object:  StoredProcedure [dbo].[sp_mitem]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_mitem](@p_formmode int, @p_itemid bigint = NULL,  @p_uid1 bigint = NULL,  @p_itemcatid bigint = NULL,      
@p_itembarcode nvarchar(25) = NULL,  @p_itemname nvarchar(200) = NULL,  @p_unitid bigint = NULL,  @p_itemdesc nvarchar(max) = NULL,  @p_ingredients nvarchar(max) = NULL,  @p_nutritioninfo nvarchar(300) = NULL,  @p_itemstatus nvarchar(20) = NULL,  @p_hsncode nvarchar(40) = NULL,    
@p_sgstper float = NULL,  @p_cgstper float = NULL,  @p_igstper float = NULL,  @p_itemwt decimal = NULL,  @p_selflifetype nvarchar(80) = NULL,  @p_selflifeval float = NULL,  @p_salerate float = NULL,  @p_purcrate float = NULL,    
@p_DefQty float = NULL,  @p_favitem bit = NULL,  @p_itmimgename nvarchar(50) = NULL,  @p_uid bigint = NULL,  @p_level nvarchar(10) = NULL,  @p_addedby bigint = NULL,  
@p_validfor nvarchar(100) = null,@p_jan bit,@p_feb bit,@p_mar bit,@p_apr bit,@p_may bit,@p_jun bit,@p_jul bit,@p_aug bit,@p_sep bit,@p_oct bit,@p_nov bit,@p_dec bit,  @p_taxtype bigint = 0,
@p_Errstr nvarchar(max) out,@p_RetVal bigint out) AS        
if @p_Formmode = 1 begin     
INSERT INTO [mitem] (itemid, uid1, itemcatid, itembarcode, itemname, unitid, itemdesc, ingredients, nutritioninfo, itemstatus, hsncode, sgstper, cgstper, igstper, itemwt, selflifetype, selflifeval, salerate, purcrate, DefQty, favitem, itmimgename, uid, tflg, level, addedby,addedatetime,validfor,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec,taxtype)  
VALUES ( @p_itemid,  @p_uid1,  @p_itemcatid,  @p_itembarcode,  @p_itemname,  @p_unitid,  @p_itemdesc,  @p_ingredients,  @p_nutritioninfo,  @p_itemstatus,    
@p_hsncode,  @p_sgstper,  @p_cgstper,  @p_igstper,  @p_itemwt,  @p_selflifetype,  @p_selflifeval,  @p_salerate,  @p_purcrate,  @p_DefQty,  @p_favitem,    
@p_itmimgename,  @p_uid,0,  @p_level,  @p_addedby,GETDATE(),@p_validfor,@p_jan,@p_feb,@p_mar,@p_apr,@p_may,@p_jun,@p_jul,@p_aug,@p_sep,@p_oct,@p_nov,@p_dec,@p_taxtype) end        
else if @p_Formmode = 2 begin     
update [mitem]  set itemid =  @p_itemid, uid1 =  @p_uid1, itemcatid =  @p_itemcatid, itembarcode =  @p_itembarcode, itemname =  @p_itemname, unitid =  @p_unitid, itemdesc =  @p_itemdesc, ingredients =  @p_ingredients, nutritioninfo =  @p_nutritioninfo,   
itemstatus =  @p_itemstatus, hsncode =  @p_hsncode, sgstper =  @p_sgstper, cgstper =  @p_cgstper, igstper =  @p_igstper, itemwt =  @p_itemwt, selflifetype =  @p_selflifetype, selflifeval =  @p_selflifeval, salerate =  @p_salerate, purcrate =  @p_purcrate,   
DefQty =  @p_DefQty, favitem =  @p_favitem, itmimgename =  @p_itmimgename, tflg =  1, updatedby =  @p_addedby,updateddatetime = GETDATE() ,  
validfor = @p_validfor,jan = @p_jan,feb = @p_feb,mar = @p_mar,apr = @p_apr,may = @p_may,jun = @p_jun,jul = @p_jul,aug = @p_aug,sep = @p_sep,oct = @p_oct,nov = @p_nov,dec = @p_dec  ,
taxtype = @p_taxtype
Where [itemid] =@p_itemid end        
else if @p_Formmode =3 begin     
update [mitem] set delflg = 1,delby = @p_addedby,deldatetime = GETDATE() WHere [itemid] =@p_itemid end      
BEGIN SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage SET @p_Retval = ERROR_NUMBER() set @p_Errstr = ERROR_MESSAGE()  END 

GO
/****** Object:  StoredProcedure [dbo].[sp_munit]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  Procedure [dbo].[sp_munit](@p_formmode int,@p_unitid BigInt,@p_uid1 BigInt,@p_unitcode nvarchar(10) = null,@p_unitname nvarchar(200) = null
,@p_NoofDecimal bigint = null, @p_uid bigint = NULL,	 @p_level nvarchar(10) = NULL, @p_addedby bigint = NULL,@p_Errstr nvarchar(max) out,
@p_RetVal bigint out ) AS  
if @p_Formmode = 0 begin   
insert into munit(unitid,uid1,unitcode,unitname,NoofDecimal,uid,level,tflg,addedby,addedatetime)  
values(@p_unitid,@p_uid1,@p_unitcode,@p_unitname, @p_NoofDecimal,@p_uid,	 @p_level,	 0,	 @p_addedby,GETDATE())  end 
else if @p_Formmode = 1 begin  update munit set unitcode = @p_unitcode,unitname = @p_unitname,
updatedby = @p_addedby,updateddatetime = GETDATE(),NoofDecimal= @p_NoofDecimal 
WHere unitid = @p_unitid   end 
else if @p_Formmode = 2 begin  update munit set delflg = 1,delby = @p_addedby,deldatetime = GETDATE() 
WHere unitid = @p_unitid   end 
BEGIN  SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage;   SET @p_Retval = ERROR_NUMBER()  set @p_Errstr = ERROR_MESSAGE()  END






GO
/****** Object:  StoredProcedure [dbo].[sp_Portallogin]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Portallogin]
	@p_userpwd NVARCHAR(200), 
	@p_username NVARCHAR(200)=null
	--@p_errorcode	INT OUT,
	--@p_errormessage	NVARCHAR(100) OUT			
AS
BEGIN	


SELECT username,userpassword,userid FROM 
mstuser 
WHERE username = @p_username and userpassword = @p_userpwd
	--BEGIN TRY
	--	Declare @userid nvarchar(100)
		
	--	set @userid = (select userid from mstuser where username = @p_username and userpassword = @p_userpwd)

	--	SET @p_errorcode = 1
	--	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess')
	--END TRY
	--BEGIN CATCH 	
	--	SET @p_errorcode = 0
	--	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid Login')
	--END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[sp_sodtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[sp_sodtl] (
 @p_Formmode int,@p_sodtlid bigint = NULL,	 @p_uid1 bigint = NULL,	 @p_soid bigint = NULL,	 @p_itemid bigint = NULL,	 @p_unitid bigint = NULL,	 @p_itmdeldate datetime = NULL,	 @p_itemdeltime datetime = NULL,	 @p_qty float = NULL,	 @p_grate float = NULL,	 @p_discperc float = NULL,	 @p_discamt float = NULL,	 @p_netrate float = NULL,	 @p_totamt float = NULL,	 @p_sgstperc float = NULL,	 @p_sgstamt float = NULL,	 @p_cgstperc float = NULL,	 @p_cgstamt float = NULL,	 @p_igstperc float = NULL,	 @p_igstamt float = NULL,	 @p_Addnote nvarchar(max) = NULL,	 @p_prclitmyn bit = NULL,	 @p_uid bigint = NULL,	 @p_level nvarchar(10) = NULL,	 @p_addedby bigint = NULL,@p_Errstr nvarchar(max) out,   @p_RetVal bigint out ) AS   
if @p_Formmode = 1 begin INSERT INTO [sodtl] (sodtlid,	uid1,	soid,	itemid,	unitid,	itmdeldate,	itemdeltime,	qty,	grate,	discperc,	discamt,	netrate,	totamt,	sgstperc,	sgstamt,	cgstperc,	cgstamt,	igstperc,	igstamt,	Addnote,	prclitmyn,	uid,	tflg,	level,	addedby,addedatetime)  
VALUES( @p_sodtlid,	 @p_uid1,	 @p_soid,	 @p_itemid,	 @p_unitid,	 @p_itmdeldate,	 @p_itemdeltime,	 @p_qty,	 @p_grate,	 @p_discperc,	 @p_discamt,	 @p_netrate,	 @p_totamt,	 @p_sgstperc,	 @p_sgstamt,	 @p_cgstperc,	 @p_cgstamt,	 @p_igstperc,	 @p_igstamt,	 @p_Addnote,	 @p_prclitmyn,	 @p_uid,	 0,	 @p_level,	 @p_addedby,GETDATE()) end    
else if @p_Formmode = 2 begin 
update [sodtl]  set itemid =  @p_itemid,	unitid =  @p_unitid,	itmdeldate =  @p_itmdeldate,	itemdeltime =  @p_itemdeltime,	qty =  @p_qty,	grate =  @p_grate,	discperc =  @p_discperc,	discamt =  @p_discamt,	netrate =  @p_netrate,	totamt =  @p_totamt,	sgstperc =  @p_sgstperc,	sgstamt =  @p_sgstamt,	cgstperc =  @p_cgstperc,	cgstamt =  @p_cgstamt,	igstperc =  @p_igstperc,	igstamt =  @p_igstamt,	Addnote =  @p_Addnote,	prclitmyn =  @p_prclitmyn,	tflg =  1,	updatedby =  @p_addedby,updateddatetime = GETDATE()
WHere [sodtlid] = @p_sodtlid and [soid]=@p_soid end  
begin SELECT ERROR_NUMBER() AS ErrorNumber,   ERROR_MESSAGE() AS ErrorMessage   SET @p_Retval = ERROR_NUMBER() set @p_Errstr = ERROR_MESSAGE()  END




GO
/****** Object:  StoredProcedure [dbo].[sp_sorder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_sorder] 
(@p_formmode int ,@p_soid float = NULL,	 @p_uid1 bigint = NULL,	 @p_custid bigint = NULL,	 @p_customername nvarchar(50) = NULL,	 @p_mobileno nvarchar(100) = NULL,	 @p_socode nvarchar(100) = NULL,	 @p_socode_1 bigint = NULL,	 @p_sodate datetime = NULL,	 @p_sotime datetime = NULL,	 @p_deliverydays bigint = NULL,	 @p_reminderdays bigint = NULL,	 @p_taxtype bigint = NULL,	 @p_roffamt float = NULL,	 @p_discamt float = NULL,	 @p_totamt float = NULL,	 @p_advamt float = NULL,	 @p_dailytrgentype bigint = NULL,	 @p_deliverycharge float = NULL,	 @p_deliverytype bigint = NULL,	 @p_deliverystatus bit = NULL,	 @p_referenceno nvarchar(20) = NULL,	 @p_referencedate datetime = NULL,	 @p_wallettype bigint = NULL,	 @p_remarks nvarchar(250) = NULL,	 @p_paymenttype bigint = NULL,	 @p_counterid bigint = NULL, @p_level nvarchar(10) = null,@p_addedby bigint = NULL,@p_uid bigint = null
,@p_Errstr nvarchar(max) out,@p_RetVal bigint out ) AS  
if @p_Formmode = 0  begin  
INSERT INTO sorder (soid,	uid1,	custid,	customername,	mobileno,	socode,	socode_1,	sodate,	sotime,	deliverydays,	reminderdays,	taxtype,	roffamt,	discamt,	totamt,	advamt,	dailytrgentype,	deliverycharge,	deliverytype,	deliverystatus,	referenceno,	referencedate,	wallettype,	remarks,	paymenttype,	counterid,	uid,tflg,[level],addedby,addedatetime)   
VALUES ( @p_soid,	 @p_uid1,	 @p_custid,	 @p_customername,	 @p_mobileno,	 @p_socode,	 @p_socode_1,	 @p_sodate,	 @p_sotime,	 @p_deliverydays,	 @p_reminderdays,	 @p_taxtype,	 @p_roffamt,	 @p_discamt,	 @p_totamt,	 @p_advamt,	 @p_dailytrgentype,	 @p_deliverycharge,	 @p_deliverytype,	 @p_deliverystatus,	 @p_referenceno,	 @p_referencedate,	 @p_wallettype,	 @p_remarks,	 @p_paymenttype,	 @p_counterid,	 @p_uid,0,@p_level,@p_addedby,GETDATE())  end 
else if @p_Formmode = 1 begin 
update [sorder]  set custid =  @p_custid,	customername =  @p_customername,	mobileno =  @p_mobileno,	socode =  @p_socode,	socode_1 =  @p_socode_1,	sodate =  @p_sodate,	sotime =  @p_sotime,	deliverydays =  @p_deliverydays,	reminderdays =  @p_reminderdays,	taxtype =  @p_taxtype,	roffamt =  @p_roffamt,	discamt =  @p_discamt,	totamt =  @p_totamt,	advamt =  @p_advamt,	dailytrgentype =  @p_dailytrgentype,	deliverycharge =  @p_deliverycharge,	deliverytype =  @p_deliverytype,	deliverystatus =  @p_deliverystatus,	referenceno =  @p_referenceno,	referencedate =  @p_referencedate,	wallettype =  @p_wallettype,	remarks =  @p_remarks,	paymenttype =  @p_paymenttype,	counterid =  @p_counterid,updatedby =  @p_addedby,updateddatetime = GETDATE(),tflg = 1
 WHere [soid] =@p_soid end  
begin SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage  SET @p_Retval = ERROR_NUMBER() set @p_Errstr = ERROR_MESSAGE()  END







GO
/****** Object:  StoredProcedure [dbo].[sp_userlogin]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_userlogin]
	@p_password NVARCHAR(200), 
	@p_email NVARCHAR(200)=null,
	@p_mobileno nvarchar(20)=null, 
	@p_deviceregid nvarchar(10),
	@p_authkey nvarchar(50),
	@p_otpno  nvarchar(50),
	@p_errorcode	INT OUT,
	@p_errormessage	NVARCHAR(100) OUT			
AS
BEGIN
	DECLARE @tblLogin AS TABLE
	(
		userid nvarchar(100), 
		username nvarchar(100),
		gender	nvarchar(100),
		address	nvarchar(100),
		city	nvarchar(100),
		age	nvarchar(100),
		email	nvarchar(100),
		mobileno nvarchar(50),
		userstatus nvarchar(100),
		img nvarchar(max)
	)
	
	Declare @userid nvarchar(100)


	if (@p_mobileno != '' )
	begin
		INSERT INTO @tblLogin	
		select id
		,firstname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
		case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno,case when userstatus = 'Inactive' then 0 else 1 end,
		case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
		from userregistration
		Where userpwd = @p_password and contactno= @p_mobileno 
	end
	else
	begin
	insert into @tblLogin
		select id
		,firstname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
		case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno,case when userstatus = 'Inactive' then 0 else 1 end,
		case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
		from userregistration
		Where userpwd = @p_password and useremail = @p_email 
	
	end

	set @userid = (select userid from @tblLogin)

	IF (EXISTS (select * from userappdevice Where id = @p_deviceregid ))
	BEGIN
		update userappdevice set userid = @userid Where id = @p_deviceregid		
	END

	IF (NOT EXISTS (select * from checkauthkey Where (userid = @userid or deviceregid = @p_deviceregid) and authkeystatus = 'Active'))
		BEGIN
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime,deviceregid) values (@userid,@p_authkey,'Active',Getdate(),@p_deviceregid)
		END
	else
		BEGIN
			/* deactive status if not active */
			update checkauthkey set authkeystatus = 'InActive' Where (userid = @userid or deviceregid=@p_deviceregid) and CONVERT(DATETIME, CONVERT(CHAR(8), adddatetime, 112)) < CONVERT(DATETIME, CONVERT(CHAR(8), getdate(), 112)) 
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime,deviceregid) values (@userid,@p_authkey,'Active',Getdate(),@p_deviceregid)
		END

	IF (NOT EXISTS (SELECT 1 FROM @tblLogin))
		BEGIN
			SELECT * FROM @tblLogin WHERE userstatus = 1
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid Login')
		END

	ELSE IF(NOT EXISTS (SELECT 1 FROM @tblLogin WHERE userstatus = 1))
		BEGIN		
			--SELECT * FROM @tblLogin WHERE patstatus = 'Active'
			update userregistration set otpno = @p_otpno Where id = @userid

			DECLARE @tblLogin1 AS TABLE 
			(userid nvarchar(100),username nvarchar(100),
			gender	nvarchar(100),address	nvarchar(100),city	nvarchar(100),age	nvarchar(100),email	nvarchar(100),mobileno nvarchar(50) ,userstatus nvarchar(100),otp nvarchar(50),	img nvarchar(max))

			if(@p_mobileno!='')
			begin
			insert into @tblLogin1 
				select id
				,firstname +''+ lastname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
				case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno, case when userstatus = 'Inactive' then 0 else 1 end ,otpno,
				case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
				from userregistration
				Where userpwd = @p_password and contactno = @p_mobileno 			
			end
			else
			begin
				insert into @tblLogin1 
				select id
				,firstname +''+ lastname,isnull(gender,'') as gender,isnull(address,'') as address,isnull(city,'') as city,
				case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail,contactno,case when userstatus = 'Inactive' then 0 else 1 end ,otpno,
				case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
				from userregistration
				Where useremail = @p_email and userpwd = @p_password 
			end

			set @userid = (select userid from @tblLogin1)

			SELECT * FROM @tblLogin1
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Inactive Account' )			
		END
	ELSE
		BEGIN
			SELECT * FROM @tblLogin
			SET @p_errorcode = 1
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess' )
		END
	SELECT @p_errorcode AS errorcode, @p_errormessage AS errormessage

END








GO
/****** Object:  StoredProcedure [dbo].[sp_userprofileupdate]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_userprofileupdate]    
 @p_userid bigint,    
 @p_firstname nvarchar(100),     
 @p_lastname nvarchar(100) =null,    
 @p_city nvarchar(100) ,    
 @p_stateid nvarchar(100)=null,     
 @p_countryid nvarchar(100)=null,     
 @p_email nvarchar(100) =null,    
 @p_mobile nvarchar(100) ,    
 @p_imgpath nvarchar(100)=null,    
 @p_deviceregid nvarchar(100)=null,    
 @p_imagename nvarchar(100)=null,    
 @p_authkey nvarchar(100),    
 @p_retid bigint OUT,    
 @p_errorcode bigint OUT,    
 @p_errormessage NVARCHAR(100) OUT    
AS    
Begin    
    
	if isnull(@p_imagename,'') <> '' 
		begin    
			update UserRegistration set firstname=@p_firstname, lastname=@p_lastname, countryid=@p_countryid, stateid=@p_stateid,     
			city=@p_city,patimg = @p_imagename,  contactno=@p_mobile
			where id = @p_userid      
       end
    else
		begin
			update UserRegistration set firstname=@p_firstname, lastname=@p_lastname, countryid=@p_countryid, stateid=@p_stateid,     
			city=@p_city,  contactno=@p_mobile
			where id = @p_userid      		
		end
   select id as userid,firstname as fullname,City,patimg,contactno as mobileno,useremail as emailid from UserRegistration where id = @p_userid    
    
   SET @p_errorcode = 1    
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Update Record')    
    
 SELECT @p_retid as retid, @p_errorcode AS errorcode, @p_errormessage AS errormessage    
End     
    
    
    
    
    
    

GO
/****** Object:  StoredProcedure [dbo].[sp_usersignup]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_usersignup]
	@p_firstname nvarchar(100), 
	@p_lastname nvarchar(100) =null,
	@p_city nvarchar(100) ,
	@p_stateid nvarchar(100)=null, 
	@p_countryid nvarchar(100)=null, 
	@p_email nvarchar(100) =null,
	@p_userpwd nvarchar(100) ,
	@p_mobile nvarchar(100) ,
	@p_deviceregid nvarchar(10),
	@p_authkey nvarchar(10),
	@p_otpno nvarchar(10),
	@p_retid bigint OUT,
	@p_errorcode	bigint OUT,
	@p_errormessage	NVARCHAR(100) OUT
AS
Begin

	declare @Userid bigint

	--//Save Record into Patient Registration Table
	IF (NOT EXISTS (select * from UserRegistration Where contactno = @p_mobile))
		BEGIN
			insert into UserRegistration (firstname, lastname, countryid, stateid, city, contactno, useremail, userpwd, socialtype,  userstatus,otpno,senddatetime,otpstatus) values(
				@p_firstname,@p_lastname,@p_countryid,@p_stateid,@p_city,@p_mobile,@p_email,@p_userpwd,'',  'Inactive',@p_otpno,GETDATE(),'Inactive')

			set @p_retid = (SELECT SCOPE_IDENTITY())

			--// Insert / update record into check authkey table
			IF (NOT EXISTS (select * from checkauthkey Where userid = @Userid and authkeystatus = 'Active'))
				BEGIN
					/*Insert record into checkauthkey*/
					insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@Userid,@p_authkey,'Active',Getdate())
				END
			else
				BEGIN
					/* deactive status if not active */
					update checkauthkey set authkeystatus = 'InActive' Where userid = @Userid

					/*Insert record into checkauthkey*/
					insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@Userid,@p_authkey,'Active',Getdate())
				END

			SET @p_errorcode = 1
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Signup Successfully')
		END
	ELSE
		BEGIN
			SET @p_retid = 0
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'User Exist')
		END
	SELECT @p_retid as retid, @p_errorcode AS errorcode, @p_errormessage AS errormessage
End 









GO
/****** Object:  StoredProcedure [dbo].[sp_userverifyotp]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_userverifyotp]
	@p_userid NVARCHAR(200), 
	@p_otpno NVARCHAR(200), 
	@p_deviceregid nvarchar(10),
	@p_authkey nvarchar(50),
	@p_errorcode	INT OUT,
	@p_errormessage	NVARCHAR(100) OUT			
AS
BEGIN
	DECLARE @tblLogin AS TABLE
	(
		userid nvarchar(100), 
		username nvarchar(100),
		gender	nvarchar(100),
		address	nvarchar(100),
		city	nvarchar(100),
		age	nvarchar(100),
		email	nvarchar(100),
		userstatus nvarchar(100),
		img nvarchar(max)
	)

	Declare @userid nvarchar(100)

	--//update otp status & patient status
	begin
	declare @strupdate nvarchar(max)
		update userregistration set userstatus = 'Active', otpstatus = 'Active' Where id = @p_userid and otpno = @p_otpno
	end
			INSERT INTO @tblLogin
			select id			
			,firstname,isnull(gender,''),isnull(address,''),isnull(city,''),
			case when isnull(dateofbirth,'') = '' then 0 else DATEDIFF(year, dateofbirth, GETDATE()) end age,useremail, case when userstatus = 'Active' Then 1 else 0 end userstatus ,
			case When isnull(patimg,'') = '' then  patimg else 'PatientProfileImg/' + patimg end as patimg
			from userregistration	
			Where id = @p_userid and otpno = @p_otpno

	set @userid = (select userid from @tblLogin)

	IF (EXISTS (select * from userappdevice Where id = @p_deviceregid ))
	BEGIN
		/*update patient id record into patientappdevice*/
		update userappdevice set userid = @userid Where id = @p_deviceregid		
	END
	IF (NOT EXISTS (select * from checkauthkey Where userid = @userid and authkeystatus = 'Active'))
		BEGIN
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@userid,@p_authkey,'Active',Getdate())
		END
	else
		BEGIN
			/* deactive status if not active */
			update checkauthkey set authkeystatus = 'InActive' Where userid = @userid
			/*Insert record into checkauthkey*/
			insert into checkauthkey (userid,authkey,authkeystatus,adddatetime) values (@userid,@p_authkey,'Active',Getdate())
		END
	IF (NOT EXISTS (SELECT 1 FROM @tblLogin))
		BEGIN
			SELECT * FROM @tblLogin WHERE userstatus = '1'
			SET @p_errorcode = 0
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid OTP')
		END
	ELSE
		BEGIN
			SELECT * FROM @tblLogin
			SET @p_errorcode = 1
			SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess' )
	END
	SELECT @p_errorcode AS errorcode, @p_errormessage AS errormessage
END




GO
/****** Object:  StoredProcedure [dbo].[spGenerateID]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
spGenerateID 'uid1','munit','1'
*/
CREATE PROCEDURE [dbo].[spGenerateID]        
 (               
    @sFieldName        sysname,  
    @sTableName        sysname,
    @level nvarchar(20)  
      
  )            
AS       
 BEGIN    
    
EXEC ('select  isnull(max(isnull([' + @sFieldName + '],0)),0)+1   
       from [' + @sTableName + ']  where level = ''' + @level + '''')  
       
       
END  



GO
/****** Object:  StoredProcedure [dbo].[spl_AddAddress]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
CREATE PROCEDURE [dbo].[spl_AddAddress]        
@p_deviceregid nvarchar(100)=null,        
@p_userid int=null,        
@p_authkey nvarchar(50) =null,        
@p_Type nvarchar(25)  = null,        
@p_FlatNo nvarchar(100)  = null,        
@p_Address1 nvarchar(255)  = null,        
@p_Address2 nvarchar(255)  = null,        
@p_City nvarchar(100)  = null,        
@p_State nvarchar(100)  = null,        
@p_Country nvarchar(100)  = null,        
@p_ZipCode nvarchar(10) = NULL,  
@p_MobileNo nvarchar(50)  = null,        
@p_FullName nvarchar(100)  = null,        
@p_DefAddress nvarchar(10) = NULL,  
  
@p_id nvarchar(100) = NULL,        
@p_formmode bigint = 0,        
      
@p_errorcode bigint out,         
@p_errormessage nvarchar(max) out        
AS        
Begin        
 
 if isnull(@p_formmode,0) = 0      
 begin      
       
  insert into userAddress ([userid],[Type],[FlatNo],[Address1],[Address2],[City],[State],[Country],[ZipCode],FullName,MobileNo,DefAddress )         
  values(@p_userid,@p_Type,@p_FlatNo,@p_Address1,@p_Address2,@p_City,@p_State,@p_Country,@p_ZipCode,@p_FullName,@p_MobileNo,@p_DefAddress )        
    
     if(@p_DefAddress=1)  
   begin  
  update userAddress set DefAddress = 0 where userid = @p_userid and id <> @@IDENTITY  
   end  
  
  select * from userAddress where id = @@IDENTITY         
  
 end       
else      
 begin      
  update userAddress set [Type]=@p_Type,[FlatNo]=@p_FlatNo,[Address1]=@p_Address1,[Address2]=@p_Address2,[City]=@p_City,[State]=@p_State      
  ,[Country] = @p_Country,[ZipCode]=@p_ZipCode,FullName = @p_FullName,DefAddress =@p_DefAddress,MobileNo = @p_MobileNo      
  Where id = @p_id and userid = @p_userid      
  
   if(@p_DefAddress=1)  
   begin  
  update userAddress set DefAddress = 0 where userid = @p_userid and id <> @p_id  
   end  
         
  select * from userAddress where id = @p_id         
 end        
 END          

GO
/****** Object:  StoredProcedure [dbo].[spl_AddToCart]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
CREATE PROCEDURE [dbo].[spl_AddToCart]          
@p_deviceregid nvarchar(100)=null,          
@itemid int=null,          
@packid int=null,          
@userid int=null,          
@ItemQty int =null,          
@p_authkey nvarchar(50) =null,          
@p_errorcode bigint out,           
@p_errormessage nvarchar(max) out          
AS          
Begin          
 
 if(isnull(@packid,0)>0)      
 BEGIN      
  if exists(Select * from mcartitem where userid = @userid and packid = @packid and itemid = @itemid)        
  begin           
   update mcartitem set itemqty = ItemQty + 1 where userid = @userid and packid = @packid and itemid = @itemid      
  end        
  else        
  begin  
   if isnull(@ItemQty,0) = 0  
    begin  
     set @ItemQty = 1  
    end          
   INSERT INTO mCartItem(userid,Itemid,packid,ItemQty) values(@userid,@itemid,@packid,@ItemQty)          
  end        
   END      
   ELSE      
   BEGIN      
  if exists(Select * from mcartitem where userid = @userid and itemid = @itemid)        
   begin           
    update mcartitem set itemqty = ItemQty + 1 where userid = @userid and itemid = @itemid        
   end        
  else        
  begin  
     if isnull(@ItemQty,0) = 0  
   begin  
    set @ItemQty = 1  
   end     
   INSERT INTO mCartItem(userid,Itemid,packid,ItemQty) values(@userid,@itemid,@packid,@ItemQty)          
  end        
 END      
 SET @p_errorcode = 1          
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')          
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage          
              
End

GO
/****** Object:  StoredProcedure [dbo].[spl_AddToFav]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spl_AddToFav]
@p_deviceregid nvarchar(100)=null,
@itemid int=null,
@userid int=null,
@favitembool BIT=null,
@p_authkey nvarchar(50) =null,
@p_errorcode bigint out, 
@p_errormessage nvarchar(max) out
AS
Begin

		if(@favitembool = 1)
			INSERT INTO mFavItem(userid,Itemid) values(@userid,@itemid)			
		else
		begin
			DELETE FROM mFavItem WHERE userid =@userid AND Itemid =@Itemid
		end
		select Itemid from mfavitem where userid = @userid
	
End 



GO
/****** Object:  StoredProcedure [dbo].[spl_calculatetax]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- spl_calculatetax '50'    
    
CREATE procedure [dbo].[spl_calculatetax]    
@p_userid nvarchar(10)    
as    
begin    
    
 select userid,
 (select top 1 scharges from mshipingchrg    
 Where DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) <=       
 CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and       
 scdate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )) as DeliveryCharges,
 
 sum(Baseamt) as ItemTotal,sum(discamt) as discamt,    
 sum(case when taxtype =0 then 
 BaseAmt - (cgstamt + sgstamt) - (discamt) else BaseAmt end) as NetAmt,
     
  sum(cgstamt) as cgstamt,sum(sgstamt)as sgstamt,sum(cgstamt+sgstamt) as totalgstamt,
      
  sum (
  
  (case when taxtype =0 then BaseAmt - (cgstamt + sgstamt) else BaseAmt  end)     
  -discamt + cgstamt + sgstamt) 
  
  + (select top 1 scharges from mshipingchrg    
   Where DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) <=       
   CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and       
   scdate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )        
   ) as TotalAmt    
 from    
 (    
 select itemid,packid,userid, Baseamt,taxtype,    
 Case when  discper >0 then (BaseAmt*Discper)/100 else 0 end as discamt,    
 case when taxtype = 0 then     
  round( (case when (cgstper+sgstper) > 0 then     
   (baseamt - (baseAmt / ((cgstper + sgstPer+100)/100)))/2    
   else 0 end),2)    
 else    
 (    
 round( (case when (sgstper) > 0 then     
   (baseamt * sgstper/100)    
   else 0 end),2)    
 )    
 end as sgstamt,    
 case when taxtype = 0 then     
  round((case when (cgstper+sgstper) > 0 then     
   (baseamt - (baseAmt / ((cgstper + sgstPer+100)/100)))/2    
   else 0 end),2)    
 else    
 ( round( (case when (cgstper) > 0 then     
   (baseamt * cgstper/100)    
   else 0 end),2)    
 )    
 end as cgstamt,    
 case when taxtype = 0 then     
  round( (case when (igstper) > 0 then     
   (baseamt - (baseAmt / ((igstper+100)/100)))    
   else 0 end),2)    
 else    
 (    
 round( (case when (igstper) > 0 then     
   (baseamt * igstper/100)    
   else 0 end),2)    
 )    
 end as igstamt    
 from    
   (select mCartItem.userid,mcartitem.Itemid,mCartItem.packid, discper,isnull(mitem.taxtype,0) as taxtype,    
     (case when Isnull(mCartItem.Packid,0) > 0 then     
   mstpackitmlink.Packval else case when isnull(pricedtl.Rate,'') = '' then mitem.salerate else pricedtl.rate end end )* mCartItem.ItemQty as  BaseAmt,          
   case when isnull(pricedtl.sgstper,0)=0 then mitem.sgstper else pricedtl.sgstper end sgstper,      
   case when isnull(pricedtl.cgstper,0)=0 then mitem.cgstper else pricedtl.cgstper end cgstper,      
   case when isnull(pricedtl.igstper,0)=0 then mitem.igstper else pricedtl.igstper end igstper    
   from  mCartItem inner join mitem on mCartItem.Itemid = mitem.itemid     
   inner join munit on mitem.unitid = munit.unitid    
   left join mstpackitmlink on mCartItem.Itemid = mstpackitmlink.itemid and     
   mCartItem.Packid = mstpackitmlink.packid      
   left join       
   (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate        
    from ( select *, row_number() over (        
    partition by [itemid]         
    order by [pldatetime] desc        
    ) as RN        
    From       
   (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,       
  mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate        
  from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level       
  Where DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=       
  CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )  and       
  pricedate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') )        
  ) as A ) X where RN = 1) pricedtl       
  on mitem.itemid = pricedtl.itemid      
  ) B ) C     
  where userid = @p_userid group by userid     
    
end

GO



GO
/****** Object:  StoredProcedure [dbo].[spl_clearcart]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
/*    
select  * from checkauthkey order by id desc    
exec spl_clearcart 2,2,0,'QCJM1BWTMBJZLGY5CPNKV6AYGVTAC1',0,''    
*/    
CREATE procedure [dbo].[spl_clearcart]    
(    
@p_userid int=null,    
@p_itemid int=null,
@p_packid int=null,          
@p_authkey nvarchar(50) =null,          
@p_errorcode bigint out,           
@p_errormessage nvarchar(max) out    
)    
as    
begin    
    
   declare @qtycnt bigint
      
 
 if(isnull(@p_packid,0)>0)  
   BEGIN  
   
   set @qtycnt = (Select itemqty from mcartitem where userid = @p_userid and packid = @p_packid and itemid = @p_itemid)    
  --if exists(Select * from mcartitem where userid = @userid and packid = @packid and itemid = @itemid)    
	  if @qtycnt > 1
		   begin       
			update mcartitem set itemqty = ItemQty - 1 where userid = @p_userid and packid = @p_packid and itemid = @p_itemid  
		   end    
	  else    
		   begin      
			delete from mcartitem where userid = @p_userid and packid = @p_packid and itemid = @p_itemid  
		   end    
	   END  
   ELSE  
   BEGIN  
	  if isnull(@p_itemid,0) > 0    
	  begin    
		  
		  set @qtycnt = (Select itemqty from mcartitem Where itemid = @p_itemid and userid = @p_userid)    
		  if @qtycnt >  1    
		   begin    
				update mcartitem set itemqty = itemqty -1 Where itemid = @p_itemid and userid = @p_userid    
		   end    
		  else    
		  begin    
				delete from mcartitem Where itemid = @p_itemid and userid = @p_userid    
		  end
	 END	      
	 else    
		  begin    
		   delete from mcartitem Where  userid = @p_userid    
		  end      
  end
    
  if @p_itemid > 0  
 begin  
  SET @p_errorcode = 1        
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Delete Data')        
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage        
   end  
    
    
end 

GO
/****** Object:  StoredProcedure [dbo].[spl_CompletedOrder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
  
exec spl_NewOrder '2019-12-30','2019-12-30'  
exec spl_NewOrder '',''  
*/  
  
CREATE procedure [dbo].[spl_CompletedOrder]  
@p_startdate nvarchar(100)  =null,  
@p_enddate nvarchar(100) =null  
As  
  
BEGIN  
  
   
  
  --select sorder.Soid,CustomerName,MobileNo,sorder.totamt as TotalAmt from sorder where ordstatus <> 'Delivered' order by sodate desc  
  
    
 select sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus   
 from sorder   
 inner join sodtl on sorder.soid = sodtl.soid  
 where ordstatus = 'Delivered'  
 and  sodate between @p_startdate and @p_enddate   
 order by sodate desc  
  
  
END  
  

GO
/****** Object:  StoredProcedure [dbo].[spl_Dashboard]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*      
exec dbo.spl_Dashboard      
*/      
CREATE procedure [dbo].[spl_Dashboard]      
As      
begin      
      
Select sum(neworder) as neworder,sum(Completedorder) as Completedorder, sum(Newuser) as Newuser,sum(Collection) as Collection ,sum(CurrentOrders) as CurrentOrders     
from(
      
Select Count(distinct sorder.soid) as NewOrder, 0 as Completedorder,0 as Newuser,0 as Collection  ,0 as CurrentOrders    
from sorder inner join sodtl on sorder.soid = sodtl.soid      
where ordstatus = 'Pending'  
  
union all      
      
Select 0 as NewOrder, Count(*) as Completedorder,0 as Newuser,0 as Collection ,0 as CurrentOrders     
from sorder       
where ordstatus = 'Delivered'      

union all      
      
Select 0 as NewOrder, 0 as Completedorder,0 as Newuser,0 as Collection ,Count(*) as CurrentOrders     
from sorder       
where (ordstatus <> 'Delivered' and ordstatus <> 'Failed' and ordstatus <> 'Pending')

 
union all      
      
Select 0 as NewOrder, 0 as Completedorder,Count(*) as Newuser,0 as Collection ,0 as CurrentOrders     
from userregistration       
where userstatus = 'Active'      
      
union all      
Select 0 as NewOrder, 0 as Completedorder,0 as Newuser,sum(totamt) as Collection ,0 as CurrentOrders     
from Bill       
) as a      
      
SELECT       
 MonthYear, SUM(TotalAmount) AS TotAmt       
FROM       
 (      
  SELECT       
   DATEPART(MM, BillDate) AS MonthNo,      
   DATEPART(YY, BillDate) AS YearNo,      
   CONVERT(NVARCHAR(3),DATENAME(MM, BillDate)) +'-'+ Convert(nvarchar(4),DATEPART(yy, BillDate)) AS MonthYear,      
   SUM(totamt) AS Totalamount      
  FROM Bill       
  WHERE BillDate >= DATEADD(MONTH, -3, GETDATE())         
  GROUP BY BillDate      
) a Group by MonthNo, MonthYear, YearNo      
ORDER BY YearNo ASC, MonthNo ASC        
      
      
SELECT       
 MonthYear, SUM(TotalAmount) AS TotAmt       
FROM       
 (      
  SELECT       
   DATEPART(MM, SoDate) AS MonthNo,      
   DATEPART(YY, SoDate) AS YearNo,      
   CONVERT(NVARCHAR(3),DATENAME(MM, SoDate)) +'-'+ Convert(nvarchar(4),DATEPART(yy, SoDate)) AS MonthYear,      
   SUM(totamt) AS Totalamount      
  FROM sorder       
  WHERE SoDate >= DATEADD(MONTH, -3, GETDATE())         
  GROUP BY SoDate      
) a Group by MonthNo, MonthYear, YearNo      
ORDER BY YearNo ASC, MonthNo ASC      
      
end 

GO
/****** Object:  StoredProcedure [dbo].[spl_DeliveryReport]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

exec spl_NewOrder '2019-12-30','2019-12-30'
exec spl_NewOrder '',''
*/

CREATE procedure [dbo].[spl_DeliveryReport]
@p_startdate nvarchar(100)  =null,
@p_enddate nvarchar(100) =null
As

BEGIN

	

	--select sorder.Soid,CustomerName,MobileNo,sorder.totamt as TotalAmt from sorder where ordstatus = 'Delivered'

	select sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus 
	from sorder 
	inner join sodtl on sorder.soid = sodtl.soid
	where ordstatus = 'Delivered'
	--and  sodate between @p_startdate and @p_enddate 
	order by sodate desc



END




GO
/****** Object:  StoredProcedure [dbo].[spl_Deliverystatushistory]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_Deliverystatushistory]      
@p_delperid bigint,      
@p_soid bigint,      
@p_itemid bigint=null,      
@p_packid bigint=null,      
@p_orderstatus nvarchar(50),      
@p_deldate nvarchar(20),      
@p_deltime nvarchar(20),      
@p_errorcode bigint out,      
@p_errormessage nvarchar(max) out         
as      
begin      
      
 if(@p_packid >0)      
 BEGIN      
  insert into OrderStatusHistory (soid,itemid,packid,orderstatus,delpersonid,deldate,deltime)       
   values(@p_soid,@p_itemid,@p_packid,@p_orderstatus,@p_delperid,@p_deldate,@p_deltime)     
       
   update orderpackageentrydtl set ordstatus = @p_orderstatus,delpersonid = @p_delperid,actualdeldate = @p_deldate,actualdeltime = @p_deltime    
   Where soid = @p_soid and itemid = @p_itemid and packid = @p_packid  and deliverydate = @p_deldate  
              
 END      
 ELSE      
 BEGIN      
  insert into OrderStatusHistory (soid,orderstatus,delpersonid,deldate,deltime)       
   values(@p_soid,@p_orderstatus,@p_delperid,@p_deldate,@p_deltime)      
       
   update sorder set ordstatus = @p_orderstatus where soid = @p_soid    
       
 END      
 SET @p_errorcode = 1              
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')              
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage       
end

GO
/****** Object:  StoredProcedure [dbo].[spl_dellogin]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
spl_dellogin '1234','sumit','',0,'',0
*/
CREATE PROCEDURE [dbo].[spl_dellogin]    
 @p_password NVARCHAR(200),     
 @p_email NVARCHAR(200),     
 @p_deviceid NVARCHAR(max) = null,     
 @p_errorcode INT OUT,    
 @p_errormessage NVARCHAR(100) OUT,    
 @p_delid int out    
AS    
BEGIN    

DECLARE @tblLogin AS TABLE  
 (  
  delid nvarchar(100),   
  delname nvarchar(100),    
  address nvarchar(100),  
  TelNo nvarchar(100),  
MobileNo nvarchar(100),  
Email nvarchar(100),  
PinCode nvarchar(100)  
 )
    
 set @p_delid =( select delid from mstuser inner join mdelboy on mstuser.userid = mdelboy.userid    
    where username = @p_email and userpassword = @p_password and Isactive = 1)    

if isnull(@p_delid,0) > 0
    begin
		update mdelboy set deviceid  = @p_deviceid  where delid = @p_delid       
         insert into @tblLogin 
         select delid,delbyName,isnull(address1,'') + '' + isnull(address2,'') as Address,isnull(TelNo,''),isnull(MobileNo,''),isnull(Email,''),isnull(PinCode,'')
         from mdelboy
         where delid = @p_delid
         select * from @tblLogin
		 SET @p_errorcode = 1    
		 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'LoginSuccess' )    		
    end
else
	begin
	select * from @tblLogin
		SET @p_errorcode = 0  
	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Invalid Login')  	
	end
      

    
 SELECT @p_errorcode AS errorcode, @p_errormessage AS errormessage,@p_delid as delid    
    
END

GO
/****** Object:  StoredProcedure [dbo].[spl_getAddress]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[spl_getAddress]  
@p_deviceregid nvarchar(100)=null,  
@p_userid int=null,  
@p_addid nvarchar(20)=null,  
@p_authkey nvarchar(50) =null,  
@p_errorcode bigint out,   
@p_errormessage nvarchar(max) out ,
@p_defaddress nvarchar(max) out   
AS  
Begin  
set @p_defaddress = ''
 
  select   id,userid,Type,FlatNo,Address1,Address2,City,State,Country,ZipCode,DefAddress,isnull(MobileNo,'') as MobileNo,isnull(FullName,'') as FullName
  from userAddress where userid = @p_userid  and id = @p_addid  
  
  select @p_DefAddress = DefAddress
  from userAddress where userid = @p_userid  and id = @p_addid  

  SET @p_errorcode = 1  
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')  
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage ,@p_defaddress as  DefAddressret
    
   
End   

GO
/****** Object:  StoredProcedure [dbo].[spl_GetCatItem]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
CREATE PROCEDURE [dbo].[spl_GetCatItem]    
--@p_deviceregid int=null,    
@p_authkey nvarchar(50) =null,    
@p_errorcode bigint out,     
@p_errormessage nvarchar(max) out    
AS    
Begin    
  --select itemcatid,itemcatcode,itemcatname,grpimage,itmcattype   
  --from mitemcat    
    
    
  Select mitemcat.itemcatid as itemcatid,itemcatcode,mitemcat.itemcatname as itemcatname,  mitemcat.grpimage, itmcattype  
 from mitemcat   
 Inner join (  
 Select distinct mitem.itemcatid from mitem   
 inner join mitemcat on (mitem.itemcatid = mitemcat.itemcatid)   
 where ISNULL(mitem.delflg,0) = 0 and mitem.itemstatus = 'active'  )   
 item on (mitemcat.itemcatid = item.itemcatid)   
 where isnull(itemcategory,'General') = 'General'
    
End     
  
    

GO
/****** Object:  StoredProcedure [dbo].[spl_GetCatWiseItem]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * From checkauthkey order by id desc        
-- spl_GetCatWiseItem 'ertwet','',20,'AQQ377ZJTZ5SKKRRF2PR1DOUCECEVT',0,''    
              
CREATE PROCEDURE [dbo].[spl_GetCatWiseItem]        
@p_deviceregid nvarchar(100)=null,                
@itemcatid nvarchar(100)=null,                
@userid nvarchar(10) =null,              
@p_authkey nvarchar(50) =null,                
@p_errorcode bigint out,                 
@p_errormessage nvarchar(max) out,      
@p_cartitemret bigint=null out      
AS                
Begin                
        
  declare @tblItem as table(              
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
  ingredients nvarchar(max),nutritioninfo nvarchar(200),itembarcode nvarchar(50),              
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500),favitem nvarchar(5) ,taxtype nvarchar(10),itemqty nvarchar(10) ,review   nvarchar(10)          
  )              
              
  declare @tblItemOffer as table(              
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
  ingredients nvarchar(max),nutritioninfo nvarchar(200),itembarcode nvarchar(50),              
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500),favitem nvarchar(5) ,taxtype nvarchar(10),itemqty nvarchar(10)  ,review   nvarchar(10)            
  )              
              
  declare @tblItempopuler as table(              
  Itemid nvarchar(10),itemcatid nvarchar(10),itemdesc nvarchar(max),              
  ingredients nvarchar(max),nutritioninfo nvarchar(200),itembarcode nvarchar(50),              
  itemname nvarchar(255),itmimgename nvarchar(max),discper nvarchar(10),              
  salerate nvarchar(10),discrate nvarchar(10),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10),              
  itemwt nvarchar(50),unitname nvarchar(100),validfor nvarchar(500),favitem nvarchar(5),taxtype nvarchar(10),itemqty nvarchar(10) ,review   nvarchar(10)        
  )              
              
  declare @MonthName nvarchar(5)               
  set @MonthName = convert(char(3), CONVERT(datetime2, SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') , 1), 0)              
              
  declare @SqlString nvarchar(max)              
              

 if(isnull(@itemcatid,'')='' or @itemcatid = 0)              
 BEGIN              
               
  -- For General Item selection                
               
 set @SqlString = ' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem ,isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty,      
     isnull(reviewitem.review,0) as  review        
     from mitem            
     inner join munit on mitem.unitid = munit.unitid       
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid          
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid                    
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid         
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
  where isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+' = 1 '              
              
         
 Insert into @tblItem exec(@sqlstring)              
               
 select * from @tblItem              
              
 --- Top Offer Item selection              
               
               
set @SqlString ='select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,              
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty ,      
     isnull(reviewitem.review,0) as  review       
     from mitem                 
     inner join munit on mitem.unitid = munit.unitid      
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid                  
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid         
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   WHERE discper >0 and isnull(mitem.itemstatus,''Active'') = ''Active''              
   and '+@MonthName+' = 1  '              
              
  insert into @tblItemOffer exec (@SqlString)              
              
  select * from @tblItemOffer              
              
 -- For populer Item selection              
               
set @SqlString = ' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,               
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem   ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty  ,      
     isnull(reviewitem.review,0) as  review      
     from mitem                   
     inner join munit on mitem.unitid = munit.unitid              
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid                  
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid               
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   where mitem.itemid in (select top 7 itemid from billdtl group by itemid order by sum(qty) desc)              
   and isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+'=1'              
              
  Insert into @tblItempopuler exec(@SqlString)      
          
  select * from @tblItempopuler              
               
 if (Select count(*) from mcartitem WHere userid = @userid) > 0      
 begin      
 set @p_cartitemret = 1      
 end      
 else      
 begin      
 set @p_cartitemret = 0      
 end      
    
  SET @p_errorcode = 1                
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage , @p_cartitemret as cartitemret      
 END                
 ELSE               
   BEGIN              
              
set @SqlString=' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem   ,isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty,      
     isnull(reviewitem.review,0) as  review        
     from mitem                 
     inner join munit on mitem.unitid = munit.unitid              
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid      
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid               
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid                
   WHERE ('+@itemcatid+' is null or itemcatid = '+@itemcatid+') and isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+'=1'              
              
        insert into @tblItem exec(@sqlstring)              
             
 --- Top Offer Item selection              
               
               
set @SqlString ='select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,  
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,              
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem  ,isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty,      
     isnull(reviewitem.review,0) as  review          
     from mitem                 
     inner join munit on mitem.unitid = munit.unitid              
  left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid           
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid              
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid              
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23), SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   WHERE ('+@itemcatid+' is null or itemcatid = '+@itemcatid+') and discper >0 and isnull(mitem.itemstatus,''Active'') = ''Active''              
   and '+@MonthName+' = 1  '              
              
  insert into @tblItemOffer exec (@SqlString)              
              
              
 -- For populer Item selection              
               
set @SqlString = ' select mitem.itemid,itemcatid,itemdesc,ingredients,nutritioninfo,itembarcode,itemname,itmimgename,discper,              
     case when isnull(pricedtl.Rate,'''') = '''' then mitem.salerate else pricedtl.rate end salerate,              
  case when isnull(pricedtl.Rate,'''')='''' then mitem.salerate else (pricedtl.rate - (pricedtl.rate * discper)/100) end DiscRate,  
     case when isnull(pricedtl.sgstper,'''')='''' then mitem.sgstper else pricedtl.sgstper end sgstper,              
     case when isnull(pricedtl.cgstper,'''')='''' then mitem.cgstper else pricedtl.cgstper end cgstper,              
     case when isnull(pricedtl.igstper,'''')='''' then mitem.igstper else pricedtl.igstper end igstper,              
     itemwt,munit.unitname,validfor,               
     case when mfavitem.itemid is null then ''0'' else ''1'' end favitem   ,isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty ,      
     isnull(reviewitem.review,0) as  review         
     from mitem                   
     inner join munit on mitem.unitid = munit.unitid              
     left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid           
     LEFT join (select distinct itemid,userid from mFavItem where userid = '+@userid+') as mfavitem on mitem.itemid = mfavitem.Itemid           
     left join (select Itemid,userid, sum(itemqty) as itemqty from mcartitem WHere userid = '+@userid+' group by Itemid,userid) as cartitem on mitem.itemid = cartitem.itemid                  
     left join               
     (select itemid,cgstper,sgstper,igstper,discper,Mrp,Rate                
   from ( select *, row_number() over (                
      partition by [itemid]                 
      order by [pldatetime] desc                
   ) as RN                
   From               
     (Select DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) as PlDateTime,               
    mprice.priceid as pricelistid,itemid,cgstper,sgstper,igstper,pricedtl.discper,Mrp,Rate                
    from mprice Inner join pricedtl on pricedtl.priceid = mprice.priceid and pricedtl.level = mprice.level               
    Where  DATEADD(day, 0, DATEDIFF(day, 0, pricedate)) + DATEADD(day, 0 - DATEDIFF(day, 0, pricetime), pricetime) <=               
    CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )  and               
    pricedate<=CONVERT(nvarchar(23),  SWITCHOFFSET(SYSDATETIMEOFFSET(), ''+05:30'') )                
    ) as A ) X where RN = 1) pricedtl               
   on mitem.itemid = pricedtl.itemid               
   where ('+@itemcatid+' is null or itemcatid = '+@itemcatid+') and  mitem.itemid in (select top 7 itemid from billdtl group by itemid order by sum(qty) desc)              
   and isnull(mitem.itemstatus,''Active'') = ''Active'' and '+@MonthName+'=1'              
              
  Insert into @tblItempopuler exec(@SqlString)              
             
              
  select * from @tblItem              
  select * from @tblItemOffer              
  select * from @tblItempopuler              
  if (Select count(*) from mcartitem WHere userid = @userid) > 0      
 begin      
  set @p_cartitemret = 1      
 end      
  else      
 begin      
  set @p_cartitemret = 0      
 end      
              
  SET @p_errorcode = 1                
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage , @p_cartitemret as cartitemret      
 END                 
End 

GO
/****** Object:  StoredProcedure [dbo].[Spl_Getcurrentorder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
    
    
*/    
    
CREATE procedure [dbo].[Spl_Getcurrentorder]    
@p_OrderDate nvarchar(25),    
@p_OrderTime nvarchar(20),    
@p_CustId nvarchar(25) ,  
@p_errorcode bigint out,       
@p_errormessage nvarchar(max) out     
As    
Begin    
    
create table #tblItem (soid nvarchar(40),socode nvarchar(50),ordstatus nvarchar(max),sodate nvarchar(25),sotime nvarchar(25),delboyname nvarchar(100))    
    
insert into #tblItem    
select soid,socode,isnull(ordstatus,'Pending') as ordstatus,convert(nvarchar, sodate,103) as sodate,convert(nvarchar, sotime,108)as sotime ,
mdelboy.delbyName + '('+ isnull(mdelboy.MobileNo,'') +')' as delboyname
from sorder
inner join mdelboy on mdelboy.delid = sorder.delpersonid
where custid = @p_CustId and format(sodate,'yyyy-MM-dd') = @p_OrderDate    
and ordstatus <> 'Delivered' and ordstatus <>'Pending' and ordstatus <> 'Failed'    
union all    
select orderpackageentrydtl.soid,socode,isnull(orderpackageentrydtl.ordstatus,'Pending') as ordstatus,convert(nvarchar,orderpackageentrydtl.deliverydate,103),convert(nvarchar,orderpackageentrydtl.deliverytime,108),
mdelboy.delbyName + '('+ isnull(mdelboy.MobileNo,'') +')' as delboyname     
from sorder 
inner join orderpackageentrydtl  on sorder.soid = orderpackageentrydtl.soid     
inner join mdelboy on mdelboy.delid = sorder.delpersonid
where custid = @p_CustId and FORMAT(orderpackageentrydtl.deliverydate,'yyyy-MM-dd') = @p_OrderDate    
and (FORMAT(DATEADD(MINUTE, -30, deliverytime),'HH:mm') >@p_OrderTime or FORMAT(DATEADD(MINUTE, +50, deliverytime),'HH:mm') >@p_OrderTime)       
and isnull(orderpackageentrydtl.ordstatus,'') <> 'Delivered' and isnull(orderpackageentrydtl.ordstatus,'') <>'Pending' and isnull(orderpackageentrydtl.ordstatus,'') <> 'Failed'    
    
    
Select * from #tblItem    
    
  SET @p_errorcode = 1      
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')      
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage      
    
    
END

GO
/****** Object:  StoredProcedure [dbo].[spl_GetDelBoyDetail]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec dbo.spl_Dashboard
*/
CREATE procedure [dbo].[spl_GetDelBoyDetail]
As
begin
		select delid AS ID,delbyName AS Name from  mdelboy
end  

GO
/****** Object:  StoredProcedure [dbo].[spl_Getdelboyorderdtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
exec spl_Getdelboyorderdtl 7242,'2020-02-10','12',0,''    
*/        
CREATE PROCEDURE [dbo].[spl_Getdelboyorderdtl]              
@p_soid nvarchar(50),           
@p_orddate nvarchar(50)  ,  
@p_packid nvarchar(50) = null,  
@p_errorcode bigint out,               
@p_errormessage nvarchar(max) out              
AS              
Begin              
          
declare @tblItem as table(            
  soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),          
sotime nvarchar(100),totamt nvarchar(100),itemname nvarchar(100),qty nvarchar(100),addnote nvarchar(max),packdays nvarchar(200),delpersonid nvarchar(100),          
packname nvarchar(100),addresstype nvarchar(100),address1 nvarchar(max),address2 nvarchar(max),packid nvarchar(100),itemid nvarchar(100),orderstatus nvarchar(100),
paymentdetails nvarchar(100)          
 )            
  
if isnull(@p_packid,0)<=0  
 begin  
 insert into @tblItem        
 Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,        
 convert(nvarchar,sotime,108) as sotime,sorder.totamt,mitem.itemname,sodtl.qty,sodtl.addnote          
 ,  0 as packdays,          
 sorder.delpersonid,'' as packname,userAddress.Type as addresstype,          
 case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +          
 case when isnull(address1,'') = '' then  '' else address1 + ',' end +          
 case when isnull(address2,'') = '' then  '' else address2 + ',' end as address1,          
           
 case When isnull(city,'') = '' then  '' else city + '' end +          
 case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +          
 case when isnull(state,'') = '' then  '' else state + '' end as address2          
 ,sodtl.packid,sodtl.itemid,      
 sorder.ordstatus as orderstaus , 
 case  when paymenttype = 0 then 'Cash'
 when paymenttype = 1 then 'Card'
 when paymenttype = 2 Then 'Wallet' else 'Cash' End as paymentdetails
  
 from sorder          
 inner join sodtl on sodtl.soid = sorder.soid          
 inner join mitem on mitem.itemid = sodtl.itemid   
 inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid           
 where sorder.soid = @p_soid    and isnull(sodtl.packid,0)<=0  
 end  
else  
 begin  
  insert into @tblItem        
   Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,        
   convert(nvarchar,sotime,108) as sotime,sorder.totamt,mitem.itemname,sodtl.qty,sodtl.addnote          
   ,  case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays,          
   sorder.delpersonid,isnull(mstpack.packname,'') as packname,userAddress.Type as addresstype,          
   case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +          
   case when isnull(address1,'') = '' then  '' else address1 + ',' end +          
   case when isnull(address2,'') = '' then  '' else address2 + ',' end as address1,          
             
   case When isnull(city,'') = '' then  '' else city + '' end +          
   case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +          
   case when isnull(state,'') = '' then  '' else state + '' end as address2          
   ,sodtl.packid,sodtl.itemid,      
   isnull(packstatus.packordstatus,'Pending') as orderstaus  ,   
   case when paymenttype = 0 then 'Cash'
   when paymenttype = 1 then 'Card'
   when paymenttype = 2 Then 'Wallet' else 'Cash' End as paymentdetails
      
   from sorder          
   inner join sodtl on sodtl.soid = sorder.soid          
   inner join mitem on mitem.itemid = sodtl.itemid   
   inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid          
     
   Inner join mstpack on sodtl.packid = mstpack.id               
   Inner join (          
   select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus        
   from orderpackageentrydtl          
   inner join mstpack on mstpack.id =  orderpackageentrydtl.packid          
   Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered' and orderpackageentrydtl.packid = @p_packid         
   group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus      
   ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid        
         
   Inner join (        
   select soid ,orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus      
   from orderpackageentrydtl        
   inner join mstpack on mstpack.id =  orderpackageentrydtl.packid    
   and orderpackageentrydtl.deliverydate = @p_orddate and  orderpackageentrydtl.packid = @p_packid  
   ) as packstatus on packstatus.soid = sorder.soid and sodtl.packid = packstatus.packid        
    where sorder.soid = @p_soid  and mstpack.id = @p_packid  
         
 end  
   
 select * from @tblItem    where orderstatus <> 'Delivered'        
            
  SET @p_errorcode = 1              
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')              
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage              
 END

GO
/****** Object:  StoredProcedure [dbo].[spl_GetdelboyorderdtlNew]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*              
exec spl_GetdelboyorderdtlNew 7261,'2020-02-18','0',0,''              
*/              
CREATE PROCEDURE [dbo].[spl_GetdelboyorderdtlNew]            
@p_soid nvarchar(50),                     
@p_orddate nvarchar(50)  ,            
@p_packid nvarchar(50) = null,            
@p_errorcode bigint out,                     
@p_errormessage nvarchar(max) out                    
AS                    
Begin                    
                
 declare @tblItem as table(                  
 soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),                
 sotime nvarchar(100),delpersonname nvarchar(500),                
 addresstype nvarchar(100),address1 nvarchar(max),address2 nvarchar(max),            
 netamt  nvarchar(100),deliverycharge nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100),orderstatus nvarchar(100),paymentType nvarchar(50),        
 packid nvarchar(100)                
  )                  
                 
 declare @tblItemdtl as table(                  
 soid nvarchar(100),orderno nvarchar(100),itemname nvarchar(100),qty nvarchar(100),addnote nvarchar(max),packdays nvarchar(200),          
 packname nvarchar(100),packid nvarchar(100),itemid nvarchar(100),            
 netamt  nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100) ,orderstatus nvarchar(100),deliverydate nvarchar(100),deliverytime nvarchar(100)              
  )                  
        
if isnull(@p_packid,0)<=0            
begin            
  insert into @tblItem              
    Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,              
  convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,                
  case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +                
  case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +                
  case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,            
  case When isnull(city,'') = '' then  '' else city + '' end +                
  case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +                
  case when isnull(state,'') = '' then  '' else state + '' end as address2                
  ,sorder.totamt - sorder.deliverycharge + sorder.discamt - (sodtl.sgstamt+sodtl.cgstamt)  as netamt,sorder.deliverycharge,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
   sorder.totamt as totamt,sorder.ordstatus,          
  case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment,'0' as packid          
  from sorder                
  inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl where isnull(sodtl.packid,0)<=0 group by soid) sodtl on sodtl.soid = sorder.soid                
  inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid                
  left join mdelboy on  sorder.delpersonid = mdelboy.delid                    
  where sorder.soid = @p_soid  --and isnull(sodtl.packid,0)<=0            
           
           
  insert into @tblItemdtl           
  Select sorder.soid,sorder.socode as orderno,mitem.itemname,sodtl.qty,sodtl.addnote                
  ,'' as packdays, '' packname,                
  sodtl.packid,sodtl.itemid,sodtl.netrate as netamt,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
  sodtl.totamt ,sorder.ordstatus ,Convert(nvarchar,sodate,103) as deliverydate,        
  convert(nvarchar,sotime,108) as deliverytime        
  from sorder                
  inner join sodtl on sodtl.soid = sorder.soid                
  inner join mitem on mitem.itemid = sodtl.itemid                
          
     where sorder.soid = @p_soid    and isnull(sodtl.packid,0)<=0         
             
   select * from @tblItem where  packid<=0        
   select * from @tblItemdtl     
 end        
 else        
 begin        
        
  insert into @tblItem              
  --Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,              
  --convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,                
  --case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +                
  --case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +                
  --case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,            
  --case When isnull(city,'') = '' then  '' else city + '' end +                
  --case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +                
  --case when isnull(state,'') = '' then  '' else state + '' end as address2                
  --,(sodtl.qty * sodtl.grate) as netamt,sorder.deliverycharge,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
  --(sodtl.grate + sorder.deliverycharge - sorder.discamt) as totamt,sorder.ordstatus,          
  --case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment ,'0' as  packid        
  --from sorder                
  --inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl where isnull(sodtl.packid,0)>0 group by soid) sodtl on sodtl.soid = sorder.soid                
  --inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid                
  --left join mdelboy on  sorder.delpersonid = mdelboy.delid        
            
  --where sorder.soid = @p_soid  --and mstpack.id = @p_packid            


    Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,              
  convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,                
  case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +                
  case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +                
  case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,            
  case When isnull(city,'') = '' then  '' else city + '' end +                
  case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +                
  case when isnull(state,'') = '' then  '' else state + '' end as address2                
  ,sorder.totamt - sorder.deliverycharge + sorder.discamt - (sodtl.sgstamt+sodtl.cgstamt)  as netamt,sorder.deliverycharge,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
   sorder.totamt as totamt,sorder.ordstatus,          
  case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment,'0' as packid          
  from sorder                
  inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl where isnull(sodtl.packid,0)>0 group by soid) sodtl on sodtl.soid = sorder.soid 
  inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid                
  left join mdelboy on  sorder.delpersonid = mdelboy.delid          
    where sorder.soid = @p_soid  --and mstpack.id = @p_packid            
           
  insert into @tblItemdtl           
  Select sorder.soid,sorder.socode as orderno,mitem.itemname,sodtl.qty,sodtl.addnote                
  ,case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays,                
  isnull(mstpack.packname,'') as packname,                
  sodtl.packid,sodtl.itemid,(sodtl.netrate) as netamt,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,            
  sodtl.totamt  ,isnull(packstatus.packordstatus,'Pending') as orderstaus  ,        
  Convert(nvarchar,packstatus.deliverydate,103) as deliverydate,        
  Convert(nvarchar,packstatus.deliverytime,108) as deliverytime        
  from sorder                
  inner join sodtl on sodtl.soid = sorder.soid                
  inner join mitem on mitem.itemid = sodtl.itemid                
  left join mstpack on sodtl.packid = mstpack.id                 
  left join (                
  select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus              
  from orderpackageentrydtl                
  inner join mstpack on mstpack.id =  orderpackageentrydtl.packid                
  Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered'                
  group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus            
  ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid             
  left join (              
  select soid ,orderpackageentrydtl.packid,orderpackageentrydtl.itemid  ,orderpackageentrydtl.ordstatus as packordstatus ,        
  deliverydate,deliverytime           
  from orderpackageentrydtl              
  inner join mstpack on mstpack.id =  orderpackageentrydtl.packid              
  and orderpackageentrydtl.deliverydate = @p_orddate            
  ) as packstatus on packstatus.soid = sorder.soid and sodtl.itemid = packstatus.itemid and sodtl.packid = packstatus.packid              
     where sorder.soid = @p_soid    and mstpack.id = @p_packid               
        
   select * from @tblItem          
   select * from @tblItemdtl        
           
 end        
         
        
                  
  SET @p_errorcode = 1                    
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')               
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage                    
 END

GO
/****** Object:  StoredProcedure [dbo].[spl_Getdelboyorderlist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*              
spl_Getdelboyorderlist '12','2020-03-02',0,''                
*/              
                
CREATE PROCEDURE [dbo].[spl_Getdelboyorderlist]                  
@p_delboyid nvarchar(50),            
@p_orddate nvarchar(50)  ,                
@p_errorcode bigint out,                   
@p_errormessage nvarchar(max) out                  
AS                  
Begin                  
                  
  declare @tblItem as table(                
  soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),        
  customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),        
  sotime nvarchar(100),totamt nvarchar(100),delpersonid nvarchar(100),         
  ordstatus  nvarchar(100),deliverydate nvarchar(100),deliverytime nvarchar(100),    
  Module nvarchar(100),packdays nvarchar(100),packid nvarchar(100),deliverytimesort datetime,ImageName nvarchar(100), orddate datetime)    
                
insert into @tblItem               
Select soid,orderno,custid,customername,mobileno,sodate,sotime,              
 totamt,delpersonid,ordstatus,delidate,delitime,Module,packdays,packid,sotimesort,ImageName  , orddate  
 from     
(    
  Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,mobileno,      
 Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,              
 sorder.totamt,sorder.delpersonid,sorder.ordstatus,Convert(nvarchar,sodate,103) as delidate,Convert(nvarchar,sotime,108) as delitime,  
 'Normal' as Module ,'0' as packdays ,0 as packid,sotime as sotimesort,mitem.itmimgename as ImageName , sodate as orddate   
 from sorder inner join (select * from (SELECT soid,itemid,packid,ROW_NUMBER() OVER (PARTITION BY soid ORDER BY itemid asc) AS rn FROM sodtl ) a where rn = 1) as sodtl on sorder.soid = sodtl.soid    
 inner join mitem on sodtl.itemid = mitem.itemid
   where isnull(packid,0) <= 0  and sorder.delpersonid = @p_delboyid and ordstatus <> 'Pending'    
     
  union    
    
  Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,mobileno,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,              
 sorder.totamt,sorder.delpersonid  ,             
 case When sorder.ordstatus ='Accept'  then sorder.ordstatus  else isnull(packstatus.packordstatus,'Pending')  end as orderstaus ,      
 Convert(nvarchar,packstatus.deliverydate,103) as delidate, Convert(nvarchar,packstatus.deliverytime,108) as delitime ,'Package' as Module    
 ,  case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays     
 ,sodtl.packid, packstatus.deliverytime as deliverytimesort,mitem.itmimgename as ImageName  , sodate as orddate  
 from sorder    
 inner join (select distinct soid,packid,itemid from sodtl where isnull(packid,0) >= 0 ) as sodtl on sodtl.soid = sorder.soid    
 inner join mitem on sodtl.itemid = mitem.itemid    
 inner join mstpack on sodtl.packid = mstpack.id               
 inner join (              
 select Distinct soid ,orderpackageentrydtl.packid ,orderpackageentrydtl.ordstatus as packordstatus,deliverydate,deliverytime     
 from orderpackageentrydtl    
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid    
 and orderpackageentrydtl.deliverydate = @p_orddate            
 ) as packstatus on packstatus.soid = sorder.soid and sodtl.packid = packstatus.packid     
 left join (            
 select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus          
 from orderpackageentrydtl            
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid            
 Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered'            
 group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus        
 ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid          
           
   where sorder.delpersonid = @p_delboyid and ordstatus <> 'Pending'        
    ) a      
  --where sodate = @p_orddate  
    
  select * from @tblItem where orddate = @p_orddate           
                
   SET @p_errorcode = 1                  
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                  
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage                  
 END 

GO
/****** Object:  StoredProcedure [dbo].[spl_GetDelDevicedata]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spl_GetDelDevicedata]  
@p_delid bigint,  
@p_errorcode int out,  
@p_errormessage nvarchar(100) out  
as   
BEGIN  
 select delid,deviceid from mdelboy where delid = @p_delid  
 SET @p_errorcode = 1              
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')              
   
END

GO

/****** Object:  StoredProcedure [dbo].[spl_getoiderlist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*            
select * from checkauthkey            
exec spl_getoiderlist 50,'2020-02-07','UYTKPWJ8BGQKWM3WNXQZLT5DH0OIYR',0,''            
*/        
              


CREATE PROCEDURE [dbo].[spl_getoiderlist]              
@p_custid nvarchar(50),            
@p_orddate nvarchar(50),        
@p_authkey nvarchar(50) =null,              
@p_errorcode bigint out,               
@p_errormessage nvarchar(max) out              
AS              
Begin       
  Create table #temptbl(soid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),socode nvarchar(100),            
  sodate nvarchar(100),sotime nvarchar(100),discamt nvarchar(50),totamt nvarchar(max),advamt nvarchar(100),deliverydate nvarchar(100),            
  deliverycharge nvarchar(100),deliverytype  nvarchar(100),ordstatus   nvarchar(100),    
  custid nvarchar(100),paymentdetails nvarchar(100),Module nvarchar(100),packdays nvarchar(100),    
  packid nvarchar(100),packname nvarchar(100),deliverytime nvarchar(100),itemname nvarchar(max),ImageName nvarchar(max))            
    
  Create table #temptblpast(soid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),socode nvarchar(100),            
  sodate nvarchar(100),sotime nvarchar(100),discamt nvarchar(50),totamt nvarchar(max),advamt nvarchar(100),deliverydate nvarchar(100),            
  deliverycharge nvarchar(100),deliverytype  nvarchar(100),ordstatus   nvarchar(100),    
  custid nvarchar(100),paymentdetails nvarchar(100),Module nvarchar(100),packdays nvarchar(100),    
  packid nvarchar(100),packname nvarchar(100),deliverytime nvarchar(100),itemname nvarchar(max),ImageName nvarchar(max) , orderdate datetime)        
                 
        
  insert into  #temptbl         
    select soid,customername,mobileno,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,discamt, totamt, advamt,    
 Convert(nvarchar,deliverydate,103) as deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,    
 packname,Convert(nvarchar,deliverytime,108) as deliverytime,itemname  ,ImageName    
    from ordermainlist    
    Where custid = @p_custid and deliverydate = @p_orddate and ordstatus <> 'Delivered'    
    
  insert into  #temptblpast         
    select top 10 soid,customername,mobileno,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,discamt, totamt, advamt,    
 Convert(nvarchar,deliverydate,103) as deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,    
 packname,Convert(nvarchar,deliverytime,108) as deliverytime,itemname ,ImageName ,  sodate + '' + sotime   
    from ordermainlist    
    Where custid = @p_custid and ordstatus = 'Delivered'    
    order by soid desc         
           
   select * from #temptbl    
   select * from #temptblpast 
   SET @p_errorcode = 1                    
   SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                    
   SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage                    
                
End 
GO
/****** Object:  StoredProcedure [dbo].[spl_GetOrderDevicedata]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spl_GetOrderDevicedata]
@p_SOId bigint,
@p_errorcode int out,
@p_errormessage nvarchar(100) out
as 
BEGIN
	select Soid,Deviceid from sorder where soid = @p_soid
	SET @p_errorcode = 1            
	SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')            
 
END

GO
/****** Object:  StoredProcedure [dbo].[spl_getorderdtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
spl_getorderdtl '11'  
*/  
CREATE procedure [dbo].[spl_getorderdtl]  
(  
@p_soid nvarchar(100)  
)  
as   
begin  
  
 select sorder.soid,custid,socode,customername,useraddress.MobileNo,Convert(nvarchar,sorder.sodate,103) + ' ' + Convert(nvarchar,sotime,108) as sodate,Convert(nvarchar,sotime,108) as sotime,
 deliverydate,totamt,taxtype ,   
 type + '-' + Flatno + ',' + Address1 + ',' + Address2 + '(' + ZipCode +')' as address,city,state ,
 case 
 when paymenttype = 0 then 'Cash'
 when paymenttype = 1 then 'Card'
 when paymenttype = 1 then 'Wallet' end as paymentstatus,case when isnull(packAvail,'')='' then 'No' else PackAvail end as packavailable
 from sorder    
 inner join userAddress on userAddress.id = sorder.DelAddressid  
 left join (select distinct soid,case when packid>0 then 'Yes' else 'No' end as packAvail from sodtl where packid >0 ) sodtl on sorder.soid = sodtl.soid 
 Where isnull(sorder.ordstatus,'Pending') = 'Pending' and sorder.soid = @p_soid  
  
  Select sorder.soid,sodtl.itemid,sorder.socode,Convert(nvarchar,sorder.sodate,103) as sodate,sorder.custid,customername, mitemcat.itemcatid,mitem.itemname,unitname,sodtl.qty,   
  sodtl.sodtlid, sodtl.grate, sodtl.discperc, sodtl.discamt, sodtl.netrate,  sodtl.cgstperc, sodtl.cgstamt, sodtl.sgstperc, sodtl.sgstamt,     
  sodtl.igstperc, sodtl.igstamt, sodtl.totamt, sodtl.netrate,mitem.DefQty,mitemcat.itemcatname ,isnull(mstpack.packname,'') as packagename 
  ,  Convert(nvarchar,packord.deliverydate,103) as deliverydate,Convert(nvarchar,packord.deliverytime,108) as deliverytime
  from sorder
  inner join sodtl on (sorder.soid = sodtl.soid)    
  inner join mitem on (sodtl.itemid = mitem.itemid) 
  inner join mitemcat on (mitem.itemcatid = mitemcat.itemcatid) 
  inner join munit on (mitem.unitid = munit.unitid)      
  left join mstpack on (mstpack.id = sodtl.packid)
  left join (select orderpackageentrydtl.itemid,orderpackageentrydtl.packid,orderpackageentrydtl.soid,min(orderpackageentrydtl.deliverydate) as deliverydate,min(orderpackageentrydtl.deliverytime) as deliverytime from orderpackageentrydtl,sodtl 
  where orderpackageentrydtl.itemid = sodtl.itemid and  orderpackageentrydtl.packid = sodtl.packid  and  orderpackageentrydtl.soid = sodtl.soid group by orderpackageentrydtl.itemid,orderpackageentrydtl.packid,orderpackageentrydtl.soid) as packord 
  on sorder.soid = packord.soid and  sodtl.itemid=packord.itemid   and sodtl.packid =packord.packid 
  where sorder.soid =   @p_soid  
  order by sorder.soid,sodtl.sodtlid   
end  

GO
/****** Object:  StoredProcedure [dbo].[spl_Getorderlistdtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*      
exec spl_Getorderlistdtl 7344,'2020-03-11',0,''      
*/      
CREATE PROCEDURE [dbo].[spl_Getorderlistdtl]    
@p_soid nvarchar(50)=null,    
@p_orddate nvarchar(50)=null,    
@p_errorcode bigint out,             
@p_errormessage nvarchar(max) out            
AS            
Begin            
        
 declare @tblItem as table(          
 soid nvarchar(100),orderno nvarchar(100),custid nvarchar(100),customername nvarchar(100),mobileno nvarchar(100),sodate nvarchar(100),        
 sotime nvarchar(100),delpersonname nvarchar(500),        
 addresstype nvarchar(100),address1 nvarchar(max),address2 nvarchar(max),    
 netamt  nvarchar(100),deliverycharge nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100),orderstatus nvarchar(100),paymentType nvarchar(50)        
  )          
         
 declare @tblItemdtl as table(          
 soid nvarchar(100),orderno nvarchar(100),itemname nvarchar(100),qty nvarchar(100),addnote nvarchar(max),packdays nvarchar(200),  
 packname nvarchar(100),packid nvarchar(100),itemid nvarchar(100),    
 netamt  nvarchar(100),discamt nvarchar(100),Tax nvarchar(100),totamt nvarchar(100)       
  )          
    
 insert into @tblItem      
 Select sorder.soid,sorder.socode as orderno,sorder.custid,sorder.customername,sorder.mobileno,Convert(nvarchar,sodate,103) as sodate,      
 convert(nvarchar,sotime,108) as sotime,mdelboy.delbyname,userAddress.Type as addresstype,        
 case When isnull(Flatno,'') = '' then  '' else Flatno + ',' end +        
 case when isnull(userAddress.address1,'') = '' then  '' else userAddress.address1 + ',' end +        
 case when isnull(userAddress.address2,'') = '' then  '' else userAddress.address2 + ',' end as address1,    
 case When isnull(city,'') = '' then  '' else city + '' end +        
 case when isnull(zipcode,'') = '' then  '' else  ' (' + zipcode + ') ' end +        
 case when isnull(state,'') = '' then  '' else state + '' end as address2        
 ,sorder.totamt - sorder.deliverycharge + sorder.discamt - (sodtl.sgstamt+sodtl.cgstamt) as netamt,sorder.deliverycharge,sorder.discamt,
 (sodtl.sgstamt+sodtl.cgstamt)as Tax,    
 sorder.totamt as totamt,sorder.ordstatus,  
 case when paymenttype = 0 then 'Cash' when paymenttype =1 then 'Card' when paymenttype=2 then 'Wallet' end as Payment  
 from sorder        
 inner join (select soid,sum(grate) as grate,sum(sgstamt) as sgstamt,sum(cgstamt)as cgstamt from sodtl group by soid) sodtl on sodtl.soid = sorder.soid        
 inner join userAddress on userAddress.id = sorder.DelAddressid and userAddress.userid = sorder.custid        
 left join mdelboy on  sorder.delpersonid = mdelboy.delid
 where sorder.soid = @p_soid      
  
  
 insert into @tblItemdtl   
 Select sorder.soid,sorder.socode as orderno,mitem.itemname,sodtl.qty,sodtl.addnote        
 ,case When sodtl.packid > 0 then  'Day '+ Convert(nvarchar,isnull(pack.packdeldays,0)) + ' of ' + Convert(nvarchar,isnull(mstpack.packdays,0)) else '' end as packdays,        
 isnull(mstpack.packname,'') as packname,        
 sodtl.packid,sodtl.itemid,sodtl.netrate as netamt,sorder.discamt,(sodtl.sgstamt+sodtl.cgstamt)as Tax,    
 sodtl.totamt as totamt  
 from sorder        
 inner join sodtl on sodtl.soid = sorder.soid        
 inner join mitem on mitem.itemid = sodtl.itemid        
 left join mstpack on sodtl.packid = mstpack.id         
 left join (        
 select soid , count(*) as packdeldays,mstpack.packdays , orderpackageentrydtl.packid  ,orderpackageentrydtl.ordstatus as packordstatus      
 from orderpackageentrydtl        
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid        
 Where isnull(orderpackageentrydtl.ordstatus,'') = 'Delivered'        
 group by soid,mstpack.packdays,orderpackageentrydtl.packid    ,orderpackageentrydtl.ordstatus    
 ) as pack on pack.soid = sorder.soid   and sodtl.packid = pack.packid     
 left join (      
 select soid ,orderpackageentrydtl.packid,orderpackageentrydtl.itemid  ,orderpackageentrydtl.ordstatus as packordstatus    
 from orderpackageentrydtl      
 inner join mstpack on mstpack.id =  orderpackageentrydtl.packid      
 and orderpackageentrydtl.deliverydate = @p_orddate    
 ) as packstatus on packstatus.soid = sorder.soid and sodtl.itemid = packstatus.itemid and sodtl.packid = packstatus.packid      
 where sorder.soid = @p_soid      
  
   select * from @tblItem  
             
   select * from @tblItemdtl          
          
  SET @p_errorcode = 1            
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')            
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage            
 END

GO
/****** Object:  StoredProcedure [dbo].[spl_getpackageitemlist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                      
select * from checkauthkey                      
exec spl_getpackageitemlist  '11', '','50','0U6Y608YH1WHKEXDW9LWQX6GANVD79',0,''                      
*/                      
                        
CREATE PROCEDURE [dbo].[spl_getpackageitemlist]                        
@p_packid nvarchar(50) =null,                     
@p_itemid nvarchar(50) =null,          
@p_userid nvarchar(50) =null,                     
@p_authkey nvarchar(50) =null,                        
@p_errorcode bigint out,                         
@p_errormessage nvarchar(max) out ,        
@p_cartitemret bigint=null out                       
AS                        
Begin                        
  Create table #temptbl(                    
 packid nvarchar(100),itemid nvarchar(100),itemcatid nvarchar(100),                    
 itemname nvarchar(100),itemdesc nvarchar(max),itmimgename nvarchar(100),                    
 itembarcode nvarchar(100),ingredients nvarchar(max),nutritioninfo nvarchar(max),                    
 discper nvarchar(10),itemwt nvarchar(25),validfor nvarchar(100),unitname nvarchar(100),                    
 salerate nvarchar(100),sgstper nvarchar(10),cgstper nvarchar(10),igstper nvarchar(10) ,packname nvarchar(200)           
 ,favitem nvarchar(10) ,taxtype nvarchar(10) , itemqty nvarchar(100),review  nvarchar(100))                      
                          
                    
if isnull(@p_itemid,'') = ''                
 begin                
                
    insert into  #temptbl                        
    Select mstpackitmlink.packid, mstpackitmlink.itemid,mitem.itemcatid, mitem.itemname,mitem.itemdesc,mitem.itmimgename,                    
    mitem.itembarcode,mitem.ingredients,mitem.nutritioninfo,0 as discperc,mitem.itemwt,mitem.validfor,                    
    munit.unitname,packval as salerate,mitem.sgstper,mitem.cgstper,mitem.igstper,mstpack.packname ,          
    case when mfavitem.itemid is null then '0' else '1' end favitem ,          
    isnull(mitem.taxtype,0) as taxtype  ,isnull(cartitem.itemqty,0) as itemqty ,        
    isnull(reviewitem.review,0) as  review                    
    from mstpackitmlink                    
    inner join mitem on mitem.itemid =   mstpackitmlink.itemid                    
    inner join munit on mitem.unitid = munit.unitid             
    left join ( Select avg(Review) as review,itemid  from mReviewItem Where userid = @p_userid  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid           
    inner join mstpack on mstpack.id = mstpackitmlink.packid          
    left join (select Itemid,userid, sum(itemqty) as itemqty,packid from mcartitem           
    WHere userid = @p_userid and packid = @p_packid group by Itemid,userid,packid) as cartitem on mitem.itemid = cartitem.itemid     
    LEFT join (select distinct itemid,userid from mFavItem where userid = @p_userid) as mfavitem on mstpackitmlink.itemid = mfavitem.Itemid                  
    where mstpackitmlink.packid = @p_packid  and mitem.itemstatus = 'Active'                  
                 
 end                
else                
 begin                
                
    insert into  #temptbl                        
    Select mstpackitmlink.packid, mstpackitmlink.itemid,mitem.itemcatid, mitem.itemname,mitem.itemdesc,mitem.itmimgename,                    
    mitem.itembarcode,mitem.ingredients,mitem.nutritioninfo,0 as discperc,mitem.itemwt,mitem.validfor,                    
    munit.unitname,packval as salerate,mitem.sgstper,mitem.cgstper,mitem.igstper,mstpack.packname  ,          
    case when mfavitem.itemid is null then '0' else '1' end favitem ,          
    isnull(mitem.taxtype,0) as taxtype ,isnull(cartitem.itemqty,0) as itemqty,        
    isnull(reviewitem.review,0) as  review            
    from mstpackitmlink                    
    inner join mitem on mitem.itemid =   mstpackitmlink.itemid                    
    inner join munit on mitem.unitid = munit.unitid           
 left join ( Select avg(Review) as review,itemid  from mReviewItem Where Review > 3  group by itemid ) as reviewitem on reviewitem.itemid =  mitem.itemid             
    inner join mstpack on mstpack.id = mstpackitmlink.packid           
    left join (select Itemid,userid, sum(itemqty) as itemqty,packid from mcartitem WHere userid = @p_userid and packid = @p_packid group by Itemid,userid,packid) as cartitem on           
    mitem.itemid = cartitem.itemid    
    LEFT join (select distinct itemid,userid from mFavItem where userid = @p_userid) as mfavitem on mstpackitmlink.itemid = mfavitem.Itemid                  
    where mstpackitmlink.packid = @p_packid  and mitem.itemstatus = 'Active'   and mstpackitmlink.itemid = @p_itemid                
                 
 end                
         
   if (Select count(*) from mcartitem WHere userid = @p_userid) > 0        
 begin        
  set @p_cartitemret = 1        
 end        
  else        
 begin        
  set @p_cartitemret = 0        
 end        
         
  select * from #temptbl                      
                      
  SET @p_errorcode = 1                        
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                        
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage, @p_cartitemret as cartitemret                        
                            
End 


GO

/****** Object:  StoredProcedure [dbo].[spl_getshippingcharge]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 /*
 exec spl_getshippingcharge '2020-02-01 10:00','2020-02-01',0,''
 */
create procedure [dbo].[spl_getshippingcharge]
 (
 @p_strcurdtTime_1 nvarchar(100),
 @p_strcurdt_1 nvarchar(100),
 @p_errorcode bigint out,       
 @p_errormessage nvarchar(max) out      
 )
 as
 begin
 
   declare @tblItem as table(    
  pricelistdate nvarchar(100),pricelistid nvarchar(100),scharges nvarchar(100) )    

	insert into @tblItem
	 Select DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) as PlDateTime, mshipingchrg.scid as pricelistid,scharges  
	 from mshipingchrg 
	 Where DATEADD(day, 0, DATEDIFF(day, 0, scdate)) + DATEADD(day, 0 - DATEDIFF(day, 0, sctime), sctime) <= CONVERT(datetime, @p_strcurdtTime_1 , 120)  
	 and scdate<=@p_strcurdt_1
 
   
  select * from @tblItem    
    
    
    
  SET @p_errorcode = 1      
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')      
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage      
 end
 
 
 


GO
/****** Object:  StoredProcedure [dbo].[spl_Itemwiseorder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE procedure [dbo].[spl_Itemwiseorder]
  @p_startdate nvarchar(100),
  @p_enddate nvarchar(100)
  as
  BEGIN
	select socode,convert(nvarchar,sodate,103) as sodate,convert(nvarchar, sotime,108) as sotime,itemname,itemid,customername,soid,Qty,Rate,NetAmt,Discamt,
		TotalAmt,TaxPerc,TaxAmt,totalamount,BillAmt,CustId,OrdStatus 
		from ItemwiseSalesOrderRegister where Sodate between @p_startdate and @p_enddate order by sodate desc
  END

GO
/****** Object:  StoredProcedure [dbo].[spl_listAddress]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spl_listAddress]
@p_deviceregid nvarchar(100)=null,
@p_userid int=null,
@p_authkey nvarchar(50) =null,
@p_errorcode bigint out, 
@p_errormessage nvarchar(max) out
AS
Begin

		select * from userAddress where userid = @p_userid 
	
End 

GO

/****** Object:  StoredProcedure [dbo].[spl_NewOrder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
        
exec spl_NewOrder '2019-12-30','2019-12-30'        
exec spl_NewOrder '',''        
*/        
        
CREATE procedure [dbo].[spl_NewOrder]        
--@p_startdate nvarchar(100)  =null,        
--@p_enddate nvarchar(100) =null        
As        
        
BEGIN        
        
         
        
  --select sorder.Soid,CustomerName,MobileNo,sorder.totamt as TotalAmt from sorder where ordstatus <> 'Delivered' order by sodate desc        
        
          
 select distinct sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus         
 from sorder         
 inner join sodtl on sorder.soid = sodtl.soid        
 where ordstatus = 'Pending'        
 --and  sodate between @p_startdate and @p_enddate         
 order by sodate desc        
        
        
END 

GO
/****** Object:  StoredProcedure [dbo].[spl_OrderSummary]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

exec spl_OrderSummary '2019-12-30','2019-12-30'
exec spl_OrderSummary '',''
*/


CREATE procedure [dbo].[spl_OrderSummary]
@p_startdate nvarchar(100)  =null,
@p_enddate nvarchar(100) =null
As

BEGIN

	

	select sorder.Soid,socode,Convert(nvarchar,sodate,103) as sodate,Convert(nvarchar,sotime,108) as sotime,CustomerName,MobileNo,sorder.totamt as TotalAmt ,ordstatus 
	from sorder 
	inner join sodtl on sorder.soid = sodtl.soid
	--where  sodate between @p_startdate and @p_enddate 
	order by sodate desc

END


GO
/****** Object:  StoredProcedure [dbo].[spl_orderupdate]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_orderupdate]        
@p_soid bigint,        
@p_errorcode bigint out,        
@p_errormessage nvarchar(max) out           
as        
begin        
        
   update sorder set ordstatus = 'Pending' where soid = @p_soid      

 SET @p_errorcode = 1                
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')                
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage         
end

GO
/****** Object:  StoredProcedure [dbo].[spl_removeaddress]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
/*    
select  * from checkauthkey order by id desc    
exec spl_removeaddress 20,17,'XE07QL0NE8N5R0KAWS0GHGS6KADQLA',0,''    
*/    
CREATE procedure [dbo].[spl_removeaddress]    
(    
@p_userid int=null,    
@p_id int=null,          
@p_authkey nvarchar(50) =null,          
@p_errorcode bigint out,           
@p_errormessage nvarchar(max) out    
)    
as    
begin    
    

	delete from userAddress Where id = @p_id and userid = @p_userid    
    
    select * from userAddress where  userid = @p_userid    
       
  SET @p_errorcode = 1        
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Delete Data')        
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage        
    
    
end

GO

/****** Object:  StoredProcedure [dbo].[spl_SalesSummary]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

exec spl_SalesSummary '2019-12-30','2019-12-30'
exec spl_SalesSummary '',''
*/

CREATE procedure [dbo].[spl_SalesSummary]
@p_startdate nvarchar(100)  =null,
@p_enddate nvarchar(100) =null
As

BEGIN

	

	select Billid,billcode,Convert(nvarchar,Billdate,103) as Billdate,COnvert(nvarchar,billtime,108) as BillTime,CustName as CustomerName,MobileNo,totamt as TotalAmt 
	from bill 
	where  Billdate between @p_startdate and @p_enddate 
	order by billdate desc

END



GO
/****** Object:  StoredProcedure [dbo].[spl_saveotploginhistory]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_saveotploginhistory]    
(    
@mode nvarchar(100),    
@mobileno nvarchar(100),    
@otp nvarchar(100),    
@errorcode bigint out,           
@errormessage nvarchar(max) out,  
@retid bigint out     
)    
as    
begin    
 if @mode = '0'    
   begin    
    if Exists (select top 1 * from userregistration where contactno = @mobileno)    
     BEGIN    
   insert into otploginhistory(mobileno,otp,adddatetime,otpstatus)    
   values(@mobileno,@otp,GETDATE(),'Inactive')    
     END    
    Else    
     BEGIN    
   insert into UserRegistration (contactno,otpstatus,userstatus)     
    values (@mobileno,'Active','Active')    
    
   insert into otploginhistory(mobileno,otp,adddatetime,otpstatus)    
    values(@mobileno,@otp,GETDATE(),'Inactive')    
     END    
   end    
 else    
 begin    
 update otploginhistory set otp = @otp Where mobileno = @mobileno and otpstatus = 'Inactive'     
 end    
  
  set @retid = (select top 1 id from userregistration where contactno = @mobileno)  
    
  SET @errorcode = 1                
  SET @errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Fetch Data')                
  SELECT  @errorcode AS errorcode, @errormessage AS ErrorMessage  , @retid as retid  
end 

GO
/****** Object:  StoredProcedure [dbo].[spl_savesodtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spl_savesodtl]          
(          
 @p_sodtlid bigint = NULL,@p_uid1 bigint = NULL,@p_soid bigint = NULL,@p_itemid bigint = NULL,@p_unitid bigint = NULL,@p_itmdeldate datetime = NULL,          
 @p_itemdeltime datetime = NULL,@p_qty float = NULL,@p_grate float = NULL,@p_discperc float = NULL,@p_discamt float = NULL,@p_netrate float = NULL,          
 @p_totamt float = NULL,@p_sgstperc float = NULL,@p_sgstamt float = NULL,@p_cgstperc float = NULL,@p_cgstamt float = NULL,@p_igstperc float = NULL,          
 @p_igstamt float = NULL,@p_Addnote nvarchar(max) = NULL,@p_prclitmyn bit = NULL,@p_uid bigint = NULL,@p_level nvarchar(10) = NULL,          
 @p_addedby bigint = NULL,@p_packid bigint = 0,@p_remarks nvarchar(max) = null,@p_custid bigint = null,@p_deldate datetime,@p_deltime datetime,@p_DeviceId nvarchar(200) = null,      
 @p_errorcode bigint out,@p_errormessage nvarchar(max) out          
)          
as          
Begin          
          
          
    set @p_errormessage=''  set @p_errorcode=0            
 select @p_level = level, @p_uid = uid from registerinfo where infoid = 1           
    set @p_uid1 = (select isnull(max(uid1), 0) + 1  from sodtl where level =@p_level)            
          
 set @p_sodtlid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_uid1)          
          set @p_unitid = (select unitid from mitem where itemid = @p_itemid)
 insert into sodtl (sodtlid,uid1,soid,itemid,unitid,itmdeldate,itemdeltime,qty,grate,discperc,discamt,netrate,totamt,sgstperc,sgstamt,cgstperc,cgstamt,igstperc,igstamt,Addnote,prclitmyn,uid,tflg,level,addedby,addedatetime,packid,deviceid)          
 values ( @p_sodtlid,@p_uid1,@p_soid,@p_itemid,@p_unitid,@p_itmdeldate,@p_itemdeltime,@p_qty,@p_grate,@p_discperc,@p_discamt,@p_netrate,@p_totamt,@p_sgstperc,@p_sgstamt,@p_cgstperc,@p_cgstamt,@p_igstperc,@p_igstamt,@p_Addnote,@p_prclitmyn,@p_uid,0,@p_level,   @p_addedby,getdate(),@p_packid,@p_DeviceId)          
          
         
    if @p_packid > 0      
    begin      
  declare @p_packdays bigint      
  declare @p_packuid1 bigint      
  declare @p_pacordid bigint      
           
    set @p_packdays = (select packdays-1 from mstpack Where id = @p_packid)      
 set @p_packuid1 = (select isnull(max(uid1), 0) + 1  from orderpackageentry where level =@p_level)                
 set @p_pacordid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_packuid1)          
           
  --insert into orderpackageentry(id,uid1,custid,soid,remarks,uid,tflg,level,addedby,addedatetime,deviceid)      
  --values (@p_pacordid,@p_packuid1,@p_custid,@p_soid,@p_remarks,@p_uid,0,@p_level,@p_addedby,getdate(),@p_DeviceId)      
      
  --declare @p_packduid1 bigint      
  --declare @p_packdordid bigint      
           
  --set @p_packduid1 = (select isnull(max(uid1), 0) + 1  from orderpackageentrydtl where level =@p_level)                
  --set @p_packdordid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_packduid1)          
           
  declare @maxdate datetime = Dateadd(day,@p_packdays,@p_deldate)      
           
      ;WITH mycte AS        
   (        
     SELECT @p_deldate as  DateValue      
     UNION ALL        
     SELECT  DateValue + 1       
     FROM    mycte           
     WHERE   DateValue  < @maxdate        
   )          
           
           
   insert into orderpackageentrydtl  (itemid,soid,packid,qty,deliverydate,deliverytime,uid,tflg,level,addedby,addedatetime,deviceid)      
   SELECT  @p_itemid,@p_soid, @p_packid,@p_qty,DateValue,@p_deltime,@p_uid,0,@p_level,@p_addedby,getdate(),@p_DeviceId      
   FROM    mycte        
   OPTION (MAXRECURSION 0)        
        
        
 end      
  
   delete from mCartItem Where  userid = @p_addedby    
           
 SET @p_errorcode = 1            
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')            
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage            
            
end

GO
/****** Object:  StoredProcedure [dbo].[spl_savesorder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from sorder            
            
CREATE procedure [dbo].[spl_savesorder]            
(            
            
 @p_soid float = NULL,@p_uid1 bigint = NULL,@p_custid bigint = NULL,@p_customername nvarchar(50) = NULL,@p_mobileno nvarchar(100) = NULL,            
 @p_socode nvarchar(100) = NULL,@p_socode_1 bigint = NULL,@p_sodate datetime = NULL,@p_sotime datetime = NULL,@p_deliverydays bigint = NULL,            
 @p_reminderdays bigint = NULL,@p_taxtype bigint = NULL,@p_roffamt float = NULL,@p_discamt float = NULL,@p_totamt float = NULL,@p_advamt float = NULL,            
 @p_deliverydate datetime = NULL,@p_dailytrgentype bigint = NULL,@p_deliverycharge float = NULL,@p_deliverytype bigint = NULL,@p_deliverystatus bit = NULL,            
 @p_referenceno nvarchar(20) = NULL,@p_referencedate datetime = NULL,@p_wallettype bigint = NULL,@p_remarks nvarchar(250) = NULL,@p_DelAddressid nvarchar(10) = null,      
 @p_paymenttype bigint = NULL,@p_counterid bigint = NULL,@p_uid bigint = NULL,@p_level nvarchar(10) = NULL,@p_addedby bigint = NULL,@p_DeviceId nvarchar(200) = null,@p_promocode nvarchar(200) = null,      
 @p_errorcode bigint out,@p_errormessage nvarchar(max) out   ,@p_respid bigint out  , @p_retuseremail nvarchar(100)=null out           
)            
as            
begin            
            
    set @p_errormessage=''  set @p_errorcode=0              
    select @p_level = level, @p_uid = uid from registerinfo where infoid = 1             
    set @p_uid1 = (select isnull(max(uid1), 0) + 1  from sorder where level =@p_level)              
    
select @p_retuseremail = useremail from userregistration Where id = @p_custid    
            
  set @p_soid =  Convert(nvarchar,@p_uid)  + Convert(nvarchar,@p_uid1)          
  set @p_socode = @p_soid      
  set @p_socode_1 = @p_uid1      
  insert into sorder (soid, uid1, custid, customername, mobileno, socode, socode_1, sodate, sotime, deliverydays, reminderdays, taxtype, roffamt, discamt, totamt, advamt, deliverydate, dailytrgentype, deliverycharge, deliverytype, deliverystatus, referenceno, referencedate, wallettype, remarks, paymenttype, counterid, uid, level, addedby, addedatetime,tflg,ordstatus,DelAddressid,deviceid,promocode)
  values( @p_soid,@p_uid1,@p_custid,@p_customername,@p_mobileno,@p_socode,@p_socode_1,@p_sodate,@p_sotime,@p_deliverydays,@p_reminderdays,@p_taxtype,@p_roffamt,@p_discamt,@p_totamt,@p_advamt,@p_deliverydate,@p_dailytrgentype,@p_deliverycharge,@p_deliverytype,
  @p_deliverystatus,@p_referenceno,@p_referencedate,@p_wallettype,@p_remarks,@p_paymenttype,@p_counterid,@p_uid,@p_level,@p_addedby,Getdate(),0,case when @p_paymenttype = '0' then 'Pending' else 'Failed' end,@p_DelAddressid,@p_DeviceId,@p_promocode)
            
  set @p_respid = @p_soid              
  SET @p_errorcode = 1              
  SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')              
  SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage ,@p_respid as respid , @p_retuseremail as retuseremail              
            
end

GO
/****** Object:  StoredProcedure [dbo].[spl_updateorderstatus]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

spl_updateorderstatus '','','',''

*/
CREATE procedure [dbo].[spl_updateorderstatus]
(
 @p_ordstatus nvarchar(100),
 @p_delpersonid nvarchar(100),
 @p_userid  nvarchar(100),
 @p_soid nvarchar(100)
 ) as 
 
 begin
 
 Update sorder set  ordstatus =@p_ordstatus, delpersonid =  @p_delpersonid , 
 updatedby =  @p_userid , updateddatetime = CONVERT(datetime2, SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30') , 1), tflg = 1 
 Where soid =  @p_soid
 
 end


GO
/****** Object:  StoredProcedure [dbo].[spl_updateorderstatusold]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spl_updateorderstatusold]        
@p_soid bigint,        
@p_errorcode bigint out,        
@p_errormessage nvarchar(max) out           
as        
begin        

   update sorder set ordstatus = 'Pending' where soid = @p_soid      
         

 SET @p_errorcode = 1                
 SET @p_errormessage = (select messagetext from mstmessagetemplate where messagetype = 'Save Data')                
 SELECT  @p_errorcode AS errorcode, @p_errormessage AS ErrorMessage         
end


GO
/****** Object:  UserDefinedFunction [dbo].[FnGetCompanyIdWithCategories]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[FnGetCompanyIdWithCategories]()
RETURNS  @rtnTable TABLE 
(
    -- columns returned by the function
    ID Bigint NOT NULL,
    Name nvarchar(255) NOT NULL
)
AS
BEGIN

--This select returns data
insert into @rtnTable
select compid,compname from mstcompany
return
END



GO
/****** Object:  UserDefinedFunction [dbo].[GetDirectoryPath]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE FUNCTION [dbo].[GetDirectoryPath] ( @Path NVARCHAR(MAX) ) RETURNS NVARCHAR(MAX) AS BEGIN RETURN LEFT(@Path, LEN(@Path) - CHARINDEX('', REVERSE(@Path))) END  



GO
/****** Object:  UserDefinedFunction [dbo].[ReturnBrcode]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ReturnBrcode]
(@comp_id bigint)
RETURNS VARCHAR(50)

AS

BEGIN

   DECLARE @brcode_name VARCHAR(50);

set @brcode_name = (select compcode from mstcompany where compid = @comp_id);

   RETURN @brcode_name;

END;



GO
/****** Object:  UserDefinedFunction [dbo].[ReturnBrcode1]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ReturnBrcode1]
()
RETURNS VARCHAR(50)

AS

BEGIN

   DECLARE @brcode_name VARCHAR(50);

set @brcode_name = (select compcode from mstcompany );

   RETURN @brcode_name;

END;



GO
/****** Object:  Table [dbo].[bill]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bill](
	[billid] [float] NOT NULL,
	[uid1] [bigint] NULL,
	[billdate] [datetime] NULL,
	[billTime] [datetime] NULL,
	[billcode] [nvarchar](100) NULL,
	[billcode_1] [bigint] NULL,
	[custname] [nvarchar](max) NULL,
	[custid] [bigint] NULL,
	[mobileno] [nvarchar](100) NULL,
	[taxtype] [nvarchar](100) NULL,
	[prclamt] [float] NULL,
	[discount] [float] NULL,
	[totamt] [float] NULL,
	[roffamt] [float] NULL,
	[grossamt] [float] NULL,
	[adddesc] [nvarchar](255) NULL,
	[cardname] [nvarchar](50) NULL,
	[referenceno] [nvarchar](50) NULL,
	[paymentdate] [datetime] NULL,
	[payremarks] [nvarchar](max) NULL,
	[cashamt] [float] NULL,
	[creditpay] [float] NULL,
	[giftcardPay] [float] NULL,
	[walletamt] [float] NULL,
	[wallettype] [bigint] NULL,
	[paidamt] [float] NULL,
	[advamt] [float] NULL,
	[amtrtn] [float] NULL,
	[totalamt] [float] NULL,
	[counterid] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[DelPersonid] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[billid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[billdtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[billdtl](
	[billdtlid] [float] NOT NULL,
	[uid1] [bigint] NULL,
	[billid] [bigint] NULL,
	[itemid] [bigint] NULL,
	[unitid] [bigint] NULL,
	[qty] [float] NULL,
	[grate] [float] NULL,
	[discperc] [float] NULL,
	[discamt] [float] NULL,
	[netrate] [float] NULL,
	[totamt] [float] NULL,
	[sgstperc] [float] NULL,
	[sgstamt] [float] NULL,
	[cgstperc] [float] NULL,
	[cgstamt] [float] NULL,
	[igstperc] [float] NULL,
	[igstamt] [float] NULL,
	[soid] [float] NULL,
	[sodtlid] [float] NULL,
	[schmid] [bigint] NULL,
	[schmdtlid] [bigint] NULL,
	[isvoid] [bit] NULL,
	[prclitmyn] [bit] NULL,
	[Addnote] [nvarchar](max) NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[billdtlid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[createsmstemplate]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[createsmstemplate](
	[SMSTemplateId] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[SMSType] [nvarchar](100) NULL,
	[SMSFor] [nvarchar](100) NULL,
	[SMSEng] [nvarchar](max) NULL,
	[SMSHindi] [nvarchar](max) NULL,
	[SMSRegional] [nvarchar](max) NULL,
	[SMSSubType] [nvarchar](100) NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[SMSTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[emailhistory]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emailhistory](
	[id] [bigint] NOT NULL,
	[senddate] [datetime] NULL,
	[sendtime] [datetime] NULL,
	[emailmessage] [nvarchar](250) NULL,
	[sendflg] [bigint] NULL,
	[emailtempname] [nvarchar](100) NULL,
	[emailtempid] [bigint] NULL,
	[emailsubject] [nvarchar](200) NULL,
	[sendemailid] [nvarchar](200) NULL,
	[ExportPdfPath] [nvarchar](max) NULL,
	[ErrDescription] [nvarchar](max) NULL,
	[uid1] [bigint] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[emailsetting]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emailsetting](
	[Id] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[portalname] [varchar](50) NULL,
	[smtpname] [varchar](150) NULL,
	[portno] [varchar](50) NULL,
	[emailid] [varchar](50) NULL,
	[password] [varchar](150) NULL,
	[receiveemail] [varchar](max) NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](20) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[EnableSSL] [nvarchar](10) NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mCartItem]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mCartItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[Itemid] [int] NOT NULL,
	[ItemQty] [int] NULL,
	[packid] [int] NULL,
 CONSTRAINT [PK_mCartItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mdelboy]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mdelboy](
	[delid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[delbyName] [nvarchar](50) NULL,
	[userid] [bigint] NULL,
	[Isactive] [bit] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[address1] [nvarchar](100) NULL,
	[address2] [nvarchar](100) NULL,
	[TelNo] [nvarchar](100) NULL,
	[MobileNo] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[PinCode] [nvarchar](100) NULL,
	[CityId] [bigint] NULL,
	[StateId] [bigint] NULL,
	[CountryId] [bigint] NULL,
	[deviceid] [nvarchar](max) NULL,
 CONSTRAINT [PK__mdelboy__00F8BFE75C043931] PRIMARY KEY CLUSTERED 
(
	[delid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mdesc]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mdesc](
	[descid] [bigint] NOT NULL,
	[uid1] [bigint] NOT NULL,
	[desccode] [nvarchar](50) NULL,
	[description] [nvarchar](100) NULL,
	[rate] [float] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[desctype] [nvarchar](100) NULL,
 CONSTRAINT [PK__mdesc__93B54C54695E344F] PRIMARY KEY CLUSTERED 
(
	[descid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mitem]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mitem](
	[itemid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[itemcatid] [bigint] NOT NULL,
	[itembarcode] [nvarchar](25) NULL,
	[itemname] [nvarchar](200) NULL,
	[unitid] [bigint] NULL,
	[itemdesc] [nvarchar](max) NULL,
	[ingredients] [nvarchar](max) NULL,
	[nutritioninfo] [nvarchar](300) NULL,
	[itemstatus] [nvarchar](20) NULL,
	[hsncode] [nvarchar](40) NULL,
	[sgstper] [float] NULL,
	[cgstper] [float] NULL,
	[igstper] [float] NULL,
	[itemwt] [decimal](10, 3) NULL,
	[selflifetype] [nvarchar](80) NULL,
	[selflifeval] [float] NULL,
	[salerate] [float] NULL,
	[purcrate] [float] NULL,
	[DefQty] [float] NULL,
	[favitem] [bit] NULL,
	[itmimgename] [nvarchar](50) NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[validfor] [nvarchar](100) NULL,
	[jan] [bit] NULL,
	[feb] [bit] NULL,
	[mar] [bit] NULL,
	[apr] [bit] NULL,
	[may] [bit] NULL,
	[jun] [bit] NULL,
	[jul] [bit] NULL,
	[aug] [bit] NULL,
	[sep] [bit] NULL,
	[oct] [bit] NULL,
	[nov] [bit] NULL,
	[dec] [bit] NULL,
	[taxtype] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[itemid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mitemcat]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mitemcat](
	[itemcatid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[itemcatcode] [nvarchar](10) NULL,
	[itemcatname] [nvarchar](100) NULL,
	[UnitId] [bigint] NULL,
	[subitemcatid] [bigint] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[itmcattype] [nvarchar](50) NULL,
	[grpimage] [nvarchar](max) NULL,
	[maincat] [int] NULL,
	[itemcategory] [nvarchar](100) NULL,
 CONSTRAINT [PK__mitemcat__37C099E464997F32] PRIMARY KEY CLUSTERED 
(
	[itemcatid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mshipingchrg]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mshipingchrg](
	[scid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[scdate] [datetime] NULL,
	[sctime] [datetime] NULL,
	[scharges] [float] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[scid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstcombo]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstcombo](
	[fieldname] [nvarchar](100) NULL,
	[fieldvalue] [nvarchar](150) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstcompany]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstcompany](
	[compid] [bigint] NOT NULL,
	[compcode] [nvarchar](10) NULL,
	[compname] [nvarchar](100) NULL,
	[compaddress1] [nvarchar](100) NULL,
	[compaddress2] [nvarchar](100) NULL,
	[compaddress3] [nvarchar](100) NULL,
	[compcity] [nvarchar](50) NULL,
	[compzipcode] [bigint] NULL,
	[compstate] [nvarchar](50) NULL,
	[compcountry] [nvarchar](50) NULL,
	[compphone] [nvarchar](20) NULL,
	[compmobile] [nvarchar](60) NULL,
	[compemailid] [nvarchar](100) NULL,
	[compwebsite] [nvarchar](100) NULL,
	[compcstno] [nvarchar](50) NULL,
	[comppanno] [nvarchar](50) NULL,
	[compvatno] [nvarchar](50) NULL,
	[compgstno] [nvarchar](50) NULL,
	[compgststno] [bigint] NULL,
	[compfssino] [nvarchar](50) NULL,
	[compnservice] [nvarchar](50) NULL,
	[compdlno1] [nvarchar](50) NULL,
	[compdlno2] [nvarchar](50) NULL,
	[branchcode] [nvarchar](20) NOT NULL,
	[branchcodeid] [bigint] NULL,
	[serialkey] [nvarchar](50) NULL,
	[custid] [nvarchar](10) NULL,
	[adduserid] [bigint] NULL,
	[adddatetime] [datetime] NULL,
	[edituserid] [bigint] NULL,
	[editdatetime] [datetime] NULL,
	[syncflg] [bigint] NULL,
	[glid] [bigint] NULL,
	[ownermobile] [nvarchar](120) NULL,
PRIMARY KEY CLUSTERED 
(
	[compid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstMessageTemplate]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstMessageTemplate](
	[msgid] [int] NOT NULL,
	[messagetype] [nvarchar](200) NULL,
	[messagetext] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstrights]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstrights](
	[rightsid] [bigint] NOT NULL,
	[rightsname] [nvarchar](100) NULL,
	[modulename] [nvarchar](50) NULL,
	[moduletype] [nvarchar](50) NULL,
	[submoduletype] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[rightsid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstsmssubtype]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstsmssubtype](
	[Id] [bigint] NULL,
	[SMSTypeId] [bigint] NULL,
	[SMSSubType] [nvarchar](200) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstsmstype]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstsmstype](
	[Id] [bigint] NULL,
	[SMSType] [nvarchar](200) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstuser]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstuser](
	[userid] [bigint] NOT NULL,
	[uid1] [bigint] NOT NULL,
	[username] [nvarchar](100) NULL,
	[userpassword] [nvarchar](100) NULL,
	[emailid] [nvarchar](50) NULL,
	[ntlock] [nvarchar](50) NULL,
	[userstatus] [bigint] NULL,
	[MobileNo] [bigint] NULL,
	[DefUser] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[macid] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mstuserrights]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mstuserrights](
	[urightsid] [bigint] NOT NULL,
	[userid] [bigint] NULL,
	[rightsid] [bigint] NULL,
	[addrights] [bit] NULL,
	[editrights] [bit] NULL,
	[deleterights] [bit] NULL,
	[printrights] [bit] NULL,
	[viewrights] [bit] NULL,
	[exportrights] [bit] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[munit]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[munit](
	[unitid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[unitcode] [nvarchar](10) NULL,
	[unitname] [nvarchar](100) NULL,
	[NoofDecimal] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[unitid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderStatusHistory]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatusHistory](
	[soid] [bigint] NULL,
	[itemid] [bigint] NULL,
	[packid] [bigint] NULL,
	[orderstatus] [nvarchar](50) NULL,
	[delpersonid] [bigint] NULL,
	[deldate] [datetime] NULL,
	[deltime] [datetime] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[paymentHistory]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paymentHistory](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Soid] [bigint] NULL,
	[PayType] [nvarchar](50) NULL,
	[PayAmt] [nvarchar](50) NULL,
	[PayStatus] [nvarchar](100) NULL,
	[Response] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[registerinfo]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[registerinfo](
	[infoid] [int] NOT NULL,
	[orgname] [nvarchar](80) NULL,
	[owname] [nvarchar](50) NULL,
	[add1] [nvarchar](250) NULL,
	[add2] [nvarchar](250) NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[country] [nvarchar](50) NULL,
	[pin] [nvarchar](50) NULL,
	[telno] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[mobileno] [nvarchar](50) NULL,
	[level] [nvarchar](5) NULL,
	[curname] [nvarchar](150) NULL,
	[cursymbol] [nvarchar](150) NULL,
	[mlvlid] [bigint] NULL,
	[lvltype] [nvarchar](50) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[uid] [bigint] NULL,
	[panno] [nvarchar](20) NULL,
	[gstin] [nvarchar](20) NULL,
	[fssino] [nvarchar](20) NULL,
	[website] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[infoid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[smshistory]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[smshistory](
	[ID] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[MOBILENO] [nvarchar](50) NULL,
	[MESSAGE] [nvarchar](max) NULL,
	[SENDFLG] [float] NULL,
	[MSGTYPE] [nvarchar](100) NULL,
	[RESPONSEID] [nvarchar](max) NULL,
	[SmsPersonName] [nvarchar](250) NULL,
	[sendDate] [datetime] NULL,
	[sendTime] [datetime] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[smssetting]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[smssetting](
	[ID] [int] NOT NULL,
	[SMSSENDTYPE] [int] NULL,
	[SMSUSERNAME] [nvarchar](100) NULL,
	[SMSUSERPASSWORD] [nvarchar](100) NULL,
	[SMSSENDERID] [nvarchar](50) NULL,
	[SmsApi] [int] NULL,
	[Feed] [nvarchar](100) NULL,
	[SmsApi1] [int] NULL,
	[AppSmsType] [int] NULL,
	[ProUserName] [nvarchar](100) NULL,
	[ProPassword] [nvarchar](100) NULL,
	[ProFeed] [nvarchar](100) NULL,
	[SMSAPIKEY] [nvarchar](250) NULL,
	[ProSMSAPIKEY] [nvarchar](250) NULL,
	[ProSMSApi] [bigint] NULL,
	[TCountryCode] [float] NULL,
	[PCountryCode] [float] NULL,
	[SMSBill] [int] NULL,
	[SMSOrder] [int] NULL,
	[ColSMS] [int] NULL,
	[BillEdit] [int] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sodtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sodtl](
	[sodtlid] [bigint] NOT NULL,
	[uid1] [bigint] NULL,
	[soid] [bigint] NULL,
	[itemid] [bigint] NULL,
	[unitid] [bigint] NULL,
	[itmdeldate] [datetime] NULL,
	[itemdeltime] [datetime] NULL,
	[qty] [float] NULL,
	[grate] [float] NULL,
	[discperc] [float] NULL,
	[discamt] [float] NULL,
	[netrate] [float] NULL,
	[totamt] [float] NULL,
	[sgstperc] [float] NULL,
	[sgstamt] [float] NULL,
	[cgstperc] [float] NULL,
	[cgstamt] [float] NULL,
	[igstperc] [float] NULL,
	[igstamt] [float] NULL,
	[Addnote] [nvarchar](max) NULL,
	[prclitmyn] [bit] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[packid] [bigint] NULL,
	[deviceid] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[sodtlid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sorder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sorder](
	[soid] [float] NOT NULL,
	[uid1] [bigint] NULL,
	[custid] [bigint] NULL,
	[customername] [nvarchar](50) NULL,
	[mobileno] [nvarchar](100) NULL,
	[socode] [nvarchar](100) NULL,
	[socode_1] [bigint] NULL,
	[sodate] [datetime] NULL,
	[sotime] [datetime] NULL,
	[deliverydays] [bigint] NULL,
	[reminderdays] [bigint] NULL,
	[taxtype] [bigint] NULL,
	[roffamt] [float] NULL,
	[discamt] [float] NULL,
	[totamt] [float] NULL,
	[advamt] [float] NULL,
	[deliverydate] [datetime] NULL,
	[dailytrgentype] [bigint] NULL,
	[deliverycharge] [float] NULL,
	[deliverytype] [bigint] NULL,
	[deliverystatus] [bit] NULL,
	[referenceno] [nvarchar](20) NULL,
	[referencedate] [datetime] NULL,
	[wallettype] [bigint] NULL,
	[remarks] [nvarchar](250) NULL,
	[paymenttype] [bigint] NULL,
	[counterid] [bigint] NULL,
	[uid] [bigint] NULL,
	[tflg] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
	[ordstatus] [nvarchar](100) NULL,
	[delpersonid] [bigint] NULL,
	[DelAddressid] [bigint] NULL,
	[deviceid] [nvarchar](200) NULL,
	[TXNID] [nvarchar](max) NULL,
	[promocode] [nvarchar](20) NULL,
 CONSTRAINT [PK__sorder__3317C4174EAA3E13] PRIMARY KEY CLUSTERED 
(
	[soid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[userAddress]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userAddress](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[Type] [nvarchar](25) NULL,
	[FlatNo] [nvarchar](100) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[DefAddress] [bigint] NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FullName] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[userappdevice]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userappdevice](
	[userid] [bigint] NULL,
	[devicetype] [nvarchar](20) NULL,
	[deviceid] [nvarchar](max) NULL,
	[devicetoken] [nvarchar](max) NULL,
	[ipaddress] [nvarchar](50) NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[devicename] [nvarchar](100) NULL,
	[deviceosversion] [nvarchar](100) NULL,
	[devicestatus] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[userlog]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userlog](
	[logid] [bigint] NOT NULL,
	[userid] [bigint] NULL,
	[logindate] [datetime] NULL,
	[logintime] [datetime] NULL,
	[logouttime] [datetime] NULL,
	[machinename] [nvarchar](50) NULL,
	[macid] [nvarchar](50) NULL,
	[totaltime] [datetime] NULL,
	[uid1] [bigint] NULL,
	[uid] [bigint] NULL,
	[level] [nvarchar](10) NULL,
	[tflg] [bigint] NULL,
	[addedby] [bigint] NULL,
	[addedatetime] [datetime] NULL,
	[updatedby] [bigint] NULL,
	[updateddatetime] [datetime] NULL,
	[delflg] [bit] NULL,
	[delby] [bigint] NULL,
	[deldatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[logid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRegistration]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRegistration](
	[firstname] [nvarchar](250) NULL,
	[lastname] [nvarchar](250) NULL,
	[dateofbirth] [datetime] NULL,
	[gender] [nvarchar](250) NULL,
	[contactno] [nvarchar](250) NULL,
	[address] [nvarchar](max) NULL,
	[useremail] [nvarchar](max) NULL,
	[userpwd] [nvarchar](max) NULL,
	[area] [nvarchar](250) NULL,
	[zipcode] [nvarchar](250) NULL,
	[city] [nvarchar](250) NULL,
	[stateid] [int] NULL,
	[countryid] [int] NULL,
	[patimg] [nvarchar](250) NULL,
	[device_id] [nvarchar](max) NULL,
	[device_type] [nvarchar](max) NULL,
	[device_token] [nvarchar](max) NULL,
	[verifylink] [nvarchar](250) NULL,
	[verificationcodedate] [datetime] NULL,
	[uid1] [varchar](50) NULL,
	[socialtype] [varchar](50) NULL,
	[height] [nvarchar](25) NULL,
	[weight] [nvarchar](25) NULL,
	[bloodgroup] [nvarchar](25) NULL,
	[otpno] [nvarchar](10) NULL,
	[senddatetime] [datetime] NULL,
	[otpstatus] [nvarchar](10) NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[userstatus] [nvarchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[mitemlist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View[dbo].[mitemlist] as (select mitem.itemid, mitem.itemcatid, mitemcat.itemcatname,mitem.itemname,   mitem.unitid, munit.unitname, mitem.itembarcode, mitem.itemstatus from mitem Left join mitemcat on (mitem.itemcatid = mitemcat.itemcatid)  Left
 join munit on (mitem.unitid = munit.unitid)   where IsNull(mitem.delflg, 0) = 0)  



GO
/****** Object:  View [dbo].[salesregisterdetail]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[salesregisterdetail] as select bill.billid,bill.billdate ,mitemlist.ItemName,billdtl.Qty,billdtl.grate,billdtl.discperc,
billdtl.discamt,billdtl.netrate,billdtl.totamt,billdtl.sgstperc,billdtl.sgstamt,billdtl.cgstperc,billdtl.cgstamt,billdtl.igstperc,billdtl.igstamt 
from bill 
inner join billdtl on bill.billid = billdtl.billid 
inner join mitemlist on mitemlist.ItemId = billdtl.ItemId 
where isnull(bill.delflg,0) = 0


GO
/****** Object:  View [dbo].[SalesRegister]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[SalesRegister] as 
select bill.billid,bill.billdate ,bill.billcode,userregistration.firstname,bill.totamt from bill inner join userregistration on userregistration.id = bill.custid where isnull(bill.delflg,0) = 0


GO
/****** Object:  View [dbo].[orderstatuslist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[orderstatuslist] as     
Select sorder.soid,customername,mobileno,socode,sodate,sotime,sorder.discamt,          
sorder.totamt,advamt,sodate as deliverydate,deliverycharge,deliverytype,ordstatus,sodtl.itemid,itemname,qty,sorder.custid,  
case  when paymenttype = 0 then 'Cash'  
when paymenttype = 1 then 'Card'  
when paymenttype = 2 Then 'Wallet' else 'Cash' End as paymentdetails,'Normal' as Module ,'0' as packdays ,0 as packid,'' as packname,sotime as deliverytime,mitem.itmimgename as ImageName
from sodtl          
inner join mitem on mitem.itemid = sodtl.itemid            
inner join sorder on sorder.soid = sodtl.soid            
where isnull(sodtl.packid,0)<=0    

GO
/****** Object:  View [dbo].[ordermainlist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[ordermainlist] as 
Select soid,customername,mobileno,socode,sodate,sotime,discamt, totamt, advamt,
deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,packname,deliverytime,
STUFF((SELECT ', ', Convert(nvarchar,i.qty) +' X '+i.itemname as [text()] 
FROM orderstatuslist i Where i.deliverydate = orderstatuslist.deliverydate and 
i.soid = orderstatuslist.soid and i.custid = orderstatuslist.custid and i.deliverytime = orderstatuslist.deliverytime      
group by i.itemname,i.qty  FOR XML PATH ('')), 1, 2, '') as itemname,
(select top 1 ImageName from orderstatuslist i where i.deliverydate = orderstatuslist.deliverydate and 
i.soid = orderstatuslist.soid and i.custid = orderstatuslist.custid and i.deliverytime = orderstatuslist.deliverytime) as ImageName
From orderstatuslist 
--Where custid = 50 and deliverydate = '2020-02-10'
Group By soid,customername,mobileno,socode,sodate,sotime,discamt, totamt, advamt,
deliverydate,deliverycharge,deliverytype,ordstatus,custid,paymentdetails,Module,packdays,packid,packname,deliverytime



GO

/****** Object:  View [dbo].[SalesBillList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[SalesBillList] as 
Select billid,billcode,billdate,billtime,custname, Case When Isnull(convert(bigint, mobileno),0) = 0 then 0  else convert(bigint, mobileno)  end as mobileno,  
totamt,cashamt,creditpay,giftcardpay, WalletAmt,advamt
from(select billid,billcode as billcode,billdate,  Convert(nvarchar(5),billtime,108) as billtime,  
Case When isnull(bill.custname,'')= '' Then userregistration.firstname   else bill.custname end as custname,    bill.mobileno,discount,totamt,  
isnull(cashAmt,0) as cashamt,  isnull(creditpay,0) as creditpay,      
isnull(giftcardPay,0) as giftcardPay,  isnull(WalletAmt,0) as WalletAmt, 
isnull(advamt,0) as advamt
from bill inner join userregistration on bill.custid=userregistration.id where Isnull(bill.Delflg,0 ) = 0 )  as a


GO
/****** Object:  View [dbo].[GetAllContactDetails]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[GetAllContactDetails] as Select Distinct MOBILENO,custname as GlName,'' as RemarkLang from SalesBillList Where isnull(Convert(BigInt,MOBILENO,0),0) > 0




GO
/****** Object:  UserDefinedFunction [dbo].[Fun_CompDtl]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[Fun_CompDtl]()    
returns table     
as    
return(select * from mstcompany  )



GO
/****** Object:  View [dbo].[CounterwiseSalesSummary]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[CounterwiseSalesSummary] as  
Select billcode, billcode_1, billdate, 
Case When IsNull(bill.custname, '') = '' Then userregistration.firstname  else bill.custname end as customername,  
'' as countername,0 as counterid, mstuser.username,  bill.totamt, cashamt, creditpay,  
giftcardPay, walletamt   
From bill 
Inner Join userregistration on (bill.custid = userregistration.id)  
inner join mstuser on (mstuser.userid = bill.addedby) 





GO
/****** Object:  View [dbo].[createsmstemplatelist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[createsmstemplatelist] as Select SMSTemplateId,uid1,SMSType,SMSFor,SMSEng,SMSHindi,SMSRegional,SMSSubType 
From CreateSMSTemplate




GO
/****** Object:  View [dbo].[datewisesalessummery]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[datewisesalessummery] as 
select billdate as billdate, convert(nvarchar, MAX(billcode_1)) as Maxbillcode, convert(nvarchar, MIN(billcode_1)) as Minbillcode, 
count(bill.billid) as totbill,  sum(totamt) AS Amount, 0 as CounterId,'' as CounterName  
From bill  
where Isnull(bill.Delflg,0)=0 
Group by billdate 


GO
/****** Object:  View [dbo].[ItemDescriptionList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[ItemDescriptionList] as select descid,desccode,description from mdesc



GO
/****** Object:  View [dbo].[ItemWiseSalesOrderRegister]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ItemWiseSalesOrderRegister] as  
Select   socode,sodate,sotime,mitem.itemname,mitem.itemid,sorder.customername,sorder.soid,      
isnull(sum(sodtl.qty),0) as Qty ,(sum(sodtl.grate)) as Rate,    sum(sodtl.netrate)as NetAMT,sodtl.discamt as Discamt, sum(sodtl.totamt) as TotalAmt,      
(sum(sodtl.sgstperc)+sum(sodtl.cgstperc)+sum(sodtl.igstperc)) as TaxPerc,    (sum(sodtl.sgstamt)+sum(sodtl.cgstamt)+sum(sodtl.igstamt)) as TaxAmt,      
(sum(sodtl.totamt)+(sum(sodtl.sgstamt)+sum(sodtl.cgstamt)+   sum(sodtl.igstamt)))  as totalamount,  sorder.totamt as BillAmt,      
sorder.custid,ordstatus   
from sorder INNER JOIN sodtl on sorder.soid= sodtl.soid           
INNER JOIN mitem on sodtl.itemid = mitem.itemid        
LEFT JOIN billdtl ON   sodtl.itemid = billdtl.itemid AND    sodtl.soid = billdtl.soid AND   sodtl.sodtlid = billdtl.sodtlid      
group by   socode,sodate,sotime,mitem.itemname,mitem.itemid,sodtl.discamt,  sorder.customername,   sorder.totamt,sorder.taxtype,sodtl.totamt,      
sorder.roffamt,sorder.soid,sodtl.itemid,   socode_1, billdtl.itemid , sorder.custid,ordstatus   
  

GO
/****** Object:  View [dbo].[ItemWiseSalesRegister]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[ItemWiseSalesRegister] as     
Select bill.billid,billcode,billdate,userregistration.firstname,
mitem.itemname,mitem.itemid,      sum(billdtl.qty) as qty,(billdtl.grate)as Rate,sum(billdtl.netrate)Netrate,      
sum(billdtl.discamt)as DiscAmt,sum(billdtl.totamt) as TOtalAMt,      ((cgstperc)+(sgstperc)+(igstperc))as taxperc,(sum(cgstamt)+sum(sgstamt) +  
sum(igstamt)) as TaxAmt,    sum(billdtl.totamt)+(sum(cgstamt)+sum(sgstamt) +sum(igstamt)) as total,  bill.roffamt,  bill.totamt,
bill.grossamt,   (bill.totalamt)-sum(cgstperc)+sum(sgstperc)+sum(igstperc) as TaxBeforamt,  (sum(qty)* sum(grate)) as grossrate,    
1 as nodecqty,billcode_1,userregistration.id ,bill.prclamt 
from bill      
INNER JOIN billdtl on bill.billid = billdtl.billid and isnull(billdtl.isvoid,0) = 0    
INNER JOIN mitem on billdtl.itemid = mitem.itemid     
INNER JOIN userregistration on bill.custid = userregistration.id      
group by billcode,billdate,bill.custname,mitem.itemname,mitem.itemid,bill.totamt,bill.taxtype,    
bill.grossamt,bill.billid,bill.totalamt,billcode_1,userregistration.id,cgstperc,sgstperc,igstperc,
billdtl.grate,bill.prclamt,bill.roffamt,userregistration.firstname




GO

/****** Object:  View [dbo].[mdelboylist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[mdelboylist] as select delid,delbyName from mdelboy




GO
/****** Object:  View [dbo].[mdesclist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[mdesclist] as select descid,desccode as DescCode,description as Description,rate as Rate from mdesc



GO
/****** Object:  View [dbo].[mitemcatlist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View[dbo].[mitemcatlist] as 
 (
 select itemcatid, itemcatcode,itemcatname,munit.unitname
 from mitemcat 
 Left join munit on munit.unitid = mitemcat.unitid 
 where ISNULL(mitemcat.delflg, 0) = 0 
 ) 



GO
/****** Object:  View [dbo].[munitlist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create View[dbo].[munitlist] as (select unitid, unitcode, unitname, NoofDecimal from munit where ISNULL(munit.delflg, 0) = 0)


GO
/****** Object:  View [dbo].[pendingsalesorder]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create view [dbo].[pendingsalesorder] as  
 select sorder.soid,sorder.socode,sorder.sodate,sorder.deliverydate,sorder.sotime,   sorder.custid,userregistration.firstname,sorder.totamt as netamt,
 sorder.advamt,   sorder.roffamt,   sodtl.itemid,sodtl.unitid,sodtl.qty,sodtl.grate,sodtl.netrate,sodtl.discamt,   sodtl.totamt, 
 sodtl.cgstamt,   sodtl.sgstamt,sodtl.igstamt,userregistration.socialtype,  userregistration.contactno, '' as custcode, mitem.itembarcode,mitem.itemname,   mitem.DefQty,mstuser.username, IsNull(sale.recqty, 0 ) as recqty,
 sorder.level   
 from sorder    
 inner join sodtl on (sorder.soid=sodtl.soid and sorder.level=sodtl.level)   
 inner Join userregistration on (sorder.custid = userregistration.id)   
 inner join mitem on (sodtl.itemid = mitem.itemid)   
 inner join mstuser on (sorder.addedby=mstuser.userid)   
 left join (
 select billdtl.itemid, billdtl.soid, billdtl.sodtlid, sum(billdtl.qty) as recqty,bill.level
 from bill 
 inner join billdtl on (bill.billid = billdtl.billid  and bill.level = billdtl.level)   
 where isnull(billdtl.soid,0) > 0 And isnull(billdtl.sodtlid, 0) > 0 
 group by billdtl.itemid, billdtl.soid, billdtl.sodtlid,bill.level
 ) sale on   (sale.soid = sodtl.soid and sale.sodtlid = sodtl.sodtlid and  sale.itemid = sodtl.itemid and sale.level = sodtl.level ) 



GO
/****** Object:  View [dbo].[PendingSalesOrderItemWise]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[PendingSalesOrderItemWise] as  
select distinct sorder.soid, sorder.socode,sorder.sodate,  Case When sorder.customername = '' Then userregistration.firstname else sorder.customername END as custname,mitem.itemid,mitem.itemname,qty ,userregistration.id   
from sorder   
INNER JOIN sodtl on sorder.soid = sodtl.soid  
INNER JOIN userregistration on sorder.custid = userregistration.id   
INNER JOIN mitem on sodtl.itemid = mitem.itemid    
where  sodtl.itemid not in (
select isnull (itemid,0) as itemid from billdtl where itemid >0) or   sodtl.soid not in (select isnull (soid,0) as soid from billdtl where soid > 0) or   
sodtl.sodtlid not in (Select isnull (sodtlid,0) as sodtlid from billdtl where sodtlid>0 )




GO
/****** Object:  View [dbo].[pensolist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[pensolist] as  
select sorder.soid,sorder.socode,sorder.sodate,sodtl.itmdeldate,sorder.sotime,   sorder.custid,userregistration.firstname,sorder.totamt as netamt,
sorder.advamt,   sorder.roffamt,   sodtl.itemid,sodtl.unitid,sodtl.qty,sodtl.grate,sodtl.netrate,sodtl.discamt,   
sodtl.totamt, sodtl.cgstamt,   sodtl.sgstamt,sodtl.igstamt,  userregistration.contactno, '' as custcode, 
mitem.itemname,mstuser.username, 
IsNull(sale.recqty, 0 ) as recqty   
from sorder    
inner join sodtl on (sorder.soid=sodtl.soid )   
 inner Join userregistration on (sorder.custid = userregistration.id)   
inner join mitem on (sodtl.itemid = mitem.itemid)   
inner join mstuser on (sorder.addedby=mstuser.userid)   
left join (select billdtl.itemid, billdtl.soid, billdtl.sodtlid, sum(billdtl.qty) as recqty   
from bill 
inner join billdtl on (bill.billid = billdtl.billid  )   
where isnull(billdtl.soid,0) > 0 And isnull(billdtl.sodtlid, 0) > 0  
group by billdtl.itemid, billdtl.soid, billdtl.sodtlid) sale on   (sale.soid = sodtl.soid and sale.sodtlid = sodtl.sodtlid and   sale.itemid = sodtl.itemid  )




GO
/****** Object:  View [dbo].[SalesOrderBookList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[SalesOrderBookList] as  
select sorder.soid,socode,sodate,userregistration.firstname,customername,  
sorder.mobileno,sorder.totamt,sorder.advamt,   CASE WHEN paymenttype=1 THEN 'Cash' WHEN paymenttype=2 THEN 'Cheque' WHEN paymenttype=3 THEN 'Credit'  
WHEN paymenttype=4 THEN 'Cr Dr Card'   WHEN paymenttype=5 THEN 'Free' WHEN paymenttype=6 THEN 'Gift Card' WHEN paymenttype=7 THEN 'Prepaid Card'  
WHEN paymenttype=8 THEN 'Pending Settlement'   WHEN paymenttype=9 THEN 'Wallet' END as paymenttype,sum(sodtl.discamt) as discamt,
counterid    
from sorder 
inner join userregistration on sorder.custid=userregistration.id   
inner join sodtl on sorder.soid = sodtl.soid  
Group by sorder.soid,socode,sodate,userregistration.firstname,customername,sorder.mobileno,   sorder.totamt,advamt,sorder.paymenttype,counterid




GO
/****** Object:  View [dbo].[SalesOrderRegister]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[SalesOrderRegister] as 
select socode,socode_1,sorder.sodate,  Case When sorder.customername = '' Then userregistration.firstname else sorder.customername END as custname,   
sum(sodtl.totamt)+sum (sodtl.discamt) as Besicamt,sum (sodtl.discamt) as Discamt,    sorder.advamt,(sum(sodtl.sgstamt)+sum(sodtl.cgstamt)+  sum(sodtl.igstamt))   as TaxAmount,  
sorder.totamt,userregistration.id
from sorder     
INNER JOIN sodtl on sorder.soid = sodtl.soid 
inner join userregistration on sorder.custid=userregistration.id  
Group by socode,socode_1,sorder.sodate,sorder.advamt,sorder.roffamt,sorder.taxtype,userregistration.firstname,sorder.totamt,  
sorder.discamt   ,sorder.customername ,userregistration.id 




GO
/****** Object:  View [dbo].[SalesTaxationList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[SalesTaxationList] as Select Distinct sgstperc,cgstperc,billid,Sum(sgstamt) as sgstamt , sum(cgstamt) as cgstamt from billDtl Group By sgstperc,cgstperc,billid



GO
/****** Object:  View [dbo].[selectDeliveryManList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[selectDeliveryManList] as select delid,delbyName as PersonName from mdelboy where  isnull(Isactive,0) = 1




GO
/****** Object:  View [dbo].[SelectPOList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[SelectPOList] as select Distinct sorder.soid,socode,sodate,deliverydate,mitem.itemid,Itemname,itembarcode,Qty,grate ,sodtlid  
from sorder 
inner join sodtl on sorder.soid = sodtl.soid 
inner join mitem on mitem.itemid  = sodtl.itemid  
where sodtl.itemid not in (Select isnull(itemid,0) as itemid from billdtl where itemid>0) or sodtl.soid not in (Select isnull(soid,0) as soid from billdtl where soid>0) or sodtl.sodtlid not in (Select isnull(sodtlid,0) as sodtlid from billdtl where sodtlid>0)




GO
/****** Object:  View [dbo].[SmsLogList]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[SmsLogList] As  Select sendDate,[MOBILENO],[SmsPersonName],[MESSAGE],[SendFlg] from smsHistory

Go
create view [dbo].[userlist] as select userid, username from mstuser 




GO
/****** Object:  View [dbo].[userloglist]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[userloglist] as 
select mstUser.UserID,mstuser.UserName,Convert(nvarchar,Userlog.Logindate,103) as Logindate,Convert(nvarchar,Userlog.logintime,108) as logintime,
Convert(nvarchar,Userlog.logouttime,108) as logouttime,Convert(nvarchar,Userlog.totaltime,108) as totaltime,MachineName,userlog.MacId
from Userlog 
inner join mstuser on Userlog.userid =  mstuser.UserID




GO
/****** Object:  View [dbo].[UserwiseSalesSummaryBillDetail]    Script Date: 17/10/2021 00:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create View [dbo].[UserwiseSalesSummaryBillDetail] as SELECT bill.billcode,(isnull(sum(billdtl.totamt),0) + isnull(bill.prclamt,0) ) as Basic,isnull(totalamt,0) as Total,   
CASE WHEN isnull(sum(cashamt),0) > 0 THEN 'Csh' WHEN isnull(sum(creditpay),0) > 0 THEN 'Cre'  WHEN isnull(sum(giftcardPay),0) > 0 THEN 'Gft' WHEN isnull(sum(walletamt),0) > 0 THEN 'Wlt' END as PayType,  
isnull(sum(sgstamt)+sum(cgstamt)+sum(igstamt),0) as Tax,mstUser.userId as userid,billdate     
FROM bill  inner join billdtl on bill.billid = billdtl.billid 
inner join mstUser on bill.addedby=mstUser.userid  
Group By userid,bill.billid,billdate,totalamt,billcode ,prclamt




GO


GO
INSERT [dbo].[bill] ([billid], [uid1], [billdate], [billTime], [billcode], [billcode_1], [custname], [custid], [mobileno], [taxtype], [prclamt], [discount], [totamt], [roffamt], [grossamt], [adddesc], [cardname], [referenceno], [paymentdate], [payremarks], [cashamt], [creditpay], [giftcardPay], [walletamt], [wallettype], [paidamt], [advamt], [amtrtn], [totalamt], [counterid], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [DelPersonid]) VALUES (11, 1, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000AB7E0016CBF0 AS DateTime), N'1', 1, N'', 71, N'', N'0', 20, 0, 200, 0, 180, N'', N'', N'', CAST(0x0000AB7E00000000 AS DateTime), N'', 200, 0, 0, 0, 0, 0, 0, 0, 200, 0, 1, N'1', 0, 1, CAST(0x0000AB7E0016CD51 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[bill] ([billid], [uid1], [billdate], [billTime], [billcode], [billcode_1], [custname], [custid], [mobileno], [taxtype], [prclamt], [discount], [totamt], [roffamt], [grossamt], [adddesc], [cardname], [referenceno], [paymentdate], [payremarks], [cashamt], [creditpay], [giftcardPay], [walletamt], [wallettype], [paidamt], [advamt], [amtrtn], [totalamt], [counterid], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [DelPersonid]) VALUES (12, 2, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000AB7E0016CBF0 AS DateTime), N'2', 2, N'', 71, N'', N'0', 20, 0, 90, 0, 70, N'', N'', N'', CAST(0x0000AB7E00000000 AS DateTime), N'', 90, 0, 0, 0, 0, 0, 0, 0, 90, 0, 1, N'1', 0, 1, CAST(0x0000AB7E0016DDDE AS DateTime), NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[bill] ([billid], [uid1], [billdate], [billTime], [billcode], [billcode_1], [custname], [custid], [mobileno], [taxtype], [prclamt], [discount], [totamt], [roffamt], [grossamt], [adddesc], [cardname], [referenceno], [paymentdate], [payremarks], [cashamt], [creditpay], [giftcardPay], [walletamt], [wallettype], [paidamt], [advamt], [amtrtn], [totalamt], [counterid], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [DelPersonid]) VALUES (13, 3, CAST(0x0000AB9B00000000 AS DateTime), CAST(0x0000AB9B011A12F0 AS DateTime), N'3', 3, N'sumit', 71, N'9427949390', N'0', 20, 0, 140, 0, 120, N'', N'', N'', CAST(0x0000AB9B00000000 AS DateTime), N'', 140, 0, 0, 0, 0, 0, 0, 0, 140, 0, 1, N'1', 0, 1, CAST(0x0000AB9B011A1429 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[billdtl] ([billdtlid], [uid1], [billid], [itemid], [unitid], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [soid], [sodtlid], [schmid], [schmdtlid], [isvoid], [prclitmyn], [Addnote], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (11, 1, 11, 13, 71, 1, 70, 0, 0, 70, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 1, N'1', 0, 1, CAST(0x0000AB7E0016CEEC AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[billdtl] ([billdtlid], [uid1], [billid], [itemid], [unitid], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [soid], [sodtlid], [schmid], [schmdtlid], [isvoid], [prclitmyn], [Addnote], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (12, 2, 11, 11, 71, 1, 50, 0, 0, 47.62, 47.62, 2.5, 1.19, 2.5, 1.19, 0, 0, 0, 0, 0, 0, 0, 0, N'', 1, N'1', 0, 1, CAST(0x0000AB7E0016CEED AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[billdtl] ([billdtlid], [uid1], [billid], [itemid], [unitid], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [soid], [sodtlid], [schmid], [schmdtlid], [isvoid], [prclitmyn], [Addnote], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (13, 3, 11, 12, 71, 1, 60, 0, 0, 60, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 1, N'1', 0, 1, CAST(0x0000AB7E0016CEED AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[billdtl] ([billdtlid], [uid1], [billid], [itemid], [unitid], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [soid], [sodtlid], [schmid], [schmdtlid], [isvoid], [prclitmyn], [Addnote], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (14, 4, 12, 18, 71, 1, 40, 0, 0, 38.1, 38.1, 2.5, 0.95, 2.5, 0.95, 0, 0, 0, 0, 0, 0, 0, 0, N'', 1, N'1', 0, 1, CAST(0x0000AB7E0016DDE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[billdtl] ([billdtlid], [uid1], [billid], [itemid], [unitid], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [soid], [sodtlid], [schmid], [schmdtlid], [isvoid], [prclitmyn], [Addnote], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (15, 5, 12, 19, 71, 1, 30, 0, 0, 28.58, 28.58, 2.5, 0.71, 2.5, 0.71, 0, 0, 0, 0, 0, 0, 0, 0, N'', 1, N'1', 0, 1, CAST(0x0000AB7E0016DDEE AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[billdtl] ([billdtlid], [uid1], [billid], [itemid], [unitid], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [soid], [sodtlid], [schmid], [schmdtlid], [isvoid], [prclitmyn], [Addnote], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (16, 6, 13, 13, 71, 1, 70, 0, 0, 70, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 1, N'1', 0, 1, CAST(0x0000AB9B011A1607 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[billdtl] ([billdtlid], [uid1], [billid], [itemid], [unitid], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [soid], [sodtlid], [schmid], [schmdtlid], [isvoid], [prclitmyn], [Addnote], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (17, 7, 13, 11, 71, 1, 50, 0, 0, 47.62, 47.62, 2.5, 1.19, 2.5, 1.19, 0, 0, 0, 0, 0, 0, 0, 0, N'', 1, N'1', 0, 1, CAST(0x0000AB9B011A1614 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (1, 1, N'Sales Order', N'Customer', N'Thanks for choosing $OrganizationName$ . We will deliver your order shortly. Order No: $OrderNo$. Amount: $Amount$', N'', N'', N'Sales Order', 7, 1, N'1', 0, CAST(0x0000AACE000B42D0 AS DateTime), 0, CAST(0x0000AB350180CBD0 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (2, 2, N'Sales Invoice', N'Customer', N'Thank you for shoping for us. your bill no : $DocNo$. Total Bill Amount :  $Amount$.Future assistance 
$OrganizationName$,$OrganizationPhone$', N'', N'', N'Sales Invoice', 1, 1, N'1', 0, CAST(0x0000AB3300023280 AS DateTime), 0, CAST(0x0000AB39000AFC80 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (3, 3, N'Sales Order', N'Order Status', N'Your Order is $OrderStatus$ With Order No : $OrderNo$.', N'', N'', N'Sales Order', 1, 0, N'1', 0, CAST(0x0000AB38017C66D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (4, 4, N'OTP SMS', N'OTP SMS', N'$otpno$ is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', NULL, NULL, N'OTP SMS', 1, 0, N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[createsmstemplate] ([SMSTemplateId], [uid1], [SMSType], [SMSFor], [SMSEng], [SMSHindi], [SMSRegional], [SMSSubType], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (5, 5, N'Forgot Password SMS', N'Forgot Password SMS', N'Please use $forgotpwd$ this password for login.', NULL, NULL, N'Forgot Password SMS', 1, 0, N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[emailsetting] ([Id], [uid1], [portalname], [smtpname], [portno], [emailid], [password], [receiveemail], [uid], [tflg], [level], [addedby], [addedatetime], [EnableSSL], [updatedby], [updateddatetime]) VALUES (1, 1, N'GMAIL', N'smtp.gmail.com', N'587', N'sumit@gmail.com', N'VDqL2QHwiM+2uDb+h4p7jQ==', N'', 7, 1, N'1', 1, CAST(0x0000AAFD017352FC AS DateTime), N'false', 1, CAST(0x0000AB390006AB6C AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[mCartItem] ON 

GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2050, 69, 16, 3, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2059, 69, 116, 7, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2066, 86, 116, 1, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2071, 50, 116, 2, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2086, 88, 16, 4, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2087, 88, 19, 3, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2101, 60, 15, 3, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2189, 95, 15, 1, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (2227, 96, 12, 1, 0)
GO
INSERT [dbo].[mCartItem] ([Id], [userid], [Itemid], [ItemQty], [packid]) VALUES (11258, 107, 16, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[mCartItem] OFF
GO
INSERT [dbo].[mdelboy] ([delid], [uid1], [delbyName], [userid], [Isactive], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [address1], [address2], [TelNo], [MobileNo], [Email], [PinCode], [CityId], [StateId], [CountryId], [deviceid]) VALUES (12, 2, N'rajan', 3, 1, 1, 0, N'1', 1, CAST(0x0000AB2F00076A70 AS DateTime), 1, CAST(0x0000AB2F00076A70 AS DateTime), NULL, NULL, NULL, N'A-123', N'', N'', N'9427949390', N'rajan@gmail.com', N'', 72, 71, 71, N'cUfjKbnWCIE:APA91bEYCYx-dA_ZhqaLfz7Sus2rr8zgPJ40tWfoQuCxuluI4UZOzIHsic4krxZ3VCDajIokwLefbps2QVfUtyqa2H3jAwRTmorFaUJ1qrHmlTSw9diubL29KBok1sg-K2LZy3q29Fj4')
GO
INSERT [dbo].[mdelboy] ([delid], [uid1], [delbyName], [userid], [Isactive], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [address1], [address2], [TelNo], [MobileNo], [Email], [PinCode], [CityId], [StateId], [CountryId], [deviceid]) VALUES (71, 1, N'sumit', 2, 1, 7, 1, N'1', 1, CAST(0x0000AAC8018271B0 AS DateTime), 1, CAST(0x0000AB2F00069780 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'9427949390', NULL, NULL, NULL, NULL, NULL, N'c3jnG-WdBtg:APA91bF27UqNa8lPTCzPakstqh6hvQFA4Ele5p2AFTYCCzZL_lEKqE26D9uiLlN3-tGemCokNuZJmRf2b6ZXsIe1LKeQTD4M5t0W9cBfMuLNgg7EtM690qmcmZHGZUbmSIW-LtlMLk7v')
GO
INSERT [dbo].[mdesc] ([descid], [uid1], [desccode], [description], [rate], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [desctype]) VALUES (71, 1, N'1', N'Suger Free', 20, 7, 0, N'1', 1, CAST(0x0000AAC90034BC00 AS DateTime), 1, CAST(0x0000AAC900350250 AS DateTime), NULL, NULL, NULL, N'Item')
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (11, 1, 16, N'1', N'Beauty n Greens', 71, N'Beauty n Greens', N'CS1 maint: BOT: original-url status unknown � CS1: Julian�Gregorian uncertainty � Articles with short description', N'Detoxification is a type of alternative-medicine treatment which aims to rid the body of ... Detoxification and body cleansing products and diets have been criticized for their unsound', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_11.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00016557 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (12, 2, 16, N'2', N'Queen Zing', 71, N'Queen Zing', N'Centuary Mattresses Zing 6 inch Queen Pocket Spring Mattress at best prices with FREE shipping & cash on delivery', N'Buy Zing Queen Size 6 Inch Pocketed Spring Mattress by Centuary Mattress', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_12.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E0001D54F AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (13, 3, 16, N'3', N'24 Carrot', 71, N'24 Carrot', N'', N'The carrot (Daucus carota subsp. sativus) is a root vegetable, usually orange in colour, though purple, black, red, white, and yellow cultivars exist', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_13.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00022E64 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (14, 4, 15, N'4', N'The Meal', 71, N'The carrot is a biennial plant in the umbellifer family Apiaceae. At first, it grows a rosette of leaves while building up the enlarged taproot.', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_14.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00026992 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (15, 5, 15, N'5', N'FOJ Special', 71, N'', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_15.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E0002C130 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (16, 6, 15, N'6', N'Power of Spinach', 71, N'Power of Spinach', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_16.JPEG', 1, 1, N'1', 1, CAST(0x0000AB7E00031DF5 AS DateTime), 1, CAST(0x0000AB7E0013B99C AS DateTime), NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (17, 7, 17, N'7', N'Lemon Zen', 71, N'Lemon Zen', N'are used in a variety of foods and drinks. ... The lemon fruit is an ellipsoid berry surrounded by a green rind, which ripens to yellow, protecting soft yellow segmented pulp', N'Lemon, Citrus limon, is a small evergreen tree in the family Rutaceae grown for its edible fruit which, among other things', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_17.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00039DD0 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (18, 8, 14, N'8', N'Beet Apple', 71, N'Beet Apple', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_18.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00041151 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (19, 9, 14, N'9', N'Mind Changer', 71, N'Mind Changer', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_19.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00047A99 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Not Valid For Pregnant Women', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (110, 10, 12, N'10', N'Heart Beeter', 71, N'Heart Beeter', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_110.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E0004BC07 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (111, 11, 12, N'11', N'Ginger Energy', 71, N'Ginger Energy', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_111.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E0004FB82 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (112, 12, 13, N'12', N'The Indian', 71, N'', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_112.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00052F40 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (113, 13, 13, N'13', N'Tangy Tomato', 71, N'', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_113.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00056D51 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (114, 14, 17, N'14', N'Apple Juice', 71, N'', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_114.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00059F24 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (115, 15, 17, N'15', N'Chiku', 71, N'Chiku', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_115.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E0005C955 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'', 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (116, 16, 17, N'16', N'Banana', 71, N'', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_116.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E0005EAF7 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Not Valid For Child', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[mitem] ([itemid], [uid1], [itemcatid], [itembarcode], [itemname], [unitid], [itemdesc], [ingredients], [nutritioninfo], [itemstatus], [hsncode], [sgstper], [cgstper], [igstper], [itemwt], [selflifetype], [selflifeval], [salerate], [purcrate], [DefQty], [favitem], [itmimgename], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [validfor], [jan], [feb], [mar], [apr], [may], [jun], [jul], [aug], [sep], [oct], [nov], [dec], [taxtype]) VALUES (117, 17, 17, N'17', N'Protein', 71, N'Protein', N'', N'', N'Active', N'', 0, 0, 0, CAST(0.000 AS Decimal(10, 3)), N'', 0, 0, 0, 1, 0, N'item_117.JPEG', 1, 0, N'1', 1, CAST(0x0000AB7E00063FE9 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Not Valid For Pregnant Women', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (11, 1, N'1', N'General Juice', 71, 0, 1, 1, N'1', 1, CAST(0x0000AB7D0186EA38 AS DateTime), 1, CAST(0x0000AB9B01217400 AS DateTime), NULL, NULL, NULL, N'All', N'itemcat_11.JPEG', 1, N'General')
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (12, 2, N'2', N'Male', 71, 11, 1, 1, N'1', 1, CAST(0x0000AB7D01872639 AS DateTime), 1, CAST(0x0000AB810180C84C AS DateTime), NULL, NULL, NULL, N'Men', N'itemcat_12.JPEG', 1, N'General')
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (13, 3, N'3', N'Female', 71, 11, 1, 1, N'1', 1, CAST(0x0000AB7D01876A96 AS DateTime), 1, CAST(0x0000AB810180CCFC AS DateTime), NULL, NULL, NULL, N'Women', N'itemcat_13.JPEG', 1, N'General')
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (14, 4, N'4', N'Child', 71, 11, 1, 1, N'1', 1, CAST(0x0000AB7D01879316 AS DateTime), 1, CAST(0x0000AB810180D1AC AS DateTime), NULL, NULL, NULL, N'Kids', N'itemcat_14.JPEG', 1, N'General')
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (15, 5, N'5', N'Super Food', 71, 0, 1, 1, N'1', 1, CAST(0x0000AB7D0187DA61 AS DateTime), 1, CAST(0x0000AB810180BA3C AS DateTime), NULL, NULL, NULL, N'All', N'itemcat_15.JPEG', 1, N'General')
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (16, 6, N'6', N'Detox', 71, 0, 1, 1, N'1', 1, CAST(0x0000AB7D01885847 AS DateTime), 1, CAST(0x0000AB810180B208 AS DateTime), NULL, NULL, NULL, N'All', N'itemcat_16.JPEG', 1, N'General')
GO
INSERT [dbo].[mitemcat] ([itemcatid], [uid1], [itemcatcode], [itemcatname], [UnitId], [subitemcatid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [itmcattype], [grpimage], [maincat], [itemcategory]) VALUES (17, 7, N'7', N'Other Juices', 71, 0, 1, 1, N'1', 1, CAST(0x0000AB7D0188CB2B AS DateTime), 1, CAST(0x0000AB810180AC2C AS DateTime), NULL, NULL, NULL, N'Other', N'itemcat_17.JPEG', 1, N'Package')
GO
INSERT [dbo].[mshipingchrg] ([scid], [uid1], [scdate], [sctime], [scharges], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (71, 1, CAST(0x0000AAD300000000 AS DateTime), CAST(0x000000000014DFC0 AS DateTime), 20, 7, 1, N'1', 1, CAST(0x0000AAD30014DFC0 AS DateTime), 1, CAST(0x0000AB2F0007B0C0 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'ItmStatus', N'Active!InActive')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'BalanceType', N'Opening!Closing')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'SalesType', N'Cash!Cr/Dr Card!GiftCard!Wallet')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'RateType', N'Inclusive!Exclide')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'selfvalue', N'Days!Month!Year')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'Seprator', N'-!\\!/')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'SMSFor', N'Customer')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'schmtype', N'Normal')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'schmop', N'%!-')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'yn', N'yes!No')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'cattype', N'All!Men!Women!Kids!Other')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'desctype', N'Item!Cancel Order!Other')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'packdays', N'10!15!20!30!35!40!45!50!55!60!65!70!75!80!85!90!95!100!105!110!115!120!125!130!135!140!145!150!155!160!165!170!175!180')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'packdays', N'10!15!20!30!35!40!45!50!55!60!65!70!75!80!85!90!95!100!105!110!115!120!125!130!135!140!145!150!155!160!165!170!175!180')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'transtype', N'Payment!Receive')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'validfor', N'!Not Valid For Pregnant Women!Not Valid For Child')
GO
INSERT [dbo].[mstcombo] ([fieldname], [fieldvalue]) VALUES (N'offervalid', N'Items!Total Amount')
GO
INSERT [dbo].[mstcompany] ([compid], [compcode], [compname], [compaddress1], [compaddress2], [compaddress3], [compcity], [compzipcode], [compstate], [compcountry], [compphone], [compmobile], [compemailid], [compwebsite], [compcstno], [comppanno], [compvatno], [compgstno], [compgststno], [compfssino], [compnservice], [compdlno1], [compdlno2], [branchcode], [branchcodeid], [serialkey], [custid], [adduserid], [adddatetime], [edituserid], [editdatetime], [syncflg], [glid], [ownermobile]) VALUES (1, N'SRAP', N'S R AMUL PARLOUR', N'B-204, SHREENAND RESIDENCY', N'NR. SADGURU HOMES', N'NEW MANINAGAR', N'AHMEDABAD', 380009, N'GUJARAT', NULL, N'8000838147', NULL, N'INFO@SRAP.COM', N'SRAP.COM', N'1234567890', N'1234567890', N'1234567890', N'1234567890', 24, N'', N'', N'1234567890', N'1234567890', N'HO1', 100000, N'P98W-X7FE-CAGI-Z31R', N'CUST0005', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (1, N'Invalid Login', N'Invalid Login Details Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (2, N'Inactive Account', N'Your account is inactive.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (3, N'Fetch Data', N'Fetch data successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (4, N'No Record', N'Record Not Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (5, N'Save Data', N'Save data successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (6, N'Delete Data', N'Delete data successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (7, N'Update Record', N'Record update successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (8, N'LoginSuccess', N'Login successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (9, N'child record', N'Child record found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (10, N'Invalid Authentication', N'Invalid Authentication Key Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (11, N'Valid Authentication', N'Authentication Key Validate Successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (12, N'No Authentication', N'Authentication Key Not Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (13, N'Signup Successfully', N'Signup Successfully')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (14, N'User Exist', N'Mobile number already registered.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (15, N'Invalid OTP', N'Invalid OTP Details Found.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'Record Exists', N'Record Already Exists')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (19, N'Log Out', N'Log Out Successfully')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'No Shift', N'No shifts available, please change the date or try again later.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'Cancel Record', N'Record cancel successfully.')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (16, N'OTP Sent', N'OTP Send successfully')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (17, N'Email Not Exist', N'Email id does not exists')
GO
INSERT [dbo].[mstMessageTemplate] ([msgid], [messagetype], [messagetext]) VALUES (18, N'Email Exist', N'This email address is already registered!')
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (1, N'Item Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (2, N'Item Group Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (3, N'Item Unit Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (4, N'Country Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (5, N'State Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (6, N'City Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (7, N'Customer Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (8, N'Counter Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (9, N'Price List Entry', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (10, N'Scheme Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (11, N'Daily Scheme Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (12, N'Delivery Person Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (13, N'Description Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (14, N'Wallet Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (15, N'Sales', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (16, N'Sales Order', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (17, N'Opening / closing Cash Entry', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (18, N'Dailly Transction Entry', N'Transaction', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (19, N'Sales Type Wise Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (20, N'Item Wise Sales Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (21, N'User Wise Sales Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (22, N'Group Wise Sales Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (23, N'User Wise Cash Summary', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (24, N'Sales Order Register', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (25, N'Sales Register', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (26, N'PriceList Report', N'MIS Reports', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (27, N'Order Approval', N'Utility', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (29, N'Package Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstrights] ([rightsid], [rightsname], [modulename], [moduletype], [submoduletype]) VALUES (30, N'Package Item Link Master', N'Master', NULL, NULL)
GO
INSERT [dbo].[mstsmssubtype] ([Id], [SMSTypeId], [SMSSubType]) VALUES (1, 1, N'Sales Order')
GO
INSERT [dbo].[mstsmssubtype] ([Id], [SMSTypeId], [SMSSubType]) VALUES (2, 2, N'Sales Invoice')
GO
INSERT [dbo].[mstsmssubtype] ([Id], [SMSTypeId], [SMSSubType]) VALUES (3, 3, N'Feedback SMS')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (1, N'Sales Order')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (2, N'Sales Invoice')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (3, N'Other')
GO
INSERT [dbo].[mstsmstype] ([Id], [SMSType]) VALUES (4, N'Custom')
GO
INSERT [dbo].[mstuser] ([userid], [uid1], [username], [userpassword], [emailid], [ntlock], [userstatus], [MobileNo], [DefUser], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [macid]) VALUES (1, 1, N'admin', N'admin', NULL, N'1', NULL, NULL, NULL, NULL, N'1', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SUMIT')
GO
INSERT [dbo].[mstuser] ([userid], [uid1], [username], [userpassword], [emailid], [ntlock], [userstatus], [MobileNo], [DefUser], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [macid]) VALUES (2, 0, N'sumit', N'123', N'abc@yopmail.com', NULL, 0, 1234567891, NULL, 0, N'1', 0, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAF00163F500 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[mstuser] ([userid], [uid1], [username], [userpassword], [emailid], [ntlock], [userstatus], [MobileNo], [DefUser], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [macid]) VALUES (3, 0, N'rajan', N'123', N'123@YOPMAIL.COM', NULL, 0, 1234567890, NULL, 7, N'1', 0, 1, CAST(0x0000AB070172C9E0 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (1, 2, 1, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (2, 2, 2, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (3, 2, 3, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (4, 2, 4, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (5, 2, 5, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (6, 2, 6, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (7, 2, 7, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (8, 2, 8, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (9, 2, 9, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (10, 2, 10, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (11, 2, 11, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (12, 2, 12, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (13, 2, 13, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (14, 2, 14, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (15, 2, 15, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (16, 2, 16, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (17, 2, 17, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (18, 2, 18, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (19, 2, 19, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (20, 2, 20, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (21, 2, 21, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (22, 2, 22, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (23, 2, 23, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (24, 2, 24, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (25, 2, 25, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (26, 2, 26, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (27, 2, 27, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (28, 1, 1, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (29, 1, 2, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (30, 1, 3, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (31, 1, 4, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (32, 1, 5, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (33, 1, 6, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (34, 1, 7, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (35, 1, 8, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (36, 1, 9, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (37, 1, 10, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (38, 1, 11, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (39, 1, 12, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (40, 1, 13, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (41, 1, 14, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (42, 1, 15, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (43, 1, 16, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (44, 1, 17, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (45, 1, 18, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (46, 1, 19, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (47, 1, 20, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (48, 1, 21, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (49, 1, 22, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (50, 1, 23, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (51, 1, 24, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (52, 1, 25, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (53, 1, 26, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (54, 1, 27, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000AAC000023280 AS DateTime), 1, CAST(0x0000AAC000046500 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (56, 1, 29, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000000000000000 AS DateTime), 0, 0, CAST(0x0000000000000000 AS DateTime))
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (56, 1, 29, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000000000000000 AS DateTime), 0, 0, CAST(0x0000000000000000 AS DateTime))
GO
INSERT [dbo].[mstuserrights] ([urightsid], [userid], [rightsid], [addrights], [editrights], [deleterights], [printrights], [viewrights], [exportrights], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (57, 1, 30, 1, 1, 1, 1, 1, 1, 0, N'1', 1, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000000000000000 AS DateTime), 0, 0, CAST(0x0000000000000000 AS DateTime))
GO
INSERT [dbo].[munit] ([unitid], [uid1], [unitcode], [unitname], [NoofDecimal], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (2, 2, NULL, N'texclunt', NULL, 7, N'1', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[munit] ([unitid], [uid1], [unitcode], [unitname], [NoofDecimal], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (3, 3, N'3', N'texcl', 2, 7, N'1', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[munit] ([unitid], [uid1], [unitcode], [unitname], [NoofDecimal], [uid], [level], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (71, 1, N'1', N'PCS', 1, 7, N'1', 0, 1, CAST(0x0000AAC201897017 AS DateTime), 1, CAST(0x0000AAC3000021D4 AS DateTime), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[OrderStatusHistory] ON 

GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (71, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000D3C2F0 AS DateTime), 1)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (71, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000D3C2F0 AS DateTime), 2)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (71, NULL, NULL, N'Delivered', 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000D3C2F0 AS DateTime), 3)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (72, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000171240 AS DateTime), 4)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (72, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000171240 AS DateTime), 5)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (72, NULL, NULL, N'Delivered', 12, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000171240 AS DateTime), 6)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (75, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 10)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (75, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 11)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (75, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 12)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (76, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F0950 AS DateTime), 13)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (76, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F0950 AS DateTime), 14)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (76, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F0950 AS DateTime), 15)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (77, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F95F0 AS DateTime), 16)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (77, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F95F0 AS DateTime), 17)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (77, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F95F0 AS DateTime), 18)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (78, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000328980 AS DateTime), 19)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (78, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000328980 AS DateTime), 21)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (714, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003CF960 AS DateTime), 34)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (714, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003CF960 AS DateTime), 35)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (715, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001A5E00 AS DateTime), 36)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (715, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001A5E00 AS DateTime), 37)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (714, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003CF960 AS DateTime), 38)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (713, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000507990 AS DateTime), 39)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (713, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000537F00 AS DateTime), 41)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (713, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000054DE90 AS DateTime), 42)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (716, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000575760 AS DateTime), 43)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (716, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000575760 AS DateTime), 44)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (719, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000030E3A0 AS DateTime), 45)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (719, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000030E3A0 AS DateTime), 46)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (718, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000317040 AS DateTime), 47)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (718, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000317040 AS DateTime), 48)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (720, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 50)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (720, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 51)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (720, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005EC1D0 AS DateTime), 52)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (721, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005EC1D0 AS DateTime), 53)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (721, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005EC1D0 AS DateTime), 54)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (721, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005EC1D0 AS DateTime), 55)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (718, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000060AE00 AS DateTime), 56)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (722, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000061C740 AS DateTime), 57)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (722, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000061C740 AS DateTime), 58)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (716, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000061C740 AS DateTime), 59)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (723, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000006253E0 AS DateTime), 60)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (717, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000062E080 AS DateTime), 63)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (723, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000636D20 AS DateTime), 64)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (724, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000063B370 AS DateTime), 67)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (726, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C301E0 AS DateTime), 68)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (726, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C301E0 AS DateTime), 69)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (726, NULL, NULL, N'Delivered', 71, CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C301E0 AS DateTime), 70)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (727, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000244140 AS DateTime), 71)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (726, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002B6560 AS DateTime), 74)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (733, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000644010 AS DateTime), 75)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (733, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000644010 AS DateTime), 76)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (732, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000662C40 AS DateTime), 78)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (732, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000662C40 AS DateTime), 79)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (732, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000662C40 AS DateTime), 80)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (726, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000AD4F30 AS DateTime), 88)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (726, NULL, NULL, N'Delivered', 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000ADDBD0 AS DateTime), 89)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (735, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000AEF510 AS DateTime), 91)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (736, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000AF3B60 AS DateTime), 92)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (740, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C693F0 AS DateTime), 94)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (740, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C693F0 AS DateTime), 95)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (740, NULL, NULL, N'Delivered', 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C693F0 AS DateTime), 96)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (742, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000AFC800 AS DateTime), 97)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (773, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB9700000000 AS DateTime), CAST(0x0000000000B9AB40 AS DateTime), 110)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (781, NULL, NULL, N'Being Prepared', 71, CAST(0x0000ABAC00000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 10110)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (781, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000ABAC00000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 10111)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (781, NULL, NULL, N'Delivered', 71, CAST(0x0000ABAC00000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 10112)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (787, NULL, NULL, N'Being Prepared', 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x000000000075D410 AS DateTime), 20110)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (787, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000761A60 AS DateTime), 20111)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (787, NULL, NULL, N'Delivered', 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000761A60 AS DateTime), 20112)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (794, NULL, NULL, N'Being Prepared', 12, CAST(0x0000ABBF00000000 AS DateTime), CAST(0x0000000000C042C0 AS DateTime), 30110)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (799, NULL, NULL, N'Being Prepared', 12, CAST(0x0000ABC600000000 AS DateTime), CAST(0x0000000000BE5690 AS DateTime), 40110)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (799, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000ABC600000000 AS DateTime), CAST(0x0000000000BE5690 AS DateTime), 40111)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (7105, NULL, NULL, N'Being Prepared', 12, CAST(0x0000ABCB00000000 AS DateTime), CAST(0x0000000000A59E70 AS DateTime), 50110)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (7114, NULL, NULL, N'Being Prepared', 12, CAST(0x0000ADAB00000000 AS DateTime), CAST(0x0000000000685EC0 AS DateTime), 60110)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (73, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x00000000002E2480 AS DateTime), 7)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (73, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x00000000002E2480 AS DateTime), 8)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (73, NULL, NULL, N'Delivered', 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x00000000002E2480 AS DateTime), 9)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (78, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000328980 AS DateTime), 20)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (710, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000389460 AS DateTime), 23)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (710, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000389460 AS DateTime), 24)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (79, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000389460 AS DateTime), 25)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (79, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000389460 AS DateTime), 26)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (710, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000392100 AS DateTime), 27)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (711, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000396750 AS DateTime), 28)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (711, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000396750 AS DateTime), 29)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (711, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003AC6E0 AS DateTime), 33)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (741, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C115B0 AS DateTime), 100)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (741, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C115B0 AS DateTime), 101)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (741, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C115B0 AS DateTime), 102)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (789, NULL, NULL, N'Being Prepared', 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000B8D850 AS DateTime), 20113)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (789, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000B91EA0 AS DateTime), 20114)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (789, NULL, NULL, N'Delivered', 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000B91EA0 AS DateTime), 20115)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (797, NULL, NULL, N'Being Prepared', 12, CAST(0x0000ABC300000000 AS DateTime), CAST(0x000000000064CCB0 AS DateTime), 30112)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (797, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000ABC300000000 AS DateTime), CAST(0x000000000064CCB0 AS DateTime), 30113)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (799, NULL, NULL, N'Delivered', 12, CAST(0x0000ABC600000000 AS DateTime), CAST(0x0000000000BE5690 AS DateTime), 40112)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (7105, NULL, NULL, N'Delivered', 12, CAST(0x0000ABCB00000000 AS DateTime), CAST(0x0000000000A59E70 AS DateTime), 50112)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (7114, NULL, NULL, N'Delivered', 12, CAST(0x0000ADAB00000000 AS DateTime), CAST(0x0000000000685EC0 AS DateTime), 60112)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (79, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000350250 AS DateTime), 22)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (712, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003A8090 AS DateTime), 30)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (712, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003A8090 AS DateTime), 31)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (712, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003A8090 AS DateTime), 32)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (719, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000331620 AS DateTime), 49)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (723, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000006253E0 AS DateTime), 61)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (717, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000062E080 AS DateTime), 62)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (722, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000636D20 AS DateTime), 65)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (724, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000063B370 AS DateTime), 66)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (731, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000667290 AS DateTime), 81)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (731, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000667290 AS DateTime), 82)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (731, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000667290 AS DateTime), 83)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (735, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000AE2220 AS DateTime), 90)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (756, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 106)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (794, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000ABBF00000000 AS DateTime), CAST(0x0000000000C042C0 AS DateTime), 30111)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (7105, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000ABCB00000000 AS DateTime), CAST(0x0000000000A59E70 AS DateTime), 50111)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (7114, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000ADAB00000000 AS DateTime), CAST(0x0000000000685EC0 AS DateTime), 60111)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (715, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000537F00 AS DateTime), 40)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (727, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000244140 AS DateTime), 72)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (727, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000244140 AS DateTime), 73)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (733, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000644010 AS DateTime), 77)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (730, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000068EB60 AS DateTime), 84)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (734, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000068EB60 AS DateTime), 85)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (734, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000068EB60 AS DateTime), 86)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (734, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000068EB60 AS DateTime), 87)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (738, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000B65F80 AS DateTime), 93)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (742, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000AFC800 AS DateTime), 98)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (742, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000AFC800 AS DateTime), 99)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (749, NULL, NULL, N'Being Prepared', 12, CAST(0x0000AB8500000000 AS DateTime), CAST(0x000000000028EC90 AS DateTime), 103)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (749, NULL, NULL, N'Out for Delivery', 12, CAST(0x0000AB8500000000 AS DateTime), CAST(0x000000000028EC90 AS DateTime), 104)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (749, NULL, NULL, N'Delivered', 12, CAST(0x0000AB8500000000 AS DateTime), CAST(0x000000000028EC90 AS DateTime), 105)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (767, NULL, NULL, N'Being Prepared', 71, CAST(0x0000AB8C00000000 AS DateTime), CAST(0x00000000008462A0 AS DateTime), 107)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (767, NULL, NULL, N'Out for Delivery', 71, CAST(0x0000AB8C00000000 AS DateTime), CAST(0x00000000008462A0 AS DateTime), 108)
GO
INSERT [dbo].[OrderStatusHistory] ([soid], [itemid], [packid], [orderstatus], [delpersonid], [deldate], [deltime], [id]) VALUES (767, NULL, NULL, N'Delivered', 71, CAST(0x0000AB8C00000000 AS DateTime), CAST(0x00000000008462A0 AS DateTime), 109)
GO
SET IDENTITY_INSERT [dbo].[OrderStatusHistory] OFF
GO
SET IDENTITY_INSERT [dbo].[paymentHistory] ON 

GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (1, 7110, N'PAYTM', N'1.00', N'TXN_SUCCESS', N'OrderBookWeb.Models.PaytmResponse')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (2, 7111, N'PAYTM', N'1.00', N'TXN_SUCCESS', N'OrderBookWeb.Models.PaytmResponse')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (3, 7112, N'PAYTM', N'1.00', N'TXN_SUCCESS', N'{"MID":"mITKOc89780088065651","ORDERID":"7112","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200123111212800110168614110251584","BANKTXNID":"131850724532","STATUS":"TXN_SUCCESS","RESPCODE":"01","RESPMSG":"Txn Success","TXNDATE":"2020-01-23 00:56:16.0","GATEWAYNAME":"WALLET","BANKNAME":"WALLET","PAYMENTMODE":"PPI","CHECKSUMHASH":"6wwWjzSDobKg2ue7vwgPJz9x6Iuz2cJhdb3QHeUT+bkwEaWuFWbGKh0VZhUxozEv7olC6dY7KqQtIShc89XVRXvu0Z/PPxRawBSlWdfATaQ="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (4, 7113, N'PAYTM', N'1.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7113","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":null,"BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"2023","RESPMSG":"Repeat Request Inconsistent","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"/2767T4+UIax1GV4QxbLjwjh23PECHVtIaswGeUI0FEz/mofCWpf3DwH2QW+yGd3z0KhivDQ2G9fX4BGaNcgEATVBm1vwEzbNjHIQLUSx0Y="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (5, 7117, N'PAYTM', N'1.00', N'TXN_SUCCESS', N'{"MID":"mITKOc89780088065651","ORDERID":"7117","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200123111212800110168662609467321","BANKTXNID":"131851115852","STATUS":"TXN_SUCCESS","RESPCODE":"01","RESPMSG":"Txn Success","TXNDATE":"2020-01-23 01:23:51.0","GATEWAYNAME":"WALLET","BANKNAME":"WALLET","PAYMENTMODE":"PPI","CHECKSUMHASH":"a1zL+D/VsX1Q1CL4EmXdLEvcv9AYwNU1Nki162FnIczZ1wrepicZrBhPDgb8VS2TEmbHd8r7BtDn6ywR2XiyOh38llHSaM5PsesNa9XPvtg="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (6, 7141, N'PAYTM', N'1.00', N'TXN_SUCCESS', N'{"MID":"mITKOc89780088065651","ORDERID":"7141","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200123111212800110168539309865885","BANKTXNID":"002312281899","STATUS":"TXN_SUCCESS","RESPCODE":"01","RESPMSG":"Txn Success","TXNDATE":"2020-01-23 17:38:27.0","GATEWAYNAME":"WALLET","BANKNAME":"WALLET","PAYMENTMODE":"PPI","CHECKSUMHASH":"whZzg1CQrPVnCIUbeO+ZCz0ObWWfJSrFaS4jvuhCv6XdZWOOt+q6f4gQw18+mAEXk202aes4LCyRDvHZ1+Q0eQsVNM7Cu5tOFrg2cu4aKHE="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (7, 7142, N'PAYTM', N'1.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7142","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200124111212800110168546809436698","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"fa1llgtfv6UoHmqnd0U1ZVYf/PB8n8szHNsPhck9VTmapI2x549W+3gVwBGK2okGTWycBXHgcz1WE8PxN0W7kzfuGrgDCg6elrGj8aM9I0s="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (8, 7142, N'PAYTM', N'1.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7142","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200124111212800110168546809436698","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"810","RESPMSG":"Merchant Transaction Failure","TXNDATE":"2020-01-24 10:05:21.0","GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"/13QvZP34DFWSZjGz3z71vjtn/c7uvEyxbCdo07YJ/bmZJr8qiL6W8B2utS2ZHffDRwc9sWsefOWUyDl8ZPfSJPMZfiA1qNpDjUGPzHatP0="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (9, 7157, N'PAYTM', N'1.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7157","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200125111212800110168902809890095","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"0e3Kycqb1sxahHSzy49oRhGpAMEzuDz7e5a/lZAOEZDsVhsOcOmfP/vWGWGzLKQU0W7OwU7gtR9Zo6CCdgyOyDvXwEYTZ++QcdGTfknpbTA="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (10, 7183, N'PAYTM', N'1.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7183","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200128111212800110168794010665141","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"4haMuM71mKrcwCOTUW+Cmk+lYNcEs+uq/8fW6cND9pN6ErDpGzUbsxH+J9hhs60Qu1R5xzEoyAk/8RVHQFKNClY4NktBcbsuHEJ/gQoMCFs="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (11, 7302, N'PAYTM', N'65.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7302","TXNAMOUNT":"65.00","CURRENCY":"INR","TXNID":"20200303111212800110168524516887559","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"9B3h/xPfyZAOcBM7bva4UanqXelOOMea6KYILflhQ769Vn/xH9XMOSzZZu7Py3oMt9wJWcqTDH2DQ+ATkTsSKjJhl5KkLSgkFcMGh03qcC0="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (12, 7307, N'PAYTM', N'210.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7307","TXNAMOUNT":"210.00","CURRENCY":"INR","TXNID":"20200305111212800110168571617593592","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"83POoIylGJoD+MM4zhQXXjBd8DAoywfS6YRyeegE7a80izeOz0vzwRw9ZnX/kKEhDkWt/Elr5v/Ld2Kzofv6hLsKTTCLxwoGZ30fSyku5tM="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (13, 7309, N'PAYTM', N'65.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7309","TXNAMOUNT":"65.00","CURRENCY":"INR","TXNID":"20200305111212800110168594317735453","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"s43sbSQz//rQZEiVeH82MyEY5ps4+MV8GUCP09GcKrutcCKbRH0gzd4ZnlxEG8bTVUuBwy6aVSGDUuNcXX76npGkop2WBLy7MfXbb0JFZzw="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (14, 7330, N'PAYTM', N'65.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7330","TXNAMOUNT":"65.00","CURRENCY":"INR","TXNID":"20200306111212800110168433917806048","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"rveew0Qg8UUl4tvO518+JWIUsDjmkax16WIwb77TcWfcTXwdVlbNL3X5llkOVhGoP6n4d07HLcMLtsYE2AObalIyfiGZo2p9LTK05MOsX28="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (15, 7331, N'PAYTM', N'1.00', N'PENDING', N'{"MID":"mITKOc89780088065651","ORDERID":"7331","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200306111212800110168448617948507","BANKTXNID":null,"STATUS":"PENDING","RESPCODE":"402","RESPMSG":"Looks like the payment is not complete. Please wait while we confirm the status with your bank.","TXNDATE":"2020-03-06 11:44:29.0","GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":"UPI","CHECKSUMHASH":"eBnsveCMO7LHOHrfuVFZajii5EiFdNfAoywnCYTpCLJ29m1V0KnAHAzGSRsIQnKjZk6uhZpfUM+rx82WtRp262+Do0vnlEURHXVUu7E4hiA="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (16, 7332, N'PAYTM', N'1.00', N'TXN_SUCCESS', N'{"MID":"mITKOc89780088065651","ORDERID":"7332","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200306111212800110168454817894511","BANKTXNID":"135068398111","STATUS":"TXN_SUCCESS","RESPCODE":"01","RESPMSG":"Txn Success","TXNDATE":"2020-03-06 11:45:12.0","GATEWAYNAME":"WALLET","BANKNAME":"WALLET","PAYMENTMODE":"PPI","CHECKSUMHASH":"53gtWZldDdlAOYdcFgx7Z5SVECmBzBhTA+yVXpw6XGV117NL4PEauA6XTLo5gX7oNfm6YCQYTLZxOe3GC6sIjRYlCbcmRUSkvZgU0U3XS6A="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (17, 7333, N'PAYTM', N'1.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7333","TXNAMOUNT":"1.00","CURRENCY":"INR","TXNID":"20200306111212800110168460117787069","BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"141","RESPMSG":"User has not completed transaction.","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"0b0WuFWgVSDokShuNFL1cXC5T6JLdgoIbZ82E4qgfzqJXFXHGyNHiDtSEGEMQTLYncDS8tGOGQe6v6l5zltmHBqMvOadtB32CeHyI+qNvfs="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (18, 774, N'PAYTM', N'520.00', N'PENDING', N'{"MID":"mITKOc89780088065651","ORDERID":"774","TXNAMOUNT":"520.00","CURRENCY":"INR","TXNID":"20200416111212800110168787123462517","BANKTXNID":null,"STATUS":"PENDING","RESPCODE":"402","RESPMSG":"Looks like the payment is not complete. Please wait while we confirm the status with your bank.","TXNDATE":"2020-04-16 12:04:57.0","GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":"UPI","CHECKSUMHASH":"reYnW0ENd4zSJQZOZF2DoaJV/IoO9dociSZHKhAkK3yS+C4ZXlUrGfQVpZgOwvy2WbpBxPdc/cw7ZkY+3AkvTOhqBvfKUTuSA98M8XuqHzE="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (10018, 7110, N'PAYTM', N'110.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7110","TXNAMOUNT":"110.00","CURRENCY":"INR","TXNID":null,"BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"325","RESPMSG":"Duplicate Order Id","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"9wOHzFZqAmdPURVu2AE+lZ4zo32pXHv7pM9G2xX6q9JlRLcYcJx+p8hoqcDZE7ry0nWecL1KTkh5hvyCVPWyj7PKX9GHxlgecFG0DZ+gPws="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (10019, 7111, N'PAYTM', N'110.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7111","TXNAMOUNT":"110.00","CURRENCY":"INR","TXNID":null,"BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"325","RESPMSG":"Duplicate Order Id","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"u4fT6ccDtR3dtPk+PGv6jXhVHPKuH1ZgLHxAgo1QRwGQIouh/0xwMCm0Gnm/hW25jdeht3I0JjuoDdxSPbOxKuwFd3zpHL0MxPESqsyvQHw="}')
GO
INSERT [dbo].[paymentHistory] ([id], [Soid], [PayType], [PayAmt], [PayStatus], [Response]) VALUES (10020, 7112, N'PAYTM', N'290.00', N'TXN_FAILURE', N'{"MID":"mITKOc89780088065651","ORDERID":"7112","TXNAMOUNT":"290.00","CURRENCY":"INR","TXNID":null,"BANKTXNID":null,"STATUS":"TXN_FAILURE","RESPCODE":"325","RESPMSG":"Duplicate Order Id","TXNDATE":null,"GATEWAYNAME":null,"BANKNAME":null,"PAYMENTMODE":null,"CHECKSUMHASH":"Je09Da9etltuO7dZf+Am1+qe7OUxqXu3O6o6yM5zlRL+DSsB4ITAs9bP4c4WUfB+iecehjZjoGPv1Kc/dMmzsUYxU9yC3ONF/0GY5mIcBC4="}')
GO
SET IDENTITY_INSERT [dbo].[paymentHistory] OFF
GO
INSERT [dbo].[registerinfo] ([infoid], [orgname], [owname], [add1], [add2], [city], [state], [country], [pin], [telno], [email], [mobileno], [level], [curname], [cursymbol], [mlvlid], [lvltype], [tflg], [addedby], [addedatetime], [updatedby], [updateddatetime], [uid], [panno], [gstin], [fssino], [website]) VALUES (1, N'infinity bit', N'sumit', N'a-402 , gp', N'navrangpura', N'ahmedabad', N'gujarat', N'india', N'380009', N'9427949391', N'sumti@yopmail.com', N'94279493941', N'1', NULL, NULL, NULL, NULL, 0, 0, CAST(0x0000AABE001A976C AS DateTime), 1, CAST(0x0000AAD000324330 AS DateTime), 7, N'BGEPP1849DDFGHH', N'24ASDSAD', N'fsdfsd', N'www.google.com')
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (1, 1, N'9427949390', N'6745 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3C00000000 AS DateTime), CAST(0x0000000001815870 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3C01815870 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (2, 2, N'9427949390', N'4453 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3C00000000 AS DateTime), CAST(0x0000000001871D00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3C01871D00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (3, 3, N'9427949390', N'Please use test@123 this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB3D00000000 AS DateTime), CAST(0x000000000004AB50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3D0004AB50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (4, 4, N'9427949390', N'8578 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3D00000000 AS DateTime), CAST(0x000000000007B0C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3D0007B0C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (5, 5, N'9427949390', N'4994 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3C00000000 AS DateTime), CAST(0x0000000000C90CC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3C00C90CC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (6, 6, N'9427949390', N'Please use test@123 this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB3C00000000 AS DateTime), CAST(0x0000000000C95310 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3C00C95310 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (7, 7, N'9427949390', N'9740 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3C00000000 AS DateTime), CAST(0x0000000000C99960 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3C00C99960 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (8, 8, N'9427949390', N'8272 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3D00000000 AS DateTime), CAST(0x0000000001869060 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3D01869060 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (9, 9, N'9427949390', N'7741 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3E00000000 AS DateTime), CAST(0x0000000001845DE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3E01845DE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (10, 10, N'9794979797', N'1650 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3E00000000 AS DateTime), CAST(0x0000000001845DE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3E01845DE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (11, 11, N'9997676464', N'3949 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB3F00000000 AS DateTime), CAST(0x0000000000563E20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB3F00563E20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (12, 12, N'8140122070', N'2161 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB4000000000 AS DateTime), CAST(0x0000000000034BC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4000034BC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (13, 13, N'8140122070', N'5390 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB4000000000 AS DateTime), CAST(0x0000000000034BC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4000034BC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (14, 14, N'8796496546', N'3472 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB4500000000 AS DateTime), CAST(0x0000000001784820 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4501784820 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (15, 15, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 745. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000000305700 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4900305700 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (16, 16, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 747. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB490033E910 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (17, 17, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 749. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000000350250 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4900350250 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (18, 18, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 751. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x000000000040D1C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB490040D1C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (19, 19, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 752. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x000000000040D1C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB490040D1C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (20, 20, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 753. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x000000000040D1C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB490040D1C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (21, 21, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 762. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000001716A50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4901716A50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (22, 22, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 764. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x00000000017801D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB49017801D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (23, 23, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 765. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000001784820 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4901784820 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (24, 24, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 774. Amount: 280', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000001841790 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4901841790 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (25, 25, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 775. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000001841790 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4901841790 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (26, 26, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 776. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x000000000184A430 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB490184A430 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (27, 27, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 777. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x000000000184A430 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB490184A430 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (28, 28, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 778. Amount: 280', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x000000000184A430 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB490184A430 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (29, 29, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 779. Amount: 410', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x00000000018530D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB49018530D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (30, 30, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 780. Amount: 410', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000001857720 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4901857720 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (31, 31, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 781. Amount: 410', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4900000000 AS DateTime), CAST(0x0000000001871D00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4901871D00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (32, 32, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 782. Amount: 200', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000011940 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00011940 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (33, 33, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 783. Amount: 200', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000011940 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00011940 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (34, 34, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 784. Amount: 400', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x000000000001A5E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A0001A5E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (35, 35, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 785. Amount: 700', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000000278D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A000278D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (36, 36, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 788. Amount: 150', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000003129F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A003129F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (37, 37, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 789. Amount: 150', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000317040 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00317040 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (38, 38, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 794. Amount: 600', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x000000000042BDF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A0042BDF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (39, 39, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 795. Amount: 600', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000430440 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00430440 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (40, 40, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 796. Amount: 600', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000430440 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00430440 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (41, 41, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 797. Amount: 290', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000004390E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A004390E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (42, 42, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 798. Amount: 300', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000465000 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00465000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (43, 43, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 799. Amount: 300', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000004722F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A004722F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (44, 44, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7100. Amount: 300', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000476940 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00476940 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (45, 45, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7101. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000005451F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A005451F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (46, 46, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7102. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x000000000182FE50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A0182FE50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (47, 47, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7103. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x000000000183D140 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A0183D140 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (48, 48, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7104. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000018603C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A018603C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (49, 49, N'9408932602', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7105. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000001887C90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A01887C90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (50, 50, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7106. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000018B3BB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A018B3BB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (51, 51, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7107. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000023280 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00023280 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (52, 52, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7108. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000034BC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00034BC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (53, 53, N'9408932602', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7109. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000004AB50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0004AB50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (54, 54, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7110. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000000C5C10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B000C5C10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (55, 55, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7111. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000000DBBA0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B000DBBA0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (56, 56, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7112. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000000F1B30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B000F1B30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (57, 57, N'9408932602', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7113. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000000FEE20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B000FEE20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (58, 58, N'9408932602', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7114. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000103470 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00103470 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (59, 59, N'9408932602', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7115. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000001220A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B001220A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (60, 60, N'9408932602', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7116. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000149970 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00149970 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (61, 61, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7117. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x0000000000D44F90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A00D44F90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (62, 62, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7118. Amount: 300', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000016BEC10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A016BEC10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (63, 63, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7119. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000016C78B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A016C78B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (64, 64, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7120. Amount: 190', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000016D4BA0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A016D4BA0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (65, 65, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7121. Amount: 146', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000016EF180 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A016EF180 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (66, 66, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7122. Amount: 520', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000016F37D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A016F37D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (67, 67, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7123. Amount: 335', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000017615A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A017615A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (68, 68, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7124. Amount: 335', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x000000000176E890 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A0176E890 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (69, 69, N'109068876884671028725', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7125. Amount: 85.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000017B93E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A017B93E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (70, 70, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7127. Amount: 137', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4A00000000 AS DateTime), CAST(0x00000000017D8010 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4A017D8010 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (71, 71, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7128. Amount: 202.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000002BF20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0002BF20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (72, 72, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7129. Amount: 202.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000002BF20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0002BF20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (73, 73, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7130. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000342F60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00342F60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (74, 74, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7131. Amount: 67.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000408B70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00408B70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (75, 75, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7132. Amount: 67.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000408B70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00408B70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (76, 76, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7133. Amount: 290', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000415E60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00415E60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (77, 77, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7134. Amount: 38', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000047F5E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0047F5E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (78, 78, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7135. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000047F5E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0047F5E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (79, 79, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7136. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000048C8D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0048C8D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (80, 80, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7137. Amount: 67.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000049E210 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0049E210 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (81, 81, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7138. Amount: 67.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000004A2860 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B004A2860 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (82, 82, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7139. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000004E4710 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B004E4710 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (83, 83, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7140. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000005451F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B005451F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (84, 84, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7141. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000000549840 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B00549840 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (85, 85, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7142. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x000000000163AEB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B0163AEB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (86, 86, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7143. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000016481A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B016481A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (87, 87, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7146. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x0000000001662780 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B01662780 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (88, 88, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7147. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4B00000000 AS DateTime), CAST(0x00000000017A3450 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4B017A3450 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (89, 89, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7149. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x000000000051D920 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C0051D920 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (90, 90, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7150. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x0000000000537F00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C00537F00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (91, 91, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7151. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x00000000005B7610 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C005B7610 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (92, 92, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7152. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x00000000005C02B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C005C02B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (93, 93, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7153. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x00000000005E3530 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C005E3530 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (94, 94, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7154. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x00000000005E7B80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C005E7B80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (95, 95, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7155. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x00000000005F0820 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C005F0820 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (96, 96, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7156. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x00000000005F4E70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C005F4E70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (97, 97, N'8140122070', N'5223 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB4C00000000 AS DateTime), CAST(0x00000000015406E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4C015406E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (98, 98, N'7698096070', N'8160 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB4D00000000 AS DateTime), CAST(0x000000000187A9A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4D0187A9A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (99, 99, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7159. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x0000000000384E10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F00384E10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (100, 100, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7160. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x00000000004B87F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F004B87F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (101, 101, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7161. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x00000000004D2DD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F004D2DD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (102, 102, N'8140122070', N'5423 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x000000000087F4B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F0087F4B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (103, 103, N'8140122070', N'5423 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x000000000087F4B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F0087F4B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (104, 104, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7165. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x0000000001838AF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F01838AF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (105, 105, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7166. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x0000000001838AF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F01838AF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (106, 106, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7167. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB4F00000000 AS DateTime), CAST(0x00000000018A68C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB4F018A68C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (107, 107, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7168. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (108, 108, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7169. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000001A5E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500001A5E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (109, 109, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7170. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000000278D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50000278D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (110, 110, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7171. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000034BC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000034BC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (111, 111, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7172. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000004F1A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500004F1A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (112, 112, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7173. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000072420 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000072420 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (113, 113, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7174. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000091050 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000091050 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (114, 114, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7175. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000091050 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000091050 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (115, 115, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7176. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000000956A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50000956A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (116, 116, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7177. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000000A6FE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50000A6FE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (117, 117, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7178. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000000AB630 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50000AB630 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (118, 118, N'8980979694', N'2278 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000202290 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000202290 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (119, 119, N'8980979694', N'8263 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000020AF30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500020AF30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (120, 120, N'8767878784', N'7544 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000220EC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000220EC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (121, 121, N'8768878784', N'3884 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000225510 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000225510 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (122, 122, N'8767878784', N'1135 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000229B60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000229B60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (123, 123, N'8767878784', N'5691 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000248790 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000248790 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (124, 124, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7179. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000270060 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000270060 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (125, 125, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7180. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000278D00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000278D00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (126, 126, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7181. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000027D350 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500027D350 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (127, 127, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7182. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000002F8410 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50002F8410 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (128, 128, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7183. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000309D50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000309D50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (129, 129, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7184. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000030E3A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500030E3A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (130, 130, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7185. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000317040 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000317040 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (131, 131, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7186. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000324330 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000324330 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (132, 132, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7187. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000032CFD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500032CFD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (133, 133, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7188. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000032CFD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500032CFD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (134, 134, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7189. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000033A2C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500033A2C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (135, 135, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7190. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000350250 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000350250 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (136, 136, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7191. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000350250 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000350250 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (137, 137, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7192. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000003548A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50003548A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (138, 138, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7193. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000049E210 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500049E210 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (139, 139, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7194. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000004AFB50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50004AFB50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (140, 140, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7195. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000004C1490 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50004C1490 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (141, 141, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7196. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x000000000052AC10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB500052AC10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (142, 142, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7197. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000568470 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000568470 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (143, 143, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7198. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000005870A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50005870A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (144, 144, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7199. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000005870A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50005870A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (145, 145, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7200. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000005A5CD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50005A5CD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (146, 146, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7201. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x00000000005EC1D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB50005EC1D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (147, 147, N'9427949390', N'5675 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000AAD660 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000AAD660 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (148, 148, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7202. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000CC9ED0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000CC9ED0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (149, 149, N'9427949390', N'9333 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5000000000 AS DateTime), CAST(0x0000000000D14A20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5000D14A20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (150, 150, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7209. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5100000000 AS DateTime), CAST(0x0000000001728390 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5101728390 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (151, 151, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7210. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5100000000 AS DateTime), CAST(0x000000000175CF50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB510175CF50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (152, 152, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7211. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5100000000 AS DateTime), CAST(0x0000000001765BF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5101765BF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (153, 153, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7212. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5100000000 AS DateTime), CAST(0x00000000017C66D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB51017C66D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (154, 154, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7213. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5100000000 AS DateTime), CAST(0x00000000017D39C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB51017D39C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (155, 155, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7214. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5100000000 AS DateTime), CAST(0x00000000017E0CB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB51017E0CB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (156, 156, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7215. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5200000000 AS DateTime), CAST(0x0000000000004650 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5200004650 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (157, 157, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7216. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5200000000 AS DateTime), CAST(0x00000000001EC300 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB52001EC300 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (158, 158, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7217. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5200000000 AS DateTime), CAST(0x000000000020AF30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB520020AF30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (159, 159, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7218. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5200000000 AS DateTime), CAST(0x000000000030E3A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB520030E3A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (160, 160, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7219. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5200000000 AS DateTime), CAST(0x000000000031B690 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB520031B690 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (161, 161, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7220. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5400000000 AS DateTime), CAST(0x0000000000182B80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5400182B80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (162, 162, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7221. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5400000000 AS DateTime), CAST(0x00000000001C03E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB54001C03E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (163, 163, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7222. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5400000000 AS DateTime), CAST(0x0000000000236E50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5400236E50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (164, 164, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7223. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5400000000 AS DateTime), CAST(0x00000000002C7EA0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB54002C7EA0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (165, 165, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7224. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5400000000 AS DateTime), CAST(0x00000000002D0B40 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB54002D0B40 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (166, 166, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7225. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5400000000 AS DateTime), CAST(0x00000000004B41A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB54004B41A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (167, 167, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7226. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5600000000 AS DateTime), CAST(0x00000000003807C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB56003807C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (168, 168, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7227. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5600000000 AS DateTime), CAST(0x0000000000392100 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5600392100 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (169, 169, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7228. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5600000000 AS DateTime), CAST(0x00000000004277A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB56004277A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (170, 170, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7229. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5600000000 AS DateTime), CAST(0x0000000000488280 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5600488280 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (171, 171, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7230. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5600000000 AS DateTime), CAST(0x00000000004B41A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB56004B41A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (172, 172, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7234. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5700000000 AS DateTime), CAST(0x0000000001894F80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5701894F80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (173, 173, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7235. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5700000000 AS DateTime), CAST(0x00000000018AAF10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB57018AAF10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (174, 174, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7236. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5800000000 AS DateTime), CAST(0x0000000000023280 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5800023280 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (175, 175, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7237. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5800000000 AS DateTime), CAST(0x00000000005265C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB58005265C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (176, 176, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7238. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5800000000 AS DateTime), CAST(0x00000000005338B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB58005338B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (177, 177, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7239. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5800000000 AS DateTime), CAST(0x0000000000549840 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5800549840 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (178, 178, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7240. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5800000000 AS DateTime), CAST(0x00000000005B7610 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB58005B7610 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (179, 179, N'8767878784', N'2610 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5800000000 AS DateTime), CAST(0x0000000000648660 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5800648660 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (180, 180, N'8767878784', N'1372 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5900000000 AS DateTime), CAST(0x00000000000B42D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB59000B42D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (181, 181, N'8787688784', N'5212 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5900000000 AS DateTime), CAST(0x00000000000B42D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB59000B42D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (182, 182, N'8767878784', N'1885 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5900000000 AS DateTime), CAST(0x00000000000C15C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB59000C15C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (183, 183, N'8767878784', N'9599 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5900000000 AS DateTime), CAST(0x00000000000C5C10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB59000C5C10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (184, 184, N'7486876122', N'3463 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5A00000000 AS DateTime), CAST(0x0000000000AEF510 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5A00AEF510 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (185, 185, N'1234567890', N'8468 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5A00000000 AS DateTime), CAST(0x0000000000B12790 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5A00B12790 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (186, 186, N'8879200190', N'6697 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5B00000000 AS DateTime), CAST(0x000000000015B2B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5B0015B2B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (187, 187, N'8879200190', N'6697 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5B00000000 AS DateTime), CAST(0x000000000015B2B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5B0015B2B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (188, 188, N'8879200190', N'1055 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5B00000000 AS DateTime), CAST(0x000000000015B2B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5B0015B2B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (189, 189, N'8879200190', N'5510 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5B00000000 AS DateTime), CAST(0x000000000015B2B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5B0015B2B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (190, 190, N'8879200190', N'1653 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5B00000000 AS DateTime), CAST(0x000000000015B2B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5B0015B2B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (191, 191, N'8879200190', N'2116 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5B00000000 AS DateTime), CAST(0x000000000015B2B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5B0015B2B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (192, 192, N'9924188822', N'4759 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x0000000001598520 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C01598520 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (193, 193, N'9924188822', N'6731 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x00000000015D5D80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C015D5D80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (194, 194, N'9924188822', N'6800 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x00000000015DA3D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C015DA3D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (195, 195, N'8767878784', N'2870 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x00000000015F9000 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C015F9000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (196, 196, N'8767878784', N'8456 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x00000000015FD650 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C015FD650 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (197, 197, N'9924188822', N'2459 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x000000000160A940 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C0160A940 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (198, 198, N'8141857824', N'4323 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x0000000001629570 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C01629570 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (199, 199, N'8141857824', N'4497 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x000000000162DBC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C0162DBC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (200, 200, N'8141857824', N'6383 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x000000000163AEB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C0163AEB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (201, 201, N'8141857824', N'3513 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x000000000163AEB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C0163AEB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (202, 202, N'8141857824', N'7793 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x0000000001666DD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C01666DD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (203, 203, N'8141857824', N'3968 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5C00000000 AS DateTime), CAST(0x000000000167CD60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5C0167CD60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (204, 204, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7251. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x00000000003D3FB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D003D3FB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (205, 205, N'8141857824', N'6380 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x000000000068EB60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D0068EB60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (206, 206, N'9924188822', N'9500 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x000000000069BE50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D0069BE50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (207, 207, N'9978488822', N'7953 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x00000000006A4AF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D006A4AF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (208, 208, N'9978488822', N'9649 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x00000000006A9140 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D006A9140 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (209, 209, N'9978488822', N'4950 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x0000000000728850 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D00728850 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (210, 210, N'9427949390', N'2619 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x0000000001471E30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D01471E30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (211, 211, N'9924188822', N'7833 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x00000000015A11C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D015A11C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (212, 212, N'9924188822', N'4902 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x00000000015C8A90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D015C8A90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (213, 213, N'9978488822', N'2680 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x00000000015D1730 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D015D1730 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (214, 214, N'9978488822', N'8570 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x0000000001601CA0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D01601CA0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (215, 215, N'9978488822', N'5435 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5D00000000 AS DateTime), CAST(0x0000000001629570 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5D01629570 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (216, 216, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7252. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5E00000000 AS DateTime), CAST(0x00000000004B87F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5E004B87F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (217, 217, N'9427949390', N'9067 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB5E00000000 AS DateTime), CAST(0x0000000000A511D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5E00A511D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (218, 218, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7254. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5E00000000 AS DateTime), CAST(0x000000000167CD60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5E0167CD60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (219, 219, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7255. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5E00000000 AS DateTime), CAST(0x0000000001746FC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5E01746FC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (220, 220, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7256. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5E00000000 AS DateTime), CAST(0x00000000018344A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5E018344A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (221, 221, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7257. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5E00000000 AS DateTime), CAST(0x0000000001845DE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5E01845DE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (222, 222, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7258. Amount: 165', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB5F00000000 AS DateTime), CAST(0x0000000001735680 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB5F01735680 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (223, 223, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7259. Amount: 40', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6000000000 AS DateTime), CAST(0x00000000001D1D20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB60001D1D20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (224, 224, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7260. Amount: 920', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6000000000 AS DateTime), CAST(0x00000000005A1680 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB60005A1680 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (225, 225, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7261. Amount: 45', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6000000000 AS DateTime), CAST(0x00000000005D1BF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB60005D1BF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (226, 226, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7262. Amount: 155', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6100000000 AS DateTime), CAST(0x00000000001C4A30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB61001C4A30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (227, 227, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7264. Amount: 155', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6100000000 AS DateTime), CAST(0x000000000063B370 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB610063B370 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (228, 228, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7265. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6500000000 AS DateTime), CAST(0x0000000000072420 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6500072420 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (229, 229, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7266. Amount: 120', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6500000000 AS DateTime), CAST(0x0000000000220EC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6500220EC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (230, 230, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7267. Amount: 120', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6500000000 AS DateTime), CAST(0x0000000000220EC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6500220EC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (231, 231, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7268. Amount: 96', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6700000000 AS DateTime), CAST(0x00000000016481A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB67016481A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (232, 232, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7269. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6700000000 AS DateTime), CAST(0x00000000016AD2D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB67016AD2D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (233, 233, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7270. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6700000000 AS DateTime), CAST(0x00000000016BEC10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB67016BEC10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (234, 234, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7271. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6700000000 AS DateTime), CAST(0x00000000016EAB30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB67016EAB30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (235, 235, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7272. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6700000000 AS DateTime), CAST(0x0000000001700AC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6701700AC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (236, 236, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7273. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6700000000 AS DateTime), CAST(0x0000000001700AC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6701700AC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (237, 237, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7274. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6700000000 AS DateTime), CAST(0x0000000001772EE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6701772EE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (238, 238, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7275. Amount: 290', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6800000000 AS DateTime), CAST(0x00000000000A6FE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB68000A6FE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (239, 239, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7280. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6A00000000 AS DateTime), CAST(0x000000000167CD60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6A0167CD60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (240, 240, N'9408932602', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7281. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6C00000000 AS DateTime), CAST(0x00000000000278D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6C000278D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (241, 241, N'4646464945', N'2361 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB6C00000000 AS DateTime), CAST(0x0000000001803F30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6C01803F30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (242, 242, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7282. Amount: 57.14420', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x0000000000469650 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D00469650 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (243, 243, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7283. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x00000000004AFB50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D004AFB50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (244, 244, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7284. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x0000000000563E20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D00563E20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (245, 245, N'6464941946', N'5757 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x000000000061C740 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D0061C740 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (246, 246, N'1111111111', N'3606 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D0066B8E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (247, 247, N'8879200190', N'5817 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x00000000008C1360 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D008C1360 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (248, 248, N'8879200190', N'6916 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x00000000008C1360 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D008C1360 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (249, 249, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7285. Amount: 45', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x00000000016F37D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D016F37D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (250, 250, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7286. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6D00000000 AS DateTime), CAST(0x0000000001815870 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6D01815870 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (251, 251, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7287. Amount: 120', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x0000000000015F90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E00015F90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (252, 252, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7288. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x0000000000030570 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E00030570 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (253, 253, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7289. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x0000000000030570 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E00030570 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (254, 254, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7290. Amount: 72.5', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x0000000000041EB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E00041EB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (255, 255, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7291. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x0000000000046500 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E00046500 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (256, 256, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7293. Amount: 120', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x00000000000CE8B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E000CE8B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (257, 257, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7294. Amount: 40', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x0000000000138030 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E00138030 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (258, 258, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7295. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x000000000014DFC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E0014DFC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (259, 259, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7296. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x000000000015F900 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E0015F900 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (260, 260, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7297. Amount: 40', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x00000000001685A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E001685A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (261, 261, N'8767678484', N'9055 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB6E00000000 AS DateTime), CAST(0x0000000000309D50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB6E00309D50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (262, 262, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7300. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7200000000 AS DateTime), CAST(0x000000000181E510 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB720181E510 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (263, 263, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7301. Amount: 70', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7200000000 AS DateTime), CAST(0x000000000181E510 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB720181E510 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (264, 264, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7302. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7200000000 AS DateTime), CAST(0x000000000183D140 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB720183D140 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (265, 265, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7303. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7200000000 AS DateTime), CAST(0x000000000183D140 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB720183D140 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (266, 266, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7306. Amount: 155', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7400000000 AS DateTime), CAST(0x00000000000278D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB74000278D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (267, 267, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7307. Amount: 210', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x0000000000297930 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7500297930 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (268, 268, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7308. Amount: 210', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000002A4C20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75002A4C20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (269, 269, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7309. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000002B1F10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75002B1F10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (270, 270, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7310. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000002BF200 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75002BF200 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (271, 271, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7311. Amount: 380', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000005C8F50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75005C8F50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (272, 272, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7312. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000005CD5A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75005CD5A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (273, 273, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7313. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000005D1BF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75005D1BF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (274, 274, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7314. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000005E7B80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75005E7B80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (275, 275, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7315. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000005F4E70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75005F4E70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (276, 276, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7316. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000006067B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75006067B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (277, 277, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7317. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x000000000060AE00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB750060AE00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (278, 278, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7318. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x000000000160A940 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB750160A940 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (279, 279, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7319. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000016BEC10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75016BEC10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (280, 280, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7320. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000016C78B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75016C78B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (281, 281, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7321. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000016CBF00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75016CBF00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (282, 282, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7322. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x000000000171B0A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB750171B0A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (283, 283, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7323. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x0000000001739CD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7501739CD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (284, 284, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7324. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x000000000173E320 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB750173E320 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (285, 285, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7325. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x0000000001742970 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7501742970 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (286, 286, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7326. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000017B4D90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75017B4D90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (287, 287, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7327. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000017B4D90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75017B4D90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (288, 288, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7328. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000017B4D90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75017B4D90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (289, 289, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7329. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000017C66D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75017C66D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (290, 290, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7330. Amount: 65', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000017CAD20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75017CAD20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (291, 291, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7331. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000017EDFA0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75017EDFA0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (292, 292, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7332. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000017F25F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75017F25F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (293, 293, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7333. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x0000000001894F80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7501894F80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (294, 294, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7334. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7500000000 AS DateTime), CAST(0x00000000018AF560 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB75018AF560 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (295, 295, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7335. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7600000000 AS DateTime), CAST(0x0000000000011940 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7600011940 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (296, 296, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7336. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7600000000 AS DateTime), CAST(0x000000000002BF20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB760002BF20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (297, 297, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7337. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7600000000 AS DateTime), CAST(0x00000000000CA260 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB76000CA260 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (298, 298, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7338. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7600000000 AS DateTime), CAST(0x00000000003FB880 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB76003FB880 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (299, 299, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7339. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7600000000 AS DateTime), CAST(0x000000000047F5E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB760047F5E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (300, 300, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7340. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7600000000 AS DateTime), CAST(0x0000000000483C30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7600483C30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (301, 301, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7341. Amount: 1', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7600000000 AS DateTime), CAST(0x0000000000537F00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7600537F00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (302, 302, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7342. Amount: 155', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7A00000000 AS DateTime), CAST(0x0000000001803F30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7A01803F30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (303, 303, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7343. Amount: 155', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7A00000000 AS DateTime), CAST(0x000000000187EFF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7A0187EFF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (304, 304, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7346. Amount: 591.00', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000000FEE20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C000FEE20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (305, 305, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7347. Amount: 312.00', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x000000000022E1B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C0022E1B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (306, 306, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7348. Amount: 522', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x000000000033E910 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C0033E910 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (307, 307, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7349. Amount: 162.00', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x0000000000350250 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C00350250 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (308, 308, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7350. Amount: 321', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000003BE020 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C003BE020 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (309, 309, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7351. Amount: 170.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x0000000000483C30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C00483C30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (310, 310, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7352. Amount: 250.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000004B41A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C004B41A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (311, 311, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7353. Amount: 70.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x0000000000602160 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C00602160 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (312, 312, N'9825960054', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7358. Amount: 70.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000017615A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C017615A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (313, 313, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7359. Amount: 70.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x0000000001784820 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C01784820 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (314, 314, N'9825960554', N'3981 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000017B0740 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C017B0740 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (315, 315, N'9825960554', N'7890 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000017B4D90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C017B4D90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (316, 316, N'9825960554', N'8394 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000017B4D90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C017B4D90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (317, 317, N'8767878784', N'8912 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB7C00000000 AS DateTime), CAST(0x00000000017CAD20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7C017CAD20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (318, 318, N'7486876122', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7360. Amount: 113', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000ADDBD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00ADDBD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (319, 319, N'7486876122', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 71. Amount: 140', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000CAF8F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00CAF8F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (320, 320, N'7486876122', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 72. Amount: 140', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000CAF8F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00CAF8F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (321, 321, N'7486876122', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 73. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000EB61D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00EB61D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (322, 322, N'7486876122', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 74. Amount: 132', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000EBEE70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00EBEE70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (323, 323, N'9427949390', N'7135 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000EF3A30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00EF3A30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (324, 324, N'9427949390', N'Please use ? ??I this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000F1F950 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00F1F950 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (325, 325, N'7486876122', N'Please use 12345678 this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB7D00000000 AS DateTime), CAST(0x0000000000F285F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB7D00F285F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (326, 326, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 75. Amount: 280.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x0000000000023280 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8000023280 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (327, 327, N'9033213625', N'7325 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000159CB70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB800159CB70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (328, 328, N'9033213625', N'7903 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x00000000015A11C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB80015A11C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (329, 329, N'9825959646', N'3127 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x00000000015A5810 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB80015A5810 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (330, 330, N'9033213625', N'2978 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x00000000015C4440 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB80015C4440 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (331, 331, N'9033213625', N'4173 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x00000000015C4440 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB80015C4440 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (332, 332, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 76. Amount: 300.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000178D4C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB800178D4C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (333, 333, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 77. Amount: 130.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000179A7B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB800179A7B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (334, 334, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 78. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000000D2F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB810000D2F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (335, 335, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 79. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000034BC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8100034BC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (336, 336, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 710. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000072420 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8100072420 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (337, 337, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 711. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000007F710 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB810007F710 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (338, 338, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 712. Amount: 50.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000091050 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8100091050 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (339, 339, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 713. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000000B42D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB81000B42D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (340, 340, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 714. Amount: 130.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000000B8920 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB81000B8920 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (341, 341, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 716. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000025E720 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB810025E720 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (342, 342, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 717. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000002746B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB81002746B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (343, 343, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 719. Amount: 60.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000285FF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8100285FF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (344, 344, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 720. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000002B6560 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB81002B6560 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (345, 345, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 721. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000002D5190 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB81002D5190 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (346, 346, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 722. Amount: 120.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000305700 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8100305700 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (347, 347, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 723. Amount: 120.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000030E3A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB810030E3A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (348, 348, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 724. Amount: 60.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000317040 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8100317040 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (349, 349, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 725. Amount: 330', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000016BA5C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB81016BA5C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (350, 350, N'7486876122', N'5638 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000B00E50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200B00E50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (351, 351, N'7486876122', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 726. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000BA37E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200BA37E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (352, 352, N'7486876122', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 727. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000BC2410 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200BC2410 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (353, 353, N'7486876122', N'3970 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C042C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200C042C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (354, 354, N'7486876122', N'6836 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C08910 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200C08910 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (355, 355, N'7486876122', N'3951 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C08910 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200C08910 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (356, 356, N'9427949390', N'1932 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C7F380 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200C7F380 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (357, 357, N'9427949390', N'8769 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000C95310 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8200C95310 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (358, 358, N'8767878784', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 726. Amount: 200.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x000000000164C7F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB820164C7F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (359, 359, N'9825960554', N'3650 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x000000000165E130 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB820165E130 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (360, 360, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 728. Amount: 172.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x00000000017A7AA0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB82017A7AA0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (361, 361, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 729. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x000000000180CBD0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB820180CBD0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (362, 362, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 730. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000001811220 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8201811220 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (363, 363, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 731. Amount: 324.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x00000000018530D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB82018530D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (364, 364, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 732. Amount: 324.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8200000000 AS DateTime), CAST(0x00000000018530D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB82018530D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (365, 365, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 733. Amount: 578', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000358EF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8300358EF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (366, 366, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 734. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000006067B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB83006067B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (367, 367, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 735. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000060AE00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB830060AE00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (368, 368, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 740. Amount: 70.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000BE1040 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8300BE1040 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (369, 369, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 741. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000165E130 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB830165E130 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (370, 370, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 742. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000016D0550 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB83016D0550 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (371, 371, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 743. Amount: 230.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000017EDFA0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB83017EDFA0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (372, 372, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 744. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000232800 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8400232800 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (373, 373, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 745. Amount: 602.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000004B41A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB84004B41A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (374, 374, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 746. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000004C5AE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB84004C5AE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (375, 375, N'9974402476', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 747. Amount: 80.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8500000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB85002068E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (376, 376, N'9974402476', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 748. Amount: 80.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8500000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB85002068E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (377, 377, N'9974402476', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 749. Amount: 80.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8500000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB85002068E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (378, 378, N'9408387650', N'3530 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000006EAFF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87006EAFF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (379, 379, N'9408387650', N'3530 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000006EAFF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87006EAFF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (380, 380, N'9408387650', N'3530 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000006EAFF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87006EAFF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (381, 381, N'9408387650', N'3530 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000006EAFF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87006EAFF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (382, 382, N'9408387650', N'2342 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000006EAFF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87006EAFF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (383, 383, N'9408387650', N'5886 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000006EAFF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87006EAFF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (384, 384, N'9408387650', N'1895 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000006F3C90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87006F3C90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (385, 385, N'9825960554', N'1143 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x000000000071FBB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB870071FBB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (386, 386, N'8140122070', N'4324 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000007E1170 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87007E1170 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (387, 387, N'9825960554', N'3684 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x0000000001705110 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8701705110 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (388, 388, N'9825960554', N'7299 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x00000000017542B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB87017542B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (389, 389, N'9825960554', N'4860 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x0000000001758900 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8701758900 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (390, 390, N'9825960554', N'7244 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x0000000001758900 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8701758900 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (391, 391, N'9825960554', N'8874 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8700000000 AS DateTime), CAST(0x000000000175CF50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB870175CF50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (392, 392, N'9825960554', N'9624 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8800000000 AS DateTime), CAST(0x000000000013C680 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB880013C680 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (393, 393, N'9825960554', N'7807 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8800000000 AS DateTime), CAST(0x000000000015B2B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB880015B2B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (394, 394, N'9825960554', N'4152 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8800000000 AS DateTime), CAST(0x00000000001685A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB88001685A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (395, 395, N'9825960554', N'3128 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8800000000 AS DateTime), CAST(0x0000000000175890 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8800175890 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (396, 396, N'9825960554', N'1633 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8800000000 AS DateTime), CAST(0x0000000000175890 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8800175890 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (397, 397, N'8140122070', N'1773 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8800000000 AS DateTime), CAST(0x0000000000761A60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8800761A60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (398, 398, N'9825960554', N'6823 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000000377B20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8900377B20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (399, 399, N'9825960554', N'1990 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000037C170 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890037C170 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (400, 400, N'9825960554', N'Please use ?FY1? this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x00000000003807C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB89003807C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (401, 401, N'9825960554', N'2621 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000000384E10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8900384E10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (402, 402, N'9825960554', N'7668 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x00000000003BE020 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB89003BE020 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (403, 403, N'8879200190', N'6266 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x00000000015DEA20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB89015DEA20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (404, 404, N'9825960557', N'4643 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000160A940 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890160A940 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (405, 405, N'9825960557', N'5523 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000160EF90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890160EF90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (406, 406, N'9825960554', N'7791 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000160EF90 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890160EF90 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (407, 407, N'9825960557', N'9334 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001624F20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901624F20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (408, 408, N'9825960554', N'6430 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001624F20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901624F20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (409, 409, N'9825960554', N'7605 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001629570 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901629570 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (410, 410, N'9825960554', N'8086 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001632210 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901632210 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (411, 411, N'9825960554', N'5912 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001685A00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901685A00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (412, 412, N'9825960554', N'6708 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x00000000016AD2D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB89016AD2D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (413, 413, N'9825960554', N'5718 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x00000000016BEC10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB89016BEC10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (414, 414, N'9825960554', N'1338 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001700AC0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901700AC0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (415, 415, N'9825960554', N'7238 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000170DDB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890170DDB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (416, 416, N'9825960554', N'3958 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000171B0A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890171B0A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (417, 417, N'9825960554', N'6713 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001731030 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901731030 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (418, 418, N'9825960554', N'1615 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000175CF50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890175CF50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (419, 419, N'9825960554', N'6551 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000176E890 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890176E890 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (420, 420, N'9825960554', N'9109 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000177BB80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890177BB80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (421, 421, N'9825960554', N'2535 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001788E70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901788E70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (422, 422, N'9825960554', N'2106 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001796160 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901796160 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (423, 423, N'9825960554', N'4053 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x00000000017CAD20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB89017CAD20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (424, 424, N'9825960554', N'1845 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x00000000017FB290 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB89017FB290 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (425, 425, N'9825960554', N'9043 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x0000000001841790 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8901841790 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (426, 426, N'9825960554', N'Please use ?FY1? this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB8900000000 AS DateTime), CAST(0x000000000184A430 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB890184A430 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (427, 427, N'9825960554', N'7930 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x00000000000B8920 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A000B8920 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (428, 428, NULL, N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 750. Amount: 96.0', 1, N'Order SMS', N'ERROR : 1', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000000179EE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A00179EE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (429, 429, N'9825960554', N'7937 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x000000000018B820 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A0018B820 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (430, 430, N'9825960554', N'5682 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000000198B10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A00198B10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (431, 431, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 751. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x00000000001CD6D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A001CD6D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (432, 432, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 752. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x00000000002F8410 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A002F8410 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (433, 433, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 753. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x00000000003661E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A003661E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (434, 434, N'9825960554', N'Please use ?FY1? this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB8A00000000 AS DateTime), CAST(0x000000000053C550 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8A0053C550 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (435, 435, N'9825960554', N'Please use 123456789 this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000DC46A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B00DC46A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (436, 436, N'9825960554', N'Please use 123456789 this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000DD1990 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B00DD1990 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (437, 437, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 754. Amount: 200.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000297930 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B00297930 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (438, 438, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 755. Amount: 200.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000297930 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B00297930 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (439, 439, N'9828960554', N'4372 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000029BF80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0029BF80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (440, 440, N'9828960554', N'5863 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000002A05D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B002A05D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (441, 441, N'9825960554', N'6212 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000002A4C20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B002A4C20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (442, 442, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 756. Amount: 156', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000002A9270 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B002A9270 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (443, 443, N'9825960554', N'7553 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000044AA20 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0044AA20 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (444, 444, N'9825960554', N'Please use 123456789 this password for login.', 1, N'Forgot Password SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000044F070 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0044F070 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (445, 445, N'9825960554', N'8593 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000004CE780 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B004CE780 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (446, 446, N'9825960554', N'8593 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000004CE780 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B004CE780 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (447, 447, N'9825960554', N'4678 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000004CE780 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B004CE780 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (448, 448, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 757. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000052F260 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0052F260 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (449, 449, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 758. Amount: 60.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000052F260 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0052F260 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (450, 450, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 759. Amount: 50.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000005338B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B005338B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (451, 451, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 760. Amount: 60.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000537F00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B00537F00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (452, 452, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 761. Amount: 150.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000537F00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B00537F00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (453, 453, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 762. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000055F7D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0055F7D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (454, 454, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 763. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000055F7D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0055F7D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (455, 455, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 764. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000005870A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B005870A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (456, 456, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 765. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x00000000005870A0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B005870A0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (457, 457, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 766. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000058B6F0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8B0058B6F0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (458, 458, N'8140122070', N'5901 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB8C00000000 AS DateTime), CAST(0x00000000007AC5B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8C007AC5B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (459, 459, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 767. Amount: 140.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB8C00000000 AS DateTime), CAST(0x00000000007B5250 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB8C007B5250 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (460, 460, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 768. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB9400000000 AS DateTime), CAST(0x00000000003734D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB94003734D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (461, 461, N'9427949391', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 769. Amount: 100', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB9400000000 AS DateTime), CAST(0x0000000000A0F320 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB9400A0F320 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (462, 462, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 770. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB9400000000 AS DateTime), CAST(0x000000000182FE50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB940182FE50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (463, 463, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 771. Amount: 142', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB9600000000 AS DateTime), CAST(0x0000000000568470 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB9600568470 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (464, 464, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 772. Amount: 142', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB9600000000 AS DateTime), CAST(0x0000000000571110 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB9600571110 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (465, 465, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 773. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB9600000000 AS DateTime), CAST(0x000000000176E890 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB960176E890 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (466, 466, N'8140122070', N'5510 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000AB9E00000000 AS DateTime), CAST(0x0000000001765BF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB9E01765BF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (467, 467, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 774. Amount: 520.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AB9E00000000 AS DateTime), CAST(0x0000000001845DE0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AB9E01845DE0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (468, 468, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 775. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABA100000000 AS DateTime), CAST(0x00000000002673C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABA1002673C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (469, 469, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 776. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABA100000000 AS DateTime), CAST(0x000000000026BA10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABA10026BA10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (470, 470, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 777. Amount: 150.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000278D00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABA100278D00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (471, 471, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 778. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000285FF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABA100285FF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (472, 472, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 779. Amount: 160.020', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000297930 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABA100297930 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (473, 473, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 780. Amount: 1285.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABA900000000 AS DateTime), CAST(0x0000000000C88020 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABA900C88020 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (474, 474, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 781. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABAC00000000 AS DateTime), CAST(0x000000000017E530 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABAC0017E530 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (475, 475, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 782. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABAC00000000 AS DateTime), CAST(0x000000000017E530 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABAC0017E530 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (476, 476, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 783. Amount: 164', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB100000000 AS DateTime), CAST(0x0000000001765BF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB101765BF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (477, 477, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 784. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB400000000 AS DateTime), CAST(0x00000000017FF8E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB4017FF8E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (478, 478, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 785. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB400000000 AS DateTime), CAST(0x000000000188C2E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB40188C2E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (479, 479, N'9998587385', N'5309 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000003C2670 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB5003C2670 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (480, 480, N'7486876122', N'7737 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000003CB310 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB5003CB310 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (481, 481, N'7486876122', N'1124 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000003CB310 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB5003CB310 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (482, 482, N'7698096070', N'8352 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000005A1680 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB5005A1680 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (483, 483, N'7698096070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 786. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000006D0A10 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB5006D0A10 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (484, 484, N'7698096070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 787. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000006D5060 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB5006D5060 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (485, 485, N'9510136305', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 788. Amount: 470.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000006D5060 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB5006D5060 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (486, 486, N'9106806269', N'3060 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000AEF510 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB500AEF510 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (487, 487, N'9106806269', N'3060 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000AEF510 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB500AEF510 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (488, 488, N'9106806269', N'1726 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000AEF510 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB500AEF510 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (489, 489, N'9106806269', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 789. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000AF81B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB500AF81B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (490, 490, N'9510136305', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 790. Amount: 200.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB600000000 AS DateTime), CAST(0x0000000000CB3F40 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB600CB3F40 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (491, 491, N'9016216485', N'9673 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB600000000 AS DateTime), CAST(0x0000000000CDFE60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB600CDFE60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (492, 492, N'9016216485', N'9173 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABB600000000 AS DateTime), CAST(0x0000000000CDFE60 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB600CDFE60 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (493, 493, N'9016216485', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 791. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB600000000 AS DateTime), CAST(0x0000000000CF5DF0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB600CF5DF0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (494, 494, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 792. Amount: 166', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABB800000000 AS DateTime), CAST(0x000000000177BB80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABB80177BB80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (495, 495, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 793. Amount: 180.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABBC00000000 AS DateTime), CAST(0x00000000003A3A40 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABBC003A3A40 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (496, 496, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 794. Amount: 140.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABBE00000000 AS DateTime), CAST(0x0000000001735680 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABBE01735680 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (497, 497, N'8140122070', N'5387 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ABBE00000000 AS DateTime), CAST(0x000000000178D4C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABBE0178D4C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (498, 498, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 795. Amount: 111', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABBF00000000 AS DateTime), CAST(0x0000000000A8EA30 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABBF00A8EA30 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (499, 499, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 796. Amount: 920.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC100000000 AS DateTime), CAST(0x00000000017FB290 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC1017FB290 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (500, 500, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 797. Amount: 90.020', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC300000000 AS DateTime), CAST(0x00000000005C4900 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC3005C4900 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (501, 501, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 798. Amount: 196', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC500000000 AS DateTime), CAST(0x00000000017B0740 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC5017B0740 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (502, 502, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 799. Amount: 248.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC500000000 AS DateTime), CAST(0x00000000017B93E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC5017B93E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (503, 503, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7100. Amount: 246.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC800000000 AS DateTime), CAST(0x0000000000A0F320 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC800A0F320 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (504, 504, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7101. Amount: 159', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC800000000 AS DateTime), CAST(0x0000000000A1C610 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC800A1C610 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (505, 505, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7102. Amount: 248.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000103470 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC900103470 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (506, 506, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7103. Amount: 248.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000103470 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC900103470 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (507, 507, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7104. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000119400 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABC900119400 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (508, 508, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7105. Amount: 604', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ABCA00000000 AS DateTime), CAST(0x00000000016208D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ABCA016208D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (509, 509, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7106. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AC1000000000 AS DateTime), CAST(0x0000000001723D40 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AC1001723D40 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (510, 510, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7107. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AC2200000000 AS DateTime), CAST(0x00000000002A05D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AC22002A05D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (511, 511, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7108. Amount: 96.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AC2200000000 AS DateTime), CAST(0x00000000016F37D0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AC22016F37D0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (512, 512, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7109. Amount: 200.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000AC3600000000 AS DateTime), CAST(0x00000000002D0B40 AS DateTime), 0, 0, N'1', 1, CAST(0x0000AC36002D0B40 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (513, 513, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7110. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ACB700000000 AS DateTime), CAST(0x000000000179A7B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ACB70179A7B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (514, 514, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7111. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ACB700000000 AS DateTime), CAST(0x000000000179EE00 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ACB70179EE00 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (515, 515, N'8140122070', N'6444 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ADA200000000 AS DateTime), CAST(0x000000000142FF80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADA20142FF80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (516, 516, N'8140122070', N'9330 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ADA200000000 AS DateTime), CAST(0x000000000142FF80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADA20142FF80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (517, 517, N'8140122070', N'2402 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ADA200000000 AS DateTime), CAST(0x000000000142FF80 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADA20142FF80 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (518, 518, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7112. Amount: 290.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADA300000000 AS DateTime), CAST(0x000000000179A7B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADA30179A7B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (519, 519, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7113. Amount: 290.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADA300000000 AS DateTime), CAST(0x000000000179A7B0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADA30179A7B0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (520, 520, N'9924188822', N'1746 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ADA500000000 AS DateTime), CAST(0x00000000005C8F50 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADA5005C8F50 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (521, 521, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7114. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADAB00000000 AS DateTime), CAST(0x0000000000602160 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADAB00602160 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (522, 522, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7115. Amount: 80.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB800000000 AS DateTime), CAST(0x0000000000423150 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB800423150 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (523, 523, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7116. Amount: 80.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB800000000 AS DateTime), CAST(0x0000000000423150 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB800423150 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (524, 524, N'8140122070', N'4804 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x000000000071B560 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB90071B560 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (525, 525, N'8140122070', N'2959 is your secret One Time Password(OTP) for account varification. Please use this password to complete varification.', 1, N'Sign up SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x000000000071B560 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB90071B560 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (526, 526, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7117. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x000000000071FBB0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB90071FBB0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (527, 527, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7118. Amount: 100.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000007CB1E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB9007CB1E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (528, 528, N'8140122070', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7119. Amount: 170.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000007CF830 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB9007CF830 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (529, 529, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7120. Amount: 140.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x0000000001788E70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB901788E70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (530, 530, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7121. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x0000000001788E70 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB901788E70 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (531, 531, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7122. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x000000000178D4C0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB90178D4C0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (532, 532, N'9825960554', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7123. Amount: 110.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADB900000000 AS DateTime), CAST(0x0000000001796160 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADB901796160 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (533, 533, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7124. Amount: 320.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000009FD9E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADC3009FD9E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smshistory] ([ID], [uid1], [MOBILENO], [MESSAGE], [SENDFLG], [MSGTYPE], [RESPONSEID], [SmsPersonName], [sendDate], [sendTime], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (534, 534, N'9427949390', N'Thanks for choosing infinity bit . We will deliver your order shortly. Order No: 7125. Amount: 320.0', 1, N'Order SMS', N'SUCESS : ', N'', CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000009FD9E0 AS DateTime), 0, 0, N'1', 1, CAST(0x0000ADC3009FD9E0 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[smssetting] ([ID], [SMSSENDTYPE], [SMSUSERNAME], [SMSUSERPASSWORD], [SMSSENDERID], [SmsApi], [Feed], [SmsApi1], [AppSmsType], [ProUserName], [ProPassword], [ProFeed], [SMSAPIKEY], [ProSMSAPIKEY], [ProSMSApi], [TCountryCode], [PCountryCode], [SMSBill], [SMSOrder], [ColSMS], [BillEdit], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime]) VALUES (1, NULL, N'kamal.raval90@yahoo.com', N'', NULL, 0, N'FOJUIC', NULL, NULL, N'kamal.raval90@yahoo.com', N'', N'FOJUIC', N'293068A9LzChkz5d73e5ea', N'293068A9LzChkz5d73e5ea', 0, 91, 91, 0, 0, 0, 0, 7, 1, N'1', 0, CAST(0x0000AACE00065823 AS DateTime), NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (71, 1, 71, 12, 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000D2A9B0 AS DateTime), 2, 60, 0, 0, 120, 140, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 50, CAST(0x0000AB7D00CB07B5 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (72, 2, 72, 12, 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000D2A9B0 AS DateTime), 2, 60, 0, 0, 120, 140, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 50, CAST(0x0000AB7D00CB08F3 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (73, 3, 73, 14, 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x00000000002D97E0 AS DateTime), 1, 80, 0, 0, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 50, CAST(0x0000AB7D00EB6C7B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (74, 4, 74, 14, 71, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x00000000002E6AD0 AS DateTime), 2, 80, 0, 0, 160, 132, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 50, CAST(0x0000AB7D00EC2DFD AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (75, 5, 75, 111, 71, CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000033A2C0 AS DateTime), 2, 130, 0, 0, 248, 280, 2.5, 6.19, 2.5, 6.19, 5, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB8000026D92 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (76, 6, 76, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001E7CB0 AS DateTime), 2, 100, 10, 24, 280, 300, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB800178EC7B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (77, 7, 76, 16, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001E7CB0 AS DateTime), 2, 100, 10, 24, 280, 300, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB800178EC7B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (78, 8, 77, 17, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F4FA0 AS DateTime), 1, 110, 0, 0, 110, 130, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB800179AAA2 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (79, 9, 78, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000031FCE0 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB810000F076 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (710, 10, 79, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000034BC00 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB810003850C AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (711, 11, 710, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000384E10 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB8100073451 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (712, 12, 711, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000396750 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB8100082B45 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (713, 13, 712, 19, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003A3A40 AS DateTime), 1, 30, 0, 0, 29, 50, 2.5, 0.71, 2.5, 0.71, 5, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB81000925C8 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (714, 14, 713, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003CB310 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB81000B7261 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (715, 15, 714, 17, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003CB310 AS DateTime), 1, 110, 0, 0, 110, 130, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB81000B8F91 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (716, 16, 715, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000DFD8B0 AS DateTime), 4, 80, 5, 16, 320, 340, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 69, CAST(0x0000AB8100120C8C AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (717, 17, 716, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000571110 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB810025F3AA AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (718, 18, 717, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005870A0 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB8100274ABA AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (719, 19, 718, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000F5D1B0 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 69, CAST(0x0000AB810027E443 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (720, 20, 719, 18, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000F65E50 AS DateTime), 1, 40, 0, 0, 38, 60, 2.5, 0.95, 2.5, 0.95, 5, 0, N'', 0, 7, 0, N'1', 69, CAST(0x0000AB8100289AA7 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (721, 21, 720, 116, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000F963C0 AS DateTime), 1, 80, 5, 4, 80, 100, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 69, CAST(0x0000AB81002BA20D AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (722, 22, 721, 18, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005E7B80 AS DateTime), 2, 40, 0, 0, 76, 100, 2.5, 1.9, 2.5, 1.9, 5, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB81002D647E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (723, 23, 722, 16, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000061C740 AS DateTime), 1, 100, 10, 10, 100, 120, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB81003077C8 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (724, 24, 723, 16, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000006253E0 AS DateTime), 1, 100, 10, 10, 100, 120, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB8100310E63 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (725, 25, 724, 18, 71, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000062E080 AS DateTime), 1, 40, 0, 0, 38, 60, 2.5, 0.95, 2.5, 0.95, 5, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB8100319154 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (726, 26, 725, 116, 71, CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000AD08E0 AS DateTime), 2, 90, 10, 28, 360, 330, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB81016BB31F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dvoFEBOl4YM:APA91bGUOUyjmlUxk3H6tID4MxVkz_Jac_Z8CnocwQYfHWtEvW68VaCr1bvoSIDKURKZTJp-t56cqJ-bt0aIGHBrmV5DB1ujYuY7-jrb2xsr9PlctRPL_-N_wpk70xpAY-lhFhhRJW3I')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (727, 27, 725, 16, 71, CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000AD08E0 AS DateTime), 2, 90, 10, 28, 360, 330, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB81016BB31F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dvoFEBOl4YM:APA91bGUOUyjmlUxk3H6tID4MxVkz_Jac_Z8CnocwQYfHWtEvW68VaCr1bvoSIDKURKZTJp-t56cqJ-bt0aIGHBrmV5DB1ujYuY7-jrb2xsr9PlctRPL_-N_wpk70xpAY-lhFhhRJW3I')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (728, 28, 726, 16, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000A6FE00 AS DateTime), 2, 90, 10, 0, 180, 200, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 60, CAST(0x0000AB8201650080 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dvoFEBOl4YM:APA91bGUOUyjmlUxk3H6tID4MxVkz_Jac_Z8CnocwQYfHWtEvW68VaCr1bvoSIDKURKZTJp-t56cqJ-bt0aIGHBrmV5DB1ujYuY7-jrb2xsr9PlctRPL_-N_wpk70xpAY-lhFhhRJW3I')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (729, 29, 727, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000000C15C0 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8201669B55 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (730, 30, 728, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 2, 76, 5, 0, 152, 172, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB82017AAC6B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (731, 31, 729, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002673C0 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB820180E953 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (732, 32, 730, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000026BA10 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8201811703 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (733, 33, 731, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002AD8C0 AS DateTime), 4, 76, 5, 0, 304, 324, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8201853334 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (734, 34, 732, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002AD8C0 AS DateTime), 4, 76, 5, 0, 304, 324, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB82018533A4 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (735, 35, 733, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 1, 90, 0, 0, 646, 578, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830035BBB9 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (736, 36, 733, 113, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 1, 90, 0, 0, 646, 578, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830035BBBE AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (737, 37, 733, 112, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 1, 90, 0, 0, 646, 578, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830035BBBE AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (738, 38, 733, 111, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 1, 90, 0, 0, 646, 578, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830035BBBE AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (739, 39, 733, 110, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 1, 90, 0, 0, 646, 578, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830035BBBE AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (740, 40, 733, 15, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 1, 90, 0, 0, 646, 578, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830035BBBE AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (741, 41, 734, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000012E6610 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830060A11D AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (742, 42, 735, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000012EAC60 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830060EFC7 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (743, 43, 736, 116, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000001709760 AS DateTime), 1, 90, 10, 0, 166, 186, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000AB8300A2EE09 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (744, 44, 736, 16, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000001709760 AS DateTime), 1, 90, 10, 0, 166, 186, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000AB8300A2EE1B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (745, 45, 737, 12, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000017AC0F0 AS DateTime), 1, 50, 0, 0, 108, 130, 2.5, 1.19, 2.5, 1.19, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000AB8300ACF215 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (746, 46, 737, 11, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000017AC0F0 AS DateTime), 1, 50, 0, 0, 108, 130, 2.5, 1.19, 2.5, 1.19, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000AB8300ACF215 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (747, 47, 738, 15, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000017B4D90 AS DateTime), 1, 90, 0, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000AB8300AD7893 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (748, 48, 739, 19, 71, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000B6A5D0 AS DateTime), 1, 30, 0, 0, 29, 50, 2.5, 0.71, 2.5, 0.71, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000AB8300AEB56F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (749, 49, 740, 11, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000008CA0 AS DateTime), 1, 50, 0, 0, 48, 70, 2.5, 1.19, 2.5, 1.19, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000AB8300BE3D17 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (750, 50, 741, 116, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000A81740 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB830165E71E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (751, 51, 742, 116, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000AF3B60 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 95, CAST(0x0000AB83016D1156 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cfmTlkU7iDE:APA91bF1g_VAVngm8VJ1ReQNMPIgkQIEZLdUwUKeIBmnYHCRGv0NXeEvNnBd3qNBi1IpQPVetcDo-cAO-uQbhfZ66GpgXU6n-YVAbZbNkRXrbePjigxk2BeFiG6ptaudQI0flTZXUlkH')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (752, 52, 743, 15, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C15C00 AS DateTime), 1, 40, 0, 0, 208, 230, 2.5, 0.95, 2.5, 0.95, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB83017F0387 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (753, 53, 743, 14, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C15C00 AS DateTime), 1, 40, 0, 0, 208, 230, 2.5, 0.95, 2.5, 0.95, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB83017F0387 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (754, 54, 743, 18, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C15C00 AS DateTime), 1, 40, 0, 0, 208, 230, 2.5, 0.95, 2.5, 0.95, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB83017F038C AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (755, 55, 744, 116, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000F0E010 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 95, CAST(0x0000AB8400232B6E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cfmTlkU7iDE:APA91bF1g_VAVngm8VJ1ReQNMPIgkQIEZLdUwUKeIBmnYHCRGv0NXeEvNnBd3qNBi1IpQPVetcDo-cAO-uQbhfZ66GpgXU6n-YVAbZbNkRXrbePjigxk2BeFiG6ptaudQI0flTZXUlkH')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (756, 56, 745, 116, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007C2540 AS DateTime), 1, 90, 0, 0, 570, 602, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB84004B5251 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (757, 57, 745, 113, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007C2540 AS DateTime), 1, 90, 0, 0, 570, 602, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB84004B5256 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (758, 58, 745, 112, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007C2540 AS DateTime), 1, 90, 0, 0, 570, 602, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB84004B5256 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (759, 59, 745, 111, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007C2540 AS DateTime), 1, 90, 0, 0, 570, 602, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB84004B5256 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (760, 60, 745, 110, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007C2540 AS DateTime), 1, 90, 0, 0, 570, 602, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB84004B5256 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (761, 61, 745, 15, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007C2540 AS DateTime), 1, 90, 0, 0, 570, 602, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB84004B5256 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (762, 62, 746, 116, 71, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007DCB20 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB84004C86ED AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (763, 63, 747, 12, 71, CAST(0x0000AB8500000000 AS DateTime), CAST(0x0000000000EDDAA0 AS DateTime), 1, 60, 0, 0, 60, 80, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 96, CAST(0x0000AB8500207F25 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fyovfmjPces:APA91bGrGmAl1O8YySGy_giJnFRGdAqnafGp7lPtTAU7ZBT19wo9jyaojWCePjdReF8cQ6oZim1jrwF5wGO8a79S639ETfjegVA45r7FM0KKQr7vTsFd0R3GfVzmrTCtcYS2BkYMve7r')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (764, 64, 748, 12, 71, CAST(0x0000AB8500000000 AS DateTime), CAST(0x0000000000EDDAA0 AS DateTime), 1, 60, 0, 0, 60, 80, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 96, CAST(0x0000AB8500207FD7 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fyovfmjPces:APA91bGrGmAl1O8YySGy_giJnFRGdAqnafGp7lPtTAU7ZBT19wo9jyaojWCePjdReF8cQ6oZim1jrwF5wGO8a79S639ETfjegVA45r7FM0KKQr7vTsFd0R3GfVzmrTCtcYS2BkYMve7r')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (765, 65, 749, 12, 71, CAST(0x0000AB8500000000 AS DateTime), CAST(0x0000000000EDDAA0 AS DateTime), 1, 60, 0, 0, 60, 80, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 96, CAST(0x0000AB8500208072 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fyovfmjPces:APA91bGrGmAl1O8YySGy_giJnFRGdAqnafGp7lPtTAU7ZBT19wo9jyaojWCePjdReF8cQ6oZim1jrwF5wGO8a79S639ETfjegVA45r7FM0KKQr7vTsFd0R3GfVzmrTCtcYS2BkYMve7r')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (766, 66, 750, 116, 71, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x00000000001FDC40 AS DateTime), 1, 80, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8A0017D07B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (767, 67, 751, 116, 71, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000000EAD530 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8A001D079E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (768, 68, 752, 116, 71, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000000FD8270 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8A002F956B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (769, 69, 753, 16, 71, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000001046040 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8A003675ED AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (770, 70, 754, 16, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000F73140 AS DateTime), 2, 90, 10, 0, 180, 200, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00298BDC AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (771, 71, 755, 16, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000F73140 AS DateTime), 2, 90, 10, 0, 180, 200, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00298CF6 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (772, 72, 756, 14, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000F890D0 AS DateTime), 2, 80, 0, 0, 80, 156, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B002AB8A6 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (773, 73, 757, 16, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000120F0C0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00530560 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (774, 74, 758, 113, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000120F0C0 AS DateTime), 1, 40, 0, 0, 40, 60, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00532D2E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (775, 75, 759, 19, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000120F0C0 AS DateTime), 1, 30, 0, 0, 29, 50, 2.5, 0.71, 2.5, 0.71, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00534518 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (776, 76, 760, 18, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001213710 AS DateTime), 1, 40, 0, 0, 38, 60, 2.5, 0.95, 2.5, 0.95, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00538BCF AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (777, 77, 761, 111, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001217D60 AS DateTime), 1, 130, 0, 0, 124, 150, 2.5, 3.1, 2.5, 3.1, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B0053AE37 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (778, 78, 762, 15, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000123F630 AS DateTime), 1, 90, 0, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00562E7E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (779, 79, 763, 15, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000123F630 AS DateTime), 1, 90, 0, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B00562FAF AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (780, 80, 764, 116, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001266F00 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B0058AB3E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (781, 81, 765, 116, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001266F00 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B0058AC7C AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (782, 82, 766, 16, 71, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000126B550 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB8B0058CCBB AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (783, 83, 767, 117, 71, CAST(0x0000AB8C00000000 AS DateTime), CAST(0x0000000001490A60 AS DateTime), 1, 30, 0, 0, 114, 140, 2.5, 3.09, 2.5, 3.09, 5, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000AB8C007B539E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (784, 84, 767, 19, 71, CAST(0x0000AB8C00000000 AS DateTime), CAST(0x0000000001490A60 AS DateTime), 1, 30, 0, 0, 114, 140, 2.5, 3.09, 2.5, 3.09, 5, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000AB8C007B53A2 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (785, 85, 768, 120, NULL, CAST(0x0000AB1C00000000 AS DateTime), CAST(0x00000000001E3660 AS DateTime), 1, 100, 10, 10, 90, 90, 0, 0, 0, 0, 0, 0, N'test 1 order', 0, 7, 0, N'1', 20, CAST(0x0000AB9400586887 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'123456789')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (786, 86, 769, 120, NULL, CAST(0x0000AB1C00000000 AS DateTime), CAST(0x00000000001E3660 AS DateTime), 1, 100, 10, 10, 90, 90, 0, 0, 0, 0, 0, 0, N'test 1 order', 0, 7, 0, N'1', 20, CAST(0x0000AB9400C22515 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'123456789')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (787, 87, 770, 116, 71, CAST(0x0000AB9500000000 AS DateTime), CAST(0x0000000000C53460 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB95001891DB AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (788, 88, 771, 116, 71, CAST(0x0000AB9600000000 AS DateTime), CAST(0x00000000012482D0 AS DateTime), 2, 76, 5, 0, 152, 142, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB960077C40B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eCC3syjYs-g:APA91bHHDM_sNXHtG0hXIv5K_8a25HKJL9XdHlM8DYAoDVC3KgD1J4YgOIGOKGHIYJEV8XeYQd6Yb1mRCopovywYQ20pDNTGoFXqOPdpUD05s3zTZy7D7xWtk3rNykJJO9s0jfNvE14B')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (789, 89, 772, 116, 71, CAST(0x0000AB9600000000 AS DateTime), CAST(0x00000000012482D0 AS DateTime), 2, 76, 5, 0, 152, 142, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB9600784ED1 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eCC3syjYs-g:APA91bHHDM_sNXHtG0hXIv5K_8a25HKJL9XdHlM8DYAoDVC3KgD1J4YgOIGOKGHIYJEV8XeYQd6Yb1mRCopovywYQ20pDNTGoFXqOPdpUD05s3zTZy7D7xWtk3rNykJJO9s0jfNvE14B')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (790, 90, 773, 16, 71, CAST(0x0000AB9700000000 AS DateTime), CAST(0x0000000000B964F0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AB97000C979C AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'ebZ0aRTO84M:APA91bGz6MHljJaWx1Npif5qoPwFtlM9NryPZ99xvWjiirn-BhAxwEjWKtMgXM0EQHlD-Rm_B-y_eTlfnBNVVnNnKsNLQ7di9PyHrB8JfIHWUQbAI2VZnX1EYYlh80VntUjxCGG1Ba1z')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (791, 91, 774, 11, 71, CAST(0x0000AB9F00000000 AS DateTime), CAST(0x0000000000C6DA40 AS DateTime), 1, 450, 0, 0, 498, 520, 0, 1.19, 0, 1.19, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000AB9F001A0D43 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (792, 92, 774, 15, 71, CAST(0x0000ABA000000000 AS DateTime), CAST(0x0000000000C6DA40 AS DateTime), 1, 450, 0, 0, 498, 520, 0, 1.19, 0, 1.19, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000AB9F001A0D43 AS DateTime), NULL, NULL, NULL, NULL, NULL, 11, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (793, 93, 775, 116, 71, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F47220 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABA10047A6D6 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (794, 94, 776, 116, 71, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F4B870 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABA10047F0FD AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (795, 95, 777, 111, 71, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F54510 AS DateTime), 1, 130, 0, 0, 124, 150, 2.5, 3.1, 2.5, 3.1, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABA10048971C AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (796, 96, 778, 116, 71, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000309D50 AS DateTime), 1, 80, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABA100496E0E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (797, 97, 779, 113, 71, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F77790 AS DateTime), 4, 40, 0, 0, 40, 160.02, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABA1004A823B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (798, 98, 780, 15, 71, CAST(0x0000ABAA00000000 AS DateTime), CAST(0x00000000000AB630 AS DateTime), 1, 500, 0, 0, 1253, 1285, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ABA900E9BC2A AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (799, 99, 780, 110, 71, CAST(0x0000ABAA00000000 AS DateTime), CAST(0x00000000000AB630 AS DateTime), 1, 500, 0, 0, 1253, 1285, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ABA900E9BC2F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7100, 100, 780, 111, 71, CAST(0x0000ABAA00000000 AS DateTime), CAST(0x00000000000AB630 AS DateTime), 1, 500, 0, 0, 1253, 1285, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ABA900E9BC2F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7101, 101, 780, 15, 71, CAST(0x0000ABC900000000 AS DateTime), CAST(0x00000000000AB630 AS DateTime), 1, 500, 0, 0, 1253, 1285, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ABA900E9BC2F AS DateTime), NULL, NULL, NULL, NULL, NULL, 11, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7102, 102, 780, 116, 71, CAST(0x0000ABC900000000 AS DateTime), CAST(0x00000000000AB630 AS DateTime), 1, 500, 0, 0, 1253, 1285, 0, 5.96, 0, 5.96, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ABA900E9BC34 AS DateTime), NULL, NULL, NULL, NULL, NULL, 11, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7103, 103, 781, 116, 71, CAST(0x0000ABAC00000000 AS DateTime), CAST(0x0000000000E5E390 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABAC0038EE75 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7104, 104, 782, 116, 71, CAST(0x0000ABAC00000000 AS DateTime), CAST(0x0000000000E5E390 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABAC00391490 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7105, 105, 783, 15, 71, CAST(0x0000ABB200000000 AS DateTime), CAST(0x0000000000B89200 AS DateTime), 2, 90, 0, 0, 180, 164, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABB2000BED19 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7106, 106, 784, 116, 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000C27540 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABB500157FC9 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7107, 107, 785, 15, 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000CB3F40 AS DateTime), 1, 90, 0, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABB5001E62F8 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7108, 108, 786, 16, 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000013A7BD0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 105, CAST(0x0000ABB5008E2ED9 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'e9ISShu3Lgs:APA91bHRUVRdhlktgCYmHfuTRaBI1XW2Uyesow1mv6cePwWqMJGwF4LsjsvtZX5UVJ9cfbLrFJHD52-e5fw4US71J9PtN8lHvzFhFbddUjNHEYc_8MZiD3bE-W0ISe9brWbdfE_et2U5')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7109, 109, 787, 16, 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000013B4EC0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 105, CAST(0x0000ABB5008E5D25 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'e9ISShu3Lgs:APA91bHRUVRdhlktgCYmHfuTRaBI1XW2Uyesow1mv6cePwWqMJGwF4LsjsvtZX5UVJ9cfbLrFJHD52-e5fw4US71J9PtN8lHvzFhFbddUjNHEYc_8MZiD3bE-W0ISe9brWbdfE_et2U5')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7110, 110, 788, 15, 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000013B0870 AS DateTime), 1, 450, 0, 0, 450, 470, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 106, CAST(0x0000ABB5008E6FC3 AS DateTime), NULL, NULL, NULL, NULL, NULL, 11, N'dbHcGKVfUs0:APA91bFPC1-lGQ0Ap2LHf0j0BPqVDALH_7pzT_WQU-bTZgjIMGykgWOPaVAHiIEczIAIpSq1s46w3JjLvWFe3IZEPkXeiUdZTYOEqrKrg4sl6Wv4KtuFU-Vf9SJjXL6ymWq0VRci0xlW')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7111, 111, 789, 16, 71, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000017D39C0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 107, CAST(0x0000ABB500D096B0 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7112, 112, 790, 114, 71, CAST(0x0000ABB700000000 AS DateTime), CAST(0x00000000000DBBA0 AS DateTime), 1, 180, 0, 0, 180, 200, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 106, CAST(0x0000ABB600EC6CF4 AS DateTime), NULL, NULL, NULL, NULL, NULL, 12, N'dbHcGKVfUs0:APA91bFPC1-lGQ0Ap2LHf0j0BPqVDALH_7pzT_WQU-bTZgjIMGykgWOPaVAHiIEczIAIpSq1s46w3JjLvWFe3IZEPkXeiUdZTYOEqrKrg4sl6Wv4KtuFU-Vf9SJjXL6ymWq0VRci0xlW')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7113, 113, 791, 16, 71, CAST(0x0000ABB700000000 AS DateTime), CAST(0x0000000000107AC0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 109, CAST(0x0000ABB600F0804F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7114, 114, 792, 117, 71, CAST(0x0000ABB900000000 AS DateTime), CAST(0x0000000000BA37E0 AS DateTime), 2, 90, 10, 0, 170, 166, 2.5, 4.76, 2.5, 4.76, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABB9000D6BBD AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'c_v97moe6JM:APA91bEKmtpjR6dgvMnL9r1S7Nxd-dMJTGzjUp_HJmRNMNZoCEsotBxTOpjxZL0_Pe7RRJGVvlsv2A7cBlXARznW72eHir43Cydn2yIlWDK6cnPbr2b4VnJQmMUGGDqSXGbgDnl9ZM0l')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7115, 115, 793, 113, 71, CAST(0x0000ABBC00000000 AS DateTime), CAST(0x000000000107AC00 AS DateTime), 4, 40, 0, 0, 160, 180, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABBC005B344E AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'c_v97moe6JM:APA91bEKmtpjR6dgvMnL9r1S7Nxd-dMJTGzjUp_HJmRNMNZoCEsotBxTOpjxZL0_Pe7RRJGVvlsv2A7cBlXARznW72eHir43Cydn2yIlWDK6cnPbr2b4VnJQmMUGGDqSXGbgDnl9ZM0l')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7116, 116, 794, 19, 71, CAST(0x0000ABBF00000000 AS DateTime), CAST(0x0000000000B5D2E0 AS DateTime), 4, 30, 0, 0, 114, 140, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABBF0009034A AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'ckQNQq8f5mY:APA91bHRu7jVzEBKJAMnysNQ4Vp_X8Zc7udMXR_-qTlWU0XjdB0rRNQwIwztvB_AlScPRmr7JXYtMFdiUHW2lDN40rk-lw8M39z4lc1p76aqUNoCTwzL43TtjehlMO1WVJWBch0KRVdA')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7117, 117, 795, 116, 71, CAST(0x0000ABBF00000000 AS DateTime), CAST(0x000000000176A240 AS DateTime), 2, 76, 5, 0, 152, 111, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ABBF00CA020B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7118, 118, 796, 15, 71, CAST(0x0000ABC200000000 AS DateTime), CAST(0x0000000000C1E8A0 AS DateTime), 2, 450, 0, 0, 900, 920, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ABC200155BE2 AS DateTime), NULL, NULL, NULL, NULL, NULL, 11, N'eHDqyK6-PSs:APA91bElCJbiMT6d3wsXuYjF3nm5QlPkALKUG7uMnR5AMJNfw1D9eHOwJxqbtSQ3pu8g6eHvQuZeIiy-ApK0qcW7TjCMHWx1r4AbwSJjzI_rXiK6bqX0Jn14hKTv9ZTzdXIzMGHPkvFu')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7119, 119, 797, 16, 71, CAST(0x0000ABC300000000 AS DateTime), CAST(0x00000000012A4760 AS DateTime), 0, 180, 0, 0, 270, 90.02, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABC3007D6F31 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'ckQNQq8f5mY:APA91bHRu7jVzEBKJAMnysNQ4Vp_X8Zc7udMXR_-qTlWU0XjdB0rRNQwIwztvB_AlScPRmr7JXYtMFdiUHW2lDN40rk-lw8M39z4lc1p76aqUNoCTwzL43TtjehlMO1WVJWBch0KRVdA')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7120, 120, 798, 17, 71, CAST(0x0000ABC600000000 AS DateTime), CAST(0x0000000000BD83A0 AS DateTime), 2, 110, 0, 0, 220, 196, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABC60010AAAB AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7121, 121, 799, 116, 71, CAST(0x0000ABC600000000 AS DateTime), CAST(0x0000000000BDC9F0 AS DateTime), 3, 76, 5, 0, 228, 248, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABC600110D86 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7122, 122, 7100, 116, 71, CAST(0x0000ABC800000000 AS DateTime), CAST(0x0000000000A8EA30 AS DateTime), 1, 60, 0, 0, 221, 246, 0, 2.38, 0, 2.38, 0, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ABC800C1FC0B AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7123, 123, 7100, 117, 71, CAST(0x0000ABC800000000 AS DateTime), CAST(0x0000000000A8EA30 AS DateTime), 1, 60, 0, 0, 221, 246, 0, 2.38, 0, 2.38, 0, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ABC800C1FC56 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7124, 124, 7100, 12, 71, CAST(0x0000ABC800000000 AS DateTime), CAST(0x0000000000A8EA30 AS DateTime), 1, 60, 0, 0, 221, 246, 0, 2.38, 0, 2.38, 0, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ABC800C1FCA2 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7125, 125, 7101, 16, 71, CAST(0x0000ABC800000000 AS DateTime), CAST(0x00000000016F37D0 AS DateTime), 1, 50, 0, 0, 228, 159, 2.5, 1.19, 2.5, 1.19, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ABC800C2D153 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7126, 126, 7101, 11, 71, CAST(0x0000ABC800000000 AS DateTime), CAST(0x00000000016F37D0 AS DateTime), 1, 50, 0, 0, 228, 159, 2.5, 1.19, 2.5, 1.19, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ABC800C2D1A3 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7127, 127, 7102, 116, 71, CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000DDEC80 AS DateTime), 3, 76, 5, 0, 228, 248, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABC900312E9A AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7128, 128, 7103, 116, 71, CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000DDEC80 AS DateTime), 3, 76, 5, 0, 228, 248, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABC900314713 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7129, 129, 7104, 116, 71, CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000DF9260 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABC90032B86D AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7130, 130, 7105, 116, 71, CAST(0x0000ABCB00000000 AS DateTime), CAST(0x0000000000A3F890 AS DateTime), 9, 76, 5, 0, 684, 604, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ABCA01831C5F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dU9dKTpn1D4:APA91bENkCMmm22h2ckhP_c61h3l4ogvmL_j5IF3x8_CzSEgV4VYkNlVW1jh__I99nrPWIegpFdEDTMlbpXNuKRkNzKwylgkb266PBU_WnNepwPaord8RkgqIECfLfZMode7iiKsOhZL')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7131, 131, 7106, 16, 71, CAST(0x0000AC1100000000 AS DateTime), CAST(0x0000000000B47350 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000AC110007C04D AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eHDqyK6-PSs:APA91bElCJbiMT6d3wsXuYjF3nm5QlPkALKUG7uMnR5AMJNfw1D9eHOwJxqbtSQ3pu8g6eHvQuZeIiy-ApK0qcW7TjCMHWx1r4AbwSJjzI_rXiK6bqX0Jn14hKTv9ZTzdXIzMGHPkvFu')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7132, 132, 7107, 116, 71, CAST(0x0000AC2200000000 AS DateTime), CAST(0x0000000000F7BDE0 AS DateTime), 1, 76, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AC22004B24A0 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eVac56m2aWU:APA91bFXmJrbxuY8pnEUwapLOsyF3NnvhhsmWV0kJfopHe2Dw-ke7LkxoSC0EEjsM6g6QQnJDCKDRZRbjiKMwmsFDl9QW9b0yW9-nUD7zA21IZdVf-wTlhKFyEBPJtjqgOH4ah_Si-HG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7133, 133, 7108, 116, 71, CAST(0x0000AC2300000000 AS DateTime), CAST(0x0000000000B1B430 AS DateTime), 1, 80, 5, 0, 76, 96, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AC230004E6AB AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eOlV1_C3RTE:APA91bGYvRBKIPSu4fN5qf-SmKhtbBe-hCGhYZ3U1uIuTZKaPTo7UBDVB_qnbB4DdeRPLlFzjNvST0zjHZJ0b6fO0QHcmAV-DZES9XH0JDJ8LF8MCOH_MmrBBMqiD1s0RvRSQvbFe_bE')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7134, 134, 7109, 16, 71, CAST(0x0000AC3600000000 AS DateTime), CAST(0x0000000000FAC350 AS DateTime), 2, 90, 10, 0, 180, 200, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000AC36004E083A AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'eVac56m2aWU:APA91bFXmJrbxuY8pnEUwapLOsyF3NnvhhsmWV0kJfopHe2Dw-ke7LkxoSC0EEjsM6g6QQnJDCKDRZRbjiKMwmsFDl9QW9b0yW9-nUD7zA21IZdVf-wTlhKFyEBPJtjqgOH4ah_Si-HG')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7135, 135, 7110, 16, 71, CAST(0x0000ACB800000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ACB7018A5412 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'duh4vpN_IUk:APA91bHdtS8g-_IPawCYvGTANrIPRkjFQy_a1T0jJ0n1c3N0KSSi-mZ_dW2i1vouQePH0wM1Rc0nLQu1okpgngZDcTCWI_kiLSclESIrav4SOxuTxxgq9uS_EC9pbzLmQBjFcjbNBjGJ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7136, 136, 7111, 16, 71, CAST(0x0000ACB800000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ACB7018A7DB3 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'duh4vpN_IUk:APA91bHdtS8g-_IPawCYvGTANrIPRkjFQy_a1T0jJ0n1c3N0KSSi-mZ_dW2i1vouQePH0wM1Rc0nLQu1okpgngZDcTCWI_kiLSclESIrav4SOxuTxxgq9uS_EC9pbzLmQBjFcjbNBjGJ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7137, 137, 7112, 16, 71, CAST(0x0000ADA400000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 3, 90, 10, 0, 270, 290, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ADA4000F2EBD AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dtpbvfapqro:APA91bGzsSIZ-cu4tyABO_bfbZs0nEVeSbddTnLBnBG3gRzyu1IxA4ojD18yenknuaUr8WXfPa_LUu_LBBwCsE6YN3ebBSJ0u3PNwBKa3b0jqona1Ac7CLHEpFp3F8k3NHyGmBR0065Z')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7138, 138, 7113, 16, 71, CAST(0x0000ADA400000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 3, 90, 10, 0, 270, 290, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ADA4000F4338 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'dtpbvfapqro:APA91bGzsSIZ-cu4tyABO_bfbZs0nEVeSbddTnLBnBG3gRzyu1IxA4ojD18yenknuaUr8WXfPa_LUu_LBBwCsE6YN3ebBSJ0u3PNwBKa3b0jqona1Ac7CLHEpFp3F8k3NHyGmBR0065Z')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7139, 139, 7114, 16, 71, CAST(0x0000ADAB00000000 AS DateTime), CAST(0x00000000012DD970 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ADAB00813077 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7140, 140, 7115, 12, 71, CAST(0x0000ADB800000000 AS DateTime), CAST(0x0000000001102FB0 AS DateTime), 1, 60, 0, 0, 60, 80, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ADB800634C51 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'e-OZGCYxkTw:APA91bGvqBbgw06GQ_wcbq_ewpvlYmYmiqK3l-7JrEH1WAqw9hT6EPTKywZIeOSUYhzlL7Xim2lnpOaQgkkuqhTum1m2L_UId2_ynr00JZ_cGa1F8h0sMV4qm_PtzaRrbd-r36wsP7OQ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7141, 141, 7116, 12, 71, CAST(0x0000ADB800000000 AS DateTime), CAST(0x0000000001102FB0 AS DateTime), 1, 60, 0, 0, 60, 80, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ADB800635636 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'e-OZGCYxkTw:APA91bGvqBbgw06GQ_wcbq_ewpvlYmYmiqK3l-7JrEH1WAqw9hT6EPTKywZIeOSUYhzlL7Xim2lnpOaQgkkuqhTum1m2L_UId2_ynr00JZ_cGa1F8h0sMV4qm_PtzaRrbd-r36wsP7OQ')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7142, 142, 7117, 18, 71, CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000013FFA10 AS DateTime), 2, 40, 0, 0, 76, 100, 2.5, 1.9, 2.5, 1.9, 5, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ADB900932315 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7143, 143, 7118, 18, 71, CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000013FFA10 AS DateTime), 2, 40, 0, 0, 76, 100, 2.5, 1.9, 2.5, 1.9, 5, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ADB9009DC3FE AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7144, 144, 7119, 11, 71, CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000014AB040 AS DateTime), 3, 50, 0, 0, 143, 170, 2.5, 3.57, 2.5, 3.57, 5, 0, N'', 0, 7, 0, N'1', 99, CAST(0x0000ADB9009E007A AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7145, 145, 7120, 110, 71, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BAC480 AS DateTime), 1, 120, 0, 0, 114, 140, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ADBA000E14F4 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7146, 146, 7121, 16, 71, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BB0AD0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ADBA000E40C6 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7147, 147, 7122, 16, 71, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BB0AD0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ADBA000E4BDC AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7148, 148, 7123, 16, 71, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 1, 90, 10, 0, 90, 110, 0, 0, 0, 0, 0, 0, N'', 0, 7, 0, N'1', 93, CAST(0x0000ADBA000EF1C6 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7149, 149, 7124, 16, 71, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 1, 120, 0, 0, 294, 320, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ADC300C0E3ED AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7150, 150, 7124, 15, 71, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 1, 120, 0, 0, 294, 320, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ADC300C0E42D AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7151, 151, 7124, 110, 71, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 1, 120, 0, 0, 294, 320, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ADC300C0E46F AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7152, 152, 7125, 16, 71, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 1, 120, 0, 0, 294, 320, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ADC300C0FB99 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7153, 153, 7125, 15, 71, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 1, 120, 0, 0, 294, 320, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ADC300C0FBD9 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y')
GO
INSERT [dbo].[sodtl] ([sodtlid], [uid1], [soid], [itemid], [unitid], [itmdeldate], [itemdeltime], [qty], [grate], [discperc], [discamt], [netrate], [totamt], [sgstperc], [sgstamt], [cgstperc], [cgstamt], [igstperc], [igstamt], [Addnote], [prclitmyn], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [packid], [deviceid]) VALUES (7154, 154, 7125, 110, 71, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 1, 120, 0, 0, 294, 320, 2.5, 2.86, 2.5, 2.86, 5, 0, N'', 0, 7, 0, N'1', 91, CAST(0x0000ADC300C0FC19 AS DateTime), NULL, NULL, NULL, NULL, NULL, 0, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (71, 1, 50, N'Sumit', N'7486876122', N'71', 1, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000D2A9B0 AS DateTime), 0, 0, 0, 140, 0, 140, 0, CAST(0x0000AB7E00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 50, CAST(0x0000AB7D00CB0781 AS DateTime), 2, CAST(0x0000AB7D013EA704 AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 61, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI', NULL, N'bnccgJ')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (72, 2, 50, N'Sumit', N'7486876122', N'72', 2, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x0000000000D2A9B0 AS DateTime), 0, 0, 0, 140, 0, 140, 0, CAST(0x0000AB7E00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 50, CAST(0x0000AB7D00CB08EF AS DateTime), 2, CAST(0x0000AB7D013EBB94 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 61, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI', NULL, N'bnccgJ')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (73, 3, 50, N'Sumit', N'7486876122', N'73', 3, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x00000000002D97E0 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB7E00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 1, N'1', 50, CAST(0x0000AB7D00EB6C48 AS DateTime), 2, CAST(0x0000AB7D015EEBC2 AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 91, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (74, 4, 50, N'Sumit', N'7486876122', N'74', 4, CAST(0x0000AB7E00000000 AS DateTime), CAST(0x00000000002E6AD0 AS DateTime), 0, 0, 0, 132, 48, 132, 0, CAST(0x0000AB7E00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 1, N'1', 50, CAST(0x0000AB7D00EC2DFD AS DateTime), 2, CAST(0x0000AB8100773018 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 61, N'dN-g-pghHDY:APA91bEAJUs6mgIPrvnPqI2a6S7HLA1Q7QBFb-6AtkOLB8xUQKJwFXGSCCXpGLaBVOrrLlYjIfqL5o2RrIlaCBLg02wZjlBRhw0OesA59CibGOn8_11xS8GQ8sum-CmAJ8itQ3xGv1TI', NULL, N'bnccgJ')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (75, 5, 60, N'test4', N'8767878784', N'75', 5, CAST(0x0000AB8000000000 AS DateTime), CAST(0x000000000033A2C0 AS DateTime), 0, 0, 0, 280, 0, 280, 0, CAST(0x0000AB8000000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB8000026D63 AS DateTime), 2, CAST(0x0000AB800075F015 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (76, 6, 60, N'test4', N'8767878784', N'76', 6, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001E7CB0 AS DateTime), 0, 0, 0, 300, 0, 300, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB800178EC47 AS DateTime), 2, CAST(0x0000AB8100611896 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 84, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (77, 7, 60, N'test4', N'8767878784', N'77', 7, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000001F4FA0 AS DateTime), 0, 0, 0, 130, 0, 130, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB800179AA9D AS DateTime), 2, CAST(0x0000AB8100618D1D AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (78, 8, 60, N'test4', N'8767878784', N'78', 8, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000031FCE0 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB810000F071 AS DateTime), 2, CAST(0x0000AB8100745671 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (79, 9, 60, N'test4', N'8767878784', N'79', 9, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000034BC00 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB8100038507 AS DateTime), 2, CAST(0x0000AB8100772A96 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 89, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (710, 10, 60, N'test4', N'8767878784', N'710', 10, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000384E10 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB810007344C AS DateTime), 2, CAST(0x0000AB81007A98F7 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (711, 11, 60, N'test4', N'8767878784', N'711', 11, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000396750 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB8100082B40 AS DateTime), 2, CAST(0x0000AB81007B95A1 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (712, 12, 60, N'test4', N'8767878784', N'712', 12, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003A3A40 AS DateTime), 0, 0, 0, 50, 0, 50, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB81000925C4 AS DateTime), 2, CAST(0x0000AB81007C8FA6 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (713, 13, 60, N'test4', N'8767878784', N'713', 13, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003CB310 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB81000B725C AS DateTime), 2, CAST(0x0000AB81007F052F AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (714, 14, 60, N'test4', N'8767878784', N'714', 14, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000003CB310 AS DateTime), 0, 0, 0, 130, 0, 130, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB81000B8F8C AS DateTime), 2, CAST(0x0000AB81007F01D5 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 89, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (715, 15, 69, N'bhavisha', N' ', N'715', 15, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000DFD8B0 AS DateTime), 0, 0, 0, 340, 0, 340, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 69, CAST(0x0000AB8100120C82 AS DateTime), 2, CAST(0x0000AB8100857FA8 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 13, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (716, 16, 60, N'test4', N'8767878784', N'716', 16, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000571110 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB810025F35A AS DateTime), 2, CAST(0x0000AB81009962E0 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (717, 17, 60, N'test4', N'8767878784', N'717', 17, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005870A0 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB8100274AB1 AS DateTime), 2, CAST(0x0000AB8100A4C9B3 AS DateTime), NULL, NULL, NULL, N'Out for Delivery', 12, 88, N'cPPBJKPQAMo:APA91bF2ongaVM8uqlPVkdxqjKRFlkCRAdmBqjfHOVWrq9kpjZHYMrzQ_JsF1Eu4I8CNvCIOOyx01tkBDZERj4Df80vjPdxQ0kAsT9_NZ4uhEwXMqQBrj6xDLTQS6S_Vu2v_-xXU2GZS', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (718, 18, 69, N'bhavisha', N' ', N'718', 18, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000F5D1B0 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 69, CAST(0x0000AB810027E43E AS DateTime), 2, CAST(0x0000AB81009C9A84 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 13, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (719, 19, 69, N'bhavisha', N'9825960554', N'719', 19, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000F65E50 AS DateTime), 0, 0, 0, 60, 0, 60, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 69, CAST(0x0000AB8100289AA2 AS DateTime), 2, CAST(0x0000AB81009C1095 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 13, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (720, 20, 69, N'bhavisha', N'9825960554', N'720', 20, CAST(0x0000AB8100000000 AS DateTime), CAST(0x0000000000F963C0 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 69, CAST(0x0000AB81002BA208 AS DateTime), 2, CAST(0x0000AB81009F0BD2 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 13, N'fnL7ZXFfXG4:APA91bFlVqW4twdKd_ixXMqzfXa1cQJId4iYUtrxcKVh8EApuh9kxzACatZDrPPeoVcfYKmA0wPz8ZNdAxxDtC10U8KJqHcwM_bnCXlrPMvofjJqnpvHde770k7O9MIf3r_bwSG7zkAP', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (721, 21, 60, N'test4', N'8767878784', N'721', 21, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000005E7B80 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB81002D647A AS DateTime), 2, CAST(0x0000AB8100A0D38A AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (722, 22, 60, N'test4', N'8767878784', N'722', 22, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000061C740 AS DateTime), 0, 0, 0, 120, 0, 120, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB81003077C3 AS DateTime), 2, CAST(0x0000AB8100A3DE89 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (723, 23, 60, N'test4', N'8767878784', N'723', 23, CAST(0x0000AB8100000000 AS DateTime), CAST(0x00000000006253E0 AS DateTime), 0, 0, 0, 120, 0, 120, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB8100310E5A AS DateTime), 2, CAST(0x0000AB8100A47B54 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 88, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (724, 24, 60, N'test4', N'8767878784', N'724', 24, CAST(0x0000AB8100000000 AS DateTime), CAST(0x000000000062E080 AS DateTime), 0, 0, 0, 60, 0, 60, 0, CAST(0x0000AB8100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB810031914B AS DateTime), 2, CAST(0x0000AB8100A5BC6C AS DateTime), NULL, NULL, NULL, N'Out for Delivery', 12, 87, N'c7txk68izOE:APA91bG6k-8oyXTrJhEnEEKuLuOxcSeQZoATirnja5-E_r2394Zp9MT6_peB1HR1dmjIEbI7WPfwB42kYBZmSlXm54Yhskpx6YSG49NeO_oM-hAY74NLpaRMU35M4E2uo2BThd2xlCzc', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (725, 25, 60, N'test4', N'8767878784', N'725', 25, CAST(0x0000AB8200000000 AS DateTime), CAST(0x0000000000AD08E0 AS DateTime), 0, 0, 0, 330, 50, 330, 0, CAST(0x0000AB8200000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB81016BB2DE AS DateTime), 2, CAST(0x0000AB8200D942F8 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 87, N'dvoFEBOl4YM:APA91bGUOUyjmlUxk3H6tID4MxVkz_Jac_Z8CnocwQYfHWtEvW68VaCr1bvoSIDKURKZTJp-t56cqJ-bt0aIGHBrmV5DB1ujYuY7-jrb2xsr9PlctRPL_-N_wpk70xpAY-lhFhhRJW3I', NULL, N'bnccgJ')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (726, 26, 60, N'test4', N'8767878784', N'726', 26, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000A6FE00 AS DateTime), 0, 0, 0, 200, 0, 200, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 60, CAST(0x0000AB8201650051 AS DateTime), 2, CAST(0x0000AB83004D060C AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 95, N'dvoFEBOl4YM:APA91bGUOUyjmlUxk3H6tID4MxVkz_Jac_Z8CnocwQYfHWtEvW68VaCr1bvoSIDKURKZTJp-t56cqJ-bt0aIGHBrmV5DB1ujYuY7-jrb2xsr9PlctRPL_-N_wpk70xpAY-lhFhhRJW3I', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (727, 27, 93, N'test', N'', N'727', 27, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000000C15C0 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8201669B51 AS DateTime), 2, CAST(0x0000AB83004E8121 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (728, 28, 93, N'test', N'9825960554', N'728', 28, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002068E0 AS DateTime), 0, 0, 0, 172, 0, 172, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB82017AAC67 AS DateTime), 1, CAST(0x0000AB83011AFBE6 AS DateTime), NULL, NULL, NULL, N'Accept', 71, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (729, 29, 93, N'test', N'9825960554', N'729', 29, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002673C0 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB820180E94F AS DateTime), 1, CAST(0x0000AB83011DE51D AS DateTime), NULL, NULL, NULL, N'Accept', 71, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (730, 30, 93, N'test', N'9825960554', N'730', 30, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000026BA10 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB82018116F9 AS DateTime), 2, CAST(0x0000AB8300D40401 AS DateTime), NULL, NULL, NULL, N'Being Prepared', 12, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (731, 31, 93, N'test', N'9825960554', N'731', 31, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002AD8C0 AS DateTime), 0, 0, 0, 324, 0, 324, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB820185332F AS DateTime), 2, CAST(0x0000AB8300D1C34F AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (732, 32, 93, N'test', N'9825960554', N'732', 32, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000002AD8C0 AS DateTime), 0, 0, 0, 324, 0, 324, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB82018533A0 AS DateTime), 2, CAST(0x0000AB8300D14998 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (733, 33, 93, N'bhavisha', N'9825960554', N'733', 33, CAST(0x0000AB8300000000 AS DateTime), CAST(0x000000000066B8E0 AS DateTime), 0, 0, 0, 578, 100, 578, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB830035BB8A AS DateTime), 2, CAST(0x0000AB8300A92490 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (734, 34, 93, N'bhavisha', N'9825960554', N'734', 34, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000012E6610 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB830060A119 AS DateTime), 2, CAST(0x0000AB8300D41DAD AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 96, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (735, 35, 93, N'bhavisha', N'9825960554', N'735', 35, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000012EAC60 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB830060EFC3 AS DateTime), 1, CAST(0x0000AB830117DA03 AS DateTime), NULL, NULL, NULL, N'Out for Delivery', 71, 96, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (736, 36, 91, N'sumit', N'', N'736', 36, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000001709760 AS DateTime), 0, 0, 0, 186, 0, 186, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000AB8300A2ED77 AS DateTime), 1, CAST(0x0000AB83011675BF AS DateTime), NULL, NULL, NULL, N'Being Prepared', 71, 97, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (737, 37, 91, N'sumit', N'', N'737', 37, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000017AC0F0 AS DateTime), 0, 0, 0, 130, 0, 130, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000AB8300ACF210 AS DateTime), 1, CAST(0x0000AB8301206E5E AS DateTime), NULL, NULL, NULL, N'Accept', 71, 97, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (738, 38, 91, N'sumit', N'', N'738', 38, CAST(0x0000AB8300000000 AS DateTime), CAST(0x00000000017B4D90 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000AB8300AD7893 AS DateTime), 1, CAST(0x0000AB830120EA7E AS DateTime), NULL, NULL, NULL, N'Being Prepared', 71, 97, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (739, 39, 91, N'sumit', N'', N'739', 39, CAST(0x0000AB8300000000 AS DateTime), CAST(0x0000000000B6A5D0 AS DateTime), 0, 0, 0, 50, 0, 50, 0, CAST(0x0000AB8300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000AB8300AEB55C AS DateTime), 1, CAST(0x0000AB8301223C3B AS DateTime), NULL, NULL, NULL, N'Reject', 71, 97, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (740, 40, 91, N'sumit', N'9427949390', N'740', 40, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000008CA0 AS DateTime), 0, 0, 0, 70, 0, 70, 0, CAST(0x0000AB8400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000AB8300BE3CBE AS DateTime), 1, CAST(0x0000AB830131ACD0 AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 97, N'eryA9aon-MU:APA91bEy8JzO3zdLa1GXlLBK7CGzxi1Q2UJNx2ODQGNLImZkWUHQAyULH01uPGpdiSziH2w1n2uONOxyOZzxIWbyrzTzgtPWgSZoK5IHt3SA1h90Ehr7C6pSlmZbMo_meJByrLLwSJJC', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (741, 41, 93, N'bhavisha', N'9825960554', N'741', 41, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000A81740 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB830165E6D8 AS DateTime), 2, CAST(0x0000AB84004DDBE4 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 98, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (742, 42, 95, N'Pinal', N'9825960554', N'742', 42, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000AF3B60 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 95, CAST(0x0000AB83016D1151 AS DateTime), 2, CAST(0x0000AB840054FA97 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 100, N'cfmTlkU7iDE:APA91bF1g_VAVngm8VJ1ReQNMPIgkQIEZLdUwUKeIBmnYHCRGv0NXeEvNnBd3qNBi1IpQPVetcDo-cAO-uQbhfZ66GpgXU6n-YVAbZbNkRXrbePjigxk2BeFiG6ptaudQI0flTZXUlkH', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (743, 43, 93, N'bhavisha', N'9825960554', N'743', 43, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000C15C00 AS DateTime), 0, 0, 0, 230, 0, 230, 0, CAST(0x0000AB8400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB83017F0382 AS DateTime), 2, CAST(0x0000AB840066F556 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 99, N'dWeoF4D9SF8:APA91bF7hdY-raHBoPUTgHakLMyebpOgpWbZKZhfw0b07ME_DeZ4hGejFE9sL1I3RJHV9QvsZHmmQVUWWpzndb9LKm7chw1tVwySHazYTWz_B09SVe8YIZVlfBUFflLyaewHbUg70Pi9', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (744, 44, 95, N'Pinal', N'9825960554', N'744', 44, CAST(0x0000AB8400000000 AS DateTime), CAST(0x0000000000F0E010 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 95, CAST(0x0000AB8400232B3B AS DateTime), 2, CAST(0x0000AB840096B42A AS DateTime), NULL, NULL, NULL, N'Accept', 12, 100, N'cfmTlkU7iDE:APA91bF1g_VAVngm8VJ1ReQNMPIgkQIEZLdUwUKeIBmnYHCRGv0NXeEvNnBd3qNBi1IpQPVetcDo-cAO-uQbhfZ66GpgXU6n-YVAbZbNkRXrbePjigxk2BeFiG6ptaudQI0flTZXUlkH', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (745, 45, 93, N'bhavisha', N'9825960554', N'745', 45, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007C2540 AS DateTime), 0, 0, 0, 602, 100, 602, 0, CAST(0x0000AB8400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB84004B524C AS DateTime), 2, CAST(0x0000AB8400BECD06 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (746, 46, 93, N'bhavisha', N'9825960554', N'746', 46, CAST(0x0000AB8400000000 AS DateTime), CAST(0x00000000007DCB20 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB84004C86E3 AS DateTime), 2, CAST(0x0000AB8C00EF1E4B AS DateTime), NULL, NULL, NULL, N'Accept', 71, 96, N'cRKbodAcozs:APA91bE_XJaj1lodN4GrWPxN5kOSXgNFUfz2MiW7DBGA_lMyg8W4i9qlwVa876CfCYA9rMhWf1JxugnFQ84q7lmMr5qB9DajzURk8y-EiF5Nd-3PhriA1YoC5XS2asnJclOC7mlE6314', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (747, 47, 96, N'jaimin', N'9974402476', N'747', 47, CAST(0x0000AB8500000000 AS DateTime), CAST(0x0000000000EDDAA0 AS DateTime), 0, 0, 0, 80, 0, 80, 0, CAST(0x0000AB8500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 96, CAST(0x0000AB8500207EED AS DateTime), 2, CAST(0x0000AB8A00A9F370 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 102, N'fyovfmjPces:APA91bGrGmAl1O8YySGy_giJnFRGdAqnafGp7lPtTAU7ZBT19wo9jyaojWCePjdReF8cQ6oZim1jrwF5wGO8a79S639ETfjegVA45r7FM0KKQr7vTsFd0R3GfVzmrTCtcYS2BkYMve7r', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (748, 48, 96, N'jaimin', N'9974402476', N'748', 48, CAST(0x0000AB8500000000 AS DateTime), CAST(0x0000000000EDDAA0 AS DateTime), 0, 0, 0, 80, 0, 80, 0, CAST(0x0000AB8500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 96, CAST(0x0000AB8500207FD7 AS DateTime), 2, CAST(0x0000AB8A00918052 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 102, N'fyovfmjPces:APA91bGrGmAl1O8YySGy_giJnFRGdAqnafGp7lPtTAU7ZBT19wo9jyaojWCePjdReF8cQ6oZim1jrwF5wGO8a79S639ETfjegVA45r7FM0KKQr7vTsFd0R3GfVzmrTCtcYS2BkYMve7r', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (749, 49, 96, N'jaimin', N'9974402476', N'749', 49, CAST(0x0000AB8500000000 AS DateTime), CAST(0x0000000000EDDAA0 AS DateTime), 0, 0, 0, 80, 0, 80, 0, CAST(0x0000AB8500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 96, CAST(0x0000AB8500208072 AS DateTime), 2, CAST(0x0000AB8500940051 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 102, N'fyovfmjPces:APA91bGrGmAl1O8YySGy_giJnFRGdAqnafGp7lPtTAU7ZBT19wo9jyaojWCePjdReF8cQ6oZim1jrwF5wGO8a79S639ETfjegVA45r7FM0KKQr7vTsFd0R3GfVzmrTCtcYS2BkYMve7r', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (750, 50, 93, N'bhavisha', N'null', N'750', 50, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x00000000001FDC40 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8A00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8A0017D047 AS DateTime), 2, CAST(0x0000AB960116BCD4 AS DateTime), NULL, NULL, NULL, N'Accept', 71, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (751, 51, 93, N'bhavisha', N'9825960554', N'751', 51, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000000EAD530 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8A00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8A001D0799 AS DateTime), 2, CAST(0x0000AB960116D9DA AS DateTime), NULL, NULL, NULL, N'Accept', 71, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (752, 52, 93, N'bhavisha', N'9825960554', N'752', 52, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000000FD8270 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8A00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8A002F9566 AS DateTime), 2, CAST(0x0000AB8A00AAF1D0 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (753, 53, 93, N'bhavisha', N'9825960554', N'753', 53, CAST(0x0000AB8A00000000 AS DateTime), CAST(0x0000000001046040 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AB8A00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8A003675BF AS DateTime), 2, CAST(0x0000AB8A00AAB186 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (754, 54, 93, N'bhavisha', N'9825960554', N'754', 54, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000F73140 AS DateTime), 0, 0, 0, 200, 36, 200, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00298B91 AS DateTime), 2, CAST(0x0000AB8B009EFD65 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (755, 55, 93, N'bhavisha', N'9825960554', N'755', 55, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000F73140 AS DateTime), 0, 0, 0, 200, 36, 200, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00298CF6 AS DateTime), 2, CAST(0x0000AB8B009ECFC8 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (756, 56, 93, N'bhavisha', N'9825960554', N'756', 56, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000000F890D0 AS DateTime), 0, 0, 0, 156, 24, 156, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B002AB8A1 AS DateTime), 2, CAST(0x0000AB8B009E3F34 AS DateTime), NULL, NULL, NULL, N'Being Prepared', 12, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'bnccgJ')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (757, 57, 93, N'bhavisha', N'9825960554', N'757', 57, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000120F0C0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00530510 AS DateTime), 1, CAST(0x0000AB9601166B58 AS DateTime), NULL, NULL, NULL, N'Accept', 71, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (758, 58, 93, N'bhavisha', N'9825960554', N'758', 58, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000120F0C0 AS DateTime), 0, 0, 0, 60, 0, 60, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00532D2E AS DateTime), 2, CAST(0x0000ABB9011B07AB AS DateTime), NULL, NULL, NULL, N'Accept', 71, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (759, 59, 93, N'bhavisha', N'9825960554', N'759', 59, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000120F0C0 AS DateTime), 0, 0, 0, 50, 0, 50, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00534518 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (760, 60, 93, N'bhavisha', N'9825960554', N'760', 60, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001213710 AS DateTime), 0, 0, 0, 60, 0, 60, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00538BCA AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (761, 61, 93, N'bhavisha', N'9825960554', N'761', 61, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001217D60 AS DateTime), 0, 0, 0, 150, 0, 150, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B0053AE37 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (762, 62, 93, N'bhavisha', N'9825960554', N'762', 62, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000123F630 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00562E70 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (763, 63, 93, N'bhavisha', N'9825960554', N'763', 63, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000123F630 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B00562FAA AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (764, 64, 93, N'bhavisha', N'9825960554', N'764', 64, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001266F00 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B0058AB34 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (765, 65, 93, N'bhavisha', N'9825960554', N'765', 65, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x0000000001266F00 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B0058AC7C AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (766, 66, 93, N'bhavisha', N'9825960554', N'766', 66, CAST(0x0000AB8B00000000 AS DateTime), CAST(0x000000000126B550 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AB8B00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB8B0058CCAD AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eonL_iTfjl4:APA91bFUEA5Ix9S_rMRqgY_uth2jrnTxcZGwB2cnQsk7iIl3Yog7VVbbjKprjyEFgsHo4ZJcPEZlKHBxuxY6-zkEC_LBRBONfG-hR-ha0vRAwV8_BScpI_ViIx7NPBa-wvYTKfv1nX-G', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (767, 67, 99, N'', N'8140122070', N'767', 67, CAST(0x0000AB8C00000000 AS DateTime), CAST(0x0000000001490A60 AS DateTime), 0, 0, 0, 140, 0, 140, 0, CAST(0x0000AB8C00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 99, CAST(0x0000AB8C007B5365 AS DateTime), 2, CAST(0x0000AB8C00EF9737 AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 103, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (768, 68, 20, N'sumit pitroda', N'9427949391', N'768', 68, CAST(0x0000AB1C00000000 AS DateTime), CAST(0x00000000001E3660 AS DateTime), 0, 0, 0, 0, 0, 100, 0, CAST(0x0000AB1C00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 20, CAST(0x0000AB9400586858 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 1, N'123456789', NULL, N'')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (769, 69, 20, N'sumit pitroda', N'9427949391', N'769', 69, CAST(0x0000AB1C00000000 AS DateTime), CAST(0x00000000001E3660 AS DateTime), 0, 0, 0, 0, 0, 100, 0, CAST(0x0000AB1C00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 20, CAST(0x0000AB9400C224E6 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 1, N'123456789', NULL, N'')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (770, 70, 93, N'bhavisha', N'9825960554', N'770', 70, CAST(0x0000AB9500000000 AS DateTime), CAST(0x0000000000C53460 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AB9500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB95001891B1 AS DateTime), 2, CAST(0x0000AB9700609337 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (771, 71, 93, N'bhavisha', N'9825960554', N'771', 71, CAST(0x0000AB9600000000 AS DateTime), CAST(0x00000000012482D0 AS DateTime), 0, 0, 0, 142, 30, 142, 0, CAST(0x0000AB9600000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB960077C3E0 AS DateTime), 2, CAST(0x0000AB97005E8878 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'eCC3syjYs-g:APA91bHHDM_sNXHtG0hXIv5K_8a25HKJL9XdHlM8DYAoDVC3KgD1J4YgOIGOKGHIYJEV8XeYQd6Yb1mRCopovywYQ20pDNTGoFXqOPdpUD05s3zTZy7D7xWtk3rNykJJO9s0jfNvE14B', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (772, 72, 93, N'bhavisha', N'9825960554', N'772', 72, CAST(0x0000AB9600000000 AS DateTime), CAST(0x00000000012482D0 AS DateTime), 0, 0, 0, 142, 30, 142, 0, CAST(0x0000AB9600000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB9600784EBE AS DateTime), 2, CAST(0x0000AB97005E77CE AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'eCC3syjYs-g:APA91bHHDM_sNXHtG0hXIv5K_8a25HKJL9XdHlM8DYAoDVC3KgD1J4YgOIGOKGHIYJEV8XeYQd6Yb1mRCopovywYQ20pDNTGoFXqOPdpUD05s3zTZy7D7xWtk3rNykJJO9s0jfNvE14B', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (773, 73, 93, N'bhavisha', N'9825960554', N'773', 73, CAST(0x0000AB9700000000 AS DateTime), CAST(0x0000000000B964F0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AB9700000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AB97000C9777 AS DateTime), 2, CAST(0x0000AB97005F21A3 AS DateTime), NULL, NULL, NULL, N'Being Prepared', 12, 101, N'ebZ0aRTO84M:APA91bGz6MHljJaWx1Npif5qoPwFtlM9NryPZ99xvWjiirn-BhAxwEjWKtMgXM0EQHlD-Rm_B-y_eTlfnBNVVnNnKsNLQ7di9PyHrB8JfIHWUQbAI2VZnX1EYYlh80VntUjxCGG1Ba1z', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (774, 74, 99, N'kamal', N'8140122070', N'774', 74, CAST(0x0000AB9F00000000 AS DateTime), CAST(0x0000000000C6DA40 AS DateTime), 0, 0, 0, 520, 0, 520, 0, CAST(0x0000AB9F00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 99, CAST(0x0000AB9F001A0D0F AS DateTime), NULL, CAST(0x0000AB9E0184A430 AS DateTime), NULL, NULL, NULL, N'Failed', NULL, 103, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (775, 75, 93, N'bhavisha', N'9825960554', N'775', 75, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F47220 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000ABA100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABA10047A6AC AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (776, 76, 93, N'bhavisha', N'9825960554', N'776', 76, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F4B870 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000ABA100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABA10047F0F9 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (777, 77, 93, N'bhavisha', N'9825960554', N'777', 77, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F54510 AS DateTime), 0, 0, 0, 150, 0, 150, 0, CAST(0x0000ABA100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABA100489718 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (778, 78, 93, N'bhavisha', N'9825960554', N'778', 78, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000309D50 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000ABA100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABA100496E09 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (779, 79, 93, N'bhavisha', N'9825960554', N'779', 79, CAST(0x0000ABA100000000 AS DateTime), CAST(0x0000000000F77790 AS DateTime), 0, 0, 0, 160.02, 0, 160.02, 0, CAST(0x0000ABA100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABA1004A8236 AS DateTime), 2, CAST(0x0000ABAC008B217E AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'cbf2ZaQTCRw:APA91bHDTLEkekF-8QG8ftwwZATo2_a7abv7I1wpYPylqEruHBBjA6itkBH_L6r0z7m0jcC9TUqxtCO84kzuffgN8Nyt1qK4LryjAxEJuLEnD4Lid4SAris_6VzzA0qgb88tL06tPzFH', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (780, 80, 99, N'kamal', N'8140122070', N'780', 80, CAST(0x0000ABAA00000000 AS DateTime), CAST(0x00000000000AB630 AS DateTime), 0, 0, 0, 1285, 0, 1285, 0, CAST(0x0000ABAA00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 99, CAST(0x0000ABA900E9BC00 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 103, N'fPFZhFQbNfs:APA91bFpVSqIcdcLnHjB2N29k-XJb79S_-i3thrYlljhO8jeQAnckJVYMl7SpPQeYwwGm3ISsIuKrHTLQDYQ20MQ5S-pawoviNvIJzOa-ZilIK90IY5PXAvZPw-vFr4kM_NU-sqyRvhG', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (781, 81, 93, N'bhavisha', N'9825960554', N'781', 81, CAST(0x0000ABAC00000000 AS DateTime), CAST(0x0000000000E5E390 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000ABAC00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABAC0038EE4E AS DateTime), 2, CAST(0x0000ABAC008BA171 AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 101, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (782, 82, 93, N'bhavisha', N'9825960554', N'782', 82, CAST(0x0000ABAC00000000 AS DateTime), CAST(0x0000000000E5E390 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000ABAC00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABAC00391490 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (783, 83, 93, N'bhavisha', N'9825960554', N'783', 83, CAST(0x0000ABB200000000 AS DateTime), CAST(0x0000000000B89200 AS DateTime), 0, 0, 0, 164, 36, 164, 0, CAST(0x0000ABB200000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABB2000BECDD AS DateTime), 2, CAST(0x0000ABBF005BC434 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (784, 84, 93, N'bhavisha', N'9825960554', N'784', 84, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000C27540 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000ABB500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABB500157F9F AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (785, 85, 93, N'bhavisha', N'9825960554', N'785', 85, CAST(0x0000ABB500000000 AS DateTime), CAST(0x0000000000CB3F40 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ABB500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 93, CAST(0x0000ABB5001E62EE AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 101, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (786, 86, 105, N'null', N'7698096070', N'786', 86, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000013A7BD0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ABB500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 105, CAST(0x0000ABB5008E2EAF AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 104, N'e9ISShu3Lgs:APA91bHRUVRdhlktgCYmHfuTRaBI1XW2Uyesow1mv6cePwWqMJGwF4LsjsvtZX5UVJ9cfbLrFJHD52-e5fw4US71J9PtN8lHvzFhFbddUjNHEYc_8MZiD3bE-W0ISe9brWbdfE_et2U5', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (787, 87, 105, N'null', N'7698096070', N'787', 87, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000013B4EC0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ABB500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 105, CAST(0x0000ABB5008E5D20 AS DateTime), 2, CAST(0x0000ABB500E0EF6E AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 104, N'e9ISShu3Lgs:APA91bHRUVRdhlktgCYmHfuTRaBI1XW2Uyesow1mv6cePwWqMJGwF4LsjsvtZX5UVJ9cfbLrFJHD52-e5fw4US71J9PtN8lHvzFhFbddUjNHEYc_8MZiD3bE-W0ISe9brWbdfE_et2U5', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (788, 88, 106, N'Sonu', N'9510136305', N'788', 88, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000013B0870 AS DateTime), 0, 0, 0, 470, 0, 470, 0, CAST(0x0000ABB500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 106, CAST(0x0000ABB5008E6FBF AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 105, N'dbHcGKVfUs0:APA91bFPC1-lGQ0Ap2LHf0j0BPqVDALH_7pzT_WQU-bTZgjIMGykgWOPaVAHiIEczIAIpSq1s46w3JjLvWFe3IZEPkXeiUdZTYOEqrKrg4sl6Wv4KtuFU-Vf9SJjXL6ymWq0VRci0xlW', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (789, 89, 107, N'null', N'9106806269', N'789', 89, CAST(0x0000ABB500000000 AS DateTime), CAST(0x00000000017D39C0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ABB500000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 107, CAST(0x0000ABB500D09686 AS DateTime), 2, CAST(0x0000ABB5012423DF AS DateTime), NULL, NULL, NULL, N'Delivered', 71, 107, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (790, 90, 106, N'Sonu', N'9510136305', N'790', 90, CAST(0x0000ABB700000000 AS DateTime), CAST(0x00000000000DBBA0 AS DateTime), 0, 0, 0, 200, 0, 200, 0, CAST(0x0000ABB700000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 106, CAST(0x0000ABB600EC6CAE AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 105, N'dbHcGKVfUs0:APA91bFPC1-lGQ0Ap2LHf0j0BPqVDALH_7pzT_WQU-bTZgjIMGykgWOPaVAHiIEczIAIpSq1s46w3JjLvWFe3IZEPkXeiUdZTYOEqrKrg4sl6Wv4KtuFU-Vf9SJjXL6ymWq0VRci0xlW', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (791, 91, 109, N'null', N'9016216485', N'791', 91, CAST(0x0000ABB700000000 AS DateTime), CAST(0x0000000000107AC0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ABB700000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 109, CAST(0x0000ABB600F0804B AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 108, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (792, 92, 93, N'bhavisha', N'9825960554', N'792', 92, CAST(0x0000ABB900000000 AS DateTime), CAST(0x0000000000BA37E0 AS DateTime), 0, 0, 0, 166, 34, 166, 0, CAST(0x0000ABB900000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABB9000D6B8E AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'c_v97moe6JM:APA91bEKmtpjR6dgvMnL9r1S7Nxd-dMJTGzjUp_HJmRNMNZoCEsotBxTOpjxZL0_Pe7RRJGVvlsv2A7cBlXARznW72eHir43Cydn2yIlWDK6cnPbr2b4VnJQmMUGGDqSXGbgDnl9ZM0l', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (793, 93, 93, N'bhavisha', N'9825960554', N'793', 93, CAST(0x0000ABBC00000000 AS DateTime), CAST(0x000000000107AC00 AS DateTime), 0, 0, 0, 180, 0, 180, 0, CAST(0x0000ABBC00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABBC005B3411 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'c_v97moe6JM:APA91bEKmtpjR6dgvMnL9r1S7Nxd-dMJTGzjUp_HJmRNMNZoCEsotBxTOpjxZL0_Pe7RRJGVvlsv2A7cBlXARznW72eHir43Cydn2yIlWDK6cnPbr2b4VnJQmMUGGDqSXGbgDnl9ZM0l', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (794, 94, 93, N'bhavisha', N'9825960554', N'794', 94, CAST(0x0000ABBF00000000 AS DateTime), CAST(0x0000000000B5D2E0 AS DateTime), 0, 0, 0, 140, 0, 140, 0, CAST(0x0000ABBF00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABBF00090324 AS DateTime), 2, CAST(0x0000ABBF005CCFD1 AS DateTime), NULL, NULL, NULL, N'Out for Delivery', 12, 101, N'ckQNQq8f5mY:APA91bHRu7jVzEBKJAMnysNQ4Vp_X8Zc7udMXR_-qTlWU0XjdB0rRNQwIwztvB_AlScPRmr7JXYtMFdiUHW2lDN40rk-lw8M39z4lc1p76aqUNoCTwzL43TtjehlMO1WVJWBch0KRVdA', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (795, 95, 91, N'sumit', N'9427949390', N'795', 95, CAST(0x0000ABBF00000000 AS DateTime), CAST(0x000000000176A240 AS DateTime), 0, 0, 0, 111, 61, 111, 0, CAST(0x0000ABBF00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000ABBF00CA01E5 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 97, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (796, 96, 99, N'kamal', N'8140122070', N'796', 96, CAST(0x0000ABC200000000 AS DateTime), CAST(0x0000000000C1E8A0 AS DateTime), 0, 0, 0, 920, 0, 920, 0, CAST(0x0000ABC200000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 99, CAST(0x0000ABC200155BAE AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 103, N'eHDqyK6-PSs:APA91bElCJbiMT6d3wsXuYjF3nm5QlPkALKUG7uMnR5AMJNfw1D9eHOwJxqbtSQ3pu8g6eHvQuZeIiy-ApK0qcW7TjCMHWx1r4AbwSJjzI_rXiK6bqX0Jn14hKTv9ZTzdXIzMGHPkvFu', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (797, 97, 93, N'bhavisha', N'9825960554', N'797', 97, CAST(0x0000ABC300000000 AS DateTime), CAST(0x00000000012A4760 AS DateTime), 0, 0, 0, 90.02, 0, 90.02, 0, CAST(0x0000ABC300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABC3007D6EFE AS DateTime), 2, CAST(0x0000ABC300D00949 AS DateTime), NULL, NULL, NULL, N'Out for Delivery', 12, 101, N'ckQNQq8f5mY:APA91bHRu7jVzEBKJAMnysNQ4Vp_X8Zc7udMXR_-qTlWU0XjdB0rRNQwIwztvB_AlScPRmr7JXYtMFdiUHW2lDN40rk-lw8M39z4lc1p76aqUNoCTwzL43TtjehlMO1WVJWBch0KRVdA', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (798, 98, 93, N'bhavisha', N'9825960554', N'798', 98, CAST(0x0000ABC600000000 AS DateTime), CAST(0x0000000000BD83A0 AS DateTime), 0, 0, 0, 196, 44, 196, 0, CAST(0x0000ABC600000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABC60010A9CD AS DateTime), 2, CAST(0x0000AC23004C1FFB AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (799, 99, 93, N'bhavisha', N'9825960554', N'799', 99, CAST(0x0000ABC600000000 AS DateTime), CAST(0x0000000000BDC9F0 AS DateTime), 0, 0, 0, 248, 0, 248, 0, CAST(0x0000ABC600000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABC600110CC4 AS DateTime), 2, CAST(0x0000ABC60063A232 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 101, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7100, 100, 91, N'sumit', N'9427949390', N'7100', 100, CAST(0x0000ABC800000000 AS DateTime), CAST(0x0000000000A8EA30 AS DateTime), 0, 0, 0, 246, 0, 246, 0, CAST(0x0000ABC800000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000ABC800C1FB7F AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 2104, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7101, 101, 91, N'sumit', N'9427949390', N'7101', 101, CAST(0x0000ABC800000000 AS DateTime), CAST(0x00000000016F37D0 AS DateTime), 0, 0, 0, 159, 91, 159, 0, CAST(0x0000ABC800000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 91, CAST(0x0000ABC800C2D103 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 2104, N'etjA1bkAa44:APA91bHSzhBJaqZIJ21tDYPYjEKw_dAGktfrHgoQ8aMNrVMGO58mgNzHBXbWYytg9kTT--TWr_xQyYGFc0hAD4tO9PAj9BEiwnvwsItt0zxHShi8BDgiC7702HlD8QyeK62bdye4O20w', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7102, 102, 93, N'bhavisha', N'9825960554', N'7102', 102, CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000DDEC80 AS DateTime), 0, 0, 0, 248, 0, 248, 0, CAST(0x0000ABC900000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 93, CAST(0x0000ABC900312E12 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 101, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7103, 103, 93, N'bhavisha', N'9825960554', N'7103', 103, CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000DDEC80 AS DateTime), 0, 0, 0, 248, 0, 248, 0, CAST(0x0000ABC900000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 93, CAST(0x0000ABC9003146CD AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 101, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7104, 104, 93, N'bhavisha', N'9825960554', N'7104', 104, CAST(0x0000ABC900000000 AS DateTime), CAST(0x0000000000DF9260 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000ABC900000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 93, CAST(0x0000ABC90032B821 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 101, N'cooUkDVwAow:APA91bFUggrsvbSR2TqoORDh-fBes3bHY5K7vbKEtuDGLR87omtDD6PR9-KNW4N18zv1hzoPzc1mRxCqkqkiAZRkWZFHpz_RDDM_5EyaeCHwwm6dwB1HRXGih4zIhzuvRVKRfxMjKALJ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7105, 105, 93, N'bhavisha', N'9825960554', N'7105', 105, CAST(0x0000ABCB00000000 AS DateTime), CAST(0x0000000000A3F890 AS DateTime), 0, 0, 0, 604, 100, 604, 0, CAST(0x0000ABCB00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ABCA01831BBA AS DateTime), 2, CAST(0x0000ABCB004A3EAD AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 101, N'dU9dKTpn1D4:APA91bENkCMmm22h2ckhP_c61h3l4ogvmL_j5IF3x8_CzSEgV4VYkNlVW1jh__I99nrPWIegpFdEDTMlbpXNuKRkNzKwylgkb266PBU_WnNepwPaord8RkgqIECfLfZMode7iiKsOhZL', NULL, N'bSF8nt')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7106, 106, 99, N'kamal', N'8140122070', N'7106', 106, CAST(0x0000AC1100000000 AS DateTime), CAST(0x0000000000B47350 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000AC1100000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 99, CAST(0x0000AC110007BFA9 AS DateTime), 2, CAST(0x0000AC11005AF2AD AS DateTime), NULL, NULL, NULL, N'Accept', 12, 103, N'eHDqyK6-PSs:APA91bElCJbiMT6d3wsXuYjF3nm5QlPkALKUG7uMnR5AMJNfw1D9eHOwJxqbtSQ3pu8g6eHvQuZeIiy-ApK0qcW7TjCMHWx1r4AbwSJjzI_rXiK6bqX0Jn14hKTv9ZTzdXIzMGHPkvFu', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7107, 107, 93, N'bhavisha', N'9825960554', N'7107', 107, CAST(0x0000AC2200000000 AS DateTime), CAST(0x0000000000F7BDE0 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AC2200000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AC22004B242A AS DateTime), 2, CAST(0x0000AC23004AB939 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'eVac56m2aWU:APA91bFXmJrbxuY8pnEUwapLOsyF3NnvhhsmWV0kJfopHe2Dw-ke7LkxoSC0EEjsM6g6QQnJDCKDRZRbjiKMwmsFDl9QW9b0yW9-nUD7zA21IZdVf-wTlhKFyEBPJtjqgOH4ah_Si-HG', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7108, 108, 93, N'bhavisha', N'9825960554', N'7108', 108, CAST(0x0000AC2300000000 AS DateTime), CAST(0x0000000000B1B430 AS DateTime), 0, 0, 0, 96, 0, 96, 0, CAST(0x0000AC2300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AC230004E615 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eOlV1_C3RTE:APA91bGYvRBKIPSu4fN5qf-SmKhtbBe-hCGhYZ3U1uIuTZKaPTo7UBDVB_qnbB4DdeRPLlFzjNvST0zjHZJ0b6fO0QHcmAV-DZES9XH0JDJ8LF8MCOH_MmrBBMqiD1s0RvRSQvbFe_bE', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7109, 109, 93, N'bhavisha', N'9825960554', N'7109', 109, CAST(0x0000AC3600000000 AS DateTime), CAST(0x0000000000FAC350 AS DateTime), 0, 0, 0, 200, 0, 200, 0, CAST(0x0000AC3600000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000AC36004E0745 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'eVac56m2aWU:APA91bFXmJrbxuY8pnEUwapLOsyF3NnvhhsmWV0kJfopHe2Dw-ke7LkxoSC0EEjsM6g6QQnJDCKDRZRbjiKMwmsFDl9QW9b0yW9-nUD7zA21IZdVf-wTlhKFyEBPJtjqgOH4ah_Si-HG', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7110, 110, 93, N'bhavisha', N'9825960554', N'7110', 110, CAST(0x0000ACB800000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ACB800000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 93, CAST(0x0000ACB7018A50BD AS DateTime), NULL, CAST(0x0000ACB70179A7B0 AS DateTime), NULL, NULL, NULL, N'Failed', NULL, 101, N'duh4vpN_IUk:APA91bHdtS8g-_IPawCYvGTANrIPRkjFQy_a1T0jJ0n1c3N0KSSi-mZ_dW2i1vouQePH0wM1Rc0nLQu1okpgngZDcTCWI_kiLSclESIrav4SOxuTxxgq9uS_EC9pbzLmQBjFcjbNBjGJ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7111, 111, 93, N'bhavisha', N'9825960554', N'7111', 111, CAST(0x0000ACB800000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ACB800000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 93, CAST(0x0000ACB7018A7D0A AS DateTime), NULL, CAST(0x0000ACB70179EE00 AS DateTime), NULL, NULL, NULL, N'Failed', NULL, 101, N'duh4vpN_IUk:APA91bHdtS8g-_IPawCYvGTANrIPRkjFQy_a1T0jJ0n1c3N0KSSi-mZ_dW2i1vouQePH0wM1Rc0nLQu1okpgngZDcTCWI_kiLSclESIrav4SOxuTxxgq9uS_EC9pbzLmQBjFcjbNBjGJ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7112, 112, 93, N'bhavisha', N'9825960554', N'7112', 112, CAST(0x0000ADA400000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 0, 0, 0, 290, 0, 290, 0, CAST(0x0000ADA400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 1, N'1', 93, CAST(0x0000ADA4000F2E98 AS DateTime), NULL, CAST(0x0000ADA30179A7B0 AS DateTime), NULL, NULL, NULL, N'Failed', NULL, 101, N'dtpbvfapqro:APA91bGzsSIZ-cu4tyABO_bfbZs0nEVeSbddTnLBnBG3gRzyu1IxA4ojD18yenknuaUr8WXfPa_LUu_LBBwCsE6YN3ebBSJ0u3PNwBKa3b0jqona1Ac7CLHEpFp3F8k3NHyGmBR0065Z', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7113, 113, 93, N'bhavisha', N'9825960554', N'7113', 113, CAST(0x0000ADA400000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 0, 0, 0, 290, 0, 290, 0, CAST(0x0000ADA400000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 1, N'1', 93, CAST(0x0000ADA4000F432F AS DateTime), 1, CAST(0x0000ADA400B1C418 AS DateTime), NULL, NULL, NULL, N'Accept', 12, 101, N'dtpbvfapqro:APA91bGzsSIZ-cu4tyABO_bfbZs0nEVeSbddTnLBnBG3gRzyu1IxA4ojD18yenknuaUr8WXfPa_LUu_LBBwCsE6YN3ebBSJ0u3PNwBKa3b0jqona1Ac7CLHEpFp3F8k3NHyGmBR0065Z', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7114, 114, 93, N'bhavisha', N'9825960554', N'7114', 114, CAST(0x0000ADAB00000000 AS DateTime), CAST(0x00000000012DD970 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ADAB00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 1, N'1', 93, CAST(0x0000ADAB00813056 AS DateTime), 1, CAST(0x0000ADAB00D3C301 AS DateTime), NULL, NULL, NULL, N'Delivered', 12, 101, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7115, 115, 99, N'kamal', N'8140122070', N'7115', 115, CAST(0x0000ADB800000000 AS DateTime), CAST(0x0000000001102FB0 AS DateTime), 0, 0, 0, 80, 0, 80, 0, CAST(0x0000ADB800000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 99, CAST(0x0000ADB800634C26 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 103, N'e-OZGCYxkTw:APA91bGvqBbgw06GQ_wcbq_ewpvlYmYmiqK3l-7JrEH1WAqw9hT6EPTKywZIeOSUYhzlL7Xim2lnpOaQgkkuqhTum1m2L_UId2_ynr00JZ_cGa1F8h0sMV4qm_PtzaRrbd-r36wsP7OQ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7116, 116, 99, N'kamal', N'8140122070', N'7116', 116, CAST(0x0000ADB800000000 AS DateTime), CAST(0x0000000001102FB0 AS DateTime), 0, 0, 0, 80, 0, 80, 0, CAST(0x0000ADB800000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 99, CAST(0x0000ADB800635636 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 103, N'e-OZGCYxkTw:APA91bGvqBbgw06GQ_wcbq_ewpvlYmYmiqK3l-7JrEH1WAqw9hT6EPTKywZIeOSUYhzlL7Xim2lnpOaQgkkuqhTum1m2L_UId2_ynr00JZ_cGa1F8h0sMV4qm_PtzaRrbd-r36wsP7OQ', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7117, 117, 99, N'kamal', N'8140122070', N'7117', 117, CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000013FFA10 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000ADB900000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 99, CAST(0x0000ADB9009322EA AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 103, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7118, 118, 99, N'kamal', N'8140122070', N'7118', 118, CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000013FFA10 AS DateTime), 0, 0, 0, 100, 0, 100, 0, CAST(0x0000ADB900000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 99, CAST(0x0000ADB9009DC3CB AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 103, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7119, 119, 99, N'kamal', N'8140122070', N'7119', 119, CAST(0x0000ADB900000000 AS DateTime), CAST(0x00000000014AB040 AS DateTime), 0, 0, 0, 170, 0, 170, 0, CAST(0x0000ADB900000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 99, CAST(0x0000ADB9009E0075 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 103, N'', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7120, 120, 93, N'bhavisha', N'9825960554', N'7120', 120, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BAC480 AS DateTime), 0, 0, 0, 140, 0, 140, 0, CAST(0x0000ADBA00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ADBA000E14D8 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7121, 121, 93, N'bhavisha', N'9825960554', N'7121', 121, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BB0AD0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ADBA00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ADBA000E40BD AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7122, 122, 93, N'bhavisha', N'9825960554', N'7122', 122, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BB0AD0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ADBA00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ADBA000E4BDC AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7123, 123, 93, N'bhavisha', N'9825960554', N'7123', 123, CAST(0x0000ADBA00000000 AS DateTime), CAST(0x0000000000BBDDC0 AS DateTime), 0, 0, 0, 110, 0, 110, 0, CAST(0x0000ADBA00000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 0, N'1', 93, CAST(0x0000ADBA000EF1B8 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Pending', NULL, 101, N'fGA_sGZKQFo:APA91bFefWpdwfSxXO_35e6Z17Z599Hb9nIJkhb6laZW1uQckXwxYVX8ZR04iQVDCUzI4PufEqaplq3-KBHje0vYgZCF3L8PsrT0Qokxd3MGBxZrY1FgsyqsWvqmMj8BcUG4cxABUiwO', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7124, 124, 91, N'sumit', N'9427949390', N'7124', 124, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 0, 0, 0, 320, 0, 320, 0, CAST(0x0000ADC300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 2, 0, 7, 0, N'1', 91, CAST(0x0000ADC300C0E335 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Failed', NULL, 2104, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y', NULL, N'0')
GO
INSERT [dbo].[sorder] ([soid], [uid1], [custid], [customername], [mobileno], [socode], [socode_1], [sodate], [sotime], [deliverydays], [reminderdays], [taxtype], [roffamt], [discamt], [totamt], [advamt], [deliverydate], [dailytrgentype], [deliverycharge], [deliverytype], [deliverystatus], [referenceno], [referencedate], [wallettype], [remarks], [paymenttype], [counterid], [uid], [tflg], [level], [addedby], [addedatetime], [updatedby], [updateddatetime], [delflg], [delby], [deldatetime], [ordstatus], [delpersonid], [DelAddressid], [deviceid], [TXNID], [promocode]) VALUES (7125, 125, 91, N'sumit', N'9427949390', N'7125', 125, CAST(0x0000ADC300000000 AS DateTime), CAST(0x00000000016D91F0 AS DateTime), 0, 0, 0, 320, 0, 320, 0, CAST(0x0000ADC300000000 AS DateTime), 0, 20, 0, 0, N'', CAST(0x0000000000000000 AS DateTime), 0, N'', 0, 0, 7, 1, N'1', 91, CAST(0x0000ADC300C0FB51 AS DateTime), 1, CAST(0x0000ADC30113A40A AS DateTime), NULL, NULL, NULL, N'Accept', 12, 2104, N'cNGi068_eEw:APA91bGqQDQ0qI4pikbFbyqA0zG5_Bf_msRr4dAYoEukzlbGD2tA222kToYXJc1gzp5MxlQAGC_Mgbgv22_9CWigeTnujHmoWkR9tiEQL8TlF3gKJY3eNCIlMUKs37i0RJmdlxGIFG3Y', NULL, N'0')
GO
SET IDENTITY_INSERT [dbo].[userAddress] ON 

GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (1, 20, N'Home', N'C-102', N'Sopan Saran', N'Chandlodia', N'Ahmedabad', N'Gujarat', N'India', N'382481', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (2, 20, N'Home', N'C-101', N'Sopan Saran', N'Chandlodia', N'Ahmedabad', N'Gujarat', N'India', N'382481', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (84, 60, N'office', N'hh', N'gfv', N'vvv', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'7426975988', N'gxgcv')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (13, 69, N'Home', N'b1 kishan park ', N'kishanpark', N'ig road', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'8885688966', N'gffbnbb')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (5, 33, N'Home', N'89', N'visckory sunrise', N'gota', N'ahmedabad', N'Stymevvista', N'India', N'789696', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (14, 71, N'Home', N'gqwe', N'my', N'fsbnb', N'shnkbv', N'ggdf', N'India', N'345787', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (15, 71, N'Home', N'gqwe', N'my', N'fsbnb', N'shnkbv', N'ggdf', N'India', N'345787', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (87, 60, N'gym', N'zbzb', N'zbbz', N'vzvzbz', N'Mehsana', N'Gujarat', N'India', N'384001', 1, N'7997979797', N'hxhzhzhzh')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (91, 50, N'Home', N'1', N'vrindavan', N'godrej garden city', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'7486876122', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (88, 60, N'home', N'f bfbfbfnf', N'bfbf', N'bfbfnf', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'5959955959', N'fhhfbfn')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (46, 5, N'Home', N'C-102', N'Sopan Saran', N'Chandlodia', N'Ahmedabad', N'Gujarat', N'India', N'382481', 0, N'9427949390', N'SUMIT')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (47, 5, N'Home', N'C-102', N'Sopan Saran', N'Chandlodia', N'Ahmedabad', N'Gujarat', N'India', N'382481', 0, N'9427949390', N'SUMIT')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (89, 60, N'home', N'cvhnnnnnbnbb', N'cvvv', N'vvvb', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'8889998889', N'yyggf')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (92, 69, N'home', N'zhsh', N'bhhhh', N'bhbb', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'6868989898', N'dbbxdndbnd')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (95, 60, N'home', N'407', N'tyrb', N'seema hall', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'9924188822', N'hcuboid tech')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (97, 91, N'home', N'n', N'302', N'green', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'9427949390', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (98, 93, N'home', N'hggy', N'gdfdgdxg', N'xgxfxfxg', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'9825960554', N'test')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (104, 105, N'home', N'4 om duplex society', N'chindiya darwaza ', N'chindiya darwaza', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'7698096070', N'dave darsh')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (102, 96, N'home', N'a-501', N'victory sunrise', N'gota', N'Mehsana', N'Gujarat', N'India', N'384002', 1, N'9974402476', N'jaimin')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (107, 107, N'home', N'13 om duplex society', N'chindiya darwaza', N'chindiya darwaza', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'9106806269', N'purv patel')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (108, 109, N'home', N'o4 om duplex society ', N'chhindiya darwaza', N'chhindiya darwaza', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'9016216485', N'tirth raval')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (1104, 91, N'home', N'qwr', N'ggh', N'vjjkk', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'3214569870', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (2104, 91, N'Office', N'qe', N'er', N'fvvv', N'Mehsana', N'Gujarat', N'India', N'384001', 1, N'1234567890', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (6, 33, N'Home', N'89', N'visckory sunrise', N'gota', N'ahmedabad', N'Stymevvista', N'India', N'789696', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (9, 57, N'Home', N'89 shripad', N'shreepad', N'gota', N'ahmedabad', N'vishwascity7', N'India', N'499446', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (10, 54, N'Home', N'32 vishawascity', N'icb', N'abc', N'ahmedabad', N'Satmev vista road', N'India', N'946464', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (44, 5, N'Home', N'C-101', N'Sopan Saran', N'Chandlodia', N'Ahmedabad', N'Gujarat', N'India', N'382481', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (45, 5, N'Home', N'C-101', N'Sopan Saran', N'Chandlodia', N'Ahmedabad', N'Gujarat', N'India', N'382481', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (85, 60, N'office', N'hh', N'hh', N'hhh', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'6161616166', N'gegeg')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (86, 60, N'Office', N'dbbd', N'vsvdv', N'gsgss', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'5859595959', N'hddhhdhdhd')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (105, 106, N'home', N'1', N'raviyat society ', N't', N'Mehsana', N'Gujarat', N'India', N'384003', 0, N'9510136305', N'sonu Raval ')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (106, 106, N'home', N'1', N'raviyat society ', N't', N'Mehsana', N'Gujarat', N'India', N'384003', 0, N'9510136305', N'sonu Raval ')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (7, 33, N'Home', N'b1', N'Kishan park', N'Ig road', N'nadiad', N'Kishan park', N'India', N'387002', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (43, 72, N'Home', N'1', N'office', N'dhdh', N'Mehsana', N'dhd', N'India', N'384001', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (93, 88, N'home', N'34', N'anandnagar', N'prahaladnagr', N'Mehsana', N'Gujarat', N'India', N'384002', 0, N'8879200190', N'barkha shah')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (94, 87, N'Home', N'B/607 Vrindavan', N'Gidrej Garden City', N'Ahmedabad', N'Mehsana', N'Gujarat', N'India', N'384001', 1, N'7486876122', N'Krunsk')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (16, 71, N'Home', N'b3 vaishali apartment ', N'home', N'near mandir', N'mahesana ', N'keshavnagar', N'India', N'380027', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (17, 71, N'Home', N'b3 vaishali apartment ', N'home', N'near mandir', N'mahesana ', N'keshavnagar', N'India', N'380027', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (100, 95, N'home', N'hwhw', N'hehehe', N'hdhdh', N'Mehsana', N'Gujarat', N'India', N'384001', 1, N'9825960554', N'test')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (24, 72, N'Home', N'1', N'home', N'xbsb', N'hs', N'shsb', N'India', N'384265', 0, NULL, NULL)
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (73, 5, N'Home', N'C-102', N'Sopan Saran', N'Chandlodia', N'Ahmedabad', N'Gujarat', N'India', N'382481', 1, N'9427949390', N'SUMIT')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (96, 93, N'home', N'b1', N'kishanpark', N'near sneh park', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'9825960554', N'bhavisha')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (61, 50, N'home', N'B/607, vrindavan', N'godrej garden city', N'ahmedabad', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'7486876122', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (103, 99, N'home', N'1 pallavi park ', N'bhairav', N'patan', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'8140122070', N'kamal')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (71, 50, N'home', N'16 shridev bunglows', N'payalpark society road', N'tb cross road', N'Mehsana', N'Gujarat', N'India', N'384001', 1, N'7486876122', N'krunal parikh')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (81, 60, N'Home', N'C-102', N'Sopan Saran', N'Chandlodia', N'Mehsana', N'Gujarat', N'India', N'384205', 0, N'9427949390', N'SUMIT')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (90, 50, N'gym', N'1', N'dhdhd', N'fhfhf', N'Mehsana', N'Gujarat', N'India', N'384001', 0, N'7486876122', N'sumit pitroda')
GO
INSERT [dbo].[userAddress] ([id], [userid], [Type], [FlatNo], [Address1], [Address2], [City], [State], [Country], [ZipCode], [DefAddress], [MobileNo], [FullName]) VALUES (101, 93, N'gym', N'hgghhh', N'ghtj', N'hhhhjj', N'Mehsana', N'Gujarat', N'India', N'384001', 1, N'9855575856', N'fghhh')
GO
SET IDENTITY_INSERT [dbo].[userAddress] OFF
GO
SET IDENTITY_INSERT [dbo].[UserRegistration] ON 

GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'barkha', NULL, NULL, NULL, N'8879200190', NULL, N'shahbarkha91@gmail.com', N'OCVIVDJC', NULL, NULL, N'Ahmedabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 88, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'sumit', NULL, NULL, NULL, N'9427949390', NULL, N'sumitpitroda@gmail.com', N'HW1R192I', NULL, NULL, N'ahmedabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 91, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9427949390', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 92, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'bhavisha', NULL, NULL, NULL, N'9825960554', NULL, N'bhavisha.hcuboidtech@gmail.com', N'MTIzNDU2Nzg5
', NULL, NULL, N'ahmedabad', NULL, NULL, N'93.jpg', NULL, NULL, NULL, NULL, NULL, NULL, N'', NULL, NULL, NULL, N'3650', CAST(0x0000AB820165F84A AS DateTime), N'Active', 93, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'Flipp', NULL, NULL, NULL, N'9825960554', NULL, N'flippbidd@gmail.com', N'DB3DBBIH', NULL, NULL, N'ahmedabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 94, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'Pinal', NULL, NULL, NULL, N'9825960554', NULL, N'ppinal.hcuboid@gmail.com', N'10YWWTHR', NULL, NULL, N'Ahmedabad ', NULL, NULL, N'95.jpg', NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 95, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'jaimin', NULL, NULL, NULL, N'9974402476', NULL, N'jaiminsolanki09@gmail.com', N'X64XC7JT', NULL, NULL, N'ahmedabad', NULL, NULL, N'96.jpg', NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 96, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9408387650', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 97, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9408387650', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 98, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'kamal', NULL, NULL, NULL, N'8140122070', NULL, NULL, NULL, NULL, NULL, N'patan', NULL, NULL, N'99.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 99, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9825960557', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 100, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9828960554', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 101, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'jaimisha', N'', NULL, NULL, N'', NULL, N'jaimishasolanki92@gmail.com', N'E4DDU0OK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 102, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9998587385', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 103, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'7486876122', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 104, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'7698096070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 105, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'Sonu', NULL, NULL, NULL, N'9510136305', NULL, N'sonukishorbhairaval@gmail.com', N'GLNLO7LP', NULL, NULL, N'patan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'gmail', NULL, NULL, NULL, NULL, NULL, NULL, 106, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (N'Patel purv', NULL, NULL, NULL, N'9106806269', NULL, NULL, NULL, NULL, NULL, N'maheshana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 107, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9106806269', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 108, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9016216485', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 109, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9016216485', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 110, N'Active')
GO
INSERT [dbo].[UserRegistration] ([firstname], [lastname], [dateofbirth], [gender], [contactno], [address], [useremail], [userpwd], [area], [zipcode], [city], [stateid], [countryid], [patimg], [device_id], [device_type], [device_token], [verifylink], [verificationcodedate], [uid1], [socialtype], [height], [weight], [bloodgroup], [otpno], [senddatetime], [otpstatus], [id], [userstatus]) VALUES (NULL, NULL, NULL, NULL, N'9924188822', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', 10102, N'Active')
GO
SET IDENTITY_INSERT [dbo].[UserRegistration] OFF
GO
